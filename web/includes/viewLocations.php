<?php
if (isset($_POST['viewlocation'])) { ?>
    <div>
        <?php view_location($_POST['viewlocation']); ?>
    </div>
    <div>
        <?php view_location_shelves($_POST['viewlocation']); ?>
    </div>
<?php }
else {
    view_all_locations();
}

function view_all_locations()
{ ?>
    <h2>View Locations</h2>
    
    <?php $q = 'SELECT * FROM stockroom_location';
    
    $result = null;
    $rows = sql_select($result, $q);
    
    if ($rows > 0) {
        if (isset($result)) { ?>
            <table cellspacing="5" cellpadding="0">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Project</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($result as $row) {
                        $id = $row['StockRoom_ID'];
                        $name = $row['StockRoom_Description'];
                        $project = $row['StockRoom_Project']; ?>

                        <tr>
                            <td><?php echo $name ?></td>
                            <td><?php echo $project ?></td>
                            <td>
                                <form name='viewlocationsform<?php echo $id ?>' action='index.php?pid=viewlocations' method='post'>
                                    <input type='hidden' name='viewlocation' value='<?php echo $id ?>'>
                                    <input type='submit' value='View'>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                        
                </tbody>
            </table>
        <?php }
    }
}

function view_location($id)
{ ?>
    <h2>Location</h2>
    
    <?php $q = 'SELECT * FROM stockroom_location WHERE StockRoom_ID=:id';
    $params = array(':id' => $id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows == 1) {
        if (isset($result)) { ?>
            <table cellspacing="5" cellpadding="0">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Project</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($result as $row) {
                        $id = $row['StockRoom_ID'];
                        $name = $row['StockRoom_Description'];
                        $project = $row['StockRoom_Project']; ?>

                        <tr>
                            <td><?php echo $name ?></td>
                            <td><?php echo $project ?></td>
                        </tr>
                    <?php } ?>
                        
                </tbody>
            </table>
        <?php }
    }
}

function view_location_shelves($id)
{ ?>
    <h2>Shelves</h2>
    
    <?php $q = 'SELECT * FROM stockroom_shelves WHERE StockRoom_LocationID=:id';
    $params = array(':id' => $id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows > 0) {
        if (isset($result)) { ?>
            <table cellspacing="5" cellpadding="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($result as $row) {
                        $name = $row['StockRoom_ShelfID'];
                        $description = $row['description']; ?>

                        <tr>
                            <td><?php echo $name ?></td>
                            <td><?php echo $description ?></td>
                        </tr>
                    <?php } ?>
                        
                </tbody>
            </table>
        <?php }
    }
}
?>