<h2>Projects</h2>

<?php
if (isset($_POST['addproject'])) {
    $code = trim($_POST['project_code']);
    $description = trim($_POST['project_description']);
    
    $q = 'INSERT INTO project (code, description) VALUES (:code, :description)';
    $params = array(':code' => $code, ':description' => $description);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Project ' . $description . ' added.</p>';
        logger('add', 'Project code :: ' . $code . ' | Project :: ' . $description);
    }
    else {
        echo '<p class="error">Project not added.</p>';
    }
}
elseif (isset($_POST['updateproject'])) {
    $id = $_POST['updateproject'];
    $code = trim($_POST['project_code']);
    $description = trim($_POST['project_description']);
    
    $q = 'UPDATE project SET code=:code, description=:description WHERE id=:id';
    $params = array(':id' => $id, ':code' => $code, ':description' => $description);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>Project ' . $description . ' updated.</p>';
        logger('update', 'Project code :: ' . $code . ' | Project :: ' . $description);
    }
    else {
        echo '<p class="error">Project not updated.</p>';
    }
}
elseif (isset($_POST['removeproject'])) {
    $id = $_POST['removeproject'];
    $code = $_POST['project_code'];
    $description = $_POST['project_description'];
    
    $q = 'DELETE FROM project WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>Project ' . $description . ' removed.</p>';
        logger('remove', 'Project code :: ' . $code . ' | Project :: ' . $description);
    }
    else {
        echo '<p class="error">Project not removed.</p>';
    }
} ?>
<div class='auxwrapper'>
    <input type='button' value='Add'>
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addprojectform' action='index.php?pid=projects' method='post'>
            <tr>
                <th>Project Code</th>
                <td>
                    <input id='focus' type='text' name='project_code' value=''>
                </td>
            </tr>
            <tr>
                <th>Name</th>
                <td>
                    <input type='text' name='project_description' value=''>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addproject' value='addproject'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = "SELECT id, code, description FROM project WHERE description NOT IN ('Unassigned') ORDER BY id";

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
        <tr class='thead'>
            <th class='view first w140'>Project Code</th>
            <th class='edit' filter='false'></th>
            <th class='view last w140'>Name</th>
            <th class='edit' filter='false'></th>
            <th class='empty'></th>
            <th class='empty'></th>
            <th class='empty'></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $code = $row["code"];
            $description = $row['description']; ?>
            <tr>
                <form name='updateprojectform<?php echo $id ?>' action='index.php?pid=projects' method='post'>
                    <td class='view w140'>
                        <div><?php echo $code; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='project_code' value='<?php echo $code; ?>'>
                    </td>
                    <td class='view w140'>
                        <div class='view'><?php echo $description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input class='edit' type='text' name='project_description' value='<?php echo $description; ?>'>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updateproject' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form name='removeprojectform<?php echo $id ?>' action='index.php?pid=projects' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removeproject' value='<?php echo $id ?>'>
                        <input type='hidden' name='project_code' value='<?php echo $code; ?>'>
                        <input type='hidden' name='project_description' value='<?php echo $description; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No projects in database.</p>';
} ?>