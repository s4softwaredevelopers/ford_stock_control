<?php

include 'Model.php';
include 'View.php';
error_reporting(E_ALL ^ E_NOTICE);

function DisplayItems() {
    echo viewMobileBarcodeItems(modelMobileGetAllPrintedItems());
}

function contrManageOrdersMarryUpdate() {
    $myArray = '';
    $id = "";
    $itemBarcode = "";
    $shelfbarcode = "";
    foreach ($_REQUEST as $key => $value) {
        if (strstr($key, 'ID')) {
            $x = str_replace('ID', '', $key);
            $id = $value;
        }
        if (strstr($key, 'BC')) {
            $x = str_replace('BC', '', $key);
            $itemBarcode = $value;
        }
        if (strstr($key, 'SC')) {
            $x = str_replace('SC', '', $key);
            $shelfbarcode = $value;
        }
        if ($id != "" && $itemBarcode != "") {
            if ($shelfbarcode == "0") {
                $status = modelUpdateItemBarcode($id, $itemBarcode);
                if ($status[0][1] == "") {
                    $status[0][1] = "Failed";
                }
                $description = modelMobileGetItemDescription($id);
                echo "<tr><td>$id</td><td>$itemBarcode</td><td>" . $description[0][1] . "</td><td>" . $status[0][1] . "</td></tr>";
                $id = "";
                $itemBarcode = "";
                $shelfbarcode = "";
            } elseif ($shelfbarcode != "") {
                $status = modelUpdateBarcodes($id, $itemBarcode, $shelfbarcode);
                if ($status[0][1] == "") {
                    $status[0][1] = "Failed";
                }
                $description = modelMobileGetItemDescription($id);
                echo "<tr><td>$id</td><td>$itemBarcode</td><td>" . $description[0][1] . "</td><td>$shelfbarcode</td><td>" . $status[0][1] . "</td></tr>";
                $id = "";
                $itemBarcode = "";
                $shelfbarcode = "";
            }
        }
    }
}

function viewMobileAllTrackedReturnItems($items) {
    if ($items[0][0] === true) {
        $html = "<table><thead><th>status</th><th>ID</th><th>Order Number</th><th>Supplier Code</th><th>Project Name</th><th>Description</th><th>Item Barcode</th><th>Database</th><th>Previous Shelf Barcode</th><th>Previous Shelf Description</th><th>Previous Storeroom Location</th></thead><tbody>";
        foreach ($items as $value) {
            $html .= "<tr><td>$value[0]</td><td>$value[1]</td><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><td>$value[7]</td><td>$value[8]</td><td>$value[9]</td></tr>";
        }
        $html .= "</tbody></table>";
    } else {
        $html = $items[0][1];
    }

    return $html;
}

function contrScanItemBarcode() {
    $status = array();
    if (isset($_REQUEST['returnUser']) && isset($_REQUEST['returnDescription'])) {
        $returnFullname = $_REQUEST['returnUser'];
        $returnDescription = $_REQUEST['returnDescription'];
        //Log return user details
    }
    $itemBarcode = "";
    $shelfbarcode = "";
    $logBarcode = "";
    foreach ($_REQUEST as $key => $value) {
        if (strstr($key, 'barcode')) {
            $x = str_replace('barcode', '', $key);
            $itemBarcode = $value;
        }
        if (strstr($key, 'shelf')) {
            $x = str_replace('shelf', '', $key);
            $shelfbarcode = $value;
        }
        if ($shelfbarcode != "" && $itemBarcode != "") {
            $status = modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfbarcode);
            if ($status[0][0] === true) {
                if (empty($returnFullname)) {
                    modelLog('Item', 'Relocate', "Tablet", $itemBarcode, $orderNo = null, "", "", "Shelf", "Shelf Barcode: " . $shelfbarcode);
                } else {
                    modelLog('Item', 'Relocate', "Tablet", $itemBarcode, $orderNo = null, $returnFullname, $returnDescription, "Shelf", "Shelf Barcode: " . $shelfbarcode);
                }
            }
            $itemBarcode = "";
            $shelfbarcode = "";
        }
    }
    if ($status[0][0] == true) {
        echo 'done';
    } else {
        echo $status[0][1];
    }
}

function contrUpdateLocationUser() {
    $itemBarcode = "";
    if (isset($_REQUEST['returnUser']) && isset($_REQUEST['returnDescription'])) {
        $returnFullname = $_REQUEST['returnUser'];
        $returnDescription = $_REQUEST['returnDescription'];
        //Log return user details
    }

    if (isset($_REQUEST['userid']) && isset($_REQUEST['password']) && isset($_REQUEST['username'])) {
        $staffCode = $_REQUEST['userid'];
        $password = $_REQUEST['password'];
        if (isset($_REQUEST['description'])) {
            $description = $_REQUEST['description'];
        }
        $username = $_REQUEST['username'];
        $shelfId = '';
        $status2 = array();

        $status = modelAuthenticateUser($staffCode, $password);
        if ($status[0][0] == true) {
            $status = modelShelfWithUser($username, $description);
            if ($status[0][0] == true) {
                $shelfId = $status[0][1];
                foreach ($_REQUEST as $key => $value) {
                    if (strstr($key, 'barcode')) {
                        $x = str_replace('barcode', '', $key);
                        $itemBarcode = $value;
                    }
                    if ($itemBarcode != "") {
                        $status = modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode);
                        if ($status[0][0] === true) {
                            if (empty($returnFullname)) {
                                modelLog('Item', 'Relocate', "Tablet", $itemBarcode, $orderNo = null, "", "", $username, $description);
                            } else {
                                modelLog('Item', 'Relocate', "Tablet", $itemBarcode, $orderNo = null, $returnFullname, $returnDescription, $username, $description);
                            }
                        }
                        $itemBarcode = '';
                    }
                }
            }
            if ($status[0][0] === true) {
                echo 'done';
            } else {
                echo $status[0][1];
            }
        } else {
            echo 'Login failed';
        }
    }
}

function controlLogin() {
    if (isset($_REQUEST['userid']) && isset($_REQUEST['password']) && isset($_REQUEST['description'])) {
        $description = $_REQUEST['description']; //Logging pupose
        $firstname = $_REQUEST['userid'];
        $password = $_REQUEST['password'];
        $status = modelAuthenticateUser($firstname, $password);
        echo $status[0][1]; //login successfully or login falied
    } elseif (isset($_REQUEST['userid']) && isset($_REQUEST['password'])) {
        $firstname = $_REQUEST['userid'];
        $password = $_REQUEST['password'];
        $status = modelAuthenticateUser($firstname, $password);
        echo $status[0][1]; //login successfully or login falied
    }
}

function contrSetLocationEmpty() {
    $status = array();
    $itemBarcode = '';
    //userId to be saved/logged
    if (isset($_REQUEST['returnUser']) && isset($_REQUEST['returnDescription'])) {
        $returnFullname = $_REQUEST['returnUser'];
        $returnDescription = $_REQUEST['returnDescription'];
        //Log return user details
    }
    foreach ($_REQUEST as $key => $value) {
        if (strstr($key, 'barcode')) {
            $x = str_replace('barcode', '', $key);
            $itemBarcode = $value;
        }
        if ($itemBarcode != "") {
            $status = modelSetLocationEmpty($itemBarcode);
            if ($status[0][0] === true) {
                if (empty($returnFullname)) {
                    modelLog('Item', 'Relocate', "Tablet", $itemBarcode, $orderNo = null, "", "", "Unlocated Items", "Ready for Relocation");
                } else {
                    modelLog('Item', 'Relocate', "Tablet", $itemBarcode, $orderNo = null, $returnFullname, $returnDescription, "Unlocated Items", "Ready for Relocation");
                }
            }
            $itemBarcode = "";
        }
    }
    echo $status[0][1];
}

function contrMobileManageSetLocation() {
    //validate user and save new location(entered shelf barcode)
    if (isset($_REQUEST["barcodeid"]) && isset($_REQUEST["shelfbarcode"])) {
        $id = $_REQUEST["barcodeid"];
        $barcode = $_REQUEST["shelfbarcode"];
        if (!empty($barcode)) {
            $status = modelUpdateShelfBarcode($id, $barcode);
            echo $status[0][1];
        } else {
            echo 'Shelf Barcode cannot be empty';
        }
    }
}

function contrMobileGetUsers() {
    echo viewMobileGetUsers(modelGetUsernames());
}

function contrMobileNoOrderItemsUpdate() {
    if (isset($_REQUEST['description']) && isset($_REQUEST['quantity'])) {
        $description = $_REQUEST['description'];
        $qty = $_REQUEST['quantity'];
        $status = modelInsertNoOrderItem($description, $qty);
        echo $status[0][1];
    }
}

function contrMobileNoOrderItemsPrint() {
    if (isset($_REQUEST['quantity'])) {
        $qty = $_REQUEST['quantity'];
        $status = modelMobileUdpRequest($qty, 5);
        if ($status[0][0] === true) {
            echo "done";
        }
    }
}

//ajax controls
function contrUsersDropdown($id) {
    return viewDropdownUsernamesMultiRelocate(modelGetUsernames(), $id);
}

function contrUserAuthenticate() {
    if (isset($_REQUEST['firstNameReceiver']) && isset($_REQUEST['returnPassword'])) {
        $firstname = $_REQUEST['firstNameReceiver'];
        $password = $_REQUEST['returnPassword'];
    }
    $status = modelAuthenticateUser($firstname, $password);
    if ($status[0][0] === true) {
        echo 'ok';
    } else {
        echo 'nok';
    }
}

function contrManageReturnRelocateUserSpecific() {

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
        switch ($action) {
            case 'later':
                $itemBarcode = '';
                foreach ($_GET as $key => $value) {
                    if (strstr($key, 'BC')) {
                        $x = str_replace('BC', '', $key);
                        $itemBarcode = $value;
                    }
                    if ($itemBarcode != "") {
                        $status = modelResetLocation($itemBarcode);
                        if ($status[0][0] === true) {
                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "", "", "No Location", "Ready for Relocation");
                            $_SESSION['status'] = "Item moved to pending items succesfully!";
                            $_SESSION['msgType'] = 'success';

                            echo "Item Barcode: $itemBarcode moved to pending items succesfully!\n\n";
                            //echo 'Item moved to pending items succesfully!';
                        } else {
                            $_SESSION['status'] = $status[0][1];
                            $_SESSION['msgType'] = 'danger';
                            echo $status[0][1];
                        }
                        $_SESSION['status'] = $status[0][1];
                        $itemBarcode = "";
                    }
                }
                echo $status[0][1];
                break;
            case 'ShelfToShelf':
//                            $itemBarcode = $_GET['barcodeVal'];
                $shelfbarcode = $_GET['toShelfBarcode'];
                $itemBarcode = '';
                foreach ($_GET as $key => $value) {
                    if (strstr($key, 'BC')) {
                        $x = str_replace('BC', '', $key);
                        $itemBarcode = $value;
                    }
                    if ($itemBarcode != "") {
                        $status = modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfbarcode);
                        if ($status[0][0] === true) {
                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", $sourceComment = null, "Shelf", "Shelf Barcode: " . $shelfbarcode);
                            $_SESSION['status'] = "Location changed succesfully!";
                            $_SESSION['msgType'] = 'success';
                            echo "Item Barcode: $itemBarcode \n From: Shelf \n To: Shelf \n Shelf Barcode: $shelfbarcode \n=====================================\n Location changed succesfully\n\n";
                            //echo 'Location changed succesfully!';
                        } else {
                            $_SESSION['status'] = $status[0][1];
                            $_SESSION['msgType'] = 'danger';
                            echo $status[0][1];
                        }
                        $itemBarcode = "";
                    }
                }

                break;
            case 'UserToShelf':
                if (isset($_GET['firstNameReturn']) && isset($_GET['pWord'])) {
                    $userId = $_GET['firstNameReturn'];
                    $password = $_GET['pWord'];
                    $status = modelAuthenticateUser($userId, $password);
                    if ($status[0][0] === true) {
                        if (isset($_GET['toShelfBarcode'])) {
//                                            $itemBarcode = $_GET['barcodeVal'];
                            $shelfBarcode = $_GET['toShelfBarcode'];
                            if (isset($_GET['returnDescription'])) {
                                $description = $_GET['returnDescription'];
                            }
                            if (empty($shelfBarcode) === false) {
                                $itemBarcode = '';
                                foreach ($_GET as $key => $value) {
                                    if (strstr($key, 'BC')) {
                                        $x = str_replace('BC', '', $key);
                                        $itemBarcode = $value;
                                    }
                                    if ($itemBarcode != "") {
                                        $status = modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfBarcode);
                                        if ($status[0][0] == true) {
                                            if (isset($_GET['returnDescription'])) {
                                                modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "User", "Name: " . $userId . ",Reason for Returning:" . $description, "Shelf", "Shelf Barcode: " . $shelfBarcode);
                                            } else {
                                                modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "User", "Name: " . $userId, "Shelf", "Shelf Barcode: " . $shelfBarcode);
                                            }
                                            $_SESSION['status'] = "Item relocated successful";
                                            $_SESSION['msgType'] = 'success';
                                            echo "Item Barcode: $itemBarcode \n From: User - $userId \n To: Shelf \n Shelf Barcode: $shelfbarcode \n=====================================\n Item relocated successful\n\n";
                                            //echo 'Item relocated successful';
                                        } else {
                                            $_SESSION['status'] = $status[0][1];
                                            $_SESSION['msgType'] = 'danger';
                                            echo $status[0][1];
                                        }
                                        $itemBarcode = "";
                                    }
                                }
                            } else {
                                $_SESSION['status'] = "Please enter a item barcode and shelf barcode.";
                                $_SESSION['msgType'] = 'danger';
                                echo 'Please enter a item barcode and shelf barcode.';
                            }
                        }
                    } else {
                        $_SESSION['status'] = "Login Failed";
                        $_SESSION['msgType'] = 'danger';
                        echo 'Login Failed';
                    }
                }
                break;
            case 'ShelfToUser':
                if (isset($_GET['firstNameReceiveUser']) && isset($_GET['receivePassword'])) {
                    $staffCode = $_GET['firstNameReceiveUser'];
                    $password = $_GET['receivePassword'];
//                                    $itemBarcode =  $_GET['barcodeVal'];
                    $description = '';
                    if (isset($_GET['receiveDescription'])) {
                        $description = $_GET['receiveDescription'];
                    }
                    $username = $_GET['username'];
                    $shelfId = '';
                    $status = modelAuthenticateUser($staffCode, $password);
                    if ($status[0][0] == true) {
                        $status = modelShelfWithUser($username, $description);
                        if ($status[0][0] == true) {
                            $shelfId = $status[0][1];
                            $itemBarcode = '';
                            foreach ($_GET as $key => $value) {
                                if (strstr($key, 'BC')) {
                                    $x = str_replace('BC', '', $key);
                                    $itemBarcode = $value;
                                }
                                if ($itemBarcode != "") {
                                    $status = modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode);
                                    if ($status[0][0] === true) {
                                        if (isset($_GET['receiveDescription'])) {
                                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username . ",Reason for taking:" . $description);
                                        } else {
                                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username);
                                        }
                                        $_SESSION['status'] = $status[0][1];
                                        //echo $status[0][1];
                                        echo "Item Barcode: $itemBarcode \n From: Shelf \n To: User - $username \n=====================================\n Item relocated successful\n\n";
                                        $_SESSION['msgType'] = 'success';
                                    } else {
                                        $_SESSION['status'] = $status[0][1];
                                        $_SESSION['msgType'] = 'danger';
                                        echo $status[0][1];
                                    }
                                    $itemBarcode = "";
                                }
                            }
                        } else {
                            $_SESSION['status'] = $status[0][1];
                            $_SESSION['msgType'] = 'danger';
                            echo $status[0][1];
                        }
                    } else {
                        $_SESSION['status'] = 'Login failed';
                        $_SESSION['msgType'] = 'danger';
                        echo 'Login failed';
                    }
                }
                //pid=setlocation fid=userupdate
                break;
            case 'UserToUser':
                //pid=setlocation fid=user
                //authenticate return user
                if (isset($_GET['pWord']) && isset($_GET['firstNameReturn'])) {
                    $staffCode = $_GET['firstNameReturn'];
                    $password = $_GET['pWord'];
                    $status = modelAuthenticateUser($staffCode, $password);
                    if ($status[0][0] == true) {
                        if (isset($_GET['logUserReturn']) && isset($_GET['returnDescription'])) {
                            $returnFullname = $_GET['logUserReturn'];
                            $returnDescription = $_GET['returnDescription'];
                            //Log return user details
                        }
                        //authenticate receiving user
                        $staffCode = $_GET['firstNameReceiveUser'];
                        $password = $_GET['receivePassword'];
                        $shelfId = '';
                        $status = modelAuthenticateUser($staffCode, $password);
                        if ($status[0][0] == true) {
                            $username = $_GET['logUserReceive'];
                            if (isset($_GET['receiveDescription'])) {
                                $description = $_GET['receiveDescription'];
                            }

                            $status = modelShelfWithUser($username, $description);
                            if ($status[0][0] == true) {
                                $shelfId = $status[0][1];
                                $itemBarcode = '';
                                foreach ($_GET as $key => $value) {
                                    if (strstr($key, 'BC')) {
                                        $x = str_replace('BC', '', $key);
                                        $itemBarcode = $value;
                                    }
                                    if ($itemBarcode != "") {
                                        $status = modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode);
                                        if (isset($_GET['receiveDescription']) && isset($_GET['returnDescription'])) {
                                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "User", "Name: " . $returnFullname . ",Reason for returning:" . $returnDescription, "User", "Name:" . $username . ",Reason for taking:" . $description);
                                        } elseif (isset($_GET['returnDescription'])) {
                                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "User", "Name: " . $returnFullname . ",Reason for returning:" . $returnDescription, "User", "Name:" . $username);
                                        } elseif (isset($_GET['receiveDescription'])) {
                                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "User", "Name: " . $returnFullname, "User", "Name:" . $username . ",Reason for taking:" . $description);
                                        } else {
                                            modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "User", "Name: " . $returnFullname, "User", "Name:" . $username);
                                        }
                                        if ($status[0][0] === true) {
                                            $_SESSION['status'] = $status[0][1];
                                            //echo 'Item relocated succesfully.';
                                            echo "Item Barcode: $itemBarcode \n From: User - $returnFullname \n To: User - $username \n=====================================\n Item relocated successful\n\n";
                                            $_SESSION['msgType'] = 'success';
                                        } else {
                                            $_SESSION['status'] = $status[0][1];
                                            echo $status[0][1];
                                            $_SESSION['msgType'] = 'danger';
                                        }

                                        $itemBarcode = "";
                                    }
                                }
                            }
                        } else {
                            $_SESSION['status'] = "Login failed for receiving user";
                            echo 'Login failed for receiving user';
                            $_SESSION['msgType'] = 'danger';
                        }
                    } else {
                        $_SESSION['status'] = "Login failed for return user";
                        echo 'Login failed for return user';
                        $_SESSION['msgType'] = 'danger';
                    }
                }
                break;
        }
    }
    unset($_GET);
}

function contrManageOrdersMarry() {

    if (isset($_REQUEST['barValue']) && isset($_REQUEST['ItemIdValue']) && isset($_REQUEST['shelfValue'])) {
        $ItemIdValue = $_REQUEST['ItemIdValue'];
        $barcodeItemValue = $_REQUEST['barValue'];
        $shelfValue = $_REQUEST['shelfValue'];
        if (empty($barcodeItemValue) && empty($shelfValue)) {
            echo 'Item Barcode and Shelf Barcode cannot be empty';
        } elseif (empty($shelfValue)) {
            $status = modelUpdateItemBarcode($ItemIdValue, $barcodeItemValue);
        } else {
            $status = modelUpdateItemBarcode($ItemIdValue, $barcodeItemValue);
            $status = modelUpdateShelfBarcode($ItemIdValue, $shelfValue);
            if ($status[0][0] === true) {
                //add to log
                //id or barcode?
                modelLog('Item', 'Marry', null, $ItemIdValue, $orderNo = null, $source = null, $sourceComment = null, "Shelf", "Shelf Barcode: " . $shelfValue);
            }
        }
        echo $status[0][1];
    }
}

function contrManageConsumables() {
    echo viewAddConsumable();
    echo viewConsumables(modelGetAllConsumables());
}

function contrAddConsumable() {
    $status = array();
    $name = $_REQUEST['name'];
    $description = $_REQUEST['description'];
    $detail = $_REQUEST['detail'];
    $supplier_code = $_REQUEST['supplier'];
    $minQty = $_REQUEST['minQty'];
    $maxQty = $_REQUEST['maxQty'];
    $qtyInStore = $_REQUEST['qtyInStore'];
    $nettValue = $_REQUEST['nettValue'];
    if (empty($name)) {
        echo 'Please enter the name of the consumable';
    } elseif (empty($description)) {
        echo 'Please enter the description of the consumable';
    } elseif (empty($detail)) {
        echo 'Please enter the details of the consumable';
    } elseif (empty($supplier_code)) {
        echo 'Please enter the supplier code of the consumable';
    } elseif (empty($minQty)) {
        echo 'Please enter the minimum quantity of the consumable';
    } elseif (empty($maxQty)) {
        echo 'Please enter the maximum quantity of the consumable';
    } elseif (empty($qtyInStore)) {
        echo 'Please enter the quantity in-store of the consumable';
    } else {
        $status = modelAddConsumable($name, $description, $detail, $minQty, $maxQty, $qtyInStore, $supplier_code, $nettValue);
        echo $status[0][1];
    }
}

function contrUpdateConsumable() {
    $status = array();
    $id = $_REQUEST['id'];
    $name = $_REQUEST['name'];
    $description = $_REQUEST['description'];
    $detail = $_REQUEST['detail'];
    $supplier_code = $_REQUEST['supplier'];
    $minQty = $_REQUEST['minQty'];
    $maxQty = $_REQUEST['maxQty'];
    $qtyInStore = $_REQUEST['qtyInStore'];
    $nettValue = $_REQUEST['nettValue'];
    if (empty($name)) {
        echo 'Please enter the name of the consumable';
    } elseif (empty($description)) {
        echo 'Please enter the description of the consumable';
    } elseif (empty($detail)) {
        echo 'Please enter the details of the consumable';
    } elseif (empty($supplier_code)) {
        echo 'Please enter the supplier code of the consumable';
    } elseif (empty($minQty)) {
        echo 'Please enter the minimum quantity of the consumable';
    } elseif (empty($maxQty)) {
        echo 'Please enter the maximum quantity of the consumable';
    } elseif (empty($qtyInStore)) {
        echo 'Please enter the quantity in-store of the consumable';
    } else {
        $status = modelUpdateConsumable($id, $name, $description, $detail, $minQty, $maxQty, $qtyInStore, $supplier_code, $nettValue);
        echo $status[0][1];
    }
}

function contrDeleteConsumable() {
    $id = $_REQUEST['id'];
    $status = modelDeleteConsumable($id);
    echo $status[0][1];
}

function contrAssignLocation() {
    $location = $_REQUEST['locationType'];
    if ($location == "shelf") {
        $shelfbarcode = $_POST['toShelfBarcode'];
        $itemBarcode = '';
        foreach ($_POST as $key => $value) {
            if (strstr($key, 'checkedItem')) {
                $x = str_replace('checkedItem', '', $key);
                $itemBarcode = $x;
            }
            if ($itemBarcode != "") {
                $status = modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfbarcode);
                if ($status[0][0] === true) {
                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", $sourceComment = null, "Shelf", "Shelf Barcode: " . $shelfbarcode);
                    $_SESSION['status'] = "Location changed succesfully!";
                    $_SESSION['msgType'] = 'success';
                    echo 'Location changed succesfully!';
                } else {
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'danger';
                    echo $status[0][1];
                }
                $itemBarcode = "";
            }
        }
    }

    if ($location == "user") {
        if (isset($_POST['staffcode']) && isset($_POST['receivePassword'])) {
            $staffCode = $_POST['staffcode'];
            $password = $_POST['receivePassword'];
            $description = '';
            if (isset($_POST['receiveDescription'])) {
                $description = $_POST['receiveDescription'];
            }
            $username = $_POST['username'];
            $shelfId = '';
            $status = modelAuthenticateUser($staffCode, $password);
            if ($status[0][0] == true) {
                $status = modelShelfWithUser($username, $description);
                if ($status[0][0] == true) {
                    $shelfId = $status[0][1];
                    $itemBarcode = '';
                    foreach ($_POST as $key => $value) {
                        if (strstr($key, 'checkedItem')) {
                            $x = str_replace('checkedItem', '', $key);
                            $itemBarcode = $x;
                        }
                        if ($itemBarcode != "") {
                            $status = modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode);
                            if ($status[0][0] === true) {
                                if (isset($_POST['receiveDescription'])) {
                                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username . ",Reason for taking:" . $description);
                                } else {
                                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username);
                                }
                                $_SESSION['status'] = "Item Relocated successfully";
                                $_SESSION['msgType'] = 'success';
                            }
                            $itemBarcode = "";
                        }
                    }
                } else {
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'danger';
                }
            } else {
                $_SESSION['status'] = 'Login failed';
                $_SESSION['msgType'] = 'danger';
            }
        }
    }
}

function contrReportbarcode() {
    if (isset($_REQUEST['barcode'])) {
        $barcode = $_REQUEST['barcode'];
         echo viewReportsBarcodeOrderMobile(modelGetItemsByBarcode($barcode), modelGetItemsByBarcodeItems($barcode));
    }
}

function contrGetDropdownIntranetProject($selectedVal) {
    echo viewDropdownProjectsMultiAjax(modelGetProjectsIntranet(), $selectedVal, "ddIntranetProjects");
}

function contrGetDropdownAutonetProject($selectedVal) {
    echo viewDropdownProjectsMultiAjax(modelGetProjectsAutonet(), $selectedVal, "ddAutonetProjects");
}

function contrReportByOrder() {
    if (isset($_REQUEST['orderNumber'])) {
        $orderNumber = $_REQUEST['orderNumber'];
        echo viewReportsOrder(modelGetItemsByOrder($orderNumber), modelGetItemsByOrderItems($orderNumber));
    }
}
function contrReportByOrderNotReceived()
{
    if (isset($_REQUEST['orderNumber'])) {
        $orderNumber = $_REQUEST['orderNumber'];
        echo viewReportsOrder(modelGetItemsByOrderNotReceived($orderNumber), modelGetItemsByOrderItemsNotReceived($orderNumber));
    }
}
