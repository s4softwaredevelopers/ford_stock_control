using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonMarry_Click(object sender, EventArgs e)
        {
            Loader();
            panelHeader.Focus();
            MyMenu.Marry();
            panelLoader.Hide();
        }

        private void Loader()
        {
            panelLoader.Show();
            panelLoader.Refresh();
            Application.DoEvents();
        }

        private void buttonRelocate_Click(object sender, EventArgs e)
        {
            Loader();
            panelHeader.Focus();
            MyMenu.RelocateMenu();
            panelLoader.Hide();
        }

        private void buttonCapture_Click(object sender, EventArgs e)
        {
            Loader();
            panelHeader.Focus();
            MyMenu.Capture();
            panelLoader.Hide();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            MyLibrary.MakeButtonMultiline(buttonAssignBarcodes);
            MyLibrary.MakeButtonMultiline(buttonCapture);
            panelHeader.Focus();

            MyLogFile.LoggerState();
            MyLogFile.LogClear();
            MyLogFile.LogFile("OPEN    ", "Start Scanner - Main Menu");
        }
    }
}