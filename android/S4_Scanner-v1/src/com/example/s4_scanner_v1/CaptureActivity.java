package com.example.s4_scanner_v1;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import MyContent.MyWebRequest;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class CaptureActivity extends Activity {

	private ProgressBar spinner;
	private Boolean bCapSuccess = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_capture);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
	      spinner = (ProgressBar)findViewById(R.id.progressCapture);
	      spinner.setVisibility(View.GONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.capture, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void CaptureButton(View view) {
		new WebRequestCapture().execute("");
	}
	
	public void PrintButton(View view) {
		new WebRequestPrint().execute("");
	}
	
	public void CapturePrintButton(View view) {
		new WebRequestCapture().execute("");
		if (bCapSuccess)
		{
			new WebRequestPrint().execute("");			
		}		
	}
    
	class WebRequestCapture extends AsyncTask<String, Void, String> {

		EditText etDescription;
		EditText etQuantity;
		String sQuantity = "0"; 
		String sDescription = ""; 
		
		@Override
		protected String doInBackground(String... params) {
			
			if (sQuantity.equals("")){ return "No Quantity";}
			if (sDescription.equals("")){ return "No Description";}
			
			String urlParameters = "?pid=untrackedReturn&fid=update"; 
			String result = "";
			try{
			
				urlParameters += SendParameters();
				
				result = MyWebRequest.HttpRequestS4(urlParameters); 
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return result;
			} catch (Exception e) {
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("done")) 
	    	{
	    		Toast.makeText(getBaseContext(),"Item Captured",Toast.LENGTH_LONG).show();
	    		etDescription.setText("");
	    		etQuantity.setText("");
	    		bCapSuccess = true;
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	etDescription = (EditText) findViewById(R.id.editTextCaptureDescription);
	    	sDescription = etDescription.getText().toString();
	    	etQuantity = (EditText) findViewById(R.id.editTextCaptureQuantity);
	    	sQuantity = etQuantity.getText().toString();
	    	bCapSuccess = false;
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
	    
	    public String SendParameters() throws UnsupportedEncodingException{
	    	String param = "";
	    	param += "&description=" + URLEncoder.encode(sDescription, "utf-8") + "&quantity=" + sQuantity;
	    	return param;
	    }

	}
	
	class WebRequestPrint extends AsyncTask<String, Void, String> {

		EditText etQuantity;
		String sQuantity = "0"; 
		
		@Override
		protected String doInBackground(String... params) {
			
			if (sQuantity.equals("")){ return "No Quantity";}
			
			String urlParameters = "?pid=untrackedReturn&fid=print";
			String result = "";
			try{
			
				urlParameters += SendParameters();
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return result;
			} catch (Exception e) {
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("done")) 
	    	{
	    		Toast.makeText(getBaseContext(),"Printed",Toast.LENGTH_LONG).show();
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	etQuantity = (EditText) findViewById(R.id.editTextCaptureQuantity);
	    	sQuantity = etQuantity.getText().toString();
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
	    
	    public String SendParameters(){
	    	String param = "";
	    	param += "&quantity=" + sQuantity;
	    	return param;
	    }

	}
}
