package MyContent;

import java.util.ArrayList;
import java.util.List;

import android.widget.ArrayAdapter;

public class QRelocateContent {

	public static List<String> ITEMS = new ArrayList<String>();
	
	public static ArrayAdapter<String> adapter;

	public static void addItem(String item) {
		if (!CheckDuplicate(item))
		{
			ITEMS.add(0,item);
		}		
	}
	
	public static void deleteItem(int itempos) {
		ITEMS.remove(itempos);
	}

	public static class RelocateItem {
		public String barcode;		

		public RelocateItem(String barcode) {
			this.barcode = barcode;
		}

		@Override
		public String toString() {
			// Text Displaying in Selection List
			return barcode;
		}
	}
	
	private static boolean CheckDuplicate(String cItem)
	{
		for ( String item : ITEMS)
		{
			if (cItem.equals(item))
			{
				return true;
			}
		}
		return false;
	}
}