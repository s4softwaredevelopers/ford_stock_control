<h2>Inventory tracking</h2>
<?php
$check_out_accordion = 'acc-inactive';
$check_in_accordion = 'acc-inactive';

$q = "SELECT id, username AS description FROM user WHERE username NOT IN ('unassigned') ORDER by username ASC";
$user_array = build_result_array($q);

if (isset($_POST['checkout'])) {
    $id = $_POST['checkout'];
    $serial_number = $_POST['serial_number'];
    $description = $_POST['part_description'];
    $product_code = $_POST['product_code'];
    $product_description = $_POST['product_description'];
    $checked_out = $_POST['checked_out'];
    $quantity_in_store = $_POST['quantity_in_store'];
    $check_out_quantity = $_POST['check_out_quantity'];
    
    $check_out_accordion = $_POST['check_out_accordion'];
    $check_in_accordion = $_POST['check_in_accordion'];
    $access_comment = $_POST['access_comment'];
	
    $user_id = array_search(($_SESSION['user']), $user_array);
    
    if ($check_out_quantity > 0) {
        if ($check_out_quantity > $quantity_in_store) {
            echo '<p class="error">Quantity to check out exceeds quantity in store of ' . $quantity_in_store . '.</p>';
        }
        else {
            $checked_out_total = $checked_out + $check_out_quantity;

            $q = 'UPDATE item SET checked_out=:checked_out, user_id=:user_id, access_comment=:access_comment, last_access=:last_access WHERE id=:id';
            $params = array(':id' => $id, ':checked_out' => $checked_out_total, ':user_id' => $user_id, ':access_comment' =>$access_comment, ':last_access'=> date("Y-m-d H:i:s"));

            $rows = sql_update($q, $params);

            if ($rows == 1) {
                $sn = $serial_number;
                if ($serial_number == '') {
                    $sn = 'n/a';
                    if ($check_out_quantity == 1) {
                        echo '<p>Non-unique part with product code ' . $product_code . ' and product name ' . $product_description . ' checked out.</p>';
                        
                    }
                    else {
                        echo '<p>Non-unique parts with product code ' . $product_code . ' and product name ' . $product_description . ' checked out.</p>';
                    }
                }
                else {
                     echo '<p>Unique part with serial number ' . $serial_number . ' checked out.</p>';
                }
                logger('check out', 'S/N :: ' . $sn . ' | Description :: ' . $description . ' | Product code :: ' . $product_code . ' | Product :: ' . $product_description . ' | Stores :: ' . $quantity_in_store . ' | C/o :: ' . $check_out_quantity . ' | User :: ' . $_SESSION['user']);
            }
            else {
                echo '<p class="error">Part not checked out.</p>';
            }
        }
    }
    else {
        echo '<p class="error">Check out quantity specified is less than 1. No parts checked out.</p>';
    }
}
elseif (isset($_POST['checkin'])) {
    $id = $_POST['checkin'];
    $serial_number = $_POST['serial_number'];
    $description = $_POST['part_description'];
    $product_code = $_POST['product_code'];
    $product_description = $_POST['product_description'];
    $checked_out = $_POST['checked_out'];
    $check_in_quantity = $_POST['check_in_quantity'];
    
    $check_out_accordion = $_POST['check_out_accordion'];
    $check_in_accordion = $_POST['check_in_accordion'];
    $access_comment = $_POST['access_comment'];
	
    $user_id = array_search(($_SESSION['user']), $user_array);
    
    if ($check_in_quantity > 0) {
        if ($check_in_quantity > $checked_out) {
            echo '<p class="error">Quantity to check in exceeds quantity checked out of ' . $checked_out . '.</p>';
        }
        else {
            $checked_out_total = $checked_out - $check_in_quantity;

            $q = 'UPDATE item SET checked_out=:checked_out, user_id=:user_id, access_comment=:access_comment, last_access=:last_access WHERE id=:id';
            $params = array(':id' => $id, ':checked_out' => $checked_out_total, ':user_id' => $user_id, ':access_comment' =>$access_comment, ':last_access'=> date("Y-m-d H:i:s"));

            $rows = sql_update($q, $params);

            if ($rows == 1) {
                $sn = $serial_number;
                if ($serial_number == '') {
                    $sn = 'n/a';
                    if ($check_in_quantity == 1) {
                        echo '<p>Non-unique part with product code ' . $product_code . ' and product name ' . $product_description . ' checked in.</p>';
                    }
                    else {
                        echo '<p>Non-unique parts with product code ' . $product_code . ' and product name ' . $product_description . ' checked in.</p>';
                    }
                }
                else {
                     echo '<p>Unique part with serial number ' . $serial_number . ' checked in.</p>';
                }
                logger('check in', 'S/N :: ' . $sn . ' | Description :: ' . $description . ' | Product code :: ' . $product_code . ' | Product :: ' . $product_description . ' | Dispatches :: ' . $checked_out . ' | C/i :: ' . $check_in_quantity . ' | User :: ' . $_SESSION['user']);
            }
            else {
                echo '<p class="error">Part not checked in.</p>';
            }
        }
    }
    else {
        echo '<p class="error">Check in quantity specified is less than 1. No parts checked in.</p>';
    }
}
?>
<div id="inventoryDiv" >
Please wait...
</div>
