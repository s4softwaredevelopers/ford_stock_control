﻿namespace S4_Barcode_Generator
{
    partial class Barcode_Generator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Barcode_Generator));
            this.tabControlGenerator = new System.Windows.Forms.TabControl();
            this.tabPageGenerator = new System.Windows.Forms.TabPage();
            this.groupBoxGenerate = new System.Windows.Forms.GroupBox();
            this.checkBoxGenerate_Print = new System.Windows.Forms.CheckBox();
            this.groupBoxGenerate_Generate = new System.Windows.Forms.GroupBox();
            this.numericUpDownGenerate_Total = new System.Windows.Forms.NumericUpDown();
            this.buttonGenerate_GenerateBarcodes = new System.Windows.Forms.Button();
            this.labelGenerate_Total = new System.Windows.Forms.Label();
            this.tabPagePrint = new System.Windows.Forms.TabPage();
            this.groupBoxPrint = new System.Windows.Forms.GroupBox();
            this.buttonPrint_ChangePrinter = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDownPrint_CopiesBarcodes = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPrint_NumberBarcodes = new System.Windows.Forms.NumericUpDown();
            this.labelPrint_Copies = new System.Windows.Forms.Label();
            this.labelPrint_Number = new System.Windows.Forms.Label();
            this.comboBoxPrint_Barcodes = new System.Windows.Forms.ComboBox();
            this.barcodesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.databaseBarcodesDataSet = new S4_Barcode_Generator.DatabaseBarcodesDataSet();
            this.buttonPrint_Print = new System.Windows.Forms.Button();
            this.labelPrint_Barcodes = new System.Windows.Forms.Label();
            this.Server = new System.Windows.Forms.TabPage();
            this.groupBoxServer = new System.Windows.Forms.GroupBox();
            this.groupBoxServer_Notes = new System.Windows.Forms.GroupBox();
            this.labelServer_Notes = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxServer_ReplyNumber = new System.Windows.Forms.TextBox();
            this.textBoxServer_PortNumber = new System.Windows.Forms.TextBox();
            this.labelServer_ReplyNumber = new System.Windows.Forms.Label();
            this.labelServer_PortNumber = new System.Windows.Forms.Label();
            this.buttonServer_Stop = new System.Windows.Forms.Button();
            this.buttonServer_Start = new System.Windows.Forms.Button();
            this.Log = new System.Windows.Forms.TabPage();
            this.groupBoxLog = new System.Windows.Forms.GroupBox();
            this.dataGridViewLog = new System.Windows.Forms.DataGridView();
            this.barcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.copiesPrintedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonLog_Switch = new System.Windows.Forms.Button();
            this.richTextBoxLog_Logger = new System.Windows.Forms.RichTextBox();
            this.tabPageZPLCode = new System.Windows.Forms.TabPage();
            this.groupBoxZPLCode_BuildingBlocks = new System.Windows.Forms.GroupBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.labelNotes = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.buttonZPL_CreateQRBarcode = new System.Windows.Forms.Button();
            this.textBoxZPL_QRText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_QRY = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_QRX = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.numericUpDownZPL_GraphicT = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_GraphicW = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownZPL_GraphicX = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonZPL_CreateGraphicBox = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_GraphicH = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_GraphicY = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.numericUpDownZPL_FontSize = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonZPL_InsetText = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_TextY = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_TextX = new System.Windows.Forms.NumericUpDown();
            this.textBoxZPL_Text = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonZPL_Finish = new System.Windows.Forms.Button();
            this.buttonZPL_Start = new System.Windows.Forms.Button();
            this.groupBoxZPL = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxZPL_Templates = new System.Windows.Forms.ComboBox();
            this.buttonZPL_Update = new System.Windows.Forms.Button();
            this.richTextBoxZPL_ZPLCode = new System.Windows.Forms.RichTextBox();
            this.tabPageSettings = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBoxMini = new System.Windows.Forms.GroupBox();
            this.checkBoxMini = new System.Windows.Forms.CheckBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.labelSettings_LastDB = new System.Windows.Forms.Label();
            this.buttonSettings_ConfigUpdate = new System.Windows.Forms.Button();
            this.textBoxSettings_Config = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labelGeneral_Status = new System.Windows.Forms.Label();
            this.barcodesTableAdapter = new S4_Barcode_Generator.DatabaseBarcodesDataSetTableAdapters.BarcodesTableAdapter();
            this.labelGeneral_NegStatus = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.buttonZPL_BarCreate = new System.Windows.Forms.Button();
            this.textBoxZPL_BarText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_BarY = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.numericUpDownZPL_BarX = new System.Windows.Forms.NumericUpDown();
            this.tabControlGenerator.SuspendLayout();
            this.tabPageGenerator.SuspendLayout();
            this.groupBoxGenerate.SuspendLayout();
            this.groupBoxGenerate_Generate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGenerate_Total)).BeginInit();
            this.tabPagePrint.SuspendLayout();
            this.groupBoxPrint.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrint_CopiesBarcodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrint_NumberBarcodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseBarcodesDataSet)).BeginInit();
            this.Server.SuspendLayout();
            this.groupBoxServer.SuspendLayout();
            this.groupBoxServer_Notes.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.Log.SuspendLayout();
            this.groupBoxLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLog)).BeginInit();
            this.tabPageZPLCode.SuspendLayout();
            this.groupBoxZPLCode_BuildingBlocks.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_QRY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_QRX)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicY)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_FontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_TextY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_TextX)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBoxZPL.SuspendLayout();
            this.tabPageSettings.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBoxMini.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_BarY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_BarX)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControlGenerator
            // 
            this.tabControlGenerator.Controls.Add(this.tabPageGenerator);
            this.tabControlGenerator.Controls.Add(this.tabPagePrint);
            this.tabControlGenerator.Controls.Add(this.Server);
            this.tabControlGenerator.Controls.Add(this.Log);
            this.tabControlGenerator.Controls.Add(this.tabPageZPLCode);
            this.tabControlGenerator.Controls.Add(this.tabPageSettings);
            this.tabControlGenerator.Location = new System.Drawing.Point(0, 0);
            this.tabControlGenerator.Name = "tabControlGenerator";
            this.tabControlGenerator.SelectedIndex = 0;
            this.tabControlGenerator.Size = new System.Drawing.Size(571, 518);
            this.tabControlGenerator.TabIndex = 0;
            this.tabControlGenerator.SelectedIndexChanged += new System.EventHandler(this.tabControlGenerator_SelectedIndexChanged);
            // 
            // tabPageGenerator
            // 
            this.tabPageGenerator.Controls.Add(this.groupBoxGenerate);
            this.tabPageGenerator.Location = new System.Drawing.Point(4, 22);
            this.tabPageGenerator.Name = "tabPageGenerator";
            this.tabPageGenerator.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGenerator.Size = new System.Drawing.Size(563, 492);
            this.tabPageGenerator.TabIndex = 0;
            this.tabPageGenerator.Text = "Generate";
            this.tabPageGenerator.UseVisualStyleBackColor = true;
            // 
            // groupBoxGenerate
            // 
            this.groupBoxGenerate.Controls.Add(this.checkBoxGenerate_Print);
            this.groupBoxGenerate.Controls.Add(this.groupBoxGenerate_Generate);
            this.groupBoxGenerate.Location = new System.Drawing.Point(6, 6);
            this.groupBoxGenerate.Name = "groupBoxGenerate";
            this.groupBoxGenerate.Size = new System.Drawing.Size(434, 160);
            this.groupBoxGenerate.TabIndex = 0;
            this.groupBoxGenerate.TabStop = false;
            this.groupBoxGenerate.Text = "Generate Barcodes";
            // 
            // checkBoxGenerate_Print
            // 
            this.checkBoxGenerate_Print.AutoSize = true;
            this.checkBoxGenerate_Print.Checked = true;
            this.checkBoxGenerate_Print.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGenerate_Print.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxGenerate_Print.Location = new System.Drawing.Point(91, 129);
            this.checkBoxGenerate_Print.Name = "checkBoxGenerate_Print";
            this.checkBoxGenerate_Print.Size = new System.Drawing.Size(246, 24);
            this.checkBoxGenerate_Print.TabIndex = 2;
            this.checkBoxGenerate_Print.Text = "Print when Barcodes Generate";
            this.checkBoxGenerate_Print.UseVisualStyleBackColor = true;
            // 
            // groupBoxGenerate_Generate
            // 
            this.groupBoxGenerate_Generate.Controls.Add(this.numericUpDownGenerate_Total);
            this.groupBoxGenerate_Generate.Controls.Add(this.buttonGenerate_GenerateBarcodes);
            this.groupBoxGenerate_Generate.Controls.Add(this.labelGenerate_Total);
            this.groupBoxGenerate_Generate.Location = new System.Drawing.Point(6, 19);
            this.groupBoxGenerate_Generate.Name = "groupBoxGenerate_Generate";
            this.groupBoxGenerate_Generate.Size = new System.Drawing.Size(422, 104);
            this.groupBoxGenerate_Generate.TabIndex = 0;
            this.groupBoxGenerate_Generate.TabStop = false;
            // 
            // numericUpDownGenerate_Total
            // 
            this.numericUpDownGenerate_Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownGenerate_Total.Location = new System.Drawing.Point(220, 18);
            this.numericUpDownGenerate_Total.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownGenerate_Total.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownGenerate_Total.Name = "numericUpDownGenerate_Total";
            this.numericUpDownGenerate_Total.Size = new System.Drawing.Size(196, 26);
            this.numericUpDownGenerate_Total.TabIndex = 2;
            this.numericUpDownGenerate_Total.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonGenerate_GenerateBarcodes
            // 
            this.buttonGenerate_GenerateBarcodes.Enabled = false;
            this.buttonGenerate_GenerateBarcodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGenerate_GenerateBarcodes.Location = new System.Drawing.Point(6, 66);
            this.buttonGenerate_GenerateBarcodes.Name = "buttonGenerate_GenerateBarcodes";
            this.buttonGenerate_GenerateBarcodes.Size = new System.Drawing.Size(410, 31);
            this.buttonGenerate_GenerateBarcodes.TabIndex = 1;
            this.buttonGenerate_GenerateBarcodes.Text = "Generate Barcodes";
            this.buttonGenerate_GenerateBarcodes.UseVisualStyleBackColor = true;
            this.buttonGenerate_GenerateBarcodes.Click += new System.EventHandler(this.buttonGenerate_GenerateBarcodes_Click);
            // 
            // labelGenerate_Total
            // 
            this.labelGenerate_Total.AutoSize = true;
            this.labelGenerate_Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGenerate_Total.Location = new System.Drawing.Point(6, 20);
            this.labelGenerate_Total.Name = "labelGenerate_Total";
            this.labelGenerate_Total.Size = new System.Drawing.Size(155, 20);
            this.labelGenerate_Total.TabIndex = 0;
            this.labelGenerate_Total.Text = "Number of Barcodes";
            // 
            // tabPagePrint
            // 
            this.tabPagePrint.Controls.Add(this.groupBoxPrint);
            this.tabPagePrint.Location = new System.Drawing.Point(4, 22);
            this.tabPagePrint.Name = "tabPagePrint";
            this.tabPagePrint.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePrint.Size = new System.Drawing.Size(563, 492);
            this.tabPagePrint.TabIndex = 1;
            this.tabPagePrint.Text = "Print";
            this.tabPagePrint.UseVisualStyleBackColor = true;
            // 
            // groupBoxPrint
            // 
            this.groupBoxPrint.Controls.Add(this.buttonPrint_ChangePrinter);
            this.groupBoxPrint.Controls.Add(this.groupBox1);
            this.groupBoxPrint.Location = new System.Drawing.Point(6, 6);
            this.groupBoxPrint.Name = "groupBoxPrint";
            this.groupBoxPrint.Size = new System.Drawing.Size(434, 225);
            this.groupBoxPrint.TabIndex = 1;
            this.groupBoxPrint.TabStop = false;
            this.groupBoxPrint.Text = "Print Barcodes";
            // 
            // buttonPrint_ChangePrinter
            // 
            this.buttonPrint_ChangePrinter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint_ChangePrinter.Location = new System.Drawing.Point(101, 187);
            this.buttonPrint_ChangePrinter.Name = "buttonPrint_ChangePrinter";
            this.buttonPrint_ChangePrinter.Size = new System.Drawing.Size(212, 31);
            this.buttonPrint_ChangePrinter.TabIndex = 3;
            this.buttonPrint_ChangePrinter.Text = "Change Printer";
            this.buttonPrint_ChangePrinter.UseVisualStyleBackColor = true;
            this.buttonPrint_ChangePrinter.Click += new System.EventHandler(this.buttonPrint_ChangePrinter_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDownPrint_CopiesBarcodes);
            this.groupBox1.Controls.Add(this.numericUpDownPrint_NumberBarcodes);
            this.groupBox1.Controls.Add(this.labelPrint_Copies);
            this.groupBox1.Controls.Add(this.labelPrint_Number);
            this.groupBox1.Controls.Add(this.comboBoxPrint_Barcodes);
            this.groupBox1.Controls.Add(this.buttonPrint_Print);
            this.groupBox1.Controls.Add(this.labelPrint_Barcodes);
            this.groupBox1.Location = new System.Drawing.Point(6, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(422, 162);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // numericUpDownPrint_CopiesBarcodes
            // 
            this.numericUpDownPrint_CopiesBarcodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownPrint_CopiesBarcodes.Location = new System.Drawing.Point(181, 80);
            this.numericUpDownPrint_CopiesBarcodes.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownPrint_CopiesBarcodes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPrint_CopiesBarcodes.Name = "numericUpDownPrint_CopiesBarcodes";
            this.numericUpDownPrint_CopiesBarcodes.Size = new System.Drawing.Size(235, 26);
            this.numericUpDownPrint_CopiesBarcodes.TabIndex = 6;
            this.numericUpDownPrint_CopiesBarcodes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericUpDownPrint_NumberBarcodes
            // 
            this.numericUpDownPrint_NumberBarcodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownPrint_NumberBarcodes.Location = new System.Drawing.Point(181, 48);
            this.numericUpDownPrint_NumberBarcodes.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownPrint_NumberBarcodes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPrint_NumberBarcodes.Name = "numericUpDownPrint_NumberBarcodes";
            this.numericUpDownPrint_NumberBarcodes.Size = new System.Drawing.Size(235, 26);
            this.numericUpDownPrint_NumberBarcodes.TabIndex = 5;
            this.numericUpDownPrint_NumberBarcodes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelPrint_Copies
            // 
            this.labelPrint_Copies.AutoSize = true;
            this.labelPrint_Copies.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrint_Copies.Location = new System.Drawing.Point(6, 82);
            this.labelPrint_Copies.Name = "labelPrint_Copies";
            this.labelPrint_Copies.Size = new System.Drawing.Size(156, 20);
            this.labelPrint_Copies.TabIndex = 4;
            this.labelPrint_Copies.Text = "Copies of Barcodes :";
            // 
            // labelPrint_Number
            // 
            this.labelPrint_Number.AutoSize = true;
            this.labelPrint_Number.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrint_Number.Location = new System.Drawing.Point(6, 50);
            this.labelPrint_Number.Name = "labelPrint_Number";
            this.labelPrint_Number.Size = new System.Drawing.Size(163, 20);
            this.labelPrint_Number.TabIndex = 3;
            this.labelPrint_Number.Text = "Number of Barcodes :";
            // 
            // comboBoxPrint_Barcodes
            // 
            this.comboBoxPrint_Barcodes.DataSource = this.barcodesBindingSource;
            this.comboBoxPrint_Barcodes.DisplayMember = "Barcode";
            this.comboBoxPrint_Barcodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPrint_Barcodes.FormattingEnabled = true;
            this.comboBoxPrint_Barcodes.Location = new System.Drawing.Point(181, 14);
            this.comboBoxPrint_Barcodes.Name = "comboBoxPrint_Barcodes";
            this.comboBoxPrint_Barcodes.Size = new System.Drawing.Size(235, 28);
            this.comboBoxPrint_Barcodes.TabIndex = 2;
            // 
            // barcodesBindingSource
            // 
            this.barcodesBindingSource.DataMember = "Barcodes";
            this.barcodesBindingSource.DataSource = this.databaseBarcodesDataSet;
            // 
            // databaseBarcodesDataSet
            // 
            this.databaseBarcodesDataSet.DataSetName = "DatabaseBarcodesDataSet";
            this.databaseBarcodesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // buttonPrint_Print
            // 
            this.buttonPrint_Print.Enabled = false;
            this.buttonPrint_Print.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrint_Print.Location = new System.Drawing.Point(6, 121);
            this.buttonPrint_Print.Name = "buttonPrint_Print";
            this.buttonPrint_Print.Size = new System.Drawing.Size(410, 31);
            this.buttonPrint_Print.TabIndex = 1;
            this.buttonPrint_Print.Text = "Print Barcodes";
            this.buttonPrint_Print.UseVisualStyleBackColor = true;
            this.buttonPrint_Print.Click += new System.EventHandler(this.buttonPrint_Print_Click);
            // 
            // labelPrint_Barcodes
            // 
            this.labelPrint_Barcodes.AutoSize = true;
            this.labelPrint_Barcodes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrint_Barcodes.Location = new System.Drawing.Point(6, 17);
            this.labelPrint_Barcodes.Name = "labelPrint_Barcodes";
            this.labelPrint_Barcodes.Size = new System.Drawing.Size(77, 20);
            this.labelPrint_Barcodes.TabIndex = 0;
            this.labelPrint_Barcodes.Text = "Barcodes";
            // 
            // Server
            // 
            this.Server.Controls.Add(this.groupBoxServer);
            this.Server.Location = new System.Drawing.Point(4, 22);
            this.Server.Name = "Server";
            this.Server.Padding = new System.Windows.Forms.Padding(3);
            this.Server.Size = new System.Drawing.Size(563, 492);
            this.Server.TabIndex = 2;
            this.Server.Text = "Server";
            this.Server.UseVisualStyleBackColor = true;
            // 
            // groupBoxServer
            // 
            this.groupBoxServer.Controls.Add(this.groupBoxServer_Notes);
            this.groupBoxServer.Controls.Add(this.groupBox2);
            this.groupBoxServer.Location = new System.Drawing.Point(6, 6);
            this.groupBoxServer.Name = "groupBoxServer";
            this.groupBoxServer.Size = new System.Drawing.Size(434, 227);
            this.groupBoxServer.TabIndex = 1;
            this.groupBoxServer.TabStop = false;
            this.groupBoxServer.Text = "Server";
            // 
            // groupBoxServer_Notes
            // 
            this.groupBoxServer_Notes.Controls.Add(this.labelServer_Notes);
            this.groupBoxServer_Notes.Location = new System.Drawing.Point(6, 174);
            this.groupBoxServer_Notes.Name = "groupBoxServer_Notes";
            this.groupBoxServer_Notes.Size = new System.Drawing.Size(422, 47);
            this.groupBoxServer_Notes.TabIndex = 4;
            this.groupBoxServer_Notes.TabStop = false;
            this.groupBoxServer_Notes.Text = "Notes";
            // 
            // labelServer_Notes
            // 
            this.labelServer_Notes.AutoSize = true;
            this.labelServer_Notes.Location = new System.Drawing.Point(7, 16);
            this.labelServer_Notes.Name = "labelServer_Notes";
            this.labelServer_Notes.Size = new System.Drawing.Size(227, 26);
            this.labelServer_Notes.TabIndex = 0;
            this.labelServer_Notes.Text = "Server Port - Port which receives remote signal\r\nReply Port - Port which is waiti" +
    "ng for reply";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxServer_ReplyNumber);
            this.groupBox2.Controls.Add(this.textBoxServer_PortNumber);
            this.groupBox2.Controls.Add(this.labelServer_ReplyNumber);
            this.groupBox2.Controls.Add(this.labelServer_PortNumber);
            this.groupBox2.Controls.Add(this.buttonServer_Stop);
            this.groupBox2.Controls.Add(this.buttonServer_Start);
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(422, 149);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            // 
            // textBoxServer_ReplyNumber
            // 
            this.textBoxServer_ReplyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServer_ReplyNumber.Location = new System.Drawing.Point(190, 108);
            this.textBoxServer_ReplyNumber.Name = "textBoxServer_ReplyNumber";
            this.textBoxServer_ReplyNumber.Size = new System.Drawing.Size(226, 26);
            this.textBoxServer_ReplyNumber.TabIndex = 7;
            // 
            // textBoxServer_PortNumber
            // 
            this.textBoxServer_PortNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxServer_PortNumber.Location = new System.Drawing.Point(190, 76);
            this.textBoxServer_PortNumber.Name = "textBoxServer_PortNumber";
            this.textBoxServer_PortNumber.Size = new System.Drawing.Size(226, 26);
            this.textBoxServer_PortNumber.TabIndex = 6;
            // 
            // labelServer_ReplyNumber
            // 
            this.labelServer_ReplyNumber.AutoSize = true;
            this.labelServer_ReplyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer_ReplyNumber.Location = new System.Drawing.Point(13, 111);
            this.labelServer_ReplyNumber.Name = "labelServer_ReplyNumber";
            this.labelServer_ReplyNumber.Size = new System.Drawing.Size(142, 20);
            this.labelServer_ReplyNumber.TabIndex = 4;
            this.labelServer_ReplyNumber.Text = "Reply Port Number";
            // 
            // labelServer_PortNumber
            // 
            this.labelServer_PortNumber.AutoSize = true;
            this.labelServer_PortNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServer_PortNumber.Location = new System.Drawing.Point(13, 79);
            this.labelServer_PortNumber.Name = "labelServer_PortNumber";
            this.labelServer_PortNumber.Size = new System.Drawing.Size(148, 20);
            this.labelServer_PortNumber.TabIndex = 3;
            this.labelServer_PortNumber.Text = "Server Port Number";
            // 
            // buttonServer_Stop
            // 
            this.buttonServer_Stop.Enabled = false;
            this.buttonServer_Stop.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonServer_Stop.Location = new System.Drawing.Point(239, 29);
            this.buttonServer_Stop.Name = "buttonServer_Stop";
            this.buttonServer_Stop.Size = new System.Drawing.Size(163, 31);
            this.buttonServer_Stop.TabIndex = 1;
            this.buttonServer_Stop.Text = "Stop Server";
            this.buttonServer_Stop.UseVisualStyleBackColor = true;
            this.buttonServer_Stop.Click += new System.EventHandler(this.buttonServer_Stop_Click);
            // 
            // buttonServer_Start
            // 
            this.buttonServer_Start.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonServer_Start.Location = new System.Drawing.Point(17, 29);
            this.buttonServer_Start.Name = "buttonServer_Start";
            this.buttonServer_Start.Size = new System.Drawing.Size(163, 31);
            this.buttonServer_Start.TabIndex = 0;
            this.buttonServer_Start.Text = "Start Server";
            this.buttonServer_Start.UseVisualStyleBackColor = true;
            this.buttonServer_Start.Click += new System.EventHandler(this.buttonServer_Start_Click);
            // 
            // Log
            // 
            this.Log.Controls.Add(this.groupBoxLog);
            this.Log.Location = new System.Drawing.Point(4, 22);
            this.Log.Name = "Log";
            this.Log.Padding = new System.Windows.Forms.Padding(3);
            this.Log.Size = new System.Drawing.Size(563, 492);
            this.Log.TabIndex = 3;
            this.Log.Text = "Log";
            this.Log.UseVisualStyleBackColor = true;
            // 
            // groupBoxLog
            // 
            this.groupBoxLog.Controls.Add(this.dataGridViewLog);
            this.groupBoxLog.Controls.Add(this.buttonLog_Switch);
            this.groupBoxLog.Controls.Add(this.richTextBoxLog_Logger);
            this.groupBoxLog.Location = new System.Drawing.Point(6, 6);
            this.groupBoxLog.Name = "groupBoxLog";
            this.groupBoxLog.Size = new System.Drawing.Size(434, 480);
            this.groupBoxLog.TabIndex = 1;
            this.groupBoxLog.TabStop = false;
            this.groupBoxLog.Text = "Logger";
            // 
            // dataGridViewLog
            // 
            this.dataGridViewLog.AllowUserToAddRows = false;
            this.dataGridViewLog.AllowUserToDeleteRows = false;
            this.dataGridViewLog.AutoGenerateColumns = false;
            this.dataGridViewLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.barcodeDataGridViewTextBoxColumn,
            this.copiesPrintedDataGridViewTextBoxColumn});
            this.dataGridViewLog.DataSource = this.barcodesBindingSource;
            this.dataGridViewLog.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewLog.Name = "dataGridViewLog";
            this.dataGridViewLog.ReadOnly = true;
            this.dataGridViewLog.Size = new System.Drawing.Size(422, 426);
            this.dataGridViewLog.TabIndex = 2;
            this.dataGridViewLog.Visible = false;
            // 
            // barcodeDataGridViewTextBoxColumn
            // 
            this.barcodeDataGridViewTextBoxColumn.DataPropertyName = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.HeaderText = "Barcode";
            this.barcodeDataGridViewTextBoxColumn.Name = "barcodeDataGridViewTextBoxColumn";
            this.barcodeDataGridViewTextBoxColumn.ReadOnly = true;
            this.barcodeDataGridViewTextBoxColumn.Width = 220;
            // 
            // copiesPrintedDataGridViewTextBoxColumn
            // 
            this.copiesPrintedDataGridViewTextBoxColumn.DataPropertyName = "Copies_Printed";
            this.copiesPrintedDataGridViewTextBoxColumn.HeaderText = "Copies_Printed";
            this.copiesPrintedDataGridViewTextBoxColumn.Name = "copiesPrintedDataGridViewTextBoxColumn";
            this.copiesPrintedDataGridViewTextBoxColumn.ReadOnly = true;
            this.copiesPrintedDataGridViewTextBoxColumn.Width = 159;
            // 
            // buttonLog_Switch
            // 
            this.buttonLog_Switch.Location = new System.Drawing.Point(6, 451);
            this.buttonLog_Switch.Name = "buttonLog_Switch";
            this.buttonLog_Switch.Size = new System.Drawing.Size(422, 23);
            this.buttonLog_Switch.TabIndex = 1;
            this.buttonLog_Switch.Text = "View all Barcodes with number of Copies printed";
            this.buttonLog_Switch.UseVisualStyleBackColor = true;
            this.buttonLog_Switch.Click += new System.EventHandler(this.buttonLog_Switch_Click);
            // 
            // richTextBoxLog_Logger
            // 
            this.richTextBoxLog_Logger.Location = new System.Drawing.Point(6, 19);
            this.richTextBoxLog_Logger.Name = "richTextBoxLog_Logger";
            this.richTextBoxLog_Logger.ReadOnly = true;
            this.richTextBoxLog_Logger.Size = new System.Drawing.Size(422, 416);
            this.richTextBoxLog_Logger.TabIndex = 0;
            this.richTextBoxLog_Logger.Text = "";
            this.richTextBoxLog_Logger.WordWrap = false;
            // 
            // tabPageZPLCode
            // 
            this.tabPageZPLCode.Controls.Add(this.groupBoxZPLCode_BuildingBlocks);
            this.tabPageZPLCode.Controls.Add(this.groupBoxZPL);
            this.tabPageZPLCode.Location = new System.Drawing.Point(4, 22);
            this.tabPageZPLCode.Name = "tabPageZPLCode";
            this.tabPageZPLCode.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageZPLCode.Size = new System.Drawing.Size(563, 492);
            this.tabPageZPLCode.TabIndex = 4;
            this.tabPageZPLCode.Text = "ZPL Code";
            this.tabPageZPLCode.UseVisualStyleBackColor = true;
            // 
            // groupBoxZPLCode_BuildingBlocks
            // 
            this.groupBoxZPLCode_BuildingBlocks.Controls.Add(this.groupBox10);
            this.groupBoxZPLCode_BuildingBlocks.Controls.Add(this.groupBox9);
            this.groupBoxZPLCode_BuildingBlocks.Controls.Add(this.groupBox8);
            this.groupBoxZPLCode_BuildingBlocks.Controls.Add(this.groupBox7);
            this.groupBoxZPLCode_BuildingBlocks.Controls.Add(this.groupBox6);
            this.groupBoxZPLCode_BuildingBlocks.Controls.Add(this.groupBox3);
            this.groupBoxZPLCode_BuildingBlocks.Location = new System.Drawing.Point(6, 257);
            this.groupBoxZPLCode_BuildingBlocks.Name = "groupBoxZPLCode_BuildingBlocks";
            this.groupBoxZPLCode_BuildingBlocks.Size = new System.Drawing.Size(550, 230);
            this.groupBoxZPLCode_BuildingBlocks.TabIndex = 3;
            this.groupBoxZPLCode_BuildingBlocks.TabStop = false;
            this.groupBoxZPLCode_BuildingBlocks.Text = "Building Blocks";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.labelNotes);
            this.groupBox9.Location = new System.Drawing.Point(367, 19);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(174, 100);
            this.groupBox9.TabIndex = 10;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Notes";
            // 
            // labelNotes
            // 
            this.labelNotes.AutoSize = true;
            this.labelNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNotes.Location = new System.Drawing.Point(3, 18);
            this.labelNotes.Name = "labelNotes";
            this.labelNotes.Size = new System.Drawing.Size(154, 78);
            this.labelNotes.TabIndex = 0;
            this.labelNotes.Text = "@PRNQTY@ \r\n- Print Quantity from Application\r\n@BARCODE@ \r\n- Barcode from Applicat" +
    "ion\r\n@DATE@ \r\n- Current Date from Application";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.buttonZPL_CreateQRBarcode);
            this.groupBox8.Controls.Add(this.textBoxZPL_QRText);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.numericUpDownZPL_QRY);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.numericUpDownZPL_QRX);
            this.groupBox8.Location = new System.Drawing.Point(6, 123);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(174, 100);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Create 2D (QR) Barcode";
            // 
            // buttonZPL_CreateQRBarcode
            // 
            this.buttonZPL_CreateQRBarcode.Location = new System.Drawing.Point(6, 70);
            this.buttonZPL_CreateQRBarcode.Name = "buttonZPL_CreateQRBarcode";
            this.buttonZPL_CreateQRBarcode.Size = new System.Drawing.Size(162, 22);
            this.buttonZPL_CreateQRBarcode.TabIndex = 64;
            this.buttonZPL_CreateQRBarcode.Text = "Create 2D (QR) Barcode";
            this.buttonZPL_CreateQRBarcode.UseVisualStyleBackColor = true;
            this.buttonZPL_CreateQRBarcode.Click += new System.EventHandler(this.buttonZPL_CreateQRBarcode_Click);
            // 
            // textBoxZPL_QRText
            // 
            this.textBoxZPL_QRText.Location = new System.Drawing.Point(6, 18);
            this.textBoxZPL_QRText.Name = "textBoxZPL_QRText";
            this.textBoxZPL_QRText.Size = new System.Drawing.Size(162, 20);
            this.textBoxZPL_QRText.TabIndex = 63;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(93, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 62;
            this.label6.Text = "Y :";
            // 
            // numericUpDownZPL_QRY
            // 
            this.numericUpDownZPL_QRY.Location = new System.Drawing.Point(116, 44);
            this.numericUpDownZPL_QRY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_QRY.Name = "numericUpDownZPL_QRY";
            this.numericUpDownZPL_QRY.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_QRY.TabIndex = 61;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 46);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 13);
            this.label15.TabIndex = 60;
            this.label15.Text = "X :";
            // 
            // numericUpDownZPL_QRX
            // 
            this.numericUpDownZPL_QRX.Location = new System.Drawing.Point(29, 44);
            this.numericUpDownZPL_QRX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_QRX.Name = "numericUpDownZPL_QRX";
            this.numericUpDownZPL_QRX.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_QRX.TabIndex = 59;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.numericUpDownZPL_GraphicT);
            this.groupBox7.Controls.Add(this.label2);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.numericUpDownZPL_GraphicW);
            this.groupBox7.Controls.Add(this.numericUpDownZPL_GraphicX);
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.buttonZPL_CreateGraphicBox);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this.numericUpDownZPL_GraphicH);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.numericUpDownZPL_GraphicY);
            this.groupBox7.Location = new System.Drawing.Point(187, 123);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(174, 100);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Graphic Box";
            // 
            // numericUpDownZPL_GraphicT
            // 
            this.numericUpDownZPL_GraphicT.Location = new System.Drawing.Point(29, 71);
            this.numericUpDownZPL_GraphicT.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_GraphicT.Name = "numericUpDownZPL_GraphicT";
            this.numericUpDownZPL_GraphicT.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_GraphicT.TabIndex = 65;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 64;
            this.label2.Text = "T :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 63;
            this.label10.Text = "W :";
            // 
            // numericUpDownZPL_GraphicW
            // 
            this.numericUpDownZPL_GraphicW.Location = new System.Drawing.Point(29, 45);
            this.numericUpDownZPL_GraphicW.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_GraphicW.Name = "numericUpDownZPL_GraphicW";
            this.numericUpDownZPL_GraphicW.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_GraphicW.TabIndex = 62;
            // 
            // numericUpDownZPL_GraphicX
            // 
            this.numericUpDownZPL_GraphicX.Location = new System.Drawing.Point(29, 19);
            this.numericUpDownZPL_GraphicX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_GraphicX.Name = "numericUpDownZPL_GraphicX";
            this.numericUpDownZPL_GraphicX.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_GraphicX.TabIndex = 60;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 13);
            this.label11.TabIndex = 61;
            this.label11.Text = "X :";
            // 
            // buttonZPL_CreateGraphicBox
            // 
            this.buttonZPL_CreateGraphicBox.Location = new System.Drawing.Point(96, 70);
            this.buttonZPL_CreateGraphicBox.Name = "buttonZPL_CreateGraphicBox";
            this.buttonZPL_CreateGraphicBox.Size = new System.Drawing.Size(72, 22);
            this.buttonZPL_CreateGraphicBox.TabIndex = 59;
            this.buttonZPL_CreateGraphicBox.Text = "Create Box";
            this.buttonZPL_CreateGraphicBox.UseVisualStyleBackColor = true;
            this.buttonZPL_CreateGraphicBox.Click += new System.EventHandler(this.buttonZPL_CreateGraphicBox_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(93, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(21, 13);
            this.label12.TabIndex = 58;
            this.label12.Text = "H :";
            // 
            // numericUpDownZPL_GraphicH
            // 
            this.numericUpDownZPL_GraphicH.Location = new System.Drawing.Point(116, 45);
            this.numericUpDownZPL_GraphicH.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_GraphicH.Name = "numericUpDownZPL_GraphicH";
            this.numericUpDownZPL_GraphicH.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_GraphicH.TabIndex = 57;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(93, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 13);
            this.label13.TabIndex = 56;
            this.label13.Text = "Y :";
            // 
            // numericUpDownZPL_GraphicY
            // 
            this.numericUpDownZPL_GraphicY.Location = new System.Drawing.Point(116, 19);
            this.numericUpDownZPL_GraphicY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_GraphicY.Name = "numericUpDownZPL_GraphicY";
            this.numericUpDownZPL_GraphicY.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_GraphicY.TabIndex = 55;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.numericUpDownZPL_FontSize);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.buttonZPL_InsetText);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.numericUpDownZPL_TextY);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.numericUpDownZPL_TextX);
            this.groupBox6.Controls.Add(this.textBoxZPL_Text);
            this.groupBox6.Location = new System.Drawing.Point(187, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(174, 100);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Text";
            // 
            // numericUpDownZPL_FontSize
            // 
            this.numericUpDownZPL_FontSize.Location = new System.Drawing.Point(29, 70);
            this.numericUpDownZPL_FontSize.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_FontSize.Name = "numericUpDownZPL_FontSize";
            this.numericUpDownZPL_FontSize.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_FontSize.TabIndex = 47;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 48;
            this.label3.Text = "Size";
            // 
            // buttonZPL_InsetText
            // 
            this.buttonZPL_InsetText.Location = new System.Drawing.Point(96, 70);
            this.buttonZPL_InsetText.Name = "buttonZPL_InsetText";
            this.buttonZPL_InsetText.Size = new System.Drawing.Size(72, 22);
            this.buttonZPL_InsetText.TabIndex = 46;
            this.buttonZPL_InsetText.Text = "Insert Text";
            this.buttonZPL_InsetText.UseVisualStyleBackColor = true;
            this.buttonZPL_InsetText.Click += new System.EventHandler(this.buttonZPL_InsetText_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(93, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Y :";
            // 
            // numericUpDownZPL_TextY
            // 
            this.numericUpDownZPL_TextY.Location = new System.Drawing.Point(116, 44);
            this.numericUpDownZPL_TextY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_TextY.Name = "numericUpDownZPL_TextY";
            this.numericUpDownZPL_TextY.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_TextY.TabIndex = 44;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "X :";
            // 
            // numericUpDownZPL_TextX
            // 
            this.numericUpDownZPL_TextX.Location = new System.Drawing.Point(29, 44);
            this.numericUpDownZPL_TextX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_TextX.Name = "numericUpDownZPL_TextX";
            this.numericUpDownZPL_TextX.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_TextX.TabIndex = 42;
            // 
            // textBoxZPL_Text
            // 
            this.textBoxZPL_Text.Location = new System.Drawing.Point(6, 18);
            this.textBoxZPL_Text.Name = "textBoxZPL_Text";
            this.textBoxZPL_Text.Size = new System.Drawing.Size(162, 20);
            this.textBoxZPL_Text.TabIndex = 41;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonZPL_Finish);
            this.groupBox3.Controls.Add(this.buttonZPL_Start);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(174, 100);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Code Outline";
            // 
            // buttonZPL_Finish
            // 
            this.buttonZPL_Finish.Location = new System.Drawing.Point(6, 63);
            this.buttonZPL_Finish.Name = "buttonZPL_Finish";
            this.buttonZPL_Finish.Size = new System.Drawing.Size(162, 22);
            this.buttonZPL_Finish.TabIndex = 30;
            this.buttonZPL_Finish.Text = "End ZPL Code";
            this.buttonZPL_Finish.UseVisualStyleBackColor = true;
            this.buttonZPL_Finish.Click += new System.EventHandler(this.buttonZPL_Finish_Click);
            // 
            // buttonZPL_Start
            // 
            this.buttonZPL_Start.Location = new System.Drawing.Point(6, 28);
            this.buttonZPL_Start.Name = "buttonZPL_Start";
            this.buttonZPL_Start.Size = new System.Drawing.Size(162, 22);
            this.buttonZPL_Start.TabIndex = 29;
            this.buttonZPL_Start.Text = "Start ZPL Code";
            this.buttonZPL_Start.UseVisualStyleBackColor = true;
            this.buttonZPL_Start.Click += new System.EventHandler(this.buttonZPL_Start_Click);
            // 
            // groupBoxZPL
            // 
            this.groupBoxZPL.Controls.Add(this.label1);
            this.groupBoxZPL.Controls.Add(this.comboBoxZPL_Templates);
            this.groupBoxZPL.Controls.Add(this.buttonZPL_Update);
            this.groupBoxZPL.Controls.Add(this.richTextBoxZPL_ZPLCode);
            this.groupBoxZPL.Location = new System.Drawing.Point(6, 6);
            this.groupBoxZPL.Name = "groupBoxZPL";
            this.groupBoxZPL.Size = new System.Drawing.Size(550, 252);
            this.groupBoxZPL.TabIndex = 2;
            this.groupBoxZPL.TabStop = false;
            this.groupBoxZPL.Text = "ZPL Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(315, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "ZPL Template";
            // 
            // comboBoxZPL_Templates
            // 
            this.comboBoxZPL_Templates.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxZPL_Templates.FormattingEnabled = true;
            this.comboBoxZPL_Templates.Location = new System.Drawing.Point(441, 216);
            this.comboBoxZPL_Templates.Name = "comboBoxZPL_Templates";
            this.comboBoxZPL_Templates.Size = new System.Drawing.Size(100, 28);
            this.comboBoxZPL_Templates.TabIndex = 3;
            this.comboBoxZPL_Templates.SelectedIndexChanged += new System.EventHandler(this.comboBoxZPL_Templates_SelectedIndexChanged);
            // 
            // buttonZPL_Update
            // 
            this.buttonZPL_Update.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonZPL_Update.Location = new System.Drawing.Point(6, 214);
            this.buttonZPL_Update.Name = "buttonZPL_Update";
            this.buttonZPL_Update.Size = new System.Drawing.Size(181, 31);
            this.buttonZPL_Update.TabIndex = 1;
            this.buttonZPL_Update.Text = "Update Code";
            this.buttonZPL_Update.UseVisualStyleBackColor = true;
            this.buttonZPL_Update.Click += new System.EventHandler(this.buttonZPL_Update_Click);
            // 
            // richTextBoxZPL_ZPLCode
            // 
            this.richTextBoxZPL_ZPLCode.Location = new System.Drawing.Point(6, 19);
            this.richTextBoxZPL_ZPLCode.Name = "richTextBoxZPL_ZPLCode";
            this.richTextBoxZPL_ZPLCode.Size = new System.Drawing.Size(535, 189);
            this.richTextBoxZPL_ZPLCode.TabIndex = 0;
            this.richTextBoxZPL_ZPLCode.Text = "";
            // 
            // tabPageSettings
            // 
            this.tabPageSettings.Controls.Add(this.groupBox4);
            this.tabPageSettings.Location = new System.Drawing.Point(4, 22);
            this.tabPageSettings.Name = "tabPageSettings";
            this.tabPageSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSettings.Size = new System.Drawing.Size(563, 492);
            this.tabPageSettings.TabIndex = 5;
            this.tabPageSettings.Text = "Settings";
            this.tabPageSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBoxMini);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(434, 227);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Settings";
            // 
            // groupBoxMini
            // 
            this.groupBoxMini.Controls.Add(this.checkBoxMini);
            this.groupBoxMini.Location = new System.Drawing.Point(6, 165);
            this.groupBoxMini.Name = "groupBoxMini";
            this.groupBoxMini.Size = new System.Drawing.Size(422, 56);
            this.groupBoxMini.TabIndex = 7;
            this.groupBoxMini.TabStop = false;
            // 
            // checkBoxMini
            // 
            this.checkBoxMini.AutoSize = true;
            this.checkBoxMini.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMini.Location = new System.Drawing.Point(10, 19);
            this.checkBoxMini.Name = "checkBoxMini";
            this.checkBoxMini.Size = new System.Drawing.Size(231, 20);
            this.checkBoxMini.TabIndex = 0;
            this.checkBoxMini.Text = "Minimized when application opens";
            this.checkBoxMini.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.labelSettings_LastDB);
            this.groupBox5.Controls.Add(this.buttonSettings_ConfigUpdate);
            this.groupBox5.Controls.Add(this.textBoxSettings_Config);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Location = new System.Drawing.Point(6, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(422, 147);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            // 
            // labelSettings_LastDB
            // 
            this.labelSettings_LastDB.AutoSize = true;
            this.labelSettings_LastDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSettings_LastDB.Location = new System.Drawing.Point(20, 113);
            this.labelSettings_LastDB.Name = "labelSettings_LastDB";
            this.labelSettings_LastDB.Size = new System.Drawing.Size(78, 16);
            this.labelSettings_LastDB.TabIndex = 10;
            this.labelSettings_LastDB.Text = "0000000000";
            // 
            // buttonSettings_ConfigUpdate
            // 
            this.buttonSettings_ConfigUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSettings_ConfigUpdate.Location = new System.Drawing.Point(253, 113);
            this.buttonSettings_ConfigUpdate.Name = "buttonSettings_ConfigUpdate";
            this.buttonSettings_ConfigUpdate.Size = new System.Drawing.Size(154, 23);
            this.buttonSettings_ConfigUpdate.TabIndex = 9;
            this.buttonSettings_ConfigUpdate.Text = "Update Config";
            this.buttonSettings_ConfigUpdate.UseVisualStyleBackColor = true;
            this.buttonSettings_ConfigUpdate.Click += new System.EventHandler(this.buttonSettings_ConfigUpdate_Click);
            // 
            // textBoxSettings_Config
            // 
            this.textBoxSettings_Config.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxSettings_Config.Location = new System.Drawing.Point(253, 87);
            this.textBoxSettings_Config.Name = "textBoxSettings_Config";
            this.textBoxSettings_Config.Size = new System.Drawing.Size(154, 22);
            this.textBoxSettings_Config.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(250, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 16);
            this.label9.TabIndex = 7;
            this.label9.Text = "Config :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Last in Database :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(400, 32);
            this.label4.TabIndex = 1;
            this.label4.Text = "Generated Barcodes takes the highest next number of the last one \r\nin the barcode" +
    "s database or config file. ";
            // 
            // labelGeneral_Status
            // 
            this.labelGeneral_Status.AutoSize = true;
            this.labelGeneral_Status.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelGeneral_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGeneral_Status.Location = new System.Drawing.Point(0, 519);
            this.labelGeneral_Status.Name = "labelGeneral_Status";
            this.labelGeneral_Status.Size = new System.Drawing.Size(46, 13);
            this.labelGeneral_Status.TabIndex = 1;
            this.labelGeneral_Status.Text = "Status 1";
            // 
            // barcodesTableAdapter
            // 
            this.barcodesTableAdapter.ClearBeforeFill = true;
            // 
            // labelGeneral_NegStatus
            // 
            this.labelGeneral_NegStatus.AutoEllipsis = true;
            this.labelGeneral_NegStatus.AutoSize = true;
            this.labelGeneral_NegStatus.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelGeneral_NegStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGeneral_NegStatus.Location = new System.Drawing.Point(0, 532);
            this.labelGeneral_NegStatus.Name = "labelGeneral_NegStatus";
            this.labelGeneral_NegStatus.Size = new System.Drawing.Size(69, 13);
            this.labelGeneral_NegStatus.TabIndex = 2;
            this.labelGeneral_NegStatus.Text = "Neg Status 1\r\n";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "Application is running in the background";
            this.notifyIcon1.BalloonTipTitle = "S4 Barcode Generator";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "S4 Barcode Generator";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.buttonZPL_BarCreate);
            this.groupBox10.Controls.Add(this.textBoxZPL_BarText);
            this.groupBox10.Controls.Add(this.label14);
            this.groupBox10.Controls.Add(this.numericUpDownZPL_BarY);
            this.groupBox10.Controls.Add(this.label16);
            this.groupBox10.Controls.Add(this.numericUpDownZPL_BarX);
            this.groupBox10.Location = new System.Drawing.Point(367, 123);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(174, 100);
            this.groupBox10.TabIndex = 11;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Create Line Barcode";
            // 
            // buttonZPL_BarCreate
            // 
            this.buttonZPL_BarCreate.Location = new System.Drawing.Point(6, 70);
            this.buttonZPL_BarCreate.Name = "buttonZPL_BarCreate";
            this.buttonZPL_BarCreate.Size = new System.Drawing.Size(162, 22);
            this.buttonZPL_BarCreate.TabIndex = 64;
            this.buttonZPL_BarCreate.Text = "Create Barcode";
            this.buttonZPL_BarCreate.UseVisualStyleBackColor = true;
            this.buttonZPL_BarCreate.Click += new System.EventHandler(this.buttonZPL_BarCreate_Click);
            // 
            // textBoxZPL_BarText
            // 
            this.textBoxZPL_BarText.Location = new System.Drawing.Point(6, 18);
            this.textBoxZPL_BarText.Name = "textBoxZPL_BarText";
            this.textBoxZPL_BarText.Size = new System.Drawing.Size(162, 20);
            this.textBoxZPL_BarText.TabIndex = 63;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(93, 46);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 13);
            this.label14.TabIndex = 62;
            this.label14.Text = "Y :";
            // 
            // numericUpDownZPL_BarY
            // 
            this.numericUpDownZPL_BarY.Location = new System.Drawing.Point(116, 44);
            this.numericUpDownZPL_BarY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_BarY.Name = "numericUpDownZPL_BarY";
            this.numericUpDownZPL_BarY.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_BarY.TabIndex = 61;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 46);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 13);
            this.label16.TabIndex = 60;
            this.label16.Text = "X :";
            // 
            // numericUpDownZPL_BarX
            // 
            this.numericUpDownZPL_BarX.Location = new System.Drawing.Point(29, 44);
            this.numericUpDownZPL_BarX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownZPL_BarX.Name = "numericUpDownZPL_BarX";
            this.numericUpDownZPL_BarX.Size = new System.Drawing.Size(52, 20);
            this.numericUpDownZPL_BarX.TabIndex = 59;
            // 
            // Barcode_Generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 545);
            this.Controls.Add(this.labelGeneral_Status);
            this.Controls.Add(this.labelGeneral_NegStatus);
            this.Controls.Add(this.tabControlGenerator);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Barcode_Generator";
            this.Text = "S4 Barcode Generator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Barcode_Generator_FormClosing);
            this.Load += new System.EventHandler(this.Barcode_Generator_Load);
            this.Resize += new System.EventHandler(this.Barcode_Generator_Resize);
            this.tabControlGenerator.ResumeLayout(false);
            this.tabPageGenerator.ResumeLayout(false);
            this.groupBoxGenerate.ResumeLayout(false);
            this.groupBoxGenerate.PerformLayout();
            this.groupBoxGenerate_Generate.ResumeLayout(false);
            this.groupBoxGenerate_Generate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownGenerate_Total)).EndInit();
            this.tabPagePrint.ResumeLayout(false);
            this.groupBoxPrint.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrint_CopiesBarcodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPrint_NumberBarcodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barcodesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.databaseBarcodesDataSet)).EndInit();
            this.Server.ResumeLayout(false);
            this.groupBoxServer.ResumeLayout(false);
            this.groupBoxServer_Notes.ResumeLayout(false);
            this.groupBoxServer_Notes.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.Log.ResumeLayout(false);
            this.groupBoxLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLog)).EndInit();
            this.tabPageZPLCode.ResumeLayout(false);
            this.groupBoxZPLCode_BuildingBlocks.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_QRY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_QRX)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_GraphicY)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_FontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_TextY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_TextX)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBoxZPL.ResumeLayout(false);
            this.groupBoxZPL.PerformLayout();
            this.tabPageSettings.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBoxMini.ResumeLayout(false);
            this.groupBoxMini.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_BarY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZPL_BarX)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlGenerator;
        private System.Windows.Forms.TabPage tabPageGenerator;
        private System.Windows.Forms.TabPage tabPagePrint;
        private System.Windows.Forms.TabPage Server;
        private System.Windows.Forms.TabPage Log;
        private System.Windows.Forms.GroupBox groupBoxGenerate;
        private System.Windows.Forms.GroupBox groupBoxPrint;
        private System.Windows.Forms.GroupBox groupBoxServer;
        private System.Windows.Forms.GroupBox groupBoxLog;
        private System.Windows.Forms.GroupBox groupBoxGenerate_Generate;
        private System.Windows.Forms.NumericUpDown numericUpDownGenerate_Total;
        private System.Windows.Forms.Button buttonGenerate_GenerateBarcodes;
        private System.Windows.Forms.Label labelGenerate_Total;
        private System.Windows.Forms.Label labelGeneral_Status;
        private System.Windows.Forms.CheckBox checkBoxGenerate_Print;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonPrint_Print;
        private System.Windows.Forms.Label labelPrint_Barcodes;
        private System.Windows.Forms.ComboBox comboBoxPrint_Barcodes;
        private System.Windows.Forms.Label labelPrint_Copies;
        private System.Windows.Forms.Label labelPrint_Number;
        private System.Windows.Forms.NumericUpDown numericUpDownPrint_CopiesBarcodes;
        private System.Windows.Forms.NumericUpDown numericUpDownPrint_NumberBarcodes;
        private System.Windows.Forms.Button buttonPrint_ChangePrinter;
        private System.Windows.Forms.TabPage tabPageZPLCode;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonServer_Stop;
        private System.Windows.Forms.Button buttonServer_Start;
        private System.Windows.Forms.Label labelServer_PortNumber;
        private System.Windows.Forms.Label labelServer_ReplyNumber;
        private System.Windows.Forms.TextBox textBoxServer_ReplyNumber;
        private System.Windows.Forms.TextBox textBoxServer_PortNumber;
        private System.Windows.Forms.RichTextBox richTextBoxLog_Logger;
        private System.Windows.Forms.GroupBox groupBoxZPL;
        private System.Windows.Forms.Button buttonZPL_Update;
        private System.Windows.Forms.RichTextBox richTextBoxZPL_ZPLCode;
        private DatabaseBarcodesDataSet databaseBarcodesDataSet;
        private System.Windows.Forms.BindingSource barcodesBindingSource;
        private DatabaseBarcodesDataSetTableAdapters.BarcodesTableAdapter barcodesTableAdapter;
        private System.Windows.Forms.Label labelGeneral_NegStatus;
        private System.Windows.Forms.GroupBox groupBoxZPLCode_BuildingBlocks;
        private System.Windows.Forms.GroupBox groupBoxServer_Notes;
        private System.Windows.Forms.Label labelServer_Notes;
        private System.Windows.Forms.DataGridView dataGridViewLog;
        private System.Windows.Forms.Button buttonLog_Switch;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn copiesPrintedDataGridViewTextBoxColumn;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button buttonZPL_CreateQRBarcode;
        private System.Windows.Forms.TextBox textBoxZPL_QRText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_QRY;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_QRX;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button buttonZPL_CreateGraphicBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_GraphicH;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_GraphicY;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button buttonZPL_InsetText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_TextY;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_TextX;
        private System.Windows.Forms.TextBox textBoxZPL_Text;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonZPL_Finish;
        private System.Windows.Forms.Button buttonZPL_Start;
        private System.Windows.Forms.Label labelNotes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxZPL_Templates;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_GraphicT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_GraphicW;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_GraphicX;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_FontSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TabPage tabPageSettings;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBoxMini;
        private System.Windows.Forms.CheckBox checkBoxMini;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label labelSettings_LastDB;
        private System.Windows.Forms.Button buttonSettings_ConfigUpdate;
        private System.Windows.Forms.TextBox textBoxSettings_Config;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button buttonZPL_BarCreate;
        private System.Windows.Forms.TextBox textBoxZPL_BarText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_BarY;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericUpDownZPL_BarX;
    }
}

