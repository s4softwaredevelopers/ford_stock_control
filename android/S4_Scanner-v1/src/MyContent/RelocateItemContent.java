package MyContent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.widget.ArrayAdapter;

public class RelocateItemContent {

	public static List<RelocateItem> ITEMS = new ArrayList<RelocateItem>();
	
	public static List<RelocateItem> ITEMS_ALL = new ArrayList<RelocateItem>();

	public static Map<String, RelocateItem> ITEM_MAP = new HashMap<String, RelocateItem>();
	
	public static List<String> ORDERS = new ArrayList<String>();
	
	public static ArrayAdapter<RelocateItem> adapter;
	
	public static ArrayAdapter<String> adapterOrders;

	public static void addItem(RelocateItem item) {
		ITEMS_ALL.add(item);
		ITEM_MAP.put(item.id, item);
		
		if (!(ORDERS.contains(item.orderNumber))){
			ORDERS.add(item.orderNumber);
		}	
	}
	
	public static void OrderNumbersSort(){
        Collections.sort(ORDERS);
        ORDERS.add(0,"All Orders");
        if (ORDERS.contains(""))
        {
        	ORDERS.remove("");
        	ORDERS.add("No Order Number");
        }
	}
	
	public static void ReOrder(String order) {
		
		ITEMS.clear();		
		
		if (order.equals("No Order Number")){
			order = "";
		}
		
		if (order.equals("")){
			order = "All Orders";
		}
		
		for (RelocateItem item : ITEMS_ALL)
		{
			if (order.equals("All Orders")){
				ITEMS.add(item);
			}
			else{
				if (item.orderNumber.equals(order))
				{
					ITEMS.add(item);
				}
			}	
		}		
	}
	
	public static void BarcodePopulate(String html){
	    	
	    	ITEMS_ALL.clear();
	    	ITEM_MAP.clear();
	    	ORDERS.clear();	    	
	    	
	    	Document doc = Jsoup.parse(html);
	    	Elements tableElements = doc.select("table");
	    	Elements tableRowElements = tableElements.select(":not(thead) tr");
	        for (int i = 0; i < tableRowElements.size(); i++) {
	            Element row = tableRowElements.get(i);
	            List<String> ColList = new ArrayList<String>();
	            Elements rowItems = row.select("td");
	            for (int j = 0; j < rowItems.size(); j++) {
	            	ColList.add(rowItems.get(j).text());
	            }
	            addItem( new RelocateItem(String.valueOf(i+1), ColList.get(1), ColList.get(2), ColList.get(3), ColList.get(4), ColList.get(5), ColList.get(6), ColList.get(7), ColList.get(8), ColList.get(9)));
	        }
	}

	public static class RelocateItem {
		public String id;
		public String itemId;
		public String orderNumber;
		public String supplierCode;
		public String projectName;
		public String description;
		public String barcode;
		public String database;
		public String previousShelfBarcode;
		public String previousShelfDescription;
		public Boolean itemSelected;
		

		public RelocateItem(String id, String itemId, String orderNumber, String supplierCode, String description, String projectName, String barcode, String database, String previousShelfBarcode, String previousShelfDescription) {
			this.id = id;
			this.itemId = itemId;
			this.orderNumber = orderNumber;
			this.supplierCode = supplierCode;
			this.projectName = projectName;
			this.description = description;
			this.barcode = barcode;
			this.database = database;
			this.previousShelfBarcode = previousShelfBarcode;
			this.previousShelfDescription = previousShelfDescription;
			this.itemSelected = false;
		}

		// Text display in List
		@Override
		public String toString() {
			return description;
		}
	}
}