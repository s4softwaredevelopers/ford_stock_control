<h2>Shelves</h2>

<?php
if (isset($_POST['addshelf'])) {
    $code = trim($_POST['shelf_code']);
    $description = trim($_POST['shelf_description']);
    $storeroom_id = $_POST['storeroom_id'];
    
    $q = 'INSERT INTO shelf (code, description, storeroom_id) VALUES (:code, :description, :storeroom_id)';
    $params = array(':code' => $code, ':description' => $description, ':storeroom_id' => $storeroom_id);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Shelf ' . $code . ' added.</p>';
        
        $q = 'SELECT storeroom.description FROM storeroom WHERE id=:storeroom_id';
        $params = array(':storeroom_id' => $storeroom_id);
        
        $result = null;
        $rows = sql_select($result, $q, $params);
        
        if ($rows == 1) {
            foreach ($result as $row) {
                $storeroom_description = $row['description'];
            }
        }
        
        logger('add', 'Shelf code :: ' . $code . ' | Shelf :: ' . $description . ' | Storeroom :: ' . $storeroom_description);
    }
    else {
        echo '<p class="error">Shelf not added.</p>';
    }
}
elseif (isset($_POST['updateshelf'])) {
    $id = $_POST['updateshelf'];
    $code = trim($_POST['shelf_code']);
    $description = trim($_POST['shelf_description']);
    $storeroom_id = $_POST['storeroom_id'];
    
    $q = 'UPDATE shelf SET code=:code, description=:description, storeroom_id=:storeroom_id WHERE id=:id';
    $params = array(':id' => $id, ':code' => $code, ':description' => $description, ':storeroom_id' => $storeroom_id);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>Shelf ' . $code . ' updated.</p>';
        
        $q = 'SELECT storeroom.description FROM storeroom WHERE id=:storeroom_id';
        $params = array(':storeroom_id' => $storeroom_id);
        
        $result = null;
        $rows = sql_select($result, $q, $params);
        
        if ($rows == 1) {
            foreach ($result as $row) {
                $storeroom_description = $row['description'];
            }
        }
        
        logger('update', 'Shelf code :: ' . $code . ' | Shelf :: ' . $description . ' | Storeroom :: ' . $storeroom_description);
    }
    else {
        echo '<p class="error">Shelf not updated.</p>';
    }
}
elseif (isset($_POST['removeshelf'])) {
    $id = $_POST['removeshelf'];
    $code = $_POST['shelf_code'];
    $description = $_POST['shelf_description'];
    $storeroom_id = $_POST['storeroom_id'];
    
    $q = 'DELETE FROM shelf WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>Shelf ' . $code . ' removed.</p>';
        
        $q = 'SELECT storeroom.description FROM storeroom WHERE id=:storeroom_id';
        $params = array(':storeroom_id' => $storeroom_id);
        
        $result = null;
        $rows = sql_select($result, $q, $params);
        
        if ($rows == 1) {
            foreach ($result as $row) {
                $storeroom_description = $row['description'];
            }
        }
        
        logger('remove', 'Shelf code :: ' . $code . ' | Shelf :: ' . $description . ' | Storeroom :: ' . $storeroom_description);
    }
    else {
        echo '<p class="error">Shelf not removed.</p>';
    }
}

$q = 'SELECT id, description FROM storeroom ORDER BY description';
$storeroom_array = build_result_array($q); ?>

<div class='auxwrapper'>
    <?php if (dependencies_exist(array('storeroom'))) {
        echo '<input type="button" value="Add">';
    }
    else {
        echo '<span class="error">Shelves dependency missing.</span>';
    } ?>
    
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addshelfform' action='index.php?pid=shelves' method='post'>
            <tr>
                <th>Shelf Code</th>
                <td>
                    <input id='focus' type='text' name='shelf_code' value=''>
                </td>
            </tr>
            <tr>
                <th>Description</th>
                <td>
                    <input type='text' name='shelf_description' value=''>
                </td>
            </tr>
            <tr>
                <th>Storeroom</th>
                <td>
                    <?php build_select_options('storeroom_id', $storeroom_array, array('class' => 'chzn-select')) ?>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addshelf' value='addshelf'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = 'SELECT shelf.id, shelf.code, shelf.description AS shelf_description, shelf.storeroom_id, 
    storeroom.description AS storeroom_description FROM shelf INNER JOIN storeroom ON storeroom_id=storeroom.id ORDER BY shelf.id';

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='view first w140'>Shelf Code</th>
                <th class='edit' filter='false'></th>
                <th class='view w140'>Description</th>
                <th class='edit' filter='false'></th>
                <th class='view last w140'>Storeroom</th>
                <th class='edit' filter='false'></th>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $code = $row['code'];
            $shelf_description = $row['shelf_description'];
            $storeroom_id = $row['storeroom_id'];
            $storeroom_description = $row['storeroom_description']; ?>
            <tr>
                <form name='updateshelfform<?php echo $id ?>' action='index.php?pid=shelves' method='post'>
                    <td class='view w140'>
                        <div><?php echo $code; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='shelf_code' value='<?php echo $code; ?>'>
                    </td>
                    <td class='view w140'>
                        <div><?php echo $shelf_description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='shelf_description' value='<?php echo $shelf_description; ?>'>
                    </td>
                    <td class='view w140'>
                        <div><?php echo $storeroom_description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <?php build_select_options('storeroom_id', $storeroom_array, array('selected' => $storeroom_id, 'class' => 'chzn-select')); ?>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updateshelf' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form name='removeshelfform<?php echo $id ?>' action='index.php?pid=shelves' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removeshelf' value='<?php echo $id ?>'>
                        <input type='hidden' name='shelf_code' value='<?php echo $code; ?>'>
                        <input type='hidden' name='shelf_description' value='<?php echo $shelf_description; ?>'>
                        <input type='hidden' name='storeroom_id' value='<?php echo $storeroom_id; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No shelves in database.</p>';
} ?>