<?php
if (isset($_SESSION['ua'])) {
    $ua = $_SESSION['ua'];
    $pid = 'inventory_tracking';
}
else {
    $ua = 0;
    $pid = 'login';
}

if (isset($_GET['pid'])) {
    $pid = $_GET['pid'];
}

if (filter($pid, $ua)) {
    switch ($pid) {
        case 'authenticate':
            include 'includes/authenticate.php';
            break;
        case 'login':
            include 'includes/login.php';
            break;
        case 'login_fail':
            include 'includes/login.php';
            include 'includes/noLogin.php';
            break;
        
        case 'inventory_tracking':
            include 'includes/inventoryTracking.php';
            break;
        
        case 'parts':
            include 'includes/parts.php';
            break;
        case 'products':
            include 'includes/products.php';
            break;
        case 'groups':
            include 'includes/groups.php';
            break;
        
        case 'storerooms':
            include 'includes/storerooms.php';
            break;
        case 'shelves':
            include 'includes/shelves.php';
            break;
        
        case 'companies':
            include 'includes/companies.php';
            break;
        case 'projects':
            include 'includes/projects.php';
            break;
        
        case 'users':
            include 'includes/users.php';
            break;
        
        case 'logs':
            include 'includes/logs.php';
            break;
    }
}
else {
    include 'includes/noAccess.php';
}
?>