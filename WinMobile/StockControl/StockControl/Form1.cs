using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Stockroom
{
    public partial class StockControl : Form
    {
        public StockControl()
        {
            InitializeComponent();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonNavigate_Click(object sender, EventArgs e)
        {
            Navigate(textBoxURL.Text);
            webBrowserStockControl.Visible = true;
            panelSettings.Visible = false;
        }

        private void Navigate(String address)
        {
            if (String.IsNullOrEmpty(address)) return;
            if (address.Equals("about:blank")) return;
            if (!address.StartsWith("http://") &&
                !address.StartsWith("https://"))
            {
                address = "http://" + address;
            }
            try
            {
                webBrowserStockControl.Navigate(new Uri(address));
            }
            catch (System.UriFormatException)
            {
                return;
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            if (panelSettings.Visible)
            {
                webBrowserStockControl.Visible = true;
                panelSettings.Visible = false;
            }
            else
            {
                webBrowserStockControl.Visible = false;
                panelSettings.Visible = true;
            }
        }
    }
}