namespace s4_Scanner
{
    partial class Relocation_Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelSource = new System.Windows.Forms.Panel();
            this.labelSource = new System.Windows.Forms.Label();
            this.buttonS_Pending = new System.Windows.Forms.Button();
            this.buttonS_User = new System.Windows.Forms.Button();
            this.buttonS_Shelf = new System.Windows.Forms.Button();
            this.panelDestination = new System.Windows.Forms.Panel();
            this.buttonChangeSource = new System.Windows.Forms.Button();
            this.labelDestination = new System.Windows.Forms.Label();
            this.buttonD_User = new System.Windows.Forms.Button();
            this.buttonD_Shelf = new System.Windows.Forms.Button();
            this.panelHeader.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.panelSource.SuspendLayout();
            this.panelDestination.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.buttonClear);
            this.panelHeader.Controls.Add(this.buttonClose);
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClear.Location = new System.Drawing.Point(161, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(30, 20);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "R";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(211, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(30, 20);
            this.buttonClose.TabIndex = 10;
            this.buttonClose.Text = " x";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Relocation";
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label2);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 300);
            this.panelLoader.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(29, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 107);
            this.label2.Text = "Loading";
            // 
            // panelSource
            // 
            this.panelSource.Controls.Add(this.labelSource);
            this.panelSource.Controls.Add(this.buttonS_Pending);
            this.panelSource.Controls.Add(this.buttonS_User);
            this.panelSource.Controls.Add(this.buttonS_Shelf);
            this.panelSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSource.Location = new System.Drawing.Point(0, 20);
            this.panelSource.Name = "panelSource";
            this.panelSource.Size = new System.Drawing.Size(240, 300);
            // 
            // labelSource
            // 
            this.labelSource.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSource.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelSource.Location = new System.Drawing.Point(15, 25);
            this.labelSource.Name = "labelSource";
            this.labelSource.Size = new System.Drawing.Size(210, 50);
            this.labelSource.Text = "Source:\r\nWhere does the items come from?";
            this.labelSource.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonS_Pending
            // 
            this.buttonS_Pending.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonS_Pending.Location = new System.Drawing.Point(15, 230);
            this.buttonS_Pending.Name = "buttonS_Pending";
            this.buttonS_Pending.Size = new System.Drawing.Size(210, 50);
            this.buttonS_Pending.TabIndex = 9;
            this.buttonS_Pending.Text = "Pending";
            this.buttonS_Pending.Click += new System.EventHandler(this.buttonS_Pending_Click);
            // 
            // buttonS_User
            // 
            this.buttonS_User.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonS_User.Location = new System.Drawing.Point(15, 160);
            this.buttonS_User.Name = "buttonS_User";
            this.buttonS_User.Size = new System.Drawing.Size(210, 50);
            this.buttonS_User.TabIndex = 8;
            this.buttonS_User.Text = "User";
            this.buttonS_User.Click += new System.EventHandler(this.buttonS_User_Click);
            // 
            // buttonS_Shelf
            // 
            this.buttonS_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonS_Shelf.Location = new System.Drawing.Point(15, 90);
            this.buttonS_Shelf.Name = "buttonS_Shelf";
            this.buttonS_Shelf.Size = new System.Drawing.Size(210, 50);
            this.buttonS_Shelf.TabIndex = 7;
            this.buttonS_Shelf.Text = "Shelf";
            this.buttonS_Shelf.Click += new System.EventHandler(this.buttonS_Shelf_Click);
            // 
            // panelDestination
            // 
            this.panelDestination.Controls.Add(this.buttonChangeSource);
            this.panelDestination.Controls.Add(this.labelDestination);
            this.panelDestination.Controls.Add(this.buttonD_User);
            this.panelDestination.Controls.Add(this.buttonD_Shelf);
            this.panelDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDestination.Location = new System.Drawing.Point(0, 20);
            this.panelDestination.Name = "panelDestination";
            this.panelDestination.Size = new System.Drawing.Size(240, 300);
            // 
            // buttonChangeSource
            // 
            this.buttonChangeSource.BackColor = System.Drawing.Color.LightGray;
            this.buttonChangeSource.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonChangeSource.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonChangeSource.Location = new System.Drawing.Point(0, 273);
            this.buttonChangeSource.Name = "buttonChangeSource";
            this.buttonChangeSource.Size = new System.Drawing.Size(240, 27);
            this.buttonChangeSource.TabIndex = 11;
            this.buttonChangeSource.Text = "Change Source";
            this.buttonChangeSource.Click += new System.EventHandler(this.buttonChangeSource_Click);
            // 
            // labelDestination
            // 
            this.labelDestination.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelDestination.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelDestination.Location = new System.Drawing.Point(15, 25);
            this.labelDestination.Name = "labelDestination";
            this.labelDestination.Size = new System.Drawing.Size(210, 50);
            this.labelDestination.Text = "Destination:\r\nTo where must the items go?";
            this.labelDestination.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonD_User
            // 
            this.buttonD_User.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonD_User.Location = new System.Drawing.Point(15, 160);
            this.buttonD_User.Name = "buttonD_User";
            this.buttonD_User.Size = new System.Drawing.Size(210, 50);
            this.buttonD_User.TabIndex = 10;
            this.buttonD_User.Text = "User";
            this.buttonD_User.Click += new System.EventHandler(this.buttonD_User_Click);
            // 
            // buttonD_Shelf
            // 
            this.buttonD_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            this.buttonD_Shelf.Location = new System.Drawing.Point(15, 90);
            this.buttonD_Shelf.Name = "buttonD_Shelf";
            this.buttonD_Shelf.Size = new System.Drawing.Size(210, 50);
            this.buttonD_Shelf.TabIndex = 9;
            this.buttonD_Shelf.Text = "Shelf";
            this.buttonD_Shelf.Click += new System.EventHandler(this.buttonD_Shelf_Click);
            // 
            // Relocation_Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.panelSource);
            this.Controls.Add(this.panelDestination);
            this.Controls.Add(this.panelHeader);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Relocation_Menu";
            this.Text = "Relocation_Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelHeader.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.panelSource.ResumeLayout(false);
            this.panelDestination.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelSource;
        private System.Windows.Forms.Label labelSource;
        private System.Windows.Forms.Button buttonS_Pending;
        private System.Windows.Forms.Button buttonS_User;
        private System.Windows.Forms.Button buttonS_Shelf;
        private System.Windows.Forms.Panel panelDestination;
        private System.Windows.Forms.Button buttonChangeSource;
        private System.Windows.Forms.Label labelDestination;
        private System.Windows.Forms.Button buttonD_User;
        private System.Windows.Forms.Button buttonD_Shelf;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label2;
    }
}