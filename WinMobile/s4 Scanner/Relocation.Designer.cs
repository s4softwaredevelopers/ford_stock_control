namespace s4_Scanner
{
    partial class Relocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelTable = new System.Windows.Forms.Panel();
            this.buttonRelocate = new System.Windows.Forms.Button();
            this.buttonLater_Shelf = new System.Windows.Forms.Button();
            this.buttonItemsListClear = new System.Windows.Forms.Button();
            this.labelBarcode = new System.Windows.Forms.Label();
            this.textBoxBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxListBarcodes = new System.Windows.Forms.TextBox();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.labelMessage = new System.Windows.Forms.Label();
            this.panelUser = new System.Windows.Forms.Panel();
            this.panelUserDetails = new System.Windows.Forms.Panel();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.comboBoxUsers = new System.Windows.Forms.ComboBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.panelUserTitle = new System.Windows.Forms.Panel();
            this.labelUserTitle = new System.Windows.Forms.Label();
            this.panelUserLogin = new System.Windows.Forms.Panel();
            this.buttonUserClose = new System.Windows.Forms.Button();
            this.buttonUserLogin = new System.Windows.Forms.Button();
            this.panelUserRelocate = new System.Windows.Forms.Panel();
            this.buttonUserCancel = new System.Windows.Forms.Button();
            this.buttonGoUser = new System.Windows.Forms.Button();
            this.panelShelf = new System.Windows.Forms.Panel();
            this.buttonShelfCancel = new System.Windows.Forms.Button();
            this.textBoxShelf = new System.Windows.Forms.TextBox();
            this.Shelf = new System.Windows.Forms.Label();
            this.buttonRelocate_Shelf = new System.Windows.Forms.Button();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelHeader.SuspendLayout();
            this.panelTable.SuspendLayout();
            this.panelFooter.SuspendLayout();
            this.panelUser.SuspendLayout();
            this.panelUserDetails.SuspendLayout();
            this.panelUserTitle.SuspendLayout();
            this.panelUserLogin.SuspendLayout();
            this.panelUserRelocate.SuspendLayout();
            this.panelShelf.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.buttonClear);
            this.panelHeader.Controls.Add(this.buttonClose);
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClear.Location = new System.Drawing.Point(161, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(30, 20);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "R";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(211, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(30, 20);
            this.buttonClose.TabIndex = 10;
            this.buttonClose.Text = " x";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Relocation";
            // 
            // panelTable
            // 
            this.panelTable.Controls.Add(this.buttonRelocate);
            this.panelTable.Controls.Add(this.buttonLater_Shelf);
            this.panelTable.Controls.Add(this.buttonItemsListClear);
            this.panelTable.Controls.Add(this.labelBarcode);
            this.panelTable.Controls.Add(this.textBoxBarcode);
            this.panelTable.Controls.Add(this.label1);
            this.panelTable.Controls.Add(this.textBoxListBarcodes);
            this.panelTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTable.Location = new System.Drawing.Point(0, 20);
            this.panelTable.Name = "panelTable";
            this.panelTable.Size = new System.Drawing.Size(240, 280);
            // 
            // buttonRelocate
            // 
            this.buttonRelocate.BackColor = System.Drawing.Color.LightGray;
            this.buttonRelocate.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonRelocate.Location = new System.Drawing.Point(5, 244);
            this.buttonRelocate.Name = "buttonRelocate";
            this.buttonRelocate.Size = new System.Drawing.Size(161, 30);
            this.buttonRelocate.TabIndex = 25;
            this.buttonRelocate.Text = "Relocate";
            this.buttonRelocate.Click += new System.EventHandler(this.buttonRelocate_Click);
            // 
            // buttonLater_Shelf
            // 
            this.buttonLater_Shelf.BackColor = System.Drawing.Color.LightGray;
            this.buttonLater_Shelf.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonLater_Shelf.Location = new System.Drawing.Point(170, 244);
            this.buttonLater_Shelf.Name = "buttonLater_Shelf";
            this.buttonLater_Shelf.Size = new System.Drawing.Size(65, 30);
            this.buttonLater_Shelf.TabIndex = 21;
            this.buttonLater_Shelf.Text = "Postpone";
            this.buttonLater_Shelf.Click += new System.EventHandler(this.buttonLater_Shelf_Click);
            // 
            // buttonItemsListClear
            // 
            this.buttonItemsListClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonItemsListClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonItemsListClear.Location = new System.Drawing.Point(205, 59);
            this.buttonItemsListClear.Name = "buttonItemsListClear";
            this.buttonItemsListClear.Size = new System.Drawing.Size(30, 21);
            this.buttonItemsListClear.TabIndex = 12;
            this.buttonItemsListClear.Text = " x";
            this.buttonItemsListClear.Click += new System.EventHandler(this.buttonItemsListClear_Click);
            // 
            // labelBarcode
            // 
            this.labelBarcode.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelBarcode.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelBarcode.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelBarcode.Location = new System.Drawing.Point(5, 5);
            this.labelBarcode.Name = "labelBarcode";
            this.labelBarcode.Size = new System.Drawing.Size(230, 21);
            this.labelBarcode.Text = "ENTER ITEM BARCODE";
            // 
            // textBoxBarcode
            // 
            this.textBoxBarcode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.textBoxBarcode.Location = new System.Drawing.Point(5, 26);
            this.textBoxBarcode.Name = "textBoxBarcode";
            this.textBoxBarcode.Size = new System.Drawing.Size(230, 29);
            this.textBoxBarcode.TabIndex = 12;
            this.textBoxBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBarcode_KeyPress);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(5, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 20);
            this.label1.Text = "ITEMS TO RELOCATE";
            // 
            // textBoxListBarcodes
            // 
            this.textBoxListBarcodes.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.textBoxListBarcodes.Location = new System.Drawing.Point(5, 79);
            this.textBoxListBarcodes.Multiline = true;
            this.textBoxListBarcodes.Name = "textBoxListBarcodes";
            this.textBoxListBarcodes.ReadOnly = true;
            this.textBoxListBarcodes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxListBarcodes.Size = new System.Drawing.Size(230, 159);
            this.textBoxListBarcodes.TabIndex = 6;
            // 
            // panelFooter
            // 
            this.panelFooter.Controls.Add(this.labelMessage);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 300);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(240, 20);
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(5, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(230, 20);
            this.labelMessage.Text = "Message";
            // 
            // panelUser
            // 
            this.panelUser.Controls.Add(this.panelUserDetails);
            this.panelUser.Controls.Add(this.panelUserTitle);
            this.panelUser.Controls.Add(this.panelUserLogin);
            this.panelUser.Controls.Add(this.panelUserRelocate);
            this.panelUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUser.Location = new System.Drawing.Point(0, 20);
            this.panelUser.Name = "panelUser";
            this.panelUser.Size = new System.Drawing.Size(240, 280);
            this.panelUser.Visible = false;
            // 
            // panelUserDetails
            // 
            this.panelUserDetails.Controls.Add(this.textBoxDescription);
            this.panelUserDetails.Controls.Add(this.labelDescription);
            this.panelUserDetails.Controls.Add(this.textBoxPassword);
            this.panelUserDetails.Controls.Add(this.comboBoxUsers);
            this.panelUserDetails.Controls.Add(this.labelPassword);
            this.panelUserDetails.Controls.Add(this.labelUser);
            this.panelUserDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserDetails.Location = new System.Drawing.Point(0, 22);
            this.panelUserDetails.Name = "panelUserDetails";
            this.panelUserDetails.Size = new System.Drawing.Size(240, 138);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxDescription.Location = new System.Drawing.Point(5, 129);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(230, 66);
            this.textBoxDescription.TabIndex = 131;
            // 
            // labelDescription
            // 
            this.labelDescription.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelDescription.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescription.Location = new System.Drawing.Point(5, 108);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(230, 21);
            this.labelDescription.Text = "Reason for Relocating Items:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxPassword.Location = new System.Drawing.Point(5, 77);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(230, 26);
            this.textBoxPassword.TabIndex = 130;
            // 
            // comboBoxUsers
            // 
            this.comboBoxUsers.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.comboBoxUsers.Location = new System.Drawing.Point(5, 24);
            this.comboBoxUsers.Name = "comboBoxUsers";
            this.comboBoxUsers.Size = new System.Drawing.Size(230, 27);
            this.comboBoxUsers.TabIndex = 129;
            // 
            // labelPassword
            // 
            this.labelPassword.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelPassword.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelPassword.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelPassword.Location = new System.Drawing.Point(5, 56);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(230, 21);
            this.labelPassword.Text = "Please enter your Password:";
            // 
            // labelUser
            // 
            this.labelUser.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelUser.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelUser.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelUser.Location = new System.Drawing.Point(5, 4);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(230, 20);
            this.labelUser.Text = "Please select Username:";
            // 
            // panelUserTitle
            // 
            this.panelUserTitle.Controls.Add(this.labelUserTitle);
            this.panelUserTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUserTitle.Location = new System.Drawing.Point(0, 0);
            this.panelUserTitle.Name = "panelUserTitle";
            this.panelUserTitle.Size = new System.Drawing.Size(240, 22);
            // 
            // labelUserTitle
            // 
            this.labelUserTitle.BackColor = System.Drawing.SystemColors.MenuText;
            this.labelUserTitle.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelUserTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelUserTitle.Location = new System.Drawing.Point(5, 2);
            this.labelUserTitle.Name = "labelUserTitle";
            this.labelUserTitle.Size = new System.Drawing.Size(230, 18);
            this.labelUserTitle.Text = "Enter User Details";
            this.labelUserTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelUserLogin
            // 
            this.panelUserLogin.Controls.Add(this.buttonUserClose);
            this.panelUserLogin.Controls.Add(this.buttonUserLogin);
            this.panelUserLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelUserLogin.Location = new System.Drawing.Point(0, 160);
            this.panelUserLogin.Name = "panelUserLogin";
            this.panelUserLogin.Size = new System.Drawing.Size(240, 60);
            // 
            // buttonUserClose
            // 
            this.buttonUserClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonUserClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUserClose.Location = new System.Drawing.Point(5, 36);
            this.buttonUserClose.Name = "buttonUserClose";
            this.buttonUserClose.Size = new System.Drawing.Size(230, 20);
            this.buttonUserClose.TabIndex = 133;
            this.buttonUserClose.Text = "Cancel";
            this.buttonUserClose.Click += new System.EventHandler(this.buttonUserClose_Click);
            // 
            // buttonUserLogin
            // 
            this.buttonUserLogin.BackColor = System.Drawing.Color.LightGray;
            this.buttonUserLogin.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUserLogin.Location = new System.Drawing.Point(5, 3);
            this.buttonUserLogin.Name = "buttonUserLogin";
            this.buttonUserLogin.Size = new System.Drawing.Size(230, 30);
            this.buttonUserLogin.TabIndex = 132;
            this.buttonUserLogin.Text = "Login";
            this.buttonUserLogin.Click += new System.EventHandler(this.buttonUserLogin_Click);
            // 
            // panelUserRelocate
            // 
            this.panelUserRelocate.Controls.Add(this.buttonUserCancel);
            this.panelUserRelocate.Controls.Add(this.buttonGoUser);
            this.panelUserRelocate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelUserRelocate.Location = new System.Drawing.Point(0, 220);
            this.panelUserRelocate.Name = "panelUserRelocate";
            this.panelUserRelocate.Size = new System.Drawing.Size(240, 60);
            // 
            // buttonUserCancel
            // 
            this.buttonUserCancel.BackColor = System.Drawing.Color.LightGray;
            this.buttonUserCancel.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUserCancel.Location = new System.Drawing.Point(5, 36);
            this.buttonUserCancel.Name = "buttonUserCancel";
            this.buttonUserCancel.Size = new System.Drawing.Size(230, 20);
            this.buttonUserCancel.TabIndex = 133;
            this.buttonUserCancel.Text = "Cancel";
            this.buttonUserCancel.Click += new System.EventHandler(this.buttonUserCancel_Click);
            // 
            // buttonGoUser
            // 
            this.buttonGoUser.BackColor = System.Drawing.Color.LightGray;
            this.buttonGoUser.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonGoUser.Location = new System.Drawing.Point(5, 3);
            this.buttonGoUser.Name = "buttonGoUser";
            this.buttonGoUser.Size = new System.Drawing.Size(230, 30);
            this.buttonGoUser.TabIndex = 132;
            this.buttonGoUser.Text = "Relocate to User";
            this.buttonGoUser.Click += new System.EventHandler(this.buttonGoUser_Click);
            // 
            // panelShelf
            // 
            this.panelShelf.Controls.Add(this.buttonShelfCancel);
            this.panelShelf.Controls.Add(this.textBoxShelf);
            this.panelShelf.Controls.Add(this.Shelf);
            this.panelShelf.Controls.Add(this.buttonRelocate_Shelf);
            this.panelShelf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelShelf.Location = new System.Drawing.Point(0, 20);
            this.panelShelf.Name = "panelShelf";
            this.panelShelf.Size = new System.Drawing.Size(240, 280);
            this.panelShelf.Visible = false;
            // 
            // buttonShelfCancel
            // 
            this.buttonShelfCancel.BackColor = System.Drawing.Color.LightGray;
            this.buttonShelfCancel.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonShelfCancel.Location = new System.Drawing.Point(5, 260);
            this.buttonShelfCancel.Name = "buttonShelfCancel";
            this.buttonShelfCancel.Size = new System.Drawing.Size(230, 20);
            this.buttonShelfCancel.TabIndex = 16;
            this.buttonShelfCancel.Text = "Cancel";
            this.buttonShelfCancel.Click += new System.EventHandler(this.buttonShelfCancel_Click);
            // 
            // textBoxShelf
            // 
            this.textBoxShelf.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.textBoxShelf.Location = new System.Drawing.Point(5, 40);
            this.textBoxShelf.Name = "textBoxShelf";
            this.textBoxShelf.Size = new System.Drawing.Size(230, 29);
            this.textBoxShelf.TabIndex = 14;
            // 
            // Shelf
            // 
            this.Shelf.BackColor = System.Drawing.SystemColors.Highlight;
            this.Shelf.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.Shelf.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Shelf.Location = new System.Drawing.Point(5, 5);
            this.Shelf.Name = "Shelf";
            this.Shelf.Size = new System.Drawing.Size(230, 35);
            this.Shelf.Text = "ENTER SHELF BARCODE \r\nBARCODE ASSIGNED TO ALL ITEMS";
            // 
            // buttonRelocate_Shelf
            // 
            this.buttonRelocate_Shelf.BackColor = System.Drawing.Color.LightGray;
            this.buttonRelocate_Shelf.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonRelocate_Shelf.Location = new System.Drawing.Point(5, 100);
            this.buttonRelocate_Shelf.Name = "buttonRelocate_Shelf";
            this.buttonRelocate_Shelf.Size = new System.Drawing.Size(230, 30);
            this.buttonRelocate_Shelf.TabIndex = 1;
            this.buttonRelocate_Shelf.Text = " Assign Shelf to Items";
            this.buttonRelocate_Shelf.Click += new System.EventHandler(this.buttonRelocate_Shelf_Click);
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label2);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 280);
            this.panelLoader.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(29, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 107);
            this.label2.Text = "Loading";
            // 
            // Relocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.panelShelf);
            this.Controls.Add(this.panelUser);
            this.Controls.Add(this.panelTable);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Relocation";
            this.Text = "Relocation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Relocation_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Relocation_KeyDown);
            this.panelHeader.ResumeLayout(false);
            this.panelTable.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            this.panelUser.ResumeLayout(false);
            this.panelUserDetails.ResumeLayout(false);
            this.panelUserTitle.ResumeLayout(false);
            this.panelUserLogin.ResumeLayout(false);
            this.panelUserRelocate.ResumeLayout(false);
            this.panelShelf.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelTable;
        private System.Windows.Forms.Button buttonRelocate;
        private System.Windows.Forms.Button buttonLater_Shelf;
        private System.Windows.Forms.Button buttonItemsListClear;
        private System.Windows.Forms.Label labelBarcode;
        private System.Windows.Forms.TextBox textBoxBarcode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxListBarcodes;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Panel panelUser;
        private System.Windows.Forms.Panel panelShelf;
        private System.Windows.Forms.Button buttonShelfCancel;
        private System.Windows.Forms.TextBox textBoxShelf;
        private System.Windows.Forms.Label Shelf;
        private System.Windows.Forms.Button buttonRelocate_Shelf;
        private System.Windows.Forms.Panel panelUserDetails;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.ComboBox comboBoxUsers;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Panel panelUserTitle;
        private System.Windows.Forms.Panel panelUserRelocate;
        private System.Windows.Forms.Button buttonUserCancel;
        private System.Windows.Forms.Button buttonGoUser;
        private System.Windows.Forms.Label labelUserTitle;
        private System.Windows.Forms.Panel panelUserLogin;
        private System.Windows.Forms.Button buttonUserClose;
        private System.Windows.Forms.Button buttonUserLogin;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label2;
    }
}