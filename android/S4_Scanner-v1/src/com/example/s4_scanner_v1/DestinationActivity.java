package com.example.s4_scanner_v1;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import MyContent.AssignedItemContent;
import MyContent.MyWebRequest;
import MyContent.QRelocateContent;
import MyContent.RelocateItemContent;
import MyContent.UsersContent;
import MyContent.AssignedItemContent.AssignedItem;
import MyContent.RelocateItemContent.RelocateItem;
import MyContent.UsersContent.Users;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

public class DestinationActivity extends Activity {

	private ProgressBar spinner;
	private AutoCompleteTextView actv;
	private String source;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_destination);
		
		 this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
	      spinner = (ProgressBar)findViewById(R.id.progressDestination);
	      spinner.setVisibility(View.GONE);
	      
	      Intent intent = getIntent();
	      source = intent.getStringExtra("SOURCE");
	      
		new WebRequestUsersPopuate().execute("");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.destination, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	class WebRequestUsersPopuate extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=login&fid=user";
			String result = "";
			try{
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				UsersPopulate(result);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return "";
			} catch (Exception e) {
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("")) 
	    	{
	    		UserDropdownCreate();
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	    public void UsersPopulate(String html){
	    	
	    	UsersContent.USERS.clear();
	    	UsersContent.USER_MAP.clear();
	    	
	    	Document doc = Jsoup.parse(html);
	    	Elements tableElements = doc.select("table");
	    	Elements tableRowElements = tableElements.select(":not(thead) tr");
	        for (int i = 0; i < tableRowElements.size(); i++) {
	            Element row = tableRowElements.get(i);
	            List<String> ColList = new ArrayList<String>();
	            Elements rowItems = row.select("td");
	            for (int j = 0; j < rowItems.size(); j++) {
	            	ColList.add(rowItems.get(j).text());
	            }
	            UsersContent.addItem( new Users(ColList.get(0), ColList.get(1)));
	        }
	    }

	}
	
    public void UserDropdownCreate(){
    	
    	actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewDestination); 
		UsersContent.adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, UsersContent.USERS_N);		
		actv.setAdapter(UsersContent.adapter);	
	
    }
    
	public void ScanBarcode(View view) {
    	Intent intent = new Intent("com.google.zxing.client.android.SCAN");
    	intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
    	startActivityForResult(intent, 0);
	}
	 
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == 0) {	    
			if (resultCode == -1) {    	
		    	((EditText) findViewById(R.id.editTextDestinationShelf)).setText(intent.getStringExtra("SCAN_RESULT"));
			} else if (resultCode == 0) {
		        //etScanner.setText("Scan cancelled.");
			}
		}
	}
    
	public void DestinationShelf(View view) {
		new WebRequestRelocateShelfUpdate().execute("");
	}
    
	class WebRequestRelocateShelfUpdate extends AsyncTask<String, Void, String> {

		String shelf = "";
		EditText etShelf;
		
		@Override
		protected String doInBackground(String... params) {
			
			if (shelf.equals("")){ return "No Shelf Barcode";}
			
			String urlParameters = "?pid=setLocation&fid=update";
			String result = "";
			try{
				
				if (source.equals("QRelocate")){
					urlParameters += SendQuickParameters();
				}
				
				if (source.equals("Relocate")){
					urlParameters += SendParameters();
				}
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return "";
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("")) 
	    	{
	    		Toast.makeText(getBaseContext(),"Items Relocated",Toast.LENGTH_LONG).show();
	    		etShelf.setText("");
	    		if (source.equals("QRelocate")){
	    			QRelocateContent.ITEMS.clear();
	    			QRelocateContent.adapter.notifyDataSetChanged();	
	    			setResult(RESULT_OK, null);
	    		}
	    		if (source.equals("Relocate")){
	    			setResult(RESULT_OK, null);
	    		}
	    		finish();
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	etShelf = (EditText) findViewById(R.id.editTextDestinationShelf);
	    	shelf = etShelf.getText().toString();
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	    public String SendQuickParameters() throws UnsupportedEncodingException{
	    	String param = "";
	    	int k = 0;
	    	for (String rItem : QRelocateContent.ITEMS){
	    		if (rItem != ""){
	    			param += "&barcode" + k + "=" + rItem + "&shelf" + k + "=" + shelf;
	    			k++;
	    		}
	    	}	
	    	if (UsersContent.bReturnUser == true)
	    	{
	    		param += "&returnUser=" + UsersContent.returnUserid + "&returnDescription=" + URLEncoder.encode(UsersContent.returnDescription, "utf-8");
	    	}
	    	return param;
	    }
	    
	    public String SendParameters(){
	    	String param = "";
	    	for (Map.Entry<String, RelocateItem> rItem : RelocateItemContent.ITEM_MAP.entrySet()){
	    		if (rItem.getValue().itemSelected){
	    			param += "&barcode" + rItem.getValue().id + "=" + rItem.getValue().barcode + "&shelf" + rItem.getValue().id + "=" + shelf;
	    		}
	    	}	  
	    	return param;
	    }
	}
	
	public void DestinationUser(View view) {
		new WebRequestRelocateUserUpdate().execute("");
	}
    
	class WebRequestRelocateUserUpdate extends AsyncTask<String, Void, String> {

		EditText etPassword;
		EditText etDescription;
		String sPassword = ""; 
		String sDescription = ""; 
		String sUser = "";
		String sUserName = "";
		
		@Override
		protected String doInBackground(String... params) {
			
			if (sUser.equals("Invalid User")){ return "Invalid User";}
			if (sUser.equals("")){ return "No User Selected";}
			if (sPassword.equals("")){ return "No Password";}
			if (sDescription.equals("")){ return "No Reasons";}
			
			String urlParameters = "?pid=setLocation&fid=userUpdate";
			String result = "";
			try{
				
				if (source.equals("QRelocate")){
					urlParameters += SendQuickParameters();
				}
				
				if (source.equals("Relocate")){
					urlParameters += SendParameters();
				}
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return result;
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("done")) 
	    	{
	    		Toast.makeText(getBaseContext(),"Items Relocated",Toast.LENGTH_LONG).show();
	    		etPassword.setText("");
	    		if (source.equals("QRelocate")){
	    			QRelocateContent.ITEMS.clear();
	    			QRelocateContent.adapter.notifyDataSetChanged();	    			
	    		}
	    		if (source.equals("Relocate")){
	    			setResult(RESULT_OK, null);
	    		}
	    		finish();
	    	}
	    	else
	    	{
	    		etPassword.setText("");
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	sUserName = actv.getText().toString();
	    	try { sUser = UsersContent.USERS_ID.get(UsersContent.USERS_N.indexOf(sUserName)); }
	    	catch(Exception e) { sUser = "Invalid User"; }	    	
	    	etPassword = (EditText) findViewById(R.id.editTextDestinationPassword);
	    	sPassword = etPassword.getText().toString();
	    	etDescription = (EditText) findViewById(R.id.editTextDestinationDescription);
	    	sDescription = etDescription.getText().toString();
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	    public String SendQuickParameters() throws UnsupportedEncodingException{
	    	String param = "";
	    	int k = 0;
	    	for (String rItem : QRelocateContent.ITEMS){
	    		if (rItem != ""){
	    			param += "&barcode" + k + "=" + rItem;
	    			k++;
	    		}
	    	}
	    	param += "&username=" + URLEncoder.encode(sUserName, "utf-8") + "&userid=" + sUser + "&password=" + sPassword + "&description=" + URLEncoder.encode(sDescription, "utf-8");
	    	if (UsersContent.bReturnUser == true)
	    	{
	    		param += "&returnUser=" + UsersContent.returnUserid + "&returnDescription=" + URLEncoder.encode(UsersContent.returnDescription, "utf-8");
	    	}
	    	return param;
	    }
	    
	    public String SendParameters() throws UnsupportedEncodingException{
	    	String param = "";
	    	for (Map.Entry<String, RelocateItem> rItem : RelocateItemContent.ITEM_MAP.entrySet()){
	    		if (rItem.getValue().itemSelected){
	    			param += "&barcode" + rItem.getValue().id + "=" + rItem.getValue().barcode;;
	    		}
	    	}	
	    	param += "&username=" + URLEncoder.encode(sUserName, "utf-8") + "&userid=" + sUser + "&password=" + sPassword + "&description=" + URLEncoder.encode(sDescription, "utf-8");
	    	if (UsersContent.bReturnUser == true)
	    	{
	    		param += "&returnUser=" + UsersContent.returnUserid + "&returnDescription=" + URLEncoder.encode(UsersContent.returnDescription, "utf-8");
	    	}
	    	return param;
	    }

	}
}
