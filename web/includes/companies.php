<h2>Companies</h2>

<?php
if (isset($_POST['addcompany'])) {
    $description = trim($_POST['company_description']);
    
    $q = 'INSERT INTO company (description) VALUES (:description)';
    $params = array(':description' => $description);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Company ' . $description . ' added.</p>';
        logger('add', 'Company :: ' . $description);
    }
    else {
        echo '<p class="error">Company not added.</p>';
    }
}
elseif (isset($_POST['updatecompany'])) {
    $id = $_POST['updatecompany'];
    $description = trim($_POST['company_description']);
    
    $q = 'UPDATE company SET description=:description WHERE id=:id';
    $params = array(':id' => $id, ':description' => $description);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>Company ' . $description . ' updated.</p>';
        logger('update', 'Company :: ' . $description);
    }
    else {
        echo '<p class="error">Company not updated.</p>';
    }
}
elseif (isset($_POST['removecompany'])) {
    $id = $_POST['removecompany'];
    $description = $_POST['company_description'];
    
    $q = 'DELETE FROM company WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>Company ' . $description . ' removed.</p>';
        logger('remove', 'Company :: ' . $description);
    }
    else {
        echo '<p class="error">Company not removed.</p>';
    }
} ?>
<div class='auxwrapper'>
    <input type='button' value='Add'>
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addcompanyform' action='index.php?pid=companies' method='post'>
            <tr>
                <th>Name</th>
                <td>
                    <input id='focus' type='text' name='company_description' value=''>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addcompany' value='addcompany'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = 'SELECT id, description FROM company ORDER BY id';

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='view first last w140'>Name</th>
                <th class='edit' filter='false'></th>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $description = $row['description']; ?>
            <tr>
                <form name='updatecompanyform<?php echo $id ?>' action='index.php?pid=companies' method='post'>
                    <td class='view w140'>
                        <div><?php echo $description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='company_description' value='<?php echo $description; ?>'>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updatecompany' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form name='removecompanyform<?php echo $id ?>' action='index.php?pid=companies' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removecompany' value='<?php echo $id ?>'>
                        <input type='hidden' name='company_description' value='<?php echo $description; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No companies in database.</p>';
} ?>