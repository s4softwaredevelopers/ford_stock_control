
<?php
include 'includes/passwordHash.php';
if (isset($_POST['login'])) 
{ 
    $user = trim($_POST['user']);
    $pass = trim($_POST['pass']);
    $q = 'SELECT * FROM user WHERE username=:user';
    $params = array(':user' => $user);
    $result = null;
    $rows = sql_select($result, $q, $params);
    $auth = false;
    if ($rows == 1) 
    {
        if (isset($result)) 
        {
            foreach ($result as $row) 
            {
                $hash = $row['password'];
            }
            $hasher = new PasswordHash(8, false);
            echo $auth = $hasher->check_password($pass, $hash);
        }
    }
     
    if ($auth) { 
        foreach ($result as $row) 
			{
               login($row['username'], $row['user_access_id']);
            }
		header("Refresh: 0.6; url=index.php?pid=inventory_tracking");
		echo "<p>Authenticating...</p>";
    }
    else {
        header("Refresh: 0.6; url=index.php?pid=login_fail");
    }
}
elseif (isset($_POST['logout'])) 
{ ?>
    <?php logout();
    header('Refresh: 0.6; url=index.php');
	echo "<p>Logging out...</p>";
}
else 
{
    include 'includes/noAccess.php';
}
?>