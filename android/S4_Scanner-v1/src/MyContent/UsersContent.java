package MyContent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.widget.ArrayAdapter;

public class UsersContent {

	public static List<Users> USERS = new ArrayList<Users>();
	
	public static List<String> USERS_N = new ArrayList<String>();
	public static List<String> USERS_ID = new ArrayList<String>();

	public static  Map<String, Users> USER_MAP = new HashMap<String, Users>();
	
	public static ArrayAdapter<String> adapter;
	
	public static String returnUserid;
	public static String returnDescription;
	public static Boolean bReturnUser = false;
	
	public static void addItem(Users item) {
		USERS.add(item);
		USER_MAP.put(item.id, item);
		USERS_N.add(item.Name);
		USERS_ID.add(item.id);
	}
	
	public static class Users {
		public String id;
		public String Name;		

		public Users(String id, String Name) {
			this.id = id;
			this.Name = Name;
		}

		public String uIdDisplay() {
			return id;
		}
		
		public String uNameDisplay() {
			return Name;
		}
		
		@Override
		public String toString() {
			// Text Displaying in Selection List
			return Name;
		}
	}
}
