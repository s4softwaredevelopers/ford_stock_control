package MyContent;


	
public class HistoryContent {
		public String id;
		public String itemId;
		public String orderNumber;
		public String supplierCode;
		public String projectName;
		public String description;
		public String barcode;
		public String database;
		

		public HistoryContent(String id, String itemId, String orderNumber, String supplierCode, String projectName, String description, String barcode, String database) {
			this.id = id;
			this.itemId = itemId;
			this.orderNumber = orderNumber;
			this.supplierCode = supplierCode;
			this.projectName = projectName;
			this.description = description;
			this.barcode = barcode;
			this.database = database;
}

	@Override
	public String toString() {
		// Text Displaying in Selection List
		return description; 
	}
}
