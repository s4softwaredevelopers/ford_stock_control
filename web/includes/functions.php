<?php
function filter($pid, $ua)
{
    $site = array(
        'authenticate' => 0, 
        'login' => 0, 
        'login_fail' => 0, 
        
        'inventory_tracking' => 1, 
        
        'parts' => 2, 
        'products' => 2, 
        'groups' => 2, 
        
        'storerooms' => 2, 
        'shelves' => 2, 
        
        'companies' => 2, 
        'projects' => 2, 
        
        'users' => 2,
        
        'logs' => 1
    );
    
    if (isset($site[$pid])) {
        if ($site[$pid] <= $ua) {
            return true;
        }
    }
    
    return false;
}

function login($username, $useraccess)
{
    $_SESSION['user'] = $username;
    $_SESSION['ua'] = $useraccess;
}

function logout()
{
    unset($_SESSION['user']);
    unset($_SESSION['ua']);
}

function dependencies_exist($dependency = array())
{
    foreach ($dependency as $table) {
        if (sql_count($table) == 0) {
            return false;
        }
    }
    return true;
}

function build_result_array($q)
{
    $result = null;
    $rows = sql_select($result, $q);
    
    $result_array = array();
    
    if ($rows > 0) {
        foreach ($result as $row) {
            $id = $row['id'];
            $description = $row['description'];

            $result_array[$id] = $description;
        }
    }
    
    return $result_array;
}

function build_select_options($name, $result_array, $params = array())
{
    if (count($result_array) > 0) {
        if (isset($params['class'])) {
            echo '<select class="' . $params['class'] . '" name="' . $name . '">';
        }
        else {
            echo '<select name="' . $name . '">';
        }
        
        if (isset($params['selected'])) {
            $selected = $params['selected'];
        }
        else {
            $selected = -1;
        }
        
        foreach ($result_array as $id => $description) {
            if ($id == $selected) {
                echo '<option value="' . $id . '" selected="selected">' . $description . '</option>';
            }
            else {
                echo '<option value="' . $id . '">' . $description . '</option>';
            }
        }
        echo '</select>';
    }
}

function logger($action, $description)
{
    $process_ucf = ucfirst(str_replace('_', ' ', $_GET['pid']));
    $action_ucf = ucfirst($action);
    
    $q = 'INSERT INTO log (process, action, user, description, date_time) VALUES (:process, :action, :user, :description, :date_time)';
    $params = array(':process' => $process_ucf, ':action' => $action_ucf, ':user' => $_SESSION['user'], ':description' => $description, ':date_time'=> date("Y-m-d H:i:s"));
    $id = null;
    $rows = sql_insert($id, $q, $params);
}

function make_thumb($src, $dest, $desired_width) {

	/* read the source image */
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}
?>