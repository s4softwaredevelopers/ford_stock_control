namespace s4_Scanner
{
    partial class Relocate_P_Shelf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFooter = new System.Windows.Forms.Panel();
            this.labelMessage = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelBase = new System.Windows.Forms.Panel();
            this.panelBarcodes = new System.Windows.Forms.Panel();
            this.panel_D_AutoShelf = new System.Windows.Forms.Panel();
            this.checkBoxSearchAutoShelf = new System.Windows.Forms.CheckBox();
            this.panel_D_Search = new System.Windows.Forms.Panel();
            this.textBoxSearchAuto = new System.Windows.Forms.TextBox();
            this.labelSearchAuto = new System.Windows.Forms.Label();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelBarcodeData = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelOldShelfDesc = new System.Windows.Forms.Label();
            this.labelOldShelf = new System.Windows.Forms.Label();
            this.labelItemNumber = new System.Windows.Forms.Label();
            this.labelProjData = new System.Windows.Forms.Label();
            this.labelSupplierData = new System.Windows.Forms.Label();
            this.labelDescription = new System.Windows.Forms.Label();
            this.labelDescriptionText = new System.Windows.Forms.Label();
            this.labelDatabase = new System.Windows.Forms.Label();
            this.labelProj = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.panelLine2 = new System.Windows.Forms.Panel();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelOptions = new System.Windows.Forms.Panel();
            this.panel_O_Button = new System.Windows.Forms.Panel();
            this.button_O_Selected = new System.Windows.Forms.Button();
            this.panel_O_Order_D = new System.Windows.Forms.Panel();
            this.labelExtraOrders = new System.Windows.Forms.Label();
            this.comboBoxOrder = new System.Windows.Forms.ComboBox();
            this.panelFooter.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.panelBase.SuspendLayout();
            this.panelBarcodes.SuspendLayout();
            this.panel_D_AutoShelf.SuspendLayout();
            this.panel_D_Search.SuspendLayout();
            this.panelInfo.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.panelOptions.SuspendLayout();
            this.panel_O_Button.SuspendLayout();
            this.panel_O_Order_D.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelFooter
            // 
            this.panelFooter.Controls.Add(this.labelMessage);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 300);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(240, 20);
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(5, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(230, 20);
            this.labelMessage.Text = "Message";
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.buttonClear);
            this.panelHeader.Controls.Add(this.buttonClose);
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClear.Location = new System.Drawing.Point(161, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(30, 20);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "R";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(211, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(30, 20);
            this.buttonClose.TabIndex = 10;
            this.buttonClose.Text = " x";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Relocate: Shelf";
            // 
            // panelBase
            // 
            this.panelBase.Controls.Add(this.panelBarcodes);
            this.panelBase.Controls.Add(this.panelInfo);
            this.panelBase.Controls.Add(this.panelLine2);
            this.panelBase.Controls.Add(this.buttonUpdate);
            this.panelBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBase.Location = new System.Drawing.Point(0, 20);
            this.panelBase.Name = "panelBase";
            this.panelBase.Size = new System.Drawing.Size(240, 280);
            // 
            // panelBarcodes
            // 
            this.panelBarcodes.BackColor = System.Drawing.Color.White;
            this.panelBarcodes.Controls.Add(this.panel_D_AutoShelf);
            this.panelBarcodes.Controls.Add(this.panel_D_Search);
            this.panelBarcodes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBarcodes.Location = new System.Drawing.Point(0, 148);
            this.panelBarcodes.Name = "panelBarcodes";
            this.panelBarcodes.Size = new System.Drawing.Size(240, 101);
            // 
            // panel_D_AutoShelf
            // 
            this.panel_D_AutoShelf.Controls.Add(this.checkBoxSearchAutoShelf);
            this.panel_D_AutoShelf.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_D_AutoShelf.Location = new System.Drawing.Point(0, 50);
            this.panel_D_AutoShelf.Name = "panel_D_AutoShelf";
            this.panel_D_AutoShelf.Size = new System.Drawing.Size(240, 50);
            this.panel_D_AutoShelf.Visible = false;
            // 
            // checkBoxSearchAutoShelf
            // 
            this.checkBoxSearchAutoShelf.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.checkBoxSearchAutoShelf.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.checkBoxSearchAutoShelf.Location = new System.Drawing.Point(5, 15);
            this.checkBoxSearchAutoShelf.Name = "checkBoxSearchAutoShelf";
            this.checkBoxSearchAutoShelf.Size = new System.Drawing.Size(230, 20);
            this.checkBoxSearchAutoShelf.TabIndex = 76;
            this.checkBoxSearchAutoShelf.Text = "AUTOSHELF - ";
            this.checkBoxSearchAutoShelf.CheckStateChanged += new System.EventHandler(this.checkBoxSearchAutoShelf_CheckStateChanged);
            // 
            // panel_D_Search
            // 
            this.panel_D_Search.Controls.Add(this.textBoxSearchAuto);
            this.panel_D_Search.Controls.Add(this.labelSearchAuto);
            this.panel_D_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_D_Search.Location = new System.Drawing.Point(0, 0);
            this.panel_D_Search.Name = "panel_D_Search";
            this.panel_D_Search.Size = new System.Drawing.Size(240, 50);
            this.panel_D_Search.Visible = false;
            // 
            // textBoxSearchAuto
            // 
            this.textBoxSearchAuto.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.textBoxSearchAuto.Location = new System.Drawing.Point(5, 18);
            this.textBoxSearchAuto.Name = "textBoxSearchAuto";
            this.textBoxSearchAuto.Size = new System.Drawing.Size(230, 29);
            this.textBoxSearchAuto.TabIndex = 79;
            this.textBoxSearchAuto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSearchAuto_KeyPress);
            // 
            // labelSearchAuto
            // 
            this.labelSearchAuto.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSearchAuto.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSearchAuto.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSearchAuto.Location = new System.Drawing.Point(5, 1);
            this.labelSearchAuto.Name = "labelSearchAuto";
            this.labelSearchAuto.Size = new System.Drawing.Size(230, 17);
            this.labelSearchAuto.Text = "SEARCH FOR AN ITEM (BARCODE)\r\n";
            // 
            // panelInfo
            // 
            this.panelInfo.Controls.Add(this.labelBarcodeData);
            this.panelInfo.Controls.Add(this.label1);
            this.panelInfo.Controls.Add(this.labelOldShelfDesc);
            this.panelInfo.Controls.Add(this.labelOldShelf);
            this.panelInfo.Controls.Add(this.labelItemNumber);
            this.panelInfo.Controls.Add(this.labelProjData);
            this.panelInfo.Controls.Add(this.labelSupplierData);
            this.panelInfo.Controls.Add(this.labelDescription);
            this.panelInfo.Controls.Add(this.labelDescriptionText);
            this.panelInfo.Controls.Add(this.labelDatabase);
            this.panelInfo.Controls.Add(this.labelProj);
            this.panelInfo.Controls.Add(this.labelSupplier);
            this.panelInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelInfo.Location = new System.Drawing.Point(0, 0);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(240, 146);
            // 
            // labelBarcodeData
            // 
            this.labelBarcodeData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelBarcodeData.Location = new System.Drawing.Point(71, 51);
            this.labelBarcodeData.Name = "labelBarcodeData";
            this.labelBarcodeData.Size = new System.Drawing.Size(166, 20);
            this.labelBarcodeData.Text = "Barcode";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(5, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.Text = "BARCODE";
            // 
            // labelOldShelfDesc
            // 
            this.labelOldShelfDesc.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelOldShelfDesc.Location = new System.Drawing.Point(71, 74);
            this.labelOldShelfDesc.Name = "labelOldShelfDesc";
            this.labelOldShelfDesc.Size = new System.Drawing.Size(166, 20);
            this.labelOldShelfDesc.Text = "Old Shelf Desc";
            // 
            // labelOldShelf
            // 
            this.labelOldShelf.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelOldShelf.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelOldShelf.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelOldShelf.Location = new System.Drawing.Point(5, 74);
            this.labelOldShelf.Name = "labelOldShelf";
            this.labelOldShelf.Size = new System.Drawing.Size(65, 17);
            this.labelOldShelf.Text = "LOCATION";
            // 
            // labelItemNumber
            // 
            this.labelItemNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelItemNumber.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelItemNumber.Location = new System.Drawing.Point(161, 0);
            this.labelItemNumber.Name = "labelItemNumber";
            this.labelItemNumber.Size = new System.Drawing.Size(80, 20);
            this.labelItemNumber.Text = "999 / 999";
            this.labelItemNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelProjData
            // 
            this.labelProjData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelProjData.Location = new System.Drawing.Point(71, 28);
            this.labelProjData.Name = "labelProjData";
            this.labelProjData.Size = new System.Drawing.Size(166, 20);
            this.labelProjData.Text = "ProjData";
            // 
            // labelSupplierData
            // 
            this.labelSupplierData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSupplierData.Location = new System.Drawing.Point(71, 5);
            this.labelSupplierData.Name = "labelSupplierData";
            this.labelSupplierData.Size = new System.Drawing.Size(94, 20);
            this.labelSupplierData.Text = "SupData";
            // 
            // labelDescription
            // 
            this.labelDescription.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelDescription.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelDescription.Location = new System.Drawing.Point(5, 97);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(65, 17);
            this.labelDescription.Text = "ITEM INFO";
            // 
            // labelDescriptionText
            // 
            this.labelDescriptionText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelDescriptionText.Location = new System.Drawing.Point(5, 97);
            this.labelDescriptionText.Name = "labelDescriptionText";
            this.labelDescriptionText.Size = new System.Drawing.Size(232, 48);
            this.labelDescriptionText.Text = "Desc Text";
            // 
            // labelDatabase
            // 
            this.labelDatabase.Location = new System.Drawing.Point(120, 5);
            this.labelDatabase.Name = "labelDatabase";
            this.labelDatabase.Size = new System.Drawing.Size(98, 19);
            this.labelDatabase.Text = "Databse";
            this.labelDatabase.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelProj
            // 
            this.labelProj.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelProj.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelProj.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelProj.Location = new System.Drawing.Point(5, 28);
            this.labelProj.Name = "labelProj";
            this.labelProj.Size = new System.Drawing.Size(65, 17);
            this.labelProj.Text = "PROJECT";
            // 
            // labelSupplier
            // 
            this.labelSupplier.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSupplier.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSupplier.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSupplier.Location = new System.Drawing.Point(5, 5);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(65, 17);
            this.labelSupplier.Text = "SUPPLIER";
            // 
            // panelLine2
            // 
            this.panelLine2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panelLine2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLine2.Location = new System.Drawing.Point(0, 249);
            this.panelLine2.Name = "panelLine2";
            this.panelLine2.Size = new System.Drawing.Size(240, 1);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.LightGray;
            this.buttonUpdate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonUpdate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonUpdate.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUpdate.Location = new System.Drawing.Point(0, 250);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(240, 30);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Update!";
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label2);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 280);
            this.panelLoader.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(25, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 107);
            this.label2.Text = "Loading";
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.panel_O_Order_D);
            this.panelOptions.Controls.Add(this.panel_O_Button);
            this.panelOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOptions.Location = new System.Drawing.Point(0, 20);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(240, 280);
            // 
            // panel_O_Button
            // 
            this.panel_O_Button.Controls.Add(this.button_O_Selected);
            this.panel_O_Button.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_O_Button.Location = new System.Drawing.Point(0, 246);
            this.panel_O_Button.Name = "panel_O_Button";
            this.panel_O_Button.Size = new System.Drawing.Size(240, 34);
            this.panel_O_Button.Visible = false;
            // 
            // button_O_Selected
            // 
            this.button_O_Selected.BackColor = System.Drawing.Color.LightGray;
            this.button_O_Selected.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.button_O_Selected.Location = new System.Drawing.Point(5, 2);
            this.button_O_Selected.Name = "button_O_Selected";
            this.button_O_Selected.Size = new System.Drawing.Size(230, 30);
            this.button_O_Selected.TabIndex = 73;
            this.button_O_Selected.Text = "Relocate";
            this.button_O_Selected.Click += new System.EventHandler(this.button_O_Selected_Click);
            // 
            // panel_O_Order_D
            // 
            this.panel_O_Order_D.Controls.Add(this.labelExtraOrders);
            this.panel_O_Order_D.Controls.Add(this.comboBoxOrder);
            this.panel_O_Order_D.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_O_Order_D.Location = new System.Drawing.Point(0, 0);
            this.panel_O_Order_D.Name = "panel_O_Order_D";
            this.panel_O_Order_D.Size = new System.Drawing.Size(240, 190);
            this.panel_O_Order_D.Visible = false;
            // 
            // labelExtraOrders
            // 
            this.labelExtraOrders.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelExtraOrders.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelExtraOrders.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelExtraOrders.Location = new System.Drawing.Point(20, 10);
            this.labelExtraOrders.Name = "labelExtraOrders";
            this.labelExtraOrders.Size = new System.Drawing.Size(200, 36);
            this.labelExtraOrders.Text = "Select Order Number to view all items of the selected Order";
            // 
            // comboBoxOrder
            // 
            this.comboBoxOrder.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.comboBoxOrder.Location = new System.Drawing.Point(20, 54);
            this.comboBoxOrder.Name = "comboBoxOrder";
            this.comboBoxOrder.Size = new System.Drawing.Size(200, 24);
            this.comboBoxOrder.TabIndex = 87;
            // 
            // Relocate_P_Shelf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelBase);
            this.Controls.Add(this.panelOptions);
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panelFooter);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Relocate_P_Shelf";
            this.Text = "Relocate_P_Shelf";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Relocate_P_Shelf_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Relocate_P_Shelf_KeyDown);
            this.panelFooter.ResumeLayout(false);
            this.panelHeader.ResumeLayout(false);
            this.panelBase.ResumeLayout(false);
            this.panelBarcodes.ResumeLayout(false);
            this.panel_D_AutoShelf.ResumeLayout(false);
            this.panel_D_Search.ResumeLayout(false);
            this.panelInfo.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.panelOptions.ResumeLayout(false);
            this.panel_O_Button.ResumeLayout(false);
            this.panel_O_Order_D.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelBase;
        private System.Windows.Forms.Panel panelBarcodes;
        private System.Windows.Forms.Panel panel_D_AutoShelf;
        private System.Windows.Forms.CheckBox checkBoxSearchAutoShelf;
        private System.Windows.Forms.Panel panel_D_Search;
        private System.Windows.Forms.TextBox textBoxSearchAuto;
        private System.Windows.Forms.Label labelSearchAuto;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Panel panelLine2;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label labelBarcodeData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelOldShelfDesc;
        private System.Windows.Forms.Label labelOldShelf;
        private System.Windows.Forms.Label labelItemNumber;
        private System.Windows.Forms.Label labelProjData;
        private System.Windows.Forms.Label labelSupplierData;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.Label labelDescriptionText;
        private System.Windows.Forms.Label labelDatabase;
        private System.Windows.Forms.Label labelProj;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelOptions;
        private System.Windows.Forms.Panel panel_O_Button;
        private System.Windows.Forms.Button button_O_Selected;
        private System.Windows.Forms.Panel panel_O_Order_D;
        private System.Windows.Forms.Label labelExtraOrders;
        private System.Windows.Forms.ComboBox comboBoxOrder;
    }
}