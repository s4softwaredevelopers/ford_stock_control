//Global variables
var usersDropdownFrom = '';
var usersDropdownTo = '';
var relocatefrom = '';
var relocateTo = '';
var relocateType = '';
var itemsBarcodes = '';
var params = '';
//store all textholder values[STATE variables]
var fromLoginSelectedValue = '';
var fromLoginPassword = '';
var fromLoginDescription = '';
var fromShelfBarcode = '';
var scannedItems = '';
var toLoginSelectedValue = '';
var toLoginPassword = '';
var toLoginDescription = '';
var toShelfBarcode = '';
var logUserReturn = "";
var logUserReceive = "";
var currentStep = 0;

//Begin Pagination
$.fn.pageMe = function (opts) {
    var $this = this,
            defaults = {
                perPage: 20,
                showPrevNext: false,
                hidePageNumbers: false
            },
    settings = $.extend(defaults, opts);

    var listElement = $this;
    var perPage = settings.perPage;
    var children = listElement.children();
    var pager = $('.pager');

    if (typeof settings.childSelector != "undefined") {
        children = listElement.find(settings.childSelector);
    }

    if (typeof settings.pagerSelector != "undefined") {
        pager = $(settings.pagerSelector);
    }

    var numItems = children.size();
    var numPages = Math.ceil(numItems / perPage);

    pager.data("curr", 0);

    if (settings.showPrevNext) {
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }

    var curr = 0;
    while (numPages > curr && (settings.hidePageNumbers == false)) {
        $('<li><a href="#" class="page_link">' + (curr + 1) + '</a></li>').appendTo(pager);
        curr++;
    }

    if (settings.showPrevNext) {
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }

    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages <= 1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");

    children.hide();
    children.slice(0, perPage).show();

    pager.find('li .page_link').click(function () {
        var clickedPage = $(this).html().valueOf() - 1;
        goTo(clickedPage, perPage);
        return false;
    });
    pager.find('li .prev_link').click(function () {
        previous();
        return false;
    });
    pager.find('li .next_link').click(function () {
        next();
        return false;
    });

    function previous() {
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }

    function next() {
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }

    function goTo(page) {
        var startAt = page * perPage,
                endOn = startAt + perPage;

        children.css('display', 'none').slice(startAt, endOn).show();

        if (page >= 1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }

        if (page < (numPages - 1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }

        pager.data("curr", page);
        pager.children().removeClass("active");
        pager.children().eq(page + 1).addClass("active");

    }
}
//End of pagination
function makeDivStepOne()
{
    var html = "<table style='margin:30px;'><tr><td><h3>Please select where the item originate from:</h3></td></tr><tr><td><input type='button' class='btn btn-success' id='FromUser' value='User / Staff'/> <h3> OR</h3> <input type='button' id='FromShelf' class='btn btn-primary'  value='Shelf in storeroom'/></td></tr><tr><td colspan='2' style='text-align:center;'></td></tr></table>";
    return html;
}
function makeDivStepTwo()
{
    var html = "";
    if ($("#tdStepOneResult").text() == "User")
    {
        html += "<table style='margin:30px;'><thead><th style='text-align:right'><h3>Login Details</h3></th></thead><tr><td style='text-align:right;padding-right:10px;'><label>Select your name: </label></td><td>" + usersDropdownFrom + "</td>\n\
<tr><td style='text-align:right;padding-right:10px;'><label>Enter your password:</label></td><td style='padding-top:5px;padding-bottom:5px;'><input placeholder='Enter password' style='width:50%' class='form-control' type='password' name='returnPassword' required/></td>\n\
</tr><tr><td style='text-align:right;padding-right:10px;'><label>Reason for returning item:</label></td><td><input style='width:50%' placeholder='Enter reason for returning item' class='form-control' type='text' name='returnDescription'  />\n\
</td></tr><tr><td></td>\n\
<td style='padding-top:5px;'><input id='btnStepTwoLoginNext' type='button' class='btn btn-primary' value='Next' /></td></tr></table>";

    }

    else if ($("#tdStepOneResult").text() == "Shelf")
    {
        html += "<table><tr><th style='padding-left:130px;'><h3>Scan Shelf</h3></th></tr><tr><td><label>Scan shelf barcode : &nbsp;</label><input style='width:40%' placeholder='Scan shelf barcode' class='form-control' type='text' name='stepOneShelfBarcodeVal' required/></td>\n\
</tr><tr><td  style='padding-left:125px;padding-top:5px;'><input id='btnStepOneScanShelfNext' type='button' class='btn btn-primary' value='Next'/></td></tr></table>";
    }
    else
    {
        html += makeDivStepOne();
    }
    return html;
}
function makeDivStepThree()
{
    var html = "<table style='margin:30px;'><thead><th colspan='2'><h3>Scan barcodes of items:</h3></th></thead><tbody><tr><td><label>Scan Barcodes:</label></td><td><input id='txtScanItem' style='width:45%;margin-bottom:5px;' placeholder='Scan item barcode and press ENTER' class='form-control' type='text' name='ShelfBarcodeVal' /></td></tr><tr><td><label>Scanned Barcodes:</label></td><td><textarea id='txtMultiBarcodes' style='width:45%;' cols='80' rows='5' class='form-control' type='text' name='barcodeValues'></textarea></td></tr><tr><td style='float:right'><input id='btnScanBarcodesNext' type='button' class='btn btn-primary' value='Next' /></td></tr;></tbody></table>";//<input id='btnScanBarcodesClear' type='button' class='btn btn-primary' value='Clear' />

    return html;
}
function makeDivStepFour()
{
    var html = "<table style='margin-left:30px;'><thead><th><h3>Please select where the item(s) are going:</h3></th></thead><tbody>\n\
<tr><td><input type='button' id='optToUser' class='btn btn-warning' value='User / Staff'/></td></tr>\n\
<tr><td><h3>OR</h3></td></tr>\n\
<tr><td><input type='button' id='optToShelf' class='btn btn-primary' value='Shelf in storeroom'/></td></tr>\n\
<tr><td><h3>OR</h3></td></tr>\n\
<tr><td><input type='button' id='optToPending' class='btn btn-info' value='Pending/ Relocate later'/></td></tr></tbody></table>";
    return html;
}
function makeDivStepFive()
{
    var html = "";
    if ($("#tdStepFourResult").text() == "User")
    {
        html += "<table style='margin:30px;'><thead><th style='text-align:right'><h3>Login Details</h3></th></thead><tr><td style='text-align:right;padding-right:10px;'><label>Select your name: </label></td><td>" + usersDropdownTo + "</td>\n\
<tr><td style='text-align:right;padding-right:10px;'><label>Enter your password:</label></td><td style='padding-top:5px;padding-bottom:5px;'><input placeholder='Enter password' style='width:50%' class='form-control' type='password' name='ToUserPassword' required/></td>\n\
</tr><tr><td style='text-align:right;padding-right:10px;'><label>Reason for taking item(s):</label></td><td><input placeholder='reason for taking items' style='width:50%' class='form-control' type='text' name='UserDescriptionValTo' />\n\
</td></tr><tr><td></td>\n\
<td style='padding-top:5px;'><input id='btnStepFiveUserNext' type='button' class='btn btn-primary' value='Next' /></td></tr></table>";

    }
    else if ($("#tdStepFourResult").text() == "Shelf")
    {
        html += "<table style='margin:30px;'><tr><th style='padding-left:130px;'><h3>Scan Shelf</h3></th></tr><tr><td><label>Scan shelf barcode : &nbsp;</label><input placeholder='Scan shelf barcode' style='width:40%' class='form-control' type='text' name='toShelfBarcodeVal' required/></td>\n\
</tr><tr><td  style='padding-left:125px;padding-top:5px;'><input id='btnStepFiveShelfNext' type='button' class='btn btn-primary' value='Next'/></td></tr></table>";
    }
    else
    {
        //go to finish step
        html += makeDivStepSix();
    }
    return html;
}
function makeDivStepSix()
{
    var html = "<div><table style='margin:30px;'><tr><td><p>Are you sure that you would like to relocate these items?</p><div class='checkbox checkbox-primary'><input type='checkbox' name='chkConfirm' id='chkConfirm' value='Yes'><label for='chkConfirm'>Yes</label></div></td></tr>\n\
<tr><td><input id='btnFinish' data-loading-text='Relocating...' type='submit' class='btn btn-success' style='display:none'  value='Finish'</td></table></div>";
    return html;
}
function makeDivStepSeven()
{
    var html = "<h4>Please scan an item first.</h4>";
    return html;
}

function clickedStepOne()
{

    $("#tdTarget").html(makeDivStepOne());
    $('#tdStepTwo,#tdStepTwoResult,#tdStepThree,#tdStepThreeResult,#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '0.2', 'border': 'none'}).prop('disabled', true).off();
}
function clickedStepTwo()
{
    $("#tdTarget").html(makeDivStepTwo());
    if (fromLoginSelectedValue != "")
    {
        $('#dropdownFrom option:selected').text(fromLoginSelectedValue);
    }
    if (fromLoginPassword != "")
    {
        $('[name=returnPassword]').val(fromLoginPassword);
    }
    if (fromLoginDescription != "")
    {
        $('[name=returnDescription]').val(fromLoginDescription);
    }
    if (fromShelfBarcode != "")
    {
        $('[name=stepOneShelfBarcodeVal]').val(fromShelfBarcode);
    }
    if ($("#tdStepOneResult").text() == "Shelf")
    {
        $('[name=stepOneShelfBarcodeVal]').focus();
    }
    $('.chosen-select').chosen();
    $('#tdStepTwo,#tdStepTwoResult,#tdStepThree,#tdStepThreeResult,#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '1', 'border': 'none'}).prop('disabled', false).on();
    $('#tdStepThree,#tdStepThreeResult,#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '0.2', 'border': 'none'}).prop('disabled', true).off();
    highlightTableRows();
}
function clickedStepThree()
{
    $("#tdTarget").html(makeDivStepThree());
    $('#txtScanItem').focus();
    if (scannedItems != "")
    {
        $('#txtMultiBarcodes').append(scannedItems);
    }
    $('#tdStepThree,#tdStepThreeResult,#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '1', 'border': 'none'}).prop('disabled', false).on();
    $('#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '0.2', 'border': 'none'}).prop('disabled', true).off();
    highlightTableRows();
}
function clickedStepFour()
{
    $("#tdTarget").html(makeDivStepFour());
    $('#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '1', 'border': 'none'}).prop('disabled', false).on();
    $('#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '0.2', 'border': 'none'}).prop('disabled', true).off();
    highlightTableRows();
}
function clickedStepFive()
{
    $("#tdTarget").html(makeDivStepFive());
    if (toLoginSelectedValue != "")
    {
        $('#dropdownTo option:selected').text(toLoginSelectedValue);
    }
    if (toLoginPassword != "")
    {
        $('[name=ToUserPassword]').val(toLoginPassword);
    }
    if (toLoginDescription != "")
    {
        $('[name=UserDescriptionValTo]').val(toLoginDescription);
    }
    if (toShelfBarcode != "")
    {
        $('[name=toShelfBarcodeVal]').val(toShelfBarcode);
    }
    $('.chosen-select').chosen();
    $('#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '1', 'border': 'none'}).prop('disabled', false).on();
    $('#tdStepSix').css({'opacity': '0.2', 'border': 'none'}).prop('disabled', true).off();
    highlightTableRows();


}
function clickedStepSix()
{
    $("#tdTarget").html(makeDivStepSix());
    $('#tdStepSix').css('opacity', '1');
    highlightTableRows();
    $('#tdStepOne,#tdStepOneResult,#tdStepTwo,#tdStepTwoResult,#tdStepThree,#tdStepThreeResult,#tdStepFour,#tdStepFourResult,#tdStepFive,#tdStepFiveResult,#tdStepSix').css({'opacity': '1', 'border': 'none'}).prop('disabled', false).on();
}

function highlightTableRows()
{
    if (currentStep > 0)
    {
        $('#tdStepOne').css('opacity', '1');
        $('#tdStepTwo').css('opacity', '1');
    }
    if ($('#tdStepOneResult').text() == "Shelf" || currentStep > 2)
    {
        $('#tdStepThree').css('opacity', '1');
        $('#tdStepTwoResult').css('opacity', '1');
    }
    if (currentStep > 3)
    {
        $('#tdStepThree').css('opacity', '1');
        $('#tdStepFour').css('opacity', '1');
        $('#tdStepThreeResult').css('opacity', '1');
    }
    if (currentStep > 4)
    {
        $('#tdStepFour').css('opacity', '1');
        $('#tdStepFive').css('opacity', '1');
        $('#tdStepFourResult').css('opacity', '1');

    }
    if (currentStep > 5)
    {
        $('#tdStepFive').css('opacity', '1');
        $('#tdStepFiveResult').css('opacity', '1');
    }
}

function UpdateShelf(id)
{
    var trId = $(id).closest("tr").attr('id');
    var barcode = $(id).closest('tr').find('[name=editBarcode]').val();
    var description = $(id).closest('tr').find('[name=editDescription]').val();
    var s4project = $(id).closest('tr').find('[name=ddIntranetProjects]').val();
    var mmproject = $(id).closest('tr').find('[name=ddAutonetProjects]').val();
    var location = $(id).closest('tr').find('[name=editLocation]').val();
    window.location = "index.php?pid=manageShelves&fid=update&shelfId=" + trId + "&editBarcode=" + barcode + "&editDescription=" + description + "&s4ProjectsAdd=" + s4project + "&mmProjectsAdd=" + mmproject + "&editLocation=" + location;
}

function UpdateUsers(id)
{
    var trId = $(id).closest("tr").attr('id');
    var username = $(id).closest('tr').find('[name=editUsername]').val();
    var UserAccess = $(id).closest('tr').find('[name=editUserAccess]').val();
    var email = $(id).closest('tr').find('[name=editEmail]').val();
    window.location = "index.php?pid=manageUsers&fid=update&UserId=" + trId + "&editUsername=" + username + "&editUserAccess=" + UserAccess + "&editEmail=" + email;
}

function UpdateConsumables(id)
{
    var trId = $(id).closest("tr").attr('id');
    var name = $(id).closest('tr').find('[name=editCategory]').val();
    var description = $(id).closest('tr').find('[name=editDescription]').val();
    var details = $(id).closest('tr').find('[name=editDetails]').val();
    var minQty = $(id).closest('tr').find('[name=editMinQty]').val();
    var maxQty = $(id).closest('tr').find('[name=editMaxQty]').val();
    var qtyInStore = $(id).closest('tr').find('[name=editQtyInStore]').val();
    var supplier = $(id).closest('tr').find('[name=editSupplierCode]').val();
    var nettValue = $(id).closest('tr').find('[name=editNettValue]').val();
    $.get("mobile.php?pid=consumable&fid=update&id=" + trId + "&name=" + name + "&description=" + description + "&detail=" + details + "&minQty=" + minQty + "&maxQty=" + maxQty + "&qtyInStore=" + qtyInStore + "&supplier=" + supplier + "&nettValue=" + nettValue, function (response)
    {
        alert(response);
        location.reload();
    });
}
;
function DeleteConfirmation(item)
{
    return confirm('Are you sure you want to delete this ' + item + '? Press the OK button now to delete, or Cancel');
}
function DeleteConsumables(id)
{
    var counsumableId = $(id).closest("tr").attr('id');
    var c = confirm("Are you sure you want to delete this consumable? Press the OK button now to delete, or Cancel");
    if (c == true)
    {
        $.get("mobile.php?pid=consumable&fid=delete&id=" + counsumableId, function (response)
        {
            alert(response);
            location.reload();
        });
    }

}
;

function isValidForm()
{
    var pwd = document.getElementsByName('password')[0].value;
    var pwdRepeat = document.getElementsByName('passwordconfirm')[0].value;
    if (pwd != pwdRepeat)
    {
        alert("Passwords does not match");
        return false;
    }
    if (document.getElementsByName('password')[0].value == '')
    {
        alert("Please enter a password.");
        return false;
    }
    if (document.getElementsByName('username')[0].value == '')
    {
        alert("Please enter a username.");
        return false;
    }
    if (document.getElementsByName('email')[0].value == '')
    {
        alert("Please enter a email address");
        return false;
    }
    if (document.getElementsByName('passwordconfirm')[0].value == '')
    {
        alert("Please confirm your password.");
        return false;
    }
    return true;
}

function ManageCaptureStock()
{
    var url = window.location.pathname;
    window.location.href = url + "?pid=storemanCaptureStock";
}

function CallCheckinTable()
{
    var url = window.location.pathname;
    window.location.href = url + "?fid=8";
}

function BarcodePrint()
{
    var url = window.location.pathname;
    window.location.href = url + "?pid=PrintBarcode";
}

function PrintShelf(id)
{
    var qty = $('[name=txtBarcodeQty]').val();
    window.location = "index.php?pid=manageShelves&fid=print&qty=" + qty;
}

//Marry item (Normal barcode scanner)
function btnMarryItem(id)
{
    var barcode = $(id).closest('tr').find('[name=txtBarcode]').val();
    var shelfBarcode = $(id).closest('tr').find('[name=txtShelf]').val();
    var trId = $(id).closest("tr").attr('id');
    $.get("mobile.php?pid=marryItems&fid=desktop&barValue=" + barcode + "&ItemIdValue=" + trId + "&shelfValue=" + shelfBarcode, function (response)
    {
        alert(response);
        location.reload();
    });
}

function submitAndClose()
{
//        document.forms['frmSetLocation'].submit();
    opener.location.reload(true);
    //self.close();
}

//End of functions

//Document ready
$(document).ready(function ()
{
    //Relocate Page ************************************************************

    $('#tdTarget').hover(function ()
    {
        $(this).css('cursor', 'pointer');
    });
    $.get("mobile.php?pid=users&fid=dropdownFrom", function (response)
    {
        usersDropdownFrom = response;
    });
    $.get("mobile.php?pid=users&fid=dropdownTo", function (response)
    {
        usersDropdownTo = response;
    });
    $('#tableRelocateWizard').on('click', '#tdStepTwo', function ()
    {
        clickedStepTwo();
    });
    $('#tableRelocateWizard').on('click', '#tdStepThree', function ()
    {
        clickedStepThree();
    });
    $('#tableRelocateWizard').on('click', '#tdStepFour', function ()
    {
        clickedStepFour();
    });
    $('#tableRelocateWizard').on('click', '#tdStepFive', function ()
    {
        clickedStepFive();
    });
    $('#tableRelocateWizard').on('click', '#tdStepSix', function ()
    {
        clickedStepSix();
    });

    $("#tdStepOne, #tdStepOneResult").click(clickedStepOne);
    $("#tdStepTwo, #tdStepTwoResult").click(clickedStepTwo);
    $("#tdStepThree, #tdStepThreeResult").click(clickedStepThree);
    $("#tdStepFour, #tdStepFourResult").click(clickedStepFour);
    $("#tdStepFive").click(clickedStepFive);
    $("#tdStepSix").click(clickedStepSix);
    $('#tdStepOne').trigger('click');

    //Show Result
    $("#FromShelf").live("click", function ()
    {
        $("#tdStepOneResult").text("Shelf");
        relocatefrom = "Shelf";
        $('#tdStepTwo').text("Scan Shelf");
        $('#tdStepTwo').hide();
        $('#tdStepTwoResult').hide();
        currentStep = 2;
        clickedStepThree();
    });
    
    $('[name=chkUnreceived]').change(function()
    {
        if ($(this).prop('checked')) {
            $.ajax({
            type: "post",
            url: "mobile.php?pid=report&fid=orderNumberNotReceived&orderNumber="+$('#orderNumVal').text(),
            dataType: "json",
            success: function (response)
            {
            },
            complete: function (response)
            {
                //$('#loader').hide();
                 $('#tblReportByOrder').replaceWith(response.responseText);
                 $('.tbOrderItemsContents').slideToggle("300");
                 $('.tbOrderItemsHeading').slideToggle("300");
            }            
            });
            
               // $('#tblReportByOrder').html('');
            
            //display all the items where quantity ordered and quantity received are not the same.update table with class name 'innerTable'
        }
        else
        {
            $.ajax({
            type: "post",
            url: "mobile.php?pid=report&fid=order&orderNumber="+$('#orderNumVal').text(),
            dataType: "json",
            beforeSend: function () {
                //$('#loader').show();
            },
            success: function (response)
            {
            },
            complete: function (response)
            {
                //$('#loader').hide();
                 $('#tblReportByOrder').replaceWith(response.responseText);
                 $('.tbOrderItemsContents').slideToggle("300");
                 $('.tbOrderItemsHeading').slideToggle("300");
            }            
            });
            ////display all the items where quantity ordered and quantity received are the same.update table with class name 'innerTable'
        }
    });
    $('#BtnReprintBarcode').click(function ()
    {
        //validate input
        $.ajax({
            type: "post",
            url: " .php?pid=barcode&fid=reprint&barcode=" + $('#InputReprintBarcode').val(),
            beforeSend: function () {
                //$('#loader').show();
                //Disable print button
                $('#BtnReprintBarcode').attr('disable', true);
            },
            success: function (response)
            {
                alert(response);
            },
            complete: function ()
            {
                //$('#loader').hide();
                //enable print button
                $('#BtnReprintBarcode').attr('disable', false);
            }
        });
    });

    $("#FromUser").live("click", function ()
    {
        $('#tdStepTwo').show();
        $('#tdStepTwoResult').show();
        $("#tdStepOneResult").text("User");
        relocatefrom = "User";
        $('#tdStepTwo').text("Login Details");
        currentStep = 2;
        clickedStepTwo();
    });

    $("#tdTarget").on("click", '#btnScanBarcodesClear', function (e)
    {
        $('#txtScanItem').val('').focus();
        $('#txtMultiBarcodes').val('');
    });


    $('#btnStepOneScanShelfNext').live("click", function ()
    {
        $("#tdStepTwoResult").text($("[name=stepOneShelfBarcodeVal]").val());
        fromShelfBarcode = $("[name=stepOneShelfBarcodeVal]").val();
        clickedStepThree();
    });

    $('#btnStepTwoLoginNext').live("click", function ()
    {
        $("#tdStepTwoResult").text($("#dropdownFrom option:selected").text());
        logUserReturn = $("#tdStepTwoResult").text();
        fromLoginSelectedValue = $("#dropdownFrom :selected").val();
        fromLoginPassword = $('[name=returnPassword]').val();
        fromLoginDescription = $('[name=returnDescription]').val();

        $.get("mobile.php?pid=users&fid=loginDetails&firstNameReceiver=" + $('#dropdownFrom :selected').val() + "&returnPassword=" + $('[name=returnPassword]').val(), function (data)
        {
            if (data == 'ok')
            {
                currentStep = 3;
                clickedStepThree();
            }
            else
            {
                alert("Invalid login details entered");
            }
        });
    });

    $("#tdTarget").on('keydown', '#txtScanItem', function (e)
    {

        if (e.keyCode == 13)
        {
            $('#txtMultiBarcodes').append($('#txtScanItem').val() + "\n");
            $('#txtScanItem').val('');
            e.preventDefault();
        }

    });

    $('#btnScanBarcodesNext').live("click", function ()
    {
        scannedItems = $('#txtMultiBarcodes').val();
        var lines = $('#txtMultiBarcodes').val().split('\n');
        var items = '';
        for (var i = 0; i < lines.length; i++)
        {
            if (lines[i] != '')
            {
                items = items + "BC" + i + "=" + lines[i] + "&";
            }
        }
        items = items.substr(0, items.length - 1);
        itemsBarcodes = items;
        $("#tdStepThreeResult").text(lines.length - 1 + " items");
        currentStep = 4;
        clickedStepFour();
    });

    $("#tdTarget").on("click", '#optToUser', function ()
    {
        $("#tdStepFourResult").text("User");
        $('#tdStepFive').text("Login Details");
        relocateTo = "User";
        currentStep = 5;
        clickedStepFive();
    });

    $("#tdTarget").on("click", '#btnStepFiveShelfNext', function ()
    {
        $("#tdStepFiveResult").text($('[name=toShelfBarcodeVal]').val());
        toShelfBarcode = $('[name=toShelfBarcodeVal]').val();
        currentStep = 6;
        if ($('[name=toShelfBarcodeVal]').val() != "")
        {
            clickedStepSix();
        }
        else
        {
            alert("Please scan a shelf.");
        }
    });

    $("#tdTarget").on("click", '#btnStepFiveUserNext', function ()
    {
        $("#tdStepFiveResult").text($("#dropdownTo :selected").text());
        logUserReceive = $("#tdStepFiveResult").text();
        toLoginSelectedValue = $("#dropdownTo :selected").val();
        toLoginPassword = $('[name=ToUserPassword]').val();
        toLoginDescription = $('[name=UserDescriptionValTo]').val();
        currentStep = 6;
        $.get("mobile.php?pid=users&fid=loginDetails&firstNameReceiver=" + $('#dropdownTo :selected').val() + "&returnPassword=" + $('[name=ToUserPassword]').val(), function (data)
        {
            if (data == 'ok')
            {
                clickedStepSix();
            }
            else
            {
                alert("Invalid login details entered");
            }
        });
    });

    $('#optToShelf').live("click", function ()
    {
        $("#tdStepFourResult").text("Shelf");
        relocateTo = "Shelf";
        $('#tdStepFive').text("Scan Shelf");
        currentStep = 5;
        clickedStepFive();
    });

    $('#optToPending').live("click", function ()
    {
        $("#tdStepFourResult").text("Pending");
        $('#tdStepFive').text("Later");
        relocateTo = "Pending";
        currentStep = 5;
        clickedStepFive();
    });

    $('input[name=chkConfirm]').live('change', function ()
    {
        if ($(this).is(':checked'))
        {
            $('#btnFinish').show();
        }
        else
        {
            $('#btnFinish').hide();
        }
    });

    $('#btnFinish').live("click", function ()
    {
        $('#btnFinish').button('loading');
        if (relocatefrom == "User" && relocateTo == "User")
        {
            relocateType = "UserToUser";
            params += "&firstNameReturn=" + fromLoginSelectedValue + "&pWord=" + fromLoginPassword + "&returnDescription=" + fromLoginDescription + "&logUserReturn=" + logUserReturn +
                    "&firstNameReceiveUser=" + toLoginSelectedValue + "&receivePassword=" + toLoginPassword + "&receiveDescription=" + toLoginDescription + "&logUserReceive=" + logUserReceive;
        }
        else if (relocatefrom == "User" && relocateTo == "Shelf")
        {
            relocateType = "UserToShelf";
            params += "&firstNameReturn=" + fromLoginSelectedValue + "&pWord=" + fromLoginPassword + "&returnDescription=" + fromLoginDescription + "&toShelfBarcode=" + toShelfBarcode;
        }
        else if (relocatefrom == "Shelf" && relocateTo == "User")
        {
            relocateType = "ShelfToUser";
            params += "&fromShelfBarcode=" + fromShelfBarcode + "&firstNameReceiveUser=" + toLoginSelectedValue + "&receivePassword=" + toLoginPassword + "&receiveDescription=" + toLoginDescription + "&username=" + logUserReceive;
        }
        else if (relocatefrom == "Shelf" && relocateTo == "Shelf")
        {
            relocateType = "ShelfToShelf";
            params += "&fromShelfBarcode=" + fromShelfBarcode + "&toShelfBarcode=" + toShelfBarcode;
        }
        else
        {
            relocateType = "later";
            //Validation to or from?
        }
        $.get("mobile.php?pid=relocate&fid=userSpecific&action=" + relocateType + "&" + itemsBarcodes + params, function (response)
        {
            alert(response);
            location.reload();
        });
        $('#btnFinish').button('reset');
    });
    //**************************************************************************
    //NEEDS TO BE COPIED TO INTRANET JAVASCRIPT FILE
    $('#btnAddConsumble').click(function ()
    {
        $.get("mobile.php?pid=consumable&fid=add&name=" + $('[name=tbName]').val() + "&description=" + $('[name=tbdescription]').val() + "&detail=" + $('[name=tbDetail]').val() + "&minQty=" + $('[name=minQty]').val() + "&maxQty=" + $('[name=maxQty]').val() + "&qtyInStore=" + $('[name=qtyInStore]').val() + "&supplier=" + $('[name=tbSupplierCode]').val() + "&nettValue=" + $('[name=nettValue]').val(), function (response)
        {
            alert(response);
            location.reload();
        });
    });
    var startDate = new Date('2014-01-01');
    var FromEndDate = new Date();
    var ToEndDate = new Date();

    ToEndDate.setDate(ToEndDate.getDate() + 365);
    $("[name='my-checkbox']").bootstrapSwitch();

    $('.from_date').datepicker({
        weekStart: 1,
        startDate: '2014-01-01',
        format: 'yyyy-mm-dd',
        endDate: FromEndDate,
        autoclose: true
    })
            .on('changeDate', function (selected) {
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                $('.to_date').datepicker('setStartDate', startDate);
            });
    $('.to_date')
            .datepicker({
                weekStart: 1,
                startDate: startDate,
                format: 'yyyy-mm-dd',
                endDate: ToEndDate,
                autoclose: true
            })
            .on('changeDate', function (selected) {
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                $('.from_date').datepicker('setEndDate', FromEndDate);
            });

    $('.chosen-select').chosen(
            {
                //inherit_select_classes: "true"
            });
    $('.chosen-select').chosen().on('chosen:showing_dropdown', function (e) //on dropdown
    {
        var select = $(this).chosen();
        if ($(select).hasClass("ddIntranetProjects"))
        {
            $.get("mobile.php?pid=intranetDropdown&fid=project&selectVal=" + $(this).chosen().val(), function (response)
            {
                $(select).chosen().append(response);
                $(select).trigger("chosen:updated");//refresh dropdown
            });
        }
        if ($(select).hasClass("ddAutonetProjects"))
        {
            $.get("mobile.php?pid=autonetDropdown&fid=project&selectVal=" + $(this).chosen().val(), function (response)
            {
                $(select).chosen().append(response);
                $(select).trigger("chosen:updated");//refresh dropdown
            });
        }
    });
    $('.tbOrderItemsContents').slideToggle("300");
    $('.tbOrderItemsHeading').slideToggle("300");
    $('#marryTable').tableFilter();
    $('#tblAwaitingRelocation').tableFilter();
    $('#tblRelocateConsumable').tableFilter();
    $('#tblConsumable').tableFilter();
    $('#tblItemHistory').tableFilter();
    $('#tblUsers').tableFilter();
    $(".test bordered").hide();
    $('#drps4u2s_chosen').css("width", "252px");
    $('#drpmmu2s_chosen').css("width", "252px");
    $('#drps4u2u_chosen').css("width", "252px");
    $('#drpmmu2u_chosen').css("width", "252px");
    $('#drps4u2u2_chosen').css("width", "252px");
    $('#drpmmu2u2_chosen').css("width", "252px");
    $('#drps4s2u_chosen').css("width", "252px");
    $('#drpmms2u_chosen').css("width", "252px");
    $('#drps4AwaitRelocateUser_chosen').css("width", "200px");
    $('#drpmmAwaitRelocateUser_chosen').css("width", "200px");

    //Add pagination tables
    $('#tblConsumable').pageMe({pagerSelector: '#marryPager', showPrevNext: true, hidePageNumbers: false, perPage: 20});
    $('#tblRelocateConsumable').pageMe({pagerSelector: '#marryPager', showPrevNext: true, hidePageNumbers: false, perPage: 20});
    $('#userTblBody').pageMe({pagerSelector: '#UserPager', showPrevNext: true, hidePageNumbers: false, perPage: 20});
    $('#shelfTblBody').pageMe({pagerSelector: '#shelfPager', showPrevNext: true, hidePageNumbers: false, perPage: 20});
    $('#logTblBody').pageMe({pagerSelector: '#logPager', showPrevNext: true, hidePageNumbers: false, perPage: 20});

    //End of pagination

    (function () {
        var s,
                OrderModule = {
                    settings: {
                        orderItemList: $('.rowOrderItems'),
                        orderItem: null,
                        orderDetail: null
                    },
                    init: function () {
                        s = this.settings;
                        this.bindActions();
                    },
                    bindActions: function () {
                        s.orderItemList.on('click', function () {
                            OrderModule.rowClicked(this);
                        });
                    },
                    rowClicked: function (sender) {
                        if (s.orderItem != null) {
                            s.orderItem.removeClass('rowHighlight');
                        }
                        if (s.orderDetail != null) {
                            s.orderDetail.hide();
                        }
                        s.orderItem = $(sender);
                        s.orderDetail = $('.' + s.orderItem.attr('id'));
                        s.orderItem.addClass('rowHighlight');
                        s.orderDetail.show();
                    }
                };
        OrderModule.init();
    })();

    $('.tblAwaitingRelocationConsumableRow').click(function (e)
    {
        $(this).closest("tr").find('[name=qtyReceived]').focus().select();
    });

    //Tracked return items table row click
    $('#tbReportByOrder tr').click(function (event)
    {
        var ItemId = $(this).closest("tr").attr('id');
        var orderNumber = $(this).closest('tr').find('[name=oNumber]').val();
        window.location = "index.php?pid=report&fid=order&orderNumber=" + orderNumber + "&itemId=" + ItemId;
    });

    $(".divRowClick").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    var myWindow;

    function openCenteredWindow(url)
    {
        var width = 1000;
        var height = 800;
        var left = parseInt((screen.availWidth / 2) - (width / 2));
        var top = parseInt((screen.availHeight / 2) - (height / 2));
        var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
        myWindow = window.open(url, "subWind", windowFeatures);
    }

    function closeWindow()
    {
        myWindow.close();
    }
    //Send submit on hasOrderNum radio button click
    $('input[name=radDbType]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('input[name=myDb]').val('s4');
//            $('#drpmm').hide();
//            $('#drps4').show();
            $('#drpmm_chosen').hide();
            $('#drps4_chosen').show();
            $('#txtSelectUser').show();
            $('#txtPwdEnter').show();
        }
        if (db == "mm")
        {
            $('input[name=myDb]').val('mm');
            $('#txtSelectUser').show();
            $('#drpmm_chosen').show();
            $('#drps4_chosen').hide();
            $('#txtPwdEnter').show();
        }
    });
    $('input[name=radDb]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('input[name=staffcodeValue]').val('s4');
            $('#drpNoOrdermm_chosen').hide();
            $('#drpNoOrders4_chosen').show();
            $('#txtSelectUsername').show();
            $('#txtEnterPwd').show();
        }
        if (db == "mm")
        {
            $('input[name=staffcodeValue]').val('mm');
            $('#txtSelectUsername').show();
            $('#drpNoOrdermm_chosen').show();
            $('#drpNoOrders4_chosen').hide();
            $('#txtEnterPwd').show();
        }
    });
    $('input[name=radDb2]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('#drpConsumablemm_chosen').hide();
            $('#drpConsumables4_chosen').show();
            $('#SelectUsername2').show();
            $('#EnterPwd2').show();
        }
        if (db == "mm")
        {
            $('#SelectUsername2').show();
            $('#drpConsumablemm_chosen').show();
            $('#drpConsumables4_chosen').hide();
            $('#EnterPwd2').show();
        }
    });

    $('input[name=radDbTypeShelfToUser]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('input[name=myDb]').val('s4');
            $('#drps4s2u_chosen').show();
            $('#drps4s2u_chosen').css("width", "200px");
            $('#drpmms2u_chosen').hide();
            $('#txtSelectUseru2s').hide();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUseru2u2').hide();
            $('#txtSelectUsers2u').show();
        }
        if (db == "mm")
        {
            $('input[name=myDb]').val('mm');
            $('#drps4s2u_chosen').hide();
            $('#drpmms2u_chosen').show();
            $('#drpmms2u_chosen').css("width", "200px");
            $('#txtSelectUseru2s').hide();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUseru2u2').hide();
            $('#txtSelectUsers2u').show();
        }
    });

    $('input[name=radDbTypeUserToUser]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('input[name=myDb]').val('s4');
            $('#drps4u2u_chosen').show();
            $('#drps4u2u_chosen').css("width", "200px");
            $('#drpmmu2u_chosen').hide();
            $('#txtSelectUseru2s').hide();
            $('#txtSelectUseru2u2').hide();
            $('#txtSelectUsers2u').hide();
            $('#txtSelectUseru2u').show();
        }
        if (db == "mm")
        {
            $('input[name=myDb]').val('s4');
            $('#drps4u2u_chosen').hide();
            $('#drpmmu2u_chosen').show();
            $('#drpmmu2u_chosen').css("width", "200px");
            $('#txtSelectUseru2s').hide();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUseru2u2').hide();
            $('#txtSelectUseru2u').show();
        }
    });

    $('input[name=radDbTypeUserToUser2]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('input[name=receiveDb]').val('s4');
            $('#drps4u2u2_chosen').show();
            $('#drps4u2u2_chosen').css("width", "200px");
            $('#drpmmu2u2_chosen').hide();
            $('#txtSelectUseru2u2').show();
            $('#txtSelectUseru2s').hide();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUsers2u').hide();
        }
        if (db == "mm")
        {
            $('input[name=receiveDb]').val('mm');
            $('#drps4u2u2_chosen').hide();
            $('#drpmmu2u2_chosen').show();
            $('#drpmmu2u2_chosen').css("width", "200px");
            $('#txtSelectUseru2s').hide();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUseru2u2').show();
            $('#txtSelectUsers2u').hide();
        }
    });

    $('input[name=radRelocateAwaitingUser]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('#drps4AwaitRelocateUser_chosen').show();
            $('#drps4AwaitRelocateUser_chosen').css("width", "200px");
            $('#drpmmAwaitRelocateUser_chosen').hide();
            $('#awaitingRelocateUserPleaseSelectText').show();
        }
        if (db == "mm")
        {
            $('#drps4AwaitRelocateUser_chosen').hide();
            $('#drpmmAwaitRelocateUser_chosen').show();
            $('#drpmmAwaitRelocateUser_chosen').css("width", "200px");
            $('#awaitingRelocateUserPleaseSelectText').show();
        }
    });

    $('input[name=btnToShelfLocation]').click(function ()
    {
        $('.divChooseLocation').hide();
        $('.divToShelf').show();
        $('#btnCancelLocationSelection').hide();
        $('#btnSaveToUser').show();
        $('[name=locationType]').val("shelf");
    });
    $('input[name=btnToUserLocation]').click(function ()
    {
        $('.divToUser').show();
        $('.divChooseLocation').hide();
        $('#btnCancelLocationSelection').hide();
        $('#btnSaveToUser').show();
        $('[name=locationType]').val("user");
    });
    $('#btnContinueRelocate').click(function ()
    {
        $('.divToUser').hide();
        $('.divToShelf').hide();
        $('.divChooseLocation').show();
        $('#btnCancelLocationSelection').show();
        $('#btnSaveToUser').hide();
    });

    $('input[name=radDbTypeUserToShelf]').click(function ()
    {
        var db = $(this).val();
        if (db == "s4")
        {
            $('#drps4u2s_chosen').show();
            $('input[name=myDb]').val('s4');
            $('#drps4u2s_chosen').css("width", "200px");
            $('#drpmmu2s_chosen').hide();
            $('#txtSelectUseru2s').show();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUseru2u2').hide();
            $('#txtSelectUsers2u').hide();
        }
        if (db == "mm")
        {
            $('input[name=myDb]').val('mm');
            $('#drps4u2s_chosen').hide();
            $('#drpmmu2s_chosen').show();
            $('#drpmmu2s_chosen').css("width", "200px");
            $('#txtSelectUseru2s').show();
            $('#txtSelectUseru2u').hide();
            $('#txtSelectUseru2u2').hide();
            $('#txtSelectUsers2u').hide();
        }
    });

    $('[name=staffcode]').change(function ()
    {
        $('[name=username]').val($('[name=staffcode] option:selected').text());
    });


    $('#ddReportProjectS4').change(function ()
    {
        $('#ddReportProjectAuto').val("").trigger('chosen:updated');
    });

    $('#ddReportProjectAuto').change(function ()
    {
        $('#ddReportProjectS4').val("").trigger('chosen:updated');
    });

    $('#drps4u2u2').change(function ()
    {
        $('input[name=staffcodeValReceiver]').val($(this).val());
        $('input[name=receiveUser]').val($('#drps4u2u2 option:selected').text());
    });

    $('#drpmmu2u2').change(function ()
    {
        $('input[name=staffcodeValReceiver]').val($(this).val());
        $('input[name=receiveUser]').val($('#drpmmu2u2 option:selected').text());
    });

    $('#drps4u2u').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
        $('input[name=returnUser]').val($('#drps4u2u option:selected').text());
    });

    $('#drpmmu2u').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
        $('input[name=returnUser]').val($('#drpmmu2u option:selected').text());
    });

    $('#drps4s2u').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
        $('input[name=username]').val($('#drps4s2u option:selected').text());
    });

    $('#drpmms2u').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
        $('input[name=username]').val($('#drpmms2u option:selected').text());
    });

    $('#drps4u2s').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
    });

    $('#drpmmu2s').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
    });

    $('#drps4').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
    });

    $('#drpmm').change(function ()
    {
        $('input[name=staffcodeVal]').val($(this).val());
    });

    $('#drpNoOrders4').change(function ()
    {
        $('input[name=staffcodeValue]').val($(this).val());
    });
    $('#drpNoOrdermm').change(function ()
    {
        $('input[name=staffcodeValue]').val($(this).val());
    });

    $('#drpConsumables4').change(function ()
    {
        $('input[name=staffcodeValue]').val($(this).val());
    });
    $('#drpConsumablemm').change(function ()
    {
        $('input[name=staffcodeValue]').val($(this).val());
    });
    
    //Order Items**************************************************************
    $('.ScanBarcode').keypress(function (e)
    {
        if (e.keyCode == 13)
        {
            var inputs = $(this).closest('form').find(':input:visible');
            inputs.eq(inputs.index(this) + 1).focus();
            return false;//stop default submit trigger
        }
    });
    //To render the input device to multiple email input using BootStrap icon
    $('.example_emailBS').multiple_emails({position: "bottom"});
    //OR $('#example_emailBS').multiple_emails("Bootstrap");

    //Shows the value of the input device, which is in JSON format
    $('#current_emailsBS').text($('#example_emailBS').val());
//End of document ready*********************************************************
});

