<?php
ob_start();
header('Content-Encoding: gzip');
include "sql.php";
include "functions.php";

$check_out_accordion = 'acc-inactive';
$check_in_accordion = 'acc-inactive';

$q = "SELECT id, username AS description FROM user WHERE username NOT IN ('unassigned') ORDER by username ASC";
$user_array = build_result_array($q);

$q = "SELECT item.id, item.serial_number, item.description AS item_description, item.last_access, item.checked_out, item.quantity, item.item_group_id, item.item_type_id, item.project_id, item.shelf_id, 
    item_group.description AS item_group, item.access_comment,
    item_type.product_code, item_type.description AS product_description, CONCAT(item_type.product_code, ' &ndash; ' , item_type.description) AS product, 
    project.description AS project, 
    CONCAT(shelf.code, ' @ ' ,storeroom.description) AS location
    FROM item
    INNER JOIN item_group ON item.item_group_id=item_group.id
    INNER JOIN item_type ON item.item_type_id=item_type.id
    INNER JOIN project ON item.project_id=project.id
    INNER JOIN shelf ON item.shelf_id=shelf.id
    INNER JOIN storeroom ON storeroom.id=shelf.storeroom_id
    WHERE item.checked_out < item.quantity
    ORDER BY item.last_access DESC";

$result = null;
$rows = sql_select($result, $q); ?>

<div id=checkout class='accordion<?php echo ' ' . $check_out_accordion; ?>'>
<input class='smlbutton' type='button' value=''>
<h3>Stores</h3>

<?php if ($rows > 0) { ?>
    <div class='tablewrapper'>
        <div class='table'>
            <table cellpadding='0' cellspacing='0'>
                <thead>
                    <tr class='thead'>
						<th class='first w140'>Last modified</th>
                        <th class='w140'>Serial Number</th>
                        <th class='w140'>Description</th>
                        <th class='w140'>Product</th>
                        <th class='w140'>Location</th>
                        <th>Project</th>
                        <th>Group</th>
                        <th>Stores</th>
						<th class='w120'>Comment</th>
                        <th class='edit' filter='false'></th>
                        <th class='last w120'>Check out quantity</th>
                        <th class='edit' filter='false'></th>
                        <th class='empty'></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($result as $row) {
                        $id = $row['id'];
                        $serial_number = $row['serial_number'];
                        $description = $row['item_description'];
						$last_access = $row['last_access'];
                        $checked_out = $row['checked_out'];
                        $quantity = $row['quantity'];
                        $group = $row['item_group'];
                        $product_code = $row['product_code'];
                        $product_description = $row['product_description'];
                        $product = $row['product'];
                        $project = $row['project'];
                        $location = $row['location'];
                        $quantity_in_store = $quantity - $checked_out;
						$access_comment = $row['access_comment'];
						$tc = "tmp_". $id;
						$fc = "fin_".$id;
						?>
                        <tr>
                            
								<td class='w120'>
                                    <div><?php echo $last_access; ?></div>
                                </td>
								<td class='w140'>
                                    
                                    <?php if ($serial_number == '') {
                                        echo '<div>n/a</div>';
                                    }
                                    else {
                                        echo '<div>' . $serial_number . '</div>';
                                    } ?>
                                </td>
                                <td class='w140'>
                                    
                                    <div><?php echo $description; ?></div>
                                </td>
                                <td class='w140'>
                                    
                                    
                                    <div><?php echo $product; ?></div>
                                </td>
                                <td class='w140'>
                                    <div><?php echo $location; ?></div>
                                </td>
                                <td>
                                    <div><?php echo $project; ?></div>
                                </td>
                                <td>
                                    <div><?php echo $group; ?></div>
                                </td>
                                <td>
                                    
                                    <div><?php echo $quantity_in_store; ?></div>
                                </td>
								<td class='view w120'>
                                    <div><?php echo $access_comment; ?></div>
                                </td>
                                <td class='edit w120'>
                                    <input class='<?php echo $tc;?>'  type='text' name='access_comment' value='<?php echo $access_comment; ?>'>
                                </td>
                                <td class='view w120'>
                                    <div>&ndash;</div>
                                </td>
                                <td class='edit w120'>
                                    <input class='<?php echo $tc;?>'  type='text' name='check_out_quantity' value='1'>
                                </td>
                                <td class='button'>
									<form name='checkoutform<?php echo $id ?>' action='index.php?pid=inventory_tracking' method='post'>
										<input type='hidden' name='serial_number' value='<?php echo $serial_number; ?>'>
										<input type='hidden' name='part_description' value='<?php echo $description; ?>'>
										<input type='hidden' name='product_code' value='<?php echo $product_code; ?>'>
										<input type='hidden' name='product_description' value='<?php echo $product_description; ?>'>
										<input type='hidden' name='quantity_in_store' value='<?php echo $quantity_in_store; ?>'>
										<input type='hidden' name='checkout' value='<?php echo $id; ?>'>
										<input type='hidden' name='checked_out' value='<?php echo $checked_out; ?>'>
										<input type='hidden' name='check_out_accordion' value=''>
										<input type='hidden' name='check_in_accordion' value=''>
										<input class='<?php echo $fc;?>'  type='hidden' name='access_comment' value='<?php echo $access_comment; ?>'>
										<input class='<?php echo $fc;?>'  type='hidden' name='check_out_quantity' value='1'>
									</form>
                                    <input class='edit' type='button' name="<?php echo str_replace("fin_", "", $fc);?>" value='Check out'>
                                </td>
                            
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
<?php }
else {
    echo '<p>No parts in store.</p>';
} ?>
    </div>
</div>

<?php
$q = "SELECT item.id, item.serial_number, item.description AS item_description, item.last_access, item.checked_out, item.item_group_id, item.item_type_id, item.project_id, item.shelf_id, item.user_id, 
    item_group.description AS item_group, item.access_comment,
    item_type.product_code, item_type.description AS product_description, CONCAT(item_type.product_code, ' &ndash; ' , item_type.description) AS product, 
    project.description AS project, 
    CONCAT(shelf.code, ' @ ' ,storeroom.description) AS location, 
    user.username
    FROM item
    INNER JOIN item_group ON item.item_group_id=item_group.id
    INNER JOIN item_type ON item.item_type_id=item_type.id
    INNER JOIN project ON item.project_id=project.id
    INNER JOIN shelf ON item.shelf_id=shelf.id
    INNER JOIN user ON item.user_id = user.id
    INNER JOIN storeroom ON storeroom.id=shelf.storeroom_id
    WHERE item.checked_out > 0
    ORDER BY item.last_access DESC";

$result = null;
$rows = sql_select($result, $q); ?>

<div id=checkin class='accordion<?php echo ' ' . $check_in_accordion; ?>'>
<input class='smlbutton' type='button' value=''>
<h3>Dispatches</h3>

<?php if ($rows > 0) { ?>
    <div class='tablewrapper'>
        <div class='table'>
            <table cellpadding='0' cellspacing='0'>
                <thead>
                    <tr class='thead'>
                        <th class='first w120'>Last modified</th>
                        <th class='w140'>Serial Number</th>
                        <th class='w140'>Description</th>
                        <th class='w140'>Product</th>
                        <th class='w140'>Location</th>
                        <th>Project</th>
                        <th>Group</th>
                        <th>Dispatches</th>
						<th class='w120'>Comment</th>
                        <th class='edit' filter='false'></th>
                        <th class='last w120'>Check in quantity</th>
                        <th class='edit' filter='false'></th>
                        <th class='empty'></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($result as $row) {
                        $id = $row['id'];
                        $serial_number = $row['serial_number'];
                        $description = $row['item_description'];
                        $last_access = $row['last_access'];
                        $checked_out = $row['checked_out'];
                        $group = $row['item_group'];
                        $product_code = $row['product_code'];
                        $product_description = $row['product_description'];
                        $product = $row['product'];
                        $project = $row['project'];
                        $location = $row['location'];
						$access_comment = $row['access_comment'];	
						$tc = "tmp_". $id;
						$fc = "fin_".$id;
						?>
                        <tr>
                            
                                <td class='w120'>
                                    <div><?php echo $last_access; ?></div>
                                </td>
                                <td class='w140'>
                                    
                                    <?php if ($serial_number == '') {
                                        echo '<div>n/a</div>';
                                    }
                                    else {
                                        echo '<div>' . $serial_number . '</div>';
                                    } ?>
                                </td>
                                <td class='w140'>
                                    
                                    <div><?php echo $description; ?></div>
                                </td>
                                <td class='w140'>
                                    
                                    <div><?php echo $product; ?></div>
                                </td>
                                <td class='w140'>
                                    <div><?php echo $location; ?></div>
                                </td>
                                <td>
                                    <div><?php echo $project; ?></div>
                                </td>
                                <td>
                                    <div><?php echo $group; ?></div>
                                </td>
                                <td>
                                    
                                    <div><?php echo $checked_out; ?></div>
                                </td>
								<td class='view w120'>
                                    <div><?php echo $access_comment; ?></div>
                                </td>
                                <td class='edit w120'>
                                    <input class='<?php echo $tc;?>' type='text' name='access_comment' value='<?php echo $access_comment; ?>'>
                                </td>
                                <td class='view w120'>
                                    <div>&ndash;</div>
                                </td>
                                <td class='edit w120'>
                                    <input class='<?php echo $tc;?>' type='text' name='check_in_quantity' value='1'>
                                </td>
                                <td class='button'>
									<form name='checkinform<?php echo $id ?>' action='index.php?pid=inventory_tracking' method='post'>
										<input type='hidden' name='serial_number' value='<?php echo $serial_number; ?>'>
										<input type='hidden' name='part_description' value='<?php echo $description; ?>'>
										<input type='hidden' name='product_code' value='<?php echo $product_code; ?>'>
										<input type='hidden' name='product_description' value='<?php echo $product_description; ?>'>
										<input type='hidden' name='checked_out' value='<?php echo $checked_out; ?>'>
										<input class='<?php echo $fc;?>'  type='hidden' name='access_comment' value='<?php echo $access_comment; ?>'>
										<input class='<?php echo $fc;?>'  type='hidden' name='check_in_quantity' value='1'>
										<input type='hidden' name='checkin' value='<?php echo $id; ?>'>
										<input type='hidden' name='check_out_accordion' value=''>
										<input type='hidden' name='check_in_accordion' value=''>
									</form>
                                    <input class='edit' name="<?php echo str_replace("fin_", "", $fc);?>" type='button' value='Check in'>
                                </td>
                            
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    <?php }
else {
    echo '<p>No parts checked out.</p>';
} ?>
    </div>
</div>
<?php
$gzip_contents = ob_get_contents();
    ob_end_clean();

    $gzip_size = strlen($gzip_contents);
    $gzip_crc = crc32($gzip_contents);

    $gzip_contents = gzcompress($gzip_contents, 9);
    $gzip_contents = substr($gzip_contents, 0, strlen($gzip_contents) - 4);

    echo "\x1f\x8b\x08\x00\x00\x00\x00\x00";
    echo $gzip_contents;
    echo pack('V', $gzip_crc);
    echo pack('V', $gzip_size);



?>