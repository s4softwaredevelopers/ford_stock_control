namespace s4_Scanner
{
    partial class Capture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.labelMessage = new System.Windows.Forms.Label();
            this.panelUser = new System.Windows.Forms.Panel();
            this.panelUserDetails = new System.Windows.Forms.Panel();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.comboBoxUsers = new System.Windows.Forms.ComboBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.panelUserTitle = new System.Windows.Forms.Panel();
            this.labelUserTitle = new System.Windows.Forms.Label();
            this.panelUserLogin = new System.Windows.Forms.Panel();
            this.buttonUserClose = new System.Windows.Forms.Button();
            this.buttonUserLogin = new System.Windows.Forms.Button();
            this.panelCapture = new System.Windows.Forms.Panel();
            this.labelCaptureTitle = new System.Windows.Forms.Label();
            this.buttonUandP = new System.Windows.Forms.Button();
            this.buttonPrint = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.panelLine2 = new System.Windows.Forms.Panel();
            this.labelQuantity = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.numericUpDownQuantity = new System.Windows.Forms.NumericUpDown();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelHeader.SuspendLayout();
            this.panelFooter.SuspendLayout();
            this.panelUser.SuspendLayout();
            this.panelUserDetails.SuspendLayout();
            this.panelUserTitle.SuspendLayout();
            this.panelUserLogin.SuspendLayout();
            this.panelCapture.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.buttonClear);
            this.panelHeader.Controls.Add(this.buttonClose);
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClear.Location = new System.Drawing.Point(161, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(30, 20);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "R";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(211, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(30, 20);
            this.buttonClose.TabIndex = 10;
            this.buttonClose.Text = " x";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Capture Item";
            // 
            // panelFooter
            // 
            this.panelFooter.Controls.Add(this.labelMessage);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 300);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(240, 20);
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(5, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(230, 20);
            this.labelMessage.Text = "Message";
            // 
            // panelUser
            // 
            this.panelUser.Controls.Add(this.panelUserDetails);
            this.panelUser.Controls.Add(this.panelUserTitle);
            this.panelUser.Controls.Add(this.panelUserLogin);
            this.panelUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUser.Location = new System.Drawing.Point(0, 20);
            this.panelUser.Name = "panelUser";
            this.panelUser.Size = new System.Drawing.Size(240, 280);
            this.panelUser.Visible = false;
            // 
            // panelUserDetails
            // 
            this.panelUserDetails.Controls.Add(this.textBoxPassword);
            this.panelUserDetails.Controls.Add(this.comboBoxUsers);
            this.panelUserDetails.Controls.Add(this.labelPassword);
            this.panelUserDetails.Controls.Add(this.labelUser);
            this.panelUserDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserDetails.Location = new System.Drawing.Point(0, 22);
            this.panelUserDetails.Name = "panelUserDetails";
            this.panelUserDetails.Size = new System.Drawing.Size(240, 198);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxPassword.Location = new System.Drawing.Point(5, 77);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(230, 26);
            this.textBoxPassword.TabIndex = 130;
            // 
            // comboBoxUsers
            // 
            this.comboBoxUsers.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.comboBoxUsers.Location = new System.Drawing.Point(5, 24);
            this.comboBoxUsers.Name = "comboBoxUsers";
            this.comboBoxUsers.Size = new System.Drawing.Size(230, 27);
            this.comboBoxUsers.TabIndex = 129;
            // 
            // labelPassword
            // 
            this.labelPassword.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelPassword.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelPassword.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelPassword.Location = new System.Drawing.Point(5, 56);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(230, 21);
            this.labelPassword.Text = "Please enter your Password:";
            // 
            // labelUser
            // 
            this.labelUser.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelUser.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelUser.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelUser.Location = new System.Drawing.Point(5, 4);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(230, 20);
            this.labelUser.Text = "Please select Username:";
            // 
            // panelUserTitle
            // 
            this.panelUserTitle.Controls.Add(this.labelUserTitle);
            this.panelUserTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUserTitle.Location = new System.Drawing.Point(0, 0);
            this.panelUserTitle.Name = "panelUserTitle";
            this.panelUserTitle.Size = new System.Drawing.Size(240, 22);
            // 
            // labelUserTitle
            // 
            this.labelUserTitle.BackColor = System.Drawing.SystemColors.MenuText;
            this.labelUserTitle.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelUserTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelUserTitle.Location = new System.Drawing.Point(5, 2);
            this.labelUserTitle.Name = "labelUserTitle";
            this.labelUserTitle.Size = new System.Drawing.Size(230, 18);
            this.labelUserTitle.Text = "Enter User Details";
            this.labelUserTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelUserLogin
            // 
            this.panelUserLogin.Controls.Add(this.buttonUserClose);
            this.panelUserLogin.Controls.Add(this.buttonUserLogin);
            this.panelUserLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelUserLogin.Location = new System.Drawing.Point(0, 220);
            this.panelUserLogin.Name = "panelUserLogin";
            this.panelUserLogin.Size = new System.Drawing.Size(240, 60);
            // 
            // buttonUserClose
            // 
            this.buttonUserClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonUserClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUserClose.Location = new System.Drawing.Point(5, 36);
            this.buttonUserClose.Name = "buttonUserClose";
            this.buttonUserClose.Size = new System.Drawing.Size(230, 20);
            this.buttonUserClose.TabIndex = 133;
            this.buttonUserClose.Text = "Cancel";
            this.buttonUserClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // buttonUserLogin
            // 
            this.buttonUserLogin.BackColor = System.Drawing.Color.LightGray;
            this.buttonUserLogin.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUserLogin.Location = new System.Drawing.Point(5, 3);
            this.buttonUserLogin.Name = "buttonUserLogin";
            this.buttonUserLogin.Size = new System.Drawing.Size(230, 30);
            this.buttonUserLogin.TabIndex = 132;
            this.buttonUserLogin.Text = "Login";
            this.buttonUserLogin.Click += new System.EventHandler(this.buttonUserLogin_Click);
            // 
            // panelCapture
            // 
            this.panelCapture.Controls.Add(this.labelCaptureTitle);
            this.panelCapture.Controls.Add(this.buttonUandP);
            this.panelCapture.Controls.Add(this.buttonPrint);
            this.panelCapture.Controls.Add(this.buttonUpdate);
            this.panelCapture.Controls.Add(this.panelLine2);
            this.panelCapture.Controls.Add(this.labelQuantity);
            this.panelCapture.Controls.Add(this.label1);
            this.panelCapture.Controls.Add(this.textBoxDescription);
            this.panelCapture.Controls.Add(this.numericUpDownQuantity);
            this.panelCapture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCapture.Location = new System.Drawing.Point(0, 20);
            this.panelCapture.Name = "panelCapture";
            this.panelCapture.Size = new System.Drawing.Size(240, 280);
            // 
            // labelCaptureTitle
            // 
            this.labelCaptureTitle.BackColor = System.Drawing.SystemColors.MenuText;
            this.labelCaptureTitle.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelCaptureTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelCaptureTitle.Location = new System.Drawing.Point(5, 5);
            this.labelCaptureTitle.Name = "labelCaptureTitle";
            this.labelCaptureTitle.Size = new System.Drawing.Size(230, 32);
            this.labelCaptureTitle.Text = "Enter the Details of the item for its Capture";
            this.labelCaptureTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonUandP
            // 
            this.buttonUandP.BackColor = System.Drawing.Color.LightGray;
            this.buttonUandP.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonUandP.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUandP.Location = new System.Drawing.Point(88, 253);
            this.buttonUandP.Name = "buttonUandP";
            this.buttonUandP.Size = new System.Drawing.Size(65, 20);
            this.buttonUandP.TabIndex = 103;
            this.buttonUandP.Text = "U && P";
            this.buttonUandP.Click += new System.EventHandler(this.buttonUandP_Click);
            // 
            // buttonPrint
            // 
            this.buttonPrint.BackColor = System.Drawing.Color.LightGray;
            this.buttonPrint.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonPrint.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonPrint.Location = new System.Drawing.Point(170, 253);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(65, 20);
            this.buttonPrint.TabIndex = 102;
            this.buttonPrint.Text = "Print";
            this.buttonPrint.Click += new System.EventHandler(this.buttonPrint_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.LightGray;
            this.buttonUpdate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonUpdate.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUpdate.Location = new System.Drawing.Point(5, 253);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(65, 20);
            this.buttonUpdate.TabIndex = 101;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // panelLine2
            // 
            this.panelLine2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panelLine2.Location = new System.Drawing.Point(0, 248);
            this.panelLine2.Name = "panelLine2";
            this.panelLine2.Size = new System.Drawing.Size(240, 1);
            // 
            // labelQuantity
            // 
            this.labelQuantity.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelQuantity.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelQuantity.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelQuantity.Location = new System.Drawing.Point(5, 217);
            this.labelQuantity.Name = "labelQuantity";
            this.labelQuantity.Size = new System.Drawing.Size(65, 21);
            this.labelQuantity.Text = "QUANTITY";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(5, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 20);
            this.label1.Text = "DESCRIPTION";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.textBoxDescription.Location = new System.Drawing.Point(5, 62);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(230, 149);
            this.textBoxDescription.TabIndex = 100;
            // 
            // numericUpDownQuantity
            // 
            this.numericUpDownQuantity.Location = new System.Drawing.Point(70, 217);
            this.numericUpDownQuantity.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDownQuantity.Name = "numericUpDownQuantity";
            this.numericUpDownQuantity.Size = new System.Drawing.Size(165, 22);
            this.numericUpDownQuantity.TabIndex = 97;
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label2);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 280);
            this.panelLoader.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(29, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 107);
            this.label2.Text = "Loading";
            // 
            // Capture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelCapture);
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.panelUser);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Capture";
            this.Text = "Capture";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Capture_Load);
            this.panelHeader.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            this.panelUser.ResumeLayout(false);
            this.panelUserDetails.ResumeLayout(false);
            this.panelUserTitle.ResumeLayout(false);
            this.panelUserLogin.ResumeLayout(false);
            this.panelCapture.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Panel panelUser;
        private System.Windows.Forms.Panel panelUserDetails;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.ComboBox comboBoxUsers;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Panel panelUserTitle;
        private System.Windows.Forms.Label labelUserTitle;
        private System.Windows.Forms.Panel panelUserLogin;
        private System.Windows.Forms.Button buttonUserClose;
        private System.Windows.Forms.Button buttonUserLogin;
        private System.Windows.Forms.Panel panelCapture;
        private System.Windows.Forms.Label labelCaptureTitle;
        private System.Windows.Forms.Button buttonUandP;
        private System.Windows.Forms.Button buttonPrint;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Panel panelLine2;
        private System.Windows.Forms.Label labelQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.NumericUpDown numericUpDownQuantity;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label2;
    }
}