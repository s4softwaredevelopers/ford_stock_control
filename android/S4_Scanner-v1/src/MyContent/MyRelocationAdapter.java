package MyContent;

import java.util.List;

import com.example.s4_scanner_v1.R;

import MyContent.RelocateItemContent.RelocateItem;
import android.R.color;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class MyRelocationAdapter extends ArrayAdapter<RelocateItemContent.RelocateItem> {
	  private final Context context;
	  private final  List<RelocateItem> ITEMS;

	  public MyRelocationAdapter(Context context, List<RelocateItem> ITEMS) {
	    super(context, R.layout.rowlayout, ITEMS);
	    this.context = context;
	    this.ITEMS = ITEMS;
	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.label);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    textView.setText(ITEMS.get(position).description);
	    
	    RelocateItem currItem = getItem(position);
	    
	    if (ITEMS.get(position).itemSelected)
	    {
	    	imageView.setImageResource(R.drawable.transparent_green_checkmark);
	    }
	    else
	    {
	    	imageView.setImageResource(color.transparent);
	    }	    

	    return rowView;
	  }
}
