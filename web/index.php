<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors', '1');
include 'includes/functions.php';
include 'includes/sql.php';

session_start();
ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>S4 Stock Control</title>
        
        <link rel='stylesheet' href='styles/stylesheet.css'>
        
        <link rel='stylesheet' href='styles/jquery.fancybox.css'>
        <link rel='stylesheet' href='styles/jquery.fancybox-buttons.css'>
        
        <link rel="stylesheet" href="styles/chosen/chosen.css" >
        
        <link rel='stylesheet' href='styles/ui-redmond/jquery.ui.theme.css'>
        <link rel='stylesheet' href='styles/ui-redmond/jquery-ui.css'>
        
        <script type='text/javascript' src='scripts/jquery-1.9.1.min.js'></script>
        <script type='text/javascript' src='scripts/picnet.table.filter.min.js'></script>
        
        <script type='text/javascript' src='scripts/jquery.fancybox.min.js'></script>
        <script type='text/javascript' src='scripts/jquery.fancybox-buttons.js'></script>
        
        <script type='text/javascript' src='scripts/tooltip.js'></script>
        
		<script type='text/javascript' src="scripts/jquery-ui-1.10.2.custom.min.js"></script>
        
        <script type="text/javascript" src='scripts/chosen/chosen.jquery.min.js'></script>
        
        <script>
		function logTime(mess)
		{
			var currentTime = new Date();
			var hours = currentTime.getHours();
			var minutes = currentTime.getMinutes();
			var seconds = currentTime.getSeconds();
			if (minutes < 10)
				minutes = "0" + minutes;

			console.log(mess + ":: " + hours + ":" + minutes + ":" + seconds);
		}
		
		function prepareAccordions()
		{
			$("input[type='button'][value='Check out']").click(function() {
				var id = $(this).attr("name");
				$("input[class='fin_" + id + "'][name='access_comment']").val($("input[class*='tmp_" + id + "'][name='access_comment']").val());
				$("input[class='fin_" + id + "'][name='check_out_quantity']").val($("input[class*='tmp_" + id + "'][name='check_out_quantity']").val());
				$("form[name='checkoutform" + id + "']").submit(); 
				return false;
			});
			
			$("input[type='button'][value='Check in']").click(function() {
				var id = $(this).attr("name");
				$("input[class='fin_" + id + "'][name='access_comment']").val($("input[class*='tmp_" + id + "'][name='access_comment']").val());
				$("input[class='fin_" + id + "'][name='check_in_quantity']").val($("input[class*='tmp_" + id + "'][name='check_in_quantity']").val());
				$("form[name='checkinform" + id + "']").submit(); 
				return false;
			});
		
		
		
		
		$('.acc-inactive').children('.tablewrapper').hide();
                
                $('.accordion .smlbutton').click(function() {
                    $(this).parent().children('.tablewrapper').slideToggle();
                    if ($(this).parent().hasClass('acc-active')) {
                        $(this).parent().removeClass('acc-active');
                        $(this).parent().addClass('acc-inactive');
                    }
                    else {
                        $(this).parent().removeClass('acc-inactive');
                        $(this).parent().addClass('acc-active');
                    }
                });
                
                $('.accordion form').submit(function() {
                    var checkout = '';
                    var checkin = '';
                    
                    if ($('#checkout').hasClass('acc-active')) {
                        checkout = 'acc-active';
                    }
                    else {
                        checkout = 'acc-inactive';
                    }
                    if ($('#checkin').hasClass('acc-active')) {
                        checkin = 'acc-active';
                    }
                    else {
                        checkin = 'acc-inactive';
                    }
                    
                    $(this).siblings().find('input[name=check_out_accordion]').val(checkout);
                    $(this).siblings().find('input[name=check_in_accordion]').val(checkin);
                    return true;
                });
		}
		function prepare()
		{
			
			$("input[type='button'][value='UP']").click(function() {
				var id = $(this).attr("name");

				$("input[class='fin_" + id + "'][name='serial_number']").val($("input[class*='tmp_" + id + "'][name='serial_number']").val());
				$("input[class='fin_" + id + "'][name='part_description']").val($("input[class*='tmp_" + id + "'][name='part_description']").val());
				$("input[class='fin_" + id + "'][name='product_id']").val($("select[class*='tmp_" + id + "'][name='product_id']").val());
				$("input[class='fin_" + id + "'][name='company_id']").val($("select[class*='tmp_" + id + "'][name='company_id']").val());
				$("input[class='fin_" + id + "'][name='shelf_id']").val($("select[class*='tmp_" + id + "'][name='shelf_id']").val());
				$("input[class='fin_" + id + "'][name='project_id']").val($("select[class*='tmp_" + id + "'][name='project_id']").val());
				$("input[class='fin_" + id + "'][name='group_id']").val($("select[class*='tmp_" + id + "'][name='group_id']").val());
				$("input[class='fin_" + id + "'][name='currency_id']").val($("select[class*='tmp_" + id + "'][name='currency_id']").val());
				$("input[class='fin_" + id + "'][name='unit_cost']").val($("input[class*='tmp_" + id + "'][name='unit_cost']").val());
				$("input[class='fin_" + id + "'][name='quantity']").val($("input[class*='tmp_" + id + "'][name='quantity']").val());
				var real = $("input[class*='tmp_" + id + "'][name='imagefile']");
				
				var cloned = real.clone(true);
				(real.replaceWith(cloned)).appendTo( $("form[name='updatepartform" + id + "']") );
				$("form[name='updatepartform" + id + "']").submit(); 
				return false;
			});
			
			
			var options = 
			{
				enableCookies : false
			};
			
			$('.table table').tableFilter(options);
			$('.edit').hide();
			$('.hide').hide();
			$('tr.filters td:empty').hide();
			
			$('.table .thead').click(function() {
				$('.edit').hide();
				$('.hide').hide();
				$('.active').removeClass('active');
				$('.view').show();
			});
		
			$('.table tr:not(.thead)').click(function(event) 
			{
				$('.edit').hide();
				$('.hide').hide();
				$('.active').removeClass('active');
				$('.view').show();
				
				if (!$(event.target).is('a')) {
					$(this).find('.view').hide();
					$(this).find('.edit').show();
					$(this).addClass('active');
					$('tr.filters td:empty').hide();
				}
			});
		
			$('.table select:not(.filter)').click(function(event) 
			{
				event.stopPropagation();
			});
		
			$('.chzn-select').chosen();
			$('.fancybox').fancybox();
			$('.fileupload').click(function() {
                    $(this).parent().find('.fileinput').trigger('click');
                });
                
                $('.fileinput').change(function() {
                    if ($(this).val() !== '') {
                        $(this).parent().find('.fileupload span').html($(this).val());
                    }
                });
				
				imagePreview();
				
				$('.actionconfirm').click(function(event) {
                    $('#confirm' + $(this).attr('id')).show();
                    event.stopPropagation();
                });
		}	//end of preparation function
		
            $(document).ready(function() {
				prepare();
				
				if ($('#inventoryDiv').length > 0) {
					var invRequest;
					if (invRequest)
						invRequest.abort();
					invRequest = $.ajax({
						url:"includes/render_inventory.php",
						type: "get"
						});
						$("div#inventoryDiv").html('<img src="images/Loading_Animation.gif" height="42" width="42">');
						invRequest.done(function(response, status, jqXHR)
						{
							$("div#inventoryDiv").html(response);
							prepare();
							prepareAccordions();
						});
				}
				
				$('#ajaxform').submit(
				function(e)
				{
					e.stopPropagation();
					var request;
					if (request)
						request.abort;
					request = $.ajax({
						url:"includes/render_parts.php",
						type: "get",
						data: "keyword=" + $('input#ajaxtext').val()
						
					});
					logTime("AJAX Request sent");
					console.log("Please wait");
						$("div#ajaxdiv").html('<img src="images/Loading_Animation.gif" height="42" width="42">');
						logTime("GIF shown on screen");
					request.done(function(response, status, jqXHR){
						//console.log(response);
						logTime("Request from AJAX finished");
						$("div#ajaxdiv").html(response);
						logTime("Finished writing HTML into browser");
						prepare();
					});
					return false;
				});
				
                
                
                
                
                
                
                $('.lightbox').fancybox();
                
                $('#focus').focus();
                

                
                
                
                if($('.auxwrapper .aux').hasClass('visible')) {
                    $('.auxwrapper input[type=button]').hide();
                }
                else {
                     $('.auxwrapper .aux').hide();
                }
                
                $('.auxwrapper input[type=button]').click(function() {
                    $(this).hide();
                    $('.auxwrapper .aux').slideToggle();
                    $('#focus').focus();
                });
                
                $('.auxwrapper .aux input[type=reset]').click(function() {
                    $('.auxwrapper .aux .errormessage').remove();
                    $('.auxwrapper .aux .fileupload span').html('Upload image');
                    $('.auxwrapper .aux').slideToggle(function() {
                        $('.quantity').show();
                        $('.auxwrapper input[type=button]').show();
                    });
                });
                
                if ($('.auxwrapper .aux input[name=serial_number]').val() !== '') {
                    $('.quantity').hide();
                }
                
                $('.auxwrapper .aux input[name=serial_number]').keyup(function() {
                    if ($(this).val() !== '') {
                        $('.quantity').hide();
                    }
                    else {
                        $('.quantity').show();
                    }
                });
                
                
                
                $('.lightbox').click(function() {
                    $('input[name=serial_number_copy]').val($('input[name=serial_number]').val());
                    $('input[name=part_description_copy]').val($('input[name=part_description]').val());
                    $('input[name=company_id_copy]').val($('select[name=company_id] option:selected').val());
                    $('input[name=shelf_id_copy]').val($('select[name=shelf_id] option:selected').val());
                    $('input[name=project_id_copy]').val($('select[name=project_id] option:selected').val());
                    $('input[name=group_id_copy]').val($('select[name=group_id] option:selected').val());
                    $('input[name=currency_id_copy]').val($('select[name=currency_id] option:selected').val());
                    $('input[name=unit_cost_copy]').val($('input[name=unit_cost]').val());
                    $('input[name=quantity_copy]').val($('input[name=quantity]').val());
                });
                
                $('#addproduct input[type=reset]').click(function() {
                    $('.fancybox-close').trigger('click');
                });
                
                
                
                
                
                $('#filterdatefrom').datepicker( {
                    showOn: 'button', 
                    buttonImage: 'styles/calendar-xxl.png', 
                    buttonImageOnly: true, 
                    defaultDate: '+1w', 
                    changeMonth: true, 
                    numberOfMonths: 1, 
                    dateFormat: 'yy-mm-dd', 
                    onClose: function(selectedDate) {
                        $('#filterdateto').datepicker(
                            'option', 
                            'minDate', 
                            selectedDate
                        );
                    }
                });
                
                $('#filterdateto').datepicker( {
                    defaultDate: '+1w', 
                    showOn: 'button', 
                    buttonImage: 'styles/calendar-xxl.png', 
                    buttonImageOnly: true, 
                    changeMonth: true, 
                    numberOfMonths: 1, 
                    dateFormat: 'yy-mm-dd', 
                    onClose: function(selectedDate) {
                        $('#filterdatefrom').datepicker(
                            'option', 
                            'maxDate', 
                            selectedDate
                        );
                    }
                });
                    
                $('.chzn-select').chosen();
            });
            
            function passwordCheck()
            {
                if ($('.password').val() !== $('.passwordconfirm').val()) {
                    if ($('.aux .errormessage').length === 0) {
                        $('.aux').prepend('<p class="error errormessage">The passwords you entered do not match.</p>');
                    }
                    else {
                        $('.aux .errormessage').text('The passwords you entered do not match.');
                    }
                    return false;
                }

                if ($('.password').val().length < 8 || $('.passwordconfirm').val().length < 8) {
                    if ($('.aux .errormessage').length === 0) {
                        $('.aux').prepend('<p class="error errormessage">Passwords must contain a minimum of 8 characters.</p>');
                    }
                    else {
                        $('.aux .errormessage').text('Passwords must contain a minimum of 8 characters.');
                    }
                    return false;
                }
                
                return true;
            }
        </script>
    </head
    <body>
        <div class ='body'>
            <div class='header'>
                <div class='logo'>
                    <img src='images/s4_logo.png' alt='S4 logo' width='68px' height='68px'>
                </div>
                <div class ='headingmenu'>
                    <div class='heading'>
                        <h1>Stock Control</h1>
                    </div>
                    <div class='menu'>
                        <?php include 'includes/menu.php'; ?>
                    </div>
                </div>
                <div class='userbar'>
                    <?php include 'includes/logout.php'; ?>
                </div>
            </div>
            <div class ='content'>
                <?php include 'includes/routing.php'; ?>
            </div>
            <div class='footer'>
                <?php include 'includes/footer.php'; ?>
            </div>
        </div>
    </body>
</html>