using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Relocate_P_Shelf : Form
    {
        List<ScanItem> ItemsList = new List<ScanItem>();
        List<int> OrderList = new List<int>();
        List<string> OrderNumberList = new List<string>();
        List<Users> UsersList = new List<Users>();
        bool bAutoShelf = false;
        string message = "";
        int itemcount = -1;
        bool state = true;

        public Relocate_P_Shelf()
        {
            InitializeComponent();
        }

        private void NextItem()// Next Item in list 
        {
            if (checkBoxSearch.Checked) { return; }
            if (state == false) { return; }
            if (itemcount == -1) { return; }

            DetailsUpdate();

            if (itemcount < (OrderList.Count - 1)) { itemcount++; }
            else { itemcount = 0; }
            ItemDisplay(OrderList[itemcount]);
        }

        private void PreviousItem() // Previous Item in list
        {
            if (checkBoxSearch.Checked) { return; }
            if (state == false) { return; }
            if (itemcount == -1) { return; }

            DetailsUpdate();

            if (itemcount > 0) { itemcount--; }
            else { itemcount = OrderList.Count - 1; }
            ItemDisplay(OrderList[itemcount]);
        }

        private void ItemDisplay(int itemnum) // Current Item Selected
        {
            labelItemNumber.Text = (itemcount + 1) + " / " + OrderList.Count;

            if (itemcount != -1)
            {
                //labelOrder.Text = ItemsList[itemnum].OrderID;
                labelProjData.Text = ItemsList[itemnum].ProjectName;
                labelSupplierData.Text = ItemsList[itemnum].SupplierCode;
                labelDescriptionText.Text = "                 " + ItemsList[itemnum].Description;
                //labelDatabase.Text = ItemsList[itemnum].Database;
                labelBarcodeData.Text = ItemsList[itemnum].Barcode;
                textBoxShelf.Text = ItemsList[itemnum].Shelf;
                PreviousLocationDisplay(itemnum);          
                if (bAutoShelf)
                {
                    if (textBoxAutoShelf.Text == textBoxShelf.Text)
                    {
                        checkBoxSearchAutoShelf.Checked = true;
                    }
                    else
                    {
                        checkBoxSearchAutoShelf.Checked = false;
                    }
                }
            }
        }

        private void PreviousLocationDisplay(int itemnum)
        {
            for (int count = 0; count < UsersList.Count; count++)
            {
                if (ItemsList[itemnum].PreviousShelf == UsersList[count].UserName)
                {
                    labelOldShelfDesc.Text = ItemsList[itemnum].PreviousShelf;
                    return;
                }
            }
            labelOldShelfDesc.Text = ItemsList[itemnum].PreviousShelfDescription;
        }

        private void DisableCheck()
        {
            if (state)
            {
                panel_O_Search_T.Enabled = true;
                panel_O_Order_T.Enabled = true;
                panel_O_Order_D.Enabled = true;
                panel_O_Shelf_T.Enabled = true;
                panel_O_Shelf_D.Enabled = true;
                button_O_Selected.Enabled = true;

                panel_D_AutoShelf.Enabled = true;
                //panel_D_Barcode.Enabled = true;
                panel_D_Search.Enabled = true;
                panel_D_Shelf.Enabled = true;
            }
            else
            {
                panel_O_Search_T.Enabled = false;
                panel_O_Order_T.Enabled = false;
                panel_O_Order_D.Enabled = false;
                panel_O_Shelf_T.Enabled = false;
                panel_O_Shelf_D.Enabled = false;
                button_O_Selected.Enabled = false;

                panel_D_AutoShelf.Enabled = false;
                //panel_D_Barcode.Enabled = false;
                panel_D_Search.Enabled = false;
                panel_D_Shelf.Enabled = false;
            }
        }

        private void GetData()
        {
            Loader();

            MyLibrary.GetPendingDataFromServer(out state, out message, out ItemsList);
            DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void SendData()
        {
            Loader();

            MyLibrary.RelocatePendingBarcode(out state, out message, ItemsList);
            DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void ClearData()
        {
            ItemsList.Clear();
            OrderList.Clear();
            OrderNumberList.Clear();
            comboBoxOrder.Items.Clear();
            itemcount = -1;
            state = true;
            checkBoxSearch.Checked = false;
            //checkBoxOrders.Checked = false;
            bAutoShelf = false;
            checkBoxSearchAutoShelf.Checked = false;
            //textBoxBarcode.Text = "";
            textBoxShelf.Text = "";
            textBoxAutoShelf.Text = "";
            textBoxSearchAuto.Text = "";

            labelDescriptionText.Text = "";
            labelDatabase.Text = "";
            labelMessage.Text = "";
            labelSupplierData.Text = "";
            labelProjData.Text = "";
            //textBoxBarcode.Text = "";
            textBoxShelf.Text = "";

            panel_O_Search_D.Show();
            panel_O_Order_D.Hide();
            panel_O_Shelf_D.Hide();
            panel_O_Button.Hide();
            label_O_Order_Title.BackColor = System.Drawing.Color.PaleTurquoise;
            label_O_Shelf_Title.BackColor = System.Drawing.Color.PaleTurquoise;
        }

        private void DetailsUpdate()
        {
            if (itemcount != -1)
            {
                if (ItemsList[OrderList[itemcount]].Shelf != textBoxShelf.Text)
                {
                    ItemsList[OrderList[itemcount]].Shelf = textBoxShelf.Text;
                }
            }            
        }

        private void BarcodeUpdate() // Check and Update Barcode
        {
            textBoxShelf.Focus();
            if (bAutoShelf)
            {
                ItemsList[OrderList[itemcount]].Shelf = textBoxAutoShelf.Text;
                textBoxShelf.Text = textBoxAutoShelf.Text;
                checkBoxSearchAutoShelf.Checked = true;
                checkBoxSearchAutoShelf.Focus();
            }
        }

        private int ItemSearch(string barcode)
        {
            OrderSort("All Orders");
            for (int count = 0; count < OrderList.Count; count++)
            {
                if (ItemsList[OrderList[count]].Barcode == barcode)
                {
                    return count;
                }
            }
            return -1;
        }

        private void BarcodeSearch(string barcode) // Search for barcode
        {
            DetailsUpdate();
            int itemcount = ItemSearch(barcode);
            if (itemcount >= 0)
            {
                ItemDisplay(OrderList[itemcount]);
                panelInfo.Show();
                panel_D_AutoShelf.Enabled = true;
                panel_D_Shelf.Enabled = true;
                textBoxShelf.Focus();
                if (bAutoShelf)
                {
                    ItemsList[OrderList[itemcount]].Shelf = textBoxAutoShelf.Text;
                    textBoxShelf.Text = textBoxAutoShelf.Text;
                    checkBoxSearchAutoShelf.Checked = true;
                    checkBoxSearchAutoShelf.Focus();
                }
                labelMessage.Text = "";
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
                labelMessage.Text = "Barcode Not in List";
                panel_D_AutoShelf.Enabled = false;
                panel_D_Shelf.Enabled = false;
                checkBoxSearchAutoShelf.Checked = false;
                panelInfo.Hide();
                textBoxSearchAuto.Text = "";
                textBoxSearchAuto.Focus();
                itemcount = -1;
            }
        }

        private void OrderSort(string Order) // Sort Order
        {
            if (Order == "No Order") { Order = ""; }
            OrderList.Clear();
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if ((ItemsList[count].OrderId == Order) || (Order == "All Orders"))
                {
                    OrderList.Add(count);
                }
            }
            if (OrderList.Count > 0) { itemcount = 0; } else { itemcount = -1; }
        }

        private void PopulateOrderNumbers() // Populate Orders 
        {
            OrderNumberList.Clear();
            List<string> temp = new List<string>();
            bool bfound;

            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].OrderId == "") { ItemsList[count].OrderId = "No Order"; }
                temp.Add(ItemsList[count].OrderId);
            }

            for (int count1 = 0; count1 < temp.Count; count1++)
            {
                bfound = false;
                for (int count2 = 0; count2 < OrderNumberList.Count; count2++)
                {
                    if (temp[count1] == OrderNumberList[count2]) { bfound = true; }
                }
                if (bfound == false) { OrderNumberList.Add(temp[count1]); }

            }

            comboBoxOrder.Items.Clear();
            comboBoxOrder.Items.Add("All Orders");
            foreach (string element in OrderNumberList)
            {
                comboBoxOrder.Items.Add(element);
            }
            if (comboBoxOrder.Items.Contains("No Order"))
            {
                comboBoxOrder.Items.Remove("No Order");
                comboBoxOrder.Items.Add("No Order");
            }
            comboBoxOrder.Text = "All Orders";
        }

        private void Loader()
        {
            panelLoader.Show();
            panelLoader.Refresh();
            buttonClear.Hide();
            buttonClear.Refresh();
            buttonClose.Hide();
            buttonClose.Refresh();
            Application.DoEvents();
            panelLoader.Hide();
            buttonClear.Show();
            buttonClose.Show();
        }



        //private void checkBoxSearch_CheckStateChanged(object sender, EventArgs e)
        //{
        //    if (checkBoxSearch.Checked)
        //    {
        //        checkBoxOrders.Checked = false;
        //        comboBoxOrder.Text = "All Orders";
        //        panel_O_Order_D.Hide();
        //    }
        //}

        //private void checkBoxOrders_CheckStateChanged(object sender, EventArgs e)
        //{
        //    if (checkBoxOrders.Checked)
        //    {
        //        panel_O_Order_D.Show();
        //        comboBoxOrder.Focus();
        //        checkBoxSearch.Checked = false;
        //    }
        //    else
        //    {
        //        comboBoxOrder.Text = "All Orders";
        //        panel_O_Order_D.Hide();
        //    }
        //}

        //private void checkBoxAutoShelf_CheckStateChanged(object sender, EventArgs e)
        //{
        //    if (checkBoxAutoShelf.Checked)
        //    {
        //        panel_O_Shelf_D.Show();
        //        textBoxAutoShelf.Focus();
        //    }
        //    else
        //    {
        //        panel_O_Shelf_D.Hide();
        //        textBoxAutoShelf.Text = "";
        //    }
        //}

        private void button_O_Selected_Click(object sender, EventArgs e)
        {
            if (textBoxAutoShelf.Text == "")
            {
                bAutoShelf = false;
            }
            else
            {
                bAutoShelf = true;
            }

            OrderSort(comboBoxOrder.Text);

            panel_O_Search_D.Show();
            label_O_Order_Title.BackColor = System.Drawing.Color.PaleTurquoise;
            label_O_Shelf_Title.BackColor = System.Drawing.Color.PaleTurquoise;
            panel_O_Button.Hide();
            panelOptions.Hide();

            panelInfo.Show();
            labelItemNumber.Show();
            panel_D_AutoShelf.Enabled = true;
            panel_D_Shelf.Enabled = true;

            panel_D_Search.Hide();
            panel_D_AutoShelf.Hide();

            //panel_D_Barcode.Show();
            panel_D_Shelf.Show();

            //textBoxBarcode.Focus();

            if (checkBoxSearch.Checked)
            {
                //panel_D_Barcode.Hide();
                panel_D_Search.Show();

                panelInfo.Hide();
                labelItemNumber.Hide();
                textBoxSearchAuto.Focus();
                panel_D_AutoShelf.Enabled = false;
                panel_D_Shelf.Enabled = false;
                itemcount = -1;
            }
            if (bAutoShelf)
            {
                panel_D_Shelf.Hide();
                panel_D_AutoShelf.Show();

                checkBoxSearchAutoShelf.Text = "AUTOSHELF - " + textBoxAutoShelf.Text;
                checkBoxSearchAutoShelf.Checked = false;
            }

            if (checkBoxSearch.Checked == false)
            {
                ItemDisplay(OrderList[itemcount]);
            }
        }

        private void checkBoxSearchAutoShelf_CheckStateChanged(object sender, EventArgs e)
        {
            if (itemcount != -1)
            {
                if (checkBoxSearchAutoShelf.Checked)
                {
                    ItemsList[OrderList[itemcount]].Shelf = textBoxAutoShelf.Text;
                    textBoxShelf.Text = textBoxAutoShelf.Text;
                }
                else
                {
                    ItemsList[OrderList[itemcount]].Shelf = "";
                    textBoxShelf.Text = "";
                }
            }
        }

        private void textBoxSearchAuto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                BarcodeSearch(textBoxSearchAuto.Text);
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            DetailsUpdate();
            SendData();
            DisableCheck();
            if (state)
            {
                this.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            DetailsUpdate();
            SendData();
            DisableCheck();
            if (state)
            {
                ClearData();
                panelOptions.Show();
                GetData();
                DisableCheck();
                if (state)
                {
                    PopulateOrderNumbers();
                    PopulateUsers();
                }
            }
        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearData();
            panelOptions.Show();
            GetData();
            DisableCheck();
            if (state)
            {
                PopulateOrderNumbers();
            }
        }

        private void Relocate_P_Shelf_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Left))
            {
                PreviousItem();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right))
            {
                NextItem();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Up))
            {
                if (panel_D_AutoShelf.Visible)
                {
                    checkBoxSearchAutoShelf.Focus();
                }
                if (panel_D_Shelf.Visible)
                {
                    textBoxShelf.Focus();
                }
                if (panel_D_Search.Visible)
                {
                    textBoxSearchAuto.Focus();
                }
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down))
            {
                if (panel_D_Search.Visible)
                {
                    textBoxSearchAuto.Focus();
                }
                if (panel_D_Shelf.Visible)
                {
                    textBoxShelf.Focus();
                }
                if (panel_D_AutoShelf.Visible)
                {
                    checkBoxSearchAutoShelf.Focus();
                }
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Escape))
            {
                DetailsUpdate();
                SendData();
                DisableCheck();
                if (state)
                {
                    this.Close();
                }
            }
        }

        private void textBoxSearchAuto_GotFocus(object sender, EventArgs e)
        {
            textBoxSearchAuto.Text = "";
        }

        private void Relocate_P_Shelf_Load(object sender, EventArgs e)
        {
            GetData();
            DisableCheck();
            if (state)
            {
                PopulateOrderNumbers();
                PopulateUsers();
            }
            label_O_Search_Title.BackColor = System.Drawing.Color.Teal;
            label_O_Order_Title.BackColor = System.Drawing.Color.PaleTurquoise;
            label_O_Shelf_Title.BackColor = System.Drawing.Color.PaleTurquoise;
        }

        private void PopulateUsers()
        {
            Loader();
            MyLibrary.UsersPopulate(out state, out message, out UsersList);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void button_O_Search_Next_Click(object sender, EventArgs e)
        {
            panel_O_Search_D.Hide();
            panel_O_Order_D.Show();
            label_O_Order_Title.BackColor = System.Drawing.Color.Teal;
            if (checkBoxSearch.Checked)
            {
                panel_O_Order_D.Hide();
                panel_O_Shelf_D.Show();
                label_O_Shelf_Title.BackColor = System.Drawing.Color.Teal;
            }
        }

        private void button_O_Order_Next_Click(object sender, EventArgs e)
        {
            panel_O_Order_D.Hide();
            panel_O_Shelf_D.Show();
            label_O_Shelf_Title.BackColor = System.Drawing.Color.Teal;
        }

        private void button_O_Shelf_Next_Click(object sender, EventArgs e)
        {
            panel_O_Shelf_D.Hide();
            panel_O_Button.Show();
        }

    }
}