<h2>Logs</h2>

<?php
if (isset($_POST['searchfilter'])) {
    $datefrom = $_POST['datefrom'] . ' 00:00:00';
    $dateto = $_POST['dateto'] . ' 23:59:59';
    $q = 'SELECT * FROM log WHERE date_time BETWEEN :datefrom AND :dateto';
    $params = array(':datefrom' => $datefrom, ':dateto' => $dateto);
    
    $result = null;
    $rows = sql_select($result, $q, $params);

}
else {
    $q = 'SELECT * FROM log';
    $datefrom = date('Y-m-d');
    $dateto = date('Y-m-d');
    $result = null;
    $rows = sql_select($result, $q);
}

if ($rows > 0) { ?>
<div class='auxwrapper'>
    <input type="button" value="Filter">
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='fiterlogform' action='index.php?pid=logs' method='post'>
            <tr>
                <th>From</th>
                <td>
                    <input id='filterdatefrom' type='text' class='datepicker' name='datefrom' value='<?php echo $datefrom; ?>'>
                </td>
            </tr>
            <tr>
                <th>To</th>
                <td>
                    <input id='filterdateto' type='text' class='datepicker' name='dateto' value='<?php echo $dateto; ?>'>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='searchfilter' value='searchfilter'>
                    <input type='submit' value='Apply'>
                    <input class='reset' type='reset' Value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php }

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='first w120'>Timestamp</th>
                <th>Process</th>
                <th>Action</th>
                <th>User</th>
                <th class='last w800'>Event Detail</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($result as $row) {
            $date_time = $row['date_time'];
            $process = $row['process'];
            $action = $row['action'];
            $user = $row['user'];
            $description = $row['description']; ?>
            <tr>
                <td class='w120'>
                    <div><?php echo $date_time; ?></div>
                </td>
                <td>
                    <div><?php echo $process; ?></div>
                </td>
                <td>
                    <div><?php echo $action; ?></div>
                </td>
                <td>
                    <div><?php echo $user; ?></div>
                </td>
                <td class='w800'>
                    <div><?php echo $description; ?></div>
                </td>
            </tr>
        <?php }; ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No log events in database.</p>';
} ?>