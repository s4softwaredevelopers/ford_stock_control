namespace s4_Scanner
{
    partial class Relocate_Pending
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.labelMessage = new System.Windows.Forms.Label();
            this.panelUser = new System.Windows.Forms.Panel();
            this.panelUserDetails = new System.Windows.Forms.Panel();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.labelDescription = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.comboBoxUsers = new System.Windows.Forms.ComboBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.labelUser = new System.Windows.Forms.Label();
            this.panelUserTitle = new System.Windows.Forms.Panel();
            this.labelUserTitle = new System.Windows.Forms.Label();
            this.panelUserRelocate = new System.Windows.Forms.Panel();
            this.buttonUserCancel = new System.Windows.Forms.Button();
            this.buttonGoUser = new System.Windows.Forms.Button();
            this.panelBase = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel_D_Marked = new System.Windows.Forms.Panel();
            this.checkBoxSelected = new System.Windows.Forms.CheckBox();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelProj = new System.Windows.Forms.Label();
            this.labelBarcodeData = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelOldShelfDesc = new System.Windows.Forms.Label();
            this.labelOldShelf = new System.Windows.Forms.Label();
            this.labelItemNumber = new System.Windows.Forms.Label();
            this.labelProjData = new System.Windows.Forms.Label();
            this.labelSupplierData = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labelDescriptionText = new System.Windows.Forms.Label();
            this.labelDatabase = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.panelLine2 = new System.Windows.Forms.Panel();
            this.panel_D_Search = new System.Windows.Forms.Panel();
            this.textBoxSearchAuto = new System.Windows.Forms.TextBox();
            this.labelSearchAuto = new System.Windows.Forms.Label();
            this.buttonRelocate = new System.Windows.Forms.Button();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panelOptions = new System.Windows.Forms.Panel();
            this.panel_O_Button = new System.Windows.Forms.Panel();
            this.button_O_Selected = new System.Windows.Forms.Button();
            this.panel_O_Order_D = new System.Windows.Forms.Panel();
            this.labelExtraOrders = new System.Windows.Forms.Label();
            this.comboBoxOrder = new System.Windows.Forms.ComboBox();
            this.panelShelf = new System.Windows.Forms.Panel();
            this.buttonShelfCancel = new System.Windows.Forms.Button();
            this.textBoxShelf = new System.Windows.Forms.TextBox();
            this.Shelf = new System.Windows.Forms.Label();
            this.buttonRelocate_Shelf = new System.Windows.Forms.Button();
            this.panelHeader.SuspendLayout();
            this.panelFooter.SuspendLayout();
            this.panelUser.SuspendLayout();
            this.panelUserDetails.SuspendLayout();
            this.panelUserTitle.SuspendLayout();
            this.panelUserRelocate.SuspendLayout();
            this.panelBase.SuspendLayout();
            this.panel_D_Marked.SuspendLayout();
            this.panelInfo.SuspendLayout();
            this.panel_D_Search.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.panelOptions.SuspendLayout();
            this.panel_O_Button.SuspendLayout();
            this.panel_O_Order_D.SuspendLayout();
            this.panelShelf.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.buttonClear);
            this.panelHeader.Controls.Add(this.buttonClose);
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClear.Location = new System.Drawing.Point(161, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(30, 20);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "R";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(211, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(30, 20);
            this.buttonClose.TabIndex = 10;
            this.buttonClose.Text = " x";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Relocate Pending";
            // 
            // panelFooter
            // 
            this.panelFooter.Controls.Add(this.labelMessage);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 300);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(240, 20);
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(5, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(230, 20);
            this.labelMessage.Text = "Message";
            // 
            // panelUser
            // 
            this.panelUser.Controls.Add(this.panelUserDetails);
            this.panelUser.Controls.Add(this.panelUserTitle);
            this.panelUser.Controls.Add(this.panelUserRelocate);
            this.panelUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUser.Location = new System.Drawing.Point(0, 20);
            this.panelUser.Name = "panelUser";
            this.panelUser.Size = new System.Drawing.Size(240, 280);
            this.panelUser.Visible = false;
            // 
            // panelUserDetails
            // 
            this.panelUserDetails.Controls.Add(this.textBoxDescription);
            this.panelUserDetails.Controls.Add(this.labelDescription);
            this.panelUserDetails.Controls.Add(this.textBoxPassword);
            this.panelUserDetails.Controls.Add(this.comboBoxUsers);
            this.panelUserDetails.Controls.Add(this.labelPassword);
            this.panelUserDetails.Controls.Add(this.labelUser);
            this.panelUserDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserDetails.Location = new System.Drawing.Point(0, 22);
            this.panelUserDetails.Name = "panelUserDetails";
            this.panelUserDetails.Size = new System.Drawing.Size(240, 198);
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxDescription.Location = new System.Drawing.Point(5, 129);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxDescription.Size = new System.Drawing.Size(230, 66);
            this.textBoxDescription.TabIndex = 131;
            // 
            // labelDescription
            // 
            this.labelDescription.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelDescription.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelDescription.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelDescription.Location = new System.Drawing.Point(5, 108);
            this.labelDescription.Name = "labelDescription";
            this.labelDescription.Size = new System.Drawing.Size(230, 21);
            this.labelDescription.Text = "Reason for Relocating Items:";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.textBoxPassword.Location = new System.Drawing.Point(5, 77);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(230, 26);
            this.textBoxPassword.TabIndex = 130;
            // 
            // comboBoxUsers
            // 
            this.comboBoxUsers.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.comboBoxUsers.Location = new System.Drawing.Point(5, 24);
            this.comboBoxUsers.Name = "comboBoxUsers";
            this.comboBoxUsers.Size = new System.Drawing.Size(230, 27);
            this.comboBoxUsers.TabIndex = 129;
            // 
            // labelPassword
            // 
            this.labelPassword.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelPassword.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelPassword.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelPassword.Location = new System.Drawing.Point(5, 56);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(230, 21);
            this.labelPassword.Text = "Please enter your Password:";
            // 
            // labelUser
            // 
            this.labelUser.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelUser.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelUser.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.labelUser.Location = new System.Drawing.Point(5, 4);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(230, 20);
            this.labelUser.Text = "Please select Username:";
            // 
            // panelUserTitle
            // 
            this.panelUserTitle.Controls.Add(this.labelUserTitle);
            this.panelUserTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelUserTitle.Location = new System.Drawing.Point(0, 0);
            this.panelUserTitle.Name = "panelUserTitle";
            this.panelUserTitle.Size = new System.Drawing.Size(240, 22);
            // 
            // labelUserTitle
            // 
            this.labelUserTitle.BackColor = System.Drawing.SystemColors.MenuText;
            this.labelUserTitle.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelUserTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelUserTitle.Location = new System.Drawing.Point(5, 2);
            this.labelUserTitle.Name = "labelUserTitle";
            this.labelUserTitle.Size = new System.Drawing.Size(230, 18);
            this.labelUserTitle.Text = "Enter User Details";
            this.labelUserTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelUserRelocate
            // 
            this.panelUserRelocate.Controls.Add(this.buttonUserCancel);
            this.panelUserRelocate.Controls.Add(this.buttonGoUser);
            this.panelUserRelocate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelUserRelocate.Location = new System.Drawing.Point(0, 220);
            this.panelUserRelocate.Name = "panelUserRelocate";
            this.panelUserRelocate.Size = new System.Drawing.Size(240, 60);
            // 
            // buttonUserCancel
            // 
            this.buttonUserCancel.BackColor = System.Drawing.Color.LightGray;
            this.buttonUserCancel.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUserCancel.Location = new System.Drawing.Point(5, 36);
            this.buttonUserCancel.Name = "buttonUserCancel";
            this.buttonUserCancel.Size = new System.Drawing.Size(230, 20);
            this.buttonUserCancel.TabIndex = 133;
            this.buttonUserCancel.Text = "Cancel";
            this.buttonUserCancel.Click += new System.EventHandler(this.buttonUserCancel_Click);
            // 
            // buttonGoUser
            // 
            this.buttonGoUser.BackColor = System.Drawing.Color.LightGray;
            this.buttonGoUser.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonGoUser.Location = new System.Drawing.Point(5, 3);
            this.buttonGoUser.Name = "buttonGoUser";
            this.buttonGoUser.Size = new System.Drawing.Size(230, 30);
            this.buttonGoUser.TabIndex = 132;
            this.buttonGoUser.Text = "Relocate to User";
            this.buttonGoUser.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // panelBase
            // 
            this.panelBase.Controls.Add(this.panel1);
            this.panelBase.Controls.Add(this.panel_D_Marked);
            this.panelBase.Controls.Add(this.panelInfo);
            this.panelBase.Controls.Add(this.panelLine2);
            this.panelBase.Controls.Add(this.panel_D_Search);
            this.panelBase.Controls.Add(this.buttonRelocate);
            this.panelBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBase.Location = new System.Drawing.Point(0, 20);
            this.panelBase.Name = "panelBase";
            this.panelBase.Size = new System.Drawing.Size(240, 280);
            this.panelBase.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 246);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 2);
            // 
            // panel_D_Marked
            // 
            this.panel_D_Marked.Controls.Add(this.checkBoxSelected);
            this.panel_D_Marked.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_D_Marked.Location = new System.Drawing.Point(0, 212);
            this.panel_D_Marked.Name = "panel_D_Marked";
            this.panel_D_Marked.Size = new System.Drawing.Size(240, 34);
            // 
            // checkBoxSelected
            // 
            this.checkBoxSelected.BackColor = System.Drawing.SystemColors.Highlight;
            this.checkBoxSelected.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.checkBoxSelected.Location = new System.Drawing.Point(14, 2);
            this.checkBoxSelected.Name = "checkBoxSelected";
            this.checkBoxSelected.Size = new System.Drawing.Size(213, 30);
            this.checkBoxSelected.TabIndex = 70;
            this.checkBoxSelected.Text = "Marked for Relocation";
            this.checkBoxSelected.CheckStateChanged += new System.EventHandler(this.checkBoxSelected_CheckStateChanged);
            // 
            // panelInfo
            // 
            this.panelInfo.Controls.Add(this.labelProj);
            this.panelInfo.Controls.Add(this.labelBarcodeData);
            this.panelInfo.Controls.Add(this.label1);
            this.panelInfo.Controls.Add(this.labelOldShelfDesc);
            this.panelInfo.Controls.Add(this.labelOldShelf);
            this.panelInfo.Controls.Add(this.labelItemNumber);
            this.panelInfo.Controls.Add(this.labelProjData);
            this.panelInfo.Controls.Add(this.labelSupplierData);
            this.panelInfo.Controls.Add(this.label2);
            this.panelInfo.Controls.Add(this.labelDescriptionText);
            this.panelInfo.Controls.Add(this.labelDatabase);
            this.panelInfo.Controls.Add(this.labelSupplier);
            this.panelInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelInfo.Location = new System.Drawing.Point(0, 54);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(240, 158);
            // 
            // labelProj
            // 
            this.labelProj.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelProj.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelProj.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelProj.Location = new System.Drawing.Point(5, 25);
            this.labelProj.Name = "labelProj";
            this.labelProj.Size = new System.Drawing.Size(65, 17);
            this.labelProj.Text = "PROJECT";
            // 
            // labelBarcodeData
            // 
            this.labelBarcodeData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelBarcodeData.Location = new System.Drawing.Point(72, 65);
            this.labelBarcodeData.Name = "labelBarcodeData";
            this.labelBarcodeData.Size = new System.Drawing.Size(165, 17);
            this.labelBarcodeData.Text = "Barcode";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(5, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.Text = "BARCODE";
            // 
            // labelOldShelfDesc
            // 
            this.labelOldShelfDesc.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelOldShelfDesc.Location = new System.Drawing.Point(72, 85);
            this.labelOldShelfDesc.Name = "labelOldShelfDesc";
            this.labelOldShelfDesc.Size = new System.Drawing.Size(165, 17);
            this.labelOldShelfDesc.Text = "Old Shelf Desc";
            // 
            // labelOldShelf
            // 
            this.labelOldShelf.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelOldShelf.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelOldShelf.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelOldShelf.Location = new System.Drawing.Point(5, 85);
            this.labelOldShelf.Name = "labelOldShelf";
            this.labelOldShelf.Size = new System.Drawing.Size(65, 17);
            this.labelOldShelf.Text = "LOCATION";
            // 
            // labelItemNumber
            // 
            this.labelItemNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelItemNumber.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelItemNumber.Location = new System.Drawing.Point(161, 0);
            this.labelItemNumber.Name = "labelItemNumber";
            this.labelItemNumber.Size = new System.Drawing.Size(80, 20);
            this.labelItemNumber.Text = "999 / 999";
            this.labelItemNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelProjData
            // 
            this.labelProjData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelProjData.Location = new System.Drawing.Point(5, 25);
            this.labelProjData.Name = "labelProjData";
            this.labelProjData.Size = new System.Drawing.Size(232, 35);
            this.labelProjData.Text = "                ProjData - Line 1\r\nLine 2";
            // 
            // labelSupplierData
            // 
            this.labelSupplierData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSupplierData.Location = new System.Drawing.Point(72, 5);
            this.labelSupplierData.Name = "labelSupplierData";
            this.labelSupplierData.Size = new System.Drawing.Size(94, 17);
            this.labelSupplierData.Text = "SupData";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label2.Location = new System.Drawing.Point(5, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.Text = "ITEM INFO";
            // 
            // labelDescriptionText
            // 
            this.labelDescriptionText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelDescriptionText.Location = new System.Drawing.Point(5, 105);
            this.labelDescriptionText.Name = "labelDescriptionText";
            this.labelDescriptionText.Size = new System.Drawing.Size(232, 50);
            this.labelDescriptionText.Text = "                Desc Text  - Line 1\r\nLine 2\r\nLine 3";
            // 
            // labelDatabase
            // 
            this.labelDatabase.Location = new System.Drawing.Point(120, 5);
            this.labelDatabase.Name = "labelDatabase";
            this.labelDatabase.Size = new System.Drawing.Size(98, 19);
            this.labelDatabase.Text = "Databse";
            this.labelDatabase.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelSupplier
            // 
            this.labelSupplier.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSupplier.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSupplier.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSupplier.Location = new System.Drawing.Point(5, 5);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(65, 17);
            this.labelSupplier.Text = "SUPPLIER";
            // 
            // panelLine2
            // 
            this.panelLine2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panelLine2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLine2.Location = new System.Drawing.Point(0, 52);
            this.panelLine2.Name = "panelLine2";
            this.panelLine2.Size = new System.Drawing.Size(240, 2);
            // 
            // panel_D_Search
            // 
            this.panel_D_Search.Controls.Add(this.textBoxSearchAuto);
            this.panel_D_Search.Controls.Add(this.labelSearchAuto);
            this.panel_D_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_D_Search.Location = new System.Drawing.Point(0, 0);
            this.panel_D_Search.Name = "panel_D_Search";
            this.panel_D_Search.Size = new System.Drawing.Size(240, 52);
            // 
            // textBoxSearchAuto
            // 
            this.textBoxSearchAuto.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.textBoxSearchAuto.Location = new System.Drawing.Point(5, 20);
            this.textBoxSearchAuto.Name = "textBoxSearchAuto";
            this.textBoxSearchAuto.Size = new System.Drawing.Size(230, 29);
            this.textBoxSearchAuto.TabIndex = 79;
            this.textBoxSearchAuto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSearchAuto_KeyPress);
            // 
            // labelSearchAuto
            // 
            this.labelSearchAuto.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSearchAuto.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSearchAuto.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSearchAuto.Location = new System.Drawing.Point(5, 3);
            this.labelSearchAuto.Name = "labelSearchAuto";
            this.labelSearchAuto.Size = new System.Drawing.Size(230, 17);
            this.labelSearchAuto.Text = "SEARCH FOR AN ITEM (BARCODE)\r\n";
            // 
            // buttonRelocate
            // 
            this.buttonRelocate.BackColor = System.Drawing.Color.LightGray;
            this.buttonRelocate.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonRelocate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonRelocate.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonRelocate.Location = new System.Drawing.Point(0, 250);
            this.buttonRelocate.Name = "buttonRelocate";
            this.buttonRelocate.Size = new System.Drawing.Size(240, 30);
            this.buttonRelocate.TabIndex = 7;
            this.buttonRelocate.Text = "Relocate";
            this.buttonRelocate.Click += new System.EventHandler(this.buttonRelocate_Click);
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label3);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 280);
            this.panelLoader.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label3.Location = new System.Drawing.Point(29, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 107);
            this.label3.Text = "Loading";
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.panel_O_Button);
            this.panelOptions.Controls.Add(this.panel_O_Order_D);
            this.panelOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOptions.Location = new System.Drawing.Point(0, 20);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(240, 280);
            // 
            // panel_O_Button
            // 
            this.panel_O_Button.Controls.Add(this.button_O_Selected);
            this.panel_O_Button.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_O_Button.Location = new System.Drawing.Point(0, 246);
            this.panel_O_Button.Name = "panel_O_Button";
            this.panel_O_Button.Size = new System.Drawing.Size(240, 34);
            this.panel_O_Button.Click += new System.EventHandler(this.button_O_Selected_Click);
            // 
            // button_O_Selected
            // 
            this.button_O_Selected.BackColor = System.Drawing.Color.LightGray;
            this.button_O_Selected.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.button_O_Selected.Location = new System.Drawing.Point(5, 2);
            this.button_O_Selected.Name = "button_O_Selected";
            this.button_O_Selected.Size = new System.Drawing.Size(230, 30);
            this.button_O_Selected.TabIndex = 73;
            this.button_O_Selected.Text = "Relocate Items";
            this.button_O_Selected.Click += new System.EventHandler(this.button_O_Selected_Click);
            // 
            // panel_O_Order_D
            // 
            this.panel_O_Order_D.Controls.Add(this.labelExtraOrders);
            this.panel_O_Order_D.Controls.Add(this.comboBoxOrder);
            this.panel_O_Order_D.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_O_Order_D.Location = new System.Drawing.Point(0, 0);
            this.panel_O_Order_D.Name = "panel_O_Order_D";
            this.panel_O_Order_D.Size = new System.Drawing.Size(240, 190);
            // 
            // labelExtraOrders
            // 
            this.labelExtraOrders.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelExtraOrders.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelExtraOrders.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelExtraOrders.Location = new System.Drawing.Point(20, 10);
            this.labelExtraOrders.Name = "labelExtraOrders";
            this.labelExtraOrders.Size = new System.Drawing.Size(200, 38);
            this.labelExtraOrders.Text = "Select Order Number to view all items of the selected Order";
            // 
            // comboBoxOrder
            // 
            this.comboBoxOrder.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.comboBoxOrder.Location = new System.Drawing.Point(20, 60);
            this.comboBoxOrder.Name = "comboBoxOrder";
            this.comboBoxOrder.Size = new System.Drawing.Size(200, 24);
            this.comboBoxOrder.TabIndex = 87;
            // 
            // panelShelf
            // 
            this.panelShelf.Controls.Add(this.buttonShelfCancel);
            this.panelShelf.Controls.Add(this.textBoxShelf);
            this.panelShelf.Controls.Add(this.Shelf);
            this.panelShelf.Controls.Add(this.buttonRelocate_Shelf);
            this.panelShelf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelShelf.Location = new System.Drawing.Point(0, 20);
            this.panelShelf.Name = "panelShelf";
            this.panelShelf.Size = new System.Drawing.Size(240, 280);
            this.panelShelf.Visible = false;
            // 
            // buttonShelfCancel
            // 
            this.buttonShelfCancel.BackColor = System.Drawing.Color.LightGray;
            this.buttonShelfCancel.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonShelfCancel.Location = new System.Drawing.Point(5, 260);
            this.buttonShelfCancel.Name = "buttonShelfCancel";
            this.buttonShelfCancel.Size = new System.Drawing.Size(230, 20);
            this.buttonShelfCancel.TabIndex = 16;
            this.buttonShelfCancel.Text = "Cancel";
            this.buttonShelfCancel.Click += new System.EventHandler(this.buttonShelfCancel_Click);
            // 
            // textBoxShelf
            // 
            this.textBoxShelf.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.textBoxShelf.Location = new System.Drawing.Point(5, 40);
            this.textBoxShelf.Name = "textBoxShelf";
            this.textBoxShelf.Size = new System.Drawing.Size(230, 29);
            this.textBoxShelf.TabIndex = 14;
            // 
            // Shelf
            // 
            this.Shelf.BackColor = System.Drawing.SystemColors.Highlight;
            this.Shelf.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.Shelf.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Shelf.Location = new System.Drawing.Point(5, 5);
            this.Shelf.Name = "Shelf";
            this.Shelf.Size = new System.Drawing.Size(230, 35);
            this.Shelf.Text = "ENTER SHELF BARCODE \r\nBARCODE ASSIGNED TO ALL ITEMS";
            // 
            // buttonRelocate_Shelf
            // 
            this.buttonRelocate_Shelf.BackColor = System.Drawing.Color.LightGray;
            this.buttonRelocate_Shelf.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonRelocate_Shelf.Location = new System.Drawing.Point(5, 100);
            this.buttonRelocate_Shelf.Name = "buttonRelocate_Shelf";
            this.buttonRelocate_Shelf.Size = new System.Drawing.Size(230, 30);
            this.buttonRelocate_Shelf.TabIndex = 1;
            this.buttonRelocate_Shelf.Text = " Assign Shelf to Items";
            this.buttonRelocate_Shelf.Click += new System.EventHandler(this.buttonRelocate_Shelf_Click);
            // 
            // Relocate_Pending
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.panelOptions);
            this.Controls.Add(this.panelShelf);
            this.Controls.Add(this.panelUser);
            this.Controls.Add(this.panelBase);
            this.Controls.Add(this.panelFooter);
            this.Controls.Add(this.panelHeader);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Relocate_Pending";
            this.Text = "Relocate_P_User";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Relocate_P_User_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Relocate_P_User_KeyDown);
            this.panelHeader.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            this.panelUser.ResumeLayout(false);
            this.panelUserDetails.ResumeLayout(false);
            this.panelUserTitle.ResumeLayout(false);
            this.panelUserRelocate.ResumeLayout(false);
            this.panelBase.ResumeLayout(false);
            this.panel_D_Marked.ResumeLayout(false);
            this.panelInfo.ResumeLayout(false);
            this.panel_D_Search.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.panelOptions.ResumeLayout(false);
            this.panel_O_Button.ResumeLayout(false);
            this.panel_O_Order_D.ResumeLayout(false);
            this.panelShelf.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Panel panelUser;
        private System.Windows.Forms.Panel panelUserDetails;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label labelDescription;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.ComboBox comboBoxUsers;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Panel panelUserTitle;
        private System.Windows.Forms.Label labelUserTitle;
        private System.Windows.Forms.Panel panelUserRelocate;
        private System.Windows.Forms.Button buttonUserCancel;
        private System.Windows.Forms.Button buttonGoUser;
        private System.Windows.Forms.Panel panelBase;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label labelBarcodeData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelOldShelfDesc;
        private System.Windows.Forms.Label labelOldShelf;
        private System.Windows.Forms.Label labelItemNumber;
        private System.Windows.Forms.Label labelProjData;
        private System.Windows.Forms.Label labelSupplierData;
        private System.Windows.Forms.Label labelDescriptionText;
        private System.Windows.Forms.Label labelDatabase;
        private System.Windows.Forms.Label labelProj;
        private System.Windows.Forms.Label labelSupplier;
        private System.Windows.Forms.Panel panel_D_Search;
        private System.Windows.Forms.TextBox textBoxSearchAuto;
        private System.Windows.Forms.Label labelSearchAuto;
        private System.Windows.Forms.Panel panelLine2;
        private System.Windows.Forms.Button buttonRelocate;
        private System.Windows.Forms.Panel panel_D_Marked;
        private System.Windows.Forms.CheckBox checkBoxSelected;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelOptions;
        private System.Windows.Forms.Panel panel_O_Button;
        private System.Windows.Forms.Button button_O_Selected;
        private System.Windows.Forms.Panel panel_O_Order_D;
        private System.Windows.Forms.Label labelExtraOrders;
        private System.Windows.Forms.ComboBox comboBoxOrder;
        private System.Windows.Forms.Panel panelShelf;
        private System.Windows.Forms.Button buttonShelfCancel;
        private System.Windows.Forms.TextBox textBoxShelf;
        private System.Windows.Forms.Label Shelf;
        private System.Windows.Forms.Button buttonRelocate_Shelf;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
    }
}