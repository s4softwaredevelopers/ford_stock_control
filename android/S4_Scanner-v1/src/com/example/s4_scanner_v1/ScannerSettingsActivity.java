package com.example.s4_scanner_v1;

import com.example.s4_scanner_v1.R;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ScannerSettingsActivity extends Activity {

	public static final String SETTINGS_SERVER = "MyServer";
	public static final String Server = "Myserver";
	EditText etServer;
    SharedPreferences sharedpreferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scanner_settings);
		
		etServer = (EditText) findViewById(R.id.editTextSettingsServer);
		
		sharedpreferences = getSharedPreferences(SETTINGS_SERVER, Context.MODE_PRIVATE);
		
	      if (sharedpreferences.contains(Server))
	      {
	    	  etServer.setText(sharedpreferences.getString(Server, ""));
	      }
	      
	      WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
	      boolean wifiEnabled = wifiManager.isWifiEnabled();
	      
	      TextView tvWifiState = (TextView) findViewById(R.id.textViewSettingsState1);
	      tvWifiState.setText("Wifi State: " + String.valueOf(wifiEnabled));
	      
	      ConnectivityManager connectivityManager =	(ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
	      NetworkInfo mobileInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	      
	      TextView tvNetworkState = (TextView) findViewById(R.id.textViewSettingsState2);
	      tvNetworkState.setText("3G/4G State: " + mobileInfo.getState().toString());

	}
	
	public void SettingsUpdate(View view){		 
	      String s  = etServer.getText().toString();
	      MainMenuActivity.Mobile_Server = s;
	      Editor editor = sharedpreferences.edit();
	      editor.putString(Server, s);
	      editor.commit(); 
	      
	      Toast.makeText(getBaseContext(),"Server Updated",Toast.LENGTH_LONG).show();
	   }
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.scanner_settings, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
