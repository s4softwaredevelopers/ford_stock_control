﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Configuration;

namespace S4_Barcode_Generator
{
    public class LogFile // Logger
    {
        private string fileName;
        public LogFile()
        {
            string appPath = Path.GetDirectoryName(Application.ExecutablePath);
            fileName = appPath + "\\BarcodeGenerateLog.txt";
        }
        public LogFile(string fileName)
        {
            this.fileName = fileName;
        }
        public void LogClear()
        {
            FileInfo f = new FileInfo(fileName);
            if (f.Exists)
            {
                long filesize = f.Length;
                if (filesize > 1048576)
                {
                    f.Delete();
                }
            }
        }
        public void MyLogFile(string strCategory, string strMessage)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(fileName, FileMode.Append)))
            {
                writer.WriteLine(DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss.fff tt") + " - {0} : {1}", strCategory, strMessage);
            }
        }
    }
}
