<h2>Products</h2>

<?php
if (isset($_POST['addproduct'])) {
    $product_code = trim($_POST['product_code']);
    $description = trim($_POST['product_description']);
    
    $q = 'INSERT INTO item_type (product_code, description) VALUES (:product_code, :description)';
    $params = array(':product_code' => $product_code, ':description' => $description);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Product ' . $description . ' added.</p>';
        logger('add', 'Product code :: ' . $product_code . ' | Product :: ' . $description);
    }
    else {
        echo '<p class="error">Product not added.</p>';
    }
}
elseif (isset($_POST['updateproduct'])) {
    $id = $_POST['updateproduct'];
    $product_code = trim($_POST['product_code']);
    $description = trim($_POST['product_description']);
    
    $q = 'UPDATE item_type SET description=:description, product_code=:product_code WHERE id=:id';
    $params = array(':id' => $id, ':product_code' => $product_code, ':description' => $description);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>Product ' . $description . ' updated.</p>';
        logger('update', 'Product code :: ' . $product_code . ' | Product :: ' . $description);
    }
    else {
        echo '<p class="error">Product not updated.</p>';
    }
}
elseif (isset($_POST['removeproduct'])) {
    $id = $_POST['removeproduct'];
    $product_code = $_POST['product_code'];
    $description = $_POST['product_description'];
    
    $q = 'DELETE FROM item_type WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>Product ' . $description . ' removed.</p>';
        logger('remove', 'Product code :: ' . $product_code . ' | Product :: ' . $description);
    }
    else {
        echo '<p class="error">Product not removed.</p>';
    }
} ?>
<div class='auxwrapper'>
    <input type='button' value='Add'>
    
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addproductform' action='index.php?pid=products' method='post'>
            <tr>
                <th>Product Code</th>
                <td>
                    <input id='focus' type='text' name='product_code' value=''>
                </td>
            </tr>
            <tr>
                <th>Name</th>
                <td>
                    <input type='text' name='product_description' value=''>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addproduct' value='addproduct'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = 'SELECT id, product_code, description FROM item_type ORDER BY id';

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='view first w140'>Product Code</th>
                <th class='edit' filter='false'></th>
                <th class='view last w140'>Name</th>
                <th class='edit' filter='false'>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        <thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $product_code = $row['product_code'];
            $description = $row['description']; ?>
            <tr>
                <form name='updateproductform<?php echo $id ?>' action='index.php?pid=products' method='post'>
                    <td class='view w140'>
                        <div><?php echo $product_code; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='product_code' value='<?php echo $product_code; ?>'>
                    </td>
                    <td class='view w140'>
                        <div><?php echo $description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='product_description' value='<?php echo $description; ?>'>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updateproduct' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form description='removeproductform<?php echo $id ?>' action='index.php?pid=products' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removeproduct' value='<?php echo $id ?>'>
                        <input type='hidden' name='product_code' value='<?php echo $product_code; ?>'>
                        <input type='hidden' name='product_description' value='<?php echo $description; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No products in database.</p>';
} ?>