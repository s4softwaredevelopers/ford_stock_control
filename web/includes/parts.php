<h2>Parts</h2>
<p>Enter any partial or full keyword and click 'Find!' to begin search</p>
<form id="ajaxform" method="GET" action="" >
<input type="text" id="ajaxtext" />
<input type="submit" name="ajaxbutton" id="ajaxbutton" value="Find!"/>
</form>
<?php  
$path = 'uploads/';
$path_thumb = 'uploads/thumbs/';

$serial_number_copy = '';
$part_description_copy = '';
$product_id_copy = -1;
$company_id_copy = -1;
$shelf_id_copy = -1;
$project_id_copy = -1;
$group_id_copy = -1;
$currency_id_copy = -1;
$unit_cost_copy = '';
$quantity_copy = '';

$auxclass = '';

if (isset($_POST['addpart'])) {
    $serial_number = trim($_POST['serial_number']);
    $description = trim($_POST['part_description']);
    
    $quantity = 1;
    if ($serial_number == '') {
        $quantity = $_POST['quantity'];
    }
    $unit_cost = $_POST['unit_cost'];
    
    $company_id = $_POST['company_id'];
    $currency_id = $_POST['currency_id'];
    $group_id = $_POST['group_id'];
    $product_id = $_POST['product_id'];
    $project_id = $_POST['project_id'];
    $shelf_id = $_POST['shelf_id'];
    
    $q = "SELECT id FROM user WHERE username='unassigned'";
    
    $result = null;
    $rows = sql_select($result, $q);
    
    if ($rows == 1) {
        foreach ($result as $row) {
            $user_id = $row['id'];
        }
    }
    
    $q = 'SELECT product_code, description FROM item_type WHERE id=:id';
    $params = array(':id' => $product_id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows == 1) {
        foreach ($result as $row) {
            $product_code = $row['product_code'];
            $product = $row['description'];
        }
    }
    
    $image = '';
    if ($_FILES['imagefile']['name'] != '') {
        $filename = $product_code . '_' . time();
        $image = $filename . str_replace('image/', '.', $_FILES['imagefile']['type']);
        move_uploaded_file($_FILES['imagefile']['tmp_name'], $path . $image);
		make_thumb($path.$image, $path_thumb.$image, 150);
    }
    
    $q = "SELECT item.id, item.quantity, item.currency_id, item.item_type_id, 
        currency.description AS currency
        FROM item
        INNER JOIN currency ON item.currency_id=currency.id
        INNER JOIN item_type ON item.item_type_id=item_type.id
        WHERE item.serial_number='' AND item.item_type_id=:item_type_id";
    $params = array(':item_type_id' => $product_id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows == 1 && $serial_number == '') {
        foreach ($result as $row) {
            $id_match = $row['id'];
            $quantity_total = $row['quantity'] + $quantity;
            $currency_id_match = $row['currency_id'];
            $currency = $row['currency'];
        }
        if ($currency_id == $currency_id_match) {
            if ($unit_cost == ''){
                $q = 'UPDATE item SET quantity=:quantity, last_access=:last_access WHERE id=:id';
                $params = array(':id' => $id_match, ':quantity' => $quantity_total, ':last_access'=> date("Y-m-d H:i:s"));
            }
            else {
                $q = 'UPDATE item SET quantity=:quantity, unit_cost=:unit_cost, last_access=:last_access WHERE id=:id';
                $params = array(':id' => $id_match, ':quantity' => $quantity_total, ':unit_cost' => $unit_cost, ':last_access'=> date("Y-m-d H:i:s"));
            }
            
            $rows = sql_update($q, $params);

            if ($rows == 1) {
                echo '<p>Non-unique part with product code ' . $product_code . ' and product name ' . $product . ' already exists. Quantity updated.';
                logger('add', 'S/N :: n/a | Description :: ' . $description . ' | Product code :: ' . $product_code . ' | Product :: ' . $product . ' | Add quantity :: ' . $quantity . ' | Quantity :: '. $quantity_total);
            }
        }
        else {
            echo '<p class="error">Non-unique part with product code ' . $product_code . ' and product name ' . $product . ' already exists. Quantity not updated. Currency mismatch. Unit cost of this part must be in ' . $currency . '.</p>';
        }
    }
    else {
        $q = 'INSERT INTO item (serial_number, description, image, quantity, unit_cost, company_id, currency_id, item_group_id, item_type_id, project_id, shelf_id, user_id, last_access, date_added) 
            VALUES (:serial_number, :description, :image, :quantity, :unit_cost, :company_id, :currency_id, :item_group_id, :item_type_id, :project_id, :shelf_id, :user_id, :last_access, :date_added)';
        $params = array(':serial_number' => $serial_number, ':description' => $description, ':image' => $image, ':quantity' => $quantity, ':unit_cost' => $unit_cost, 
            ':company_id' => $company_id, ':currency_id' => $currency_id, ':item_group_id' => $group_id, ':item_type_id' => $product_id, ':project_id' => $project_id, ':shelf_id' => $shelf_id, ':user_id' => $user_id, ':last_access'=> date("Y-m-d H:i:s"), ':date_added'=> date("Y-m-d H:i:s"));

        $id = null;
        $rows = sql_insert($id, $q, $params);

        if ($rows == 1) {
            $sn = $serial_number;
            if ($serial_number == '') {
                echo '<p>Non-unique part with product code ' . $product_code . ' and product name ' . $product . ' added.</p>';
                $sn = 'n/a';
            }
            else {
                echo '<p>Unique part with serial number ' . $serial_number . ' added.</p>';
            }
            logger('add', 'S/N :: ' . $sn . ' | Description :: ' . $description . ' | Product code :: ' . $product_code . ' | Product :: ' . $product . ' | Quantity :: ' . $quantity);
        }
        else {
            echo '<p class="error">Part not added.</p>';
        }
    }
}
elseif (isset($_POST['updatepart'])) {
    $id = $_POST['updatepart'];
    $serial_number = trim($_POST['serial_number']);
    $description = trim($_POST['part_description']);
    
    $quantity = trim($_POST['quantity']);
    $unit_cost = trim($_POST['unit_cost']);
    
    $company_id = $_POST['company_id'];
    $currency_id = $_POST['currency_id'];
    $group_id = $_POST['group_id'];
    $product_id = $_POST['product_id'];
    $project_id = $_POST['project_id'];
    $shelf_id = $_POST['shelf_id'];
    
    $q = 'SELECT product_code, description FROM item_type WHERE id=:id';
    $params = array(':id' => $product_id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows == 1) {
        foreach ($result as $row) {
            $product_code = $row['product_code'];
            $product = $row['description'];
        }
    }
    
    $image = $_POST['image'];
    if ($_FILES['imagefile']['name'] != '') {
        if ($image != '' && file_exists($path . $image)) {
            unlink($path . $image);
        }
		if ($image != '' && file_exists($path_thumb . $image)) {
            unlink($path_thumb . $image);
        }
        $filename = $product_code . '_' . time();
        $image = $filename . str_replace('image/', '.', $_FILES['imagefile']['type']);
        move_uploaded_file($_FILES['imagefile']['tmp_name'], $path . $image);
		make_thumb($path.$image, $path_thumb.$image, 150);
    }
    
    $q = 'UPDATE item SET serial_number=:serial_number, description=:description, image=:image, quantity=:quantity, unit_cost=:unit_cost, 
        company_id=:company_id, currency_id=:currency_id, item_group_id=:item_group_id, item_type_id=:item_type_id, project_id=:project_id, shelf_id=:shelf_id, last_access=:last_access
        WHERE id=:id';
    $params = array(':id' => $id, ':serial_number' => $serial_number, ':description' => $description, ':image' => $image, ':quantity' => $quantity, ':unit_cost' => $unit_cost, ':company_id' => $company_id, ':currency_id' => $currency_id, ':item_group_id' => $group_id, ':item_type_id' => $product_id, ':project_id' => $project_id, ':shelf_id' => $shelf_id, ':last_access'=> date("Y-m-d H:i:s"));
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1 || $_FILES['imagefile']['name'] != '') {
        $sn = $serial_number;
        if ($serial_number == '') {
            echo '<p>Non-unique part with product code ' . $product_code . ' and product name ' . $product . ' updated.</p>';
            $sn = 'n/a';
        }
        else {
            echo '<p>Unique part with serial number ' . $serial_number . ' updated.</p>';
        }
        logger('update', 'S/N :: ' . $sn . ' | Description :: ' . $description . ' | Product code :: ' . $product_code . ' | Product :: ' . $product . ' | Quantity :: ' . $quantity);
    }
    else {
        echo '<p class="error">Part not updated.</p>';
    }
}
elseif (isset($_POST['removepart'])) {
    $id = $_POST['removepart'];
    
    $q = 'SELECT item.id, item.serial_number, item.description AS item_description, item.image, item.quantity, item.item_type_id, 
        item_type.product_code, item_type.description AS product
        FROM item
        INNER JOIN item_type ON item.item_type_id=item_type.id
        WHERE item.id=:id';
    $params = array(':id' => $id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows == 1) {
        foreach ($result as $row) {
            $serial_number = $row['serial_number'];
            $description = $row['item_description'];
            $quantity = $row['quantity'];
            $image = $row['image'];
            $product_code = $row['product_code'];
            $product = $row['product'];
        }
    }
    
    $q = 'DELETE FROM item WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        if ($image != '' && file_exists($path . $image)) {
            unlink($path . $image);
        }
		if ($image != '' && file_exists($path_thumb . $image)) {
            unlink($path_thumb . $image);
        }
        
        $sn = $serial_number;
        if ($serial_number == '') {
            echo '<p>Non-unique part with product code ' . $product_code . ' and product name ' . $product . ' removed.</p>';
            $sn = 'n/a';
        }
        else {
            echo '<p>Unique part with serial number ' . $serial_number . ' removed.</p>';
        }
        logger('remove', 'S/N :: ' . $sn . ' | Description :: ' . $description . ' | Product code :: ' . $product_code . ' | Product :: ' . $product . ' | Quantity :: ' . $quantity);
    }
    else {
        echo '<p class="error">Part not removed.</p>';
    }
}
elseif (isset($_POST['addnewproduct'])) {
    $product_code = trim($_POST['product_code']);
    $product_description = trim($_POST['product_description']);
    
    $serial_number_copy = trim($_POST['serial_number_copy']);
    $part_description_copy = trim($_POST['part_description_copy']);
    $company_id_copy = trim($_POST['company_id_copy']);
    $shelf_id_copy = trim($_POST['shelf_id_copy']);
    $project_id_copy = trim($_POST['project_id_copy']);
    $group_id_copy = trim($_POST['group_id_copy']);
    $currency_id_copy = trim($_POST['currency_id_copy']);
    $unit_cost_copy = trim($_POST['unit_cost_copy']);
    $quantity_copy = trim($_POST['quantity_copy']);
    
    $q = 'INSERT INTO item_type (product_code, description) VALUES (:product_code, :description)';
    $params = array(':product_code' => $product_code, ':description' => $product_description);
    
    $rows = sql_insert($product_id_copy, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Product ' . $product_description . ' added.</p>';
        logger('add', 'Product code :: ' . $product_code . ' | Product :: ' . $product_description);
    }
    else {
        echo '<p class="error">Product not added.</p>';
    }
    
    $auxclass = ' visible';
}

$q = 'SELECT id, description FROM company ORDER BY description ASC';
$company_array = build_result_array($q);

$q = 'SELECT id, description FROM currency ORDER BY id ASC';
$currency_array = build_result_array($q);

$q = $q = "(SELECT id, description FROM item_group WHERE description='Unassigned') UNION (SELECT id, description FROM item_group WHERE description NOT IN ('Unassigned') ORDER BY description ASC)";
$group_array = build_result_array($q);

$q = "SELECT id, CONCAT(product_code, ' &ndash; ', item_type.description) AS description FROM item_type ORDER BY item_type.description ASC";
$product_array = build_result_array($q);

$q = $q = "(SELECT id, description FROM project WHERE description='Unassigned') UNION (SELECT id, description FROM project WHERE description NOT IN ('Unassigned') ORDER BY description ASC)";
$project_array = build_result_array($q);

$q = "SELECT shelf.id, CONCAT(shelf.code, ' @ ', storeroom.description) AS description FROM shelf INNER JOIN storeroom ON shelf.storeroom_id=storeroom.id ORDER BY storeroom.description ASC, shelf.description ASC";
$location_array = build_result_array($q);
?>
<div id='addproduct' class='hidden'>
    <h3>Add Product</h3>
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addnewproductform' action='index.php?pid=parts' method='post'>
            <tr>
                <th>Product Code</th>
                <td>
                    <input type='text' name='product_code' value=''>
                </td>
            </tr>
            <tr>
                <th>Name</th>
                <td>
                    <input type='text' name='product_description' value=''>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addnewproduct' value='addnewproduct'>
                    <input type='hidden' name='serial_number_copy' value=''>
                    <input type='hidden' name='part_description_copy' value=''>
                    <input type='hidden' name='company_id_copy' value=''>
                    <input type='hidden' name='shelf_id_copy' value=''>
                    <input type='hidden' name='project_id_copy' value=''>
                    <input type='hidden' name='group_id_copy' value=''>
                    <input type='hidden' name='currency_id_copy' value=''>
                    <input type='hidden' name='unit_cost_copy' value=''>
                    <input type='hidden' name='quantity_copy' value=''>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<div class='auxwrapper'>
    <?php if (dependencies_exist(array('item_type', 'shelf', 'company'))) {
        echo '<input type="button" value="Add">';
    }
    else {
        echo '<span class="error">Parts dependency missing.</span>';
    } ?>
    
    <div class='aux<?php echo $auxclass; ?>'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addpartform' action='index.php?pid=parts' method='post' enctype="multipart/form-data">
            <tr>
                <th>Serial Number</th>
                <td>
                    <input id='focus' type='text' name='serial_number' value='<?php echo $serial_number_copy ?>'>
                </td>
            </tr>
            <tr>
                <th>Description</th>
                <td>
                    <input type='text' name='part_description' value='<?php echo $part_description_copy ?>'>
                </td>
            </tr>
            <tr>
                <th>Product</th>
                <td>
                    <?php build_select_options('product_id', $product_array, array('class' => 'chzn-select', 'selected' => $product_id_copy)) ?>
                    <div class='addlink'><a class='lightbox' href='#addproduct'>New Product</a></div>
                </td>
            </tr>
            <tr>
                <th>Company</th>
                <td>
                    <?php build_select_options('company_id', $company_array, array('class' => 'chzn-select', 'selected' => $company_id_copy)) ?>
                </td>
            </tr>
            <tr>
                <th>Location</th>
                <td>
                    <?php build_select_options('shelf_id', $location_array, array('class' => 'chzn-select', 'selected' => $shelf_id_copy)) ?>
                </td>
            </tr>
            <tr>
                <th>Project</th>
                <td>
                    <?php build_select_options('project_id', $project_array, array('class' => 'chzn-select', 'selected' => $project_id_copy)) ?>
                </td>
            </tr>
            <tr>
                <th>Group</th>
                <td>
                    <?php build_select_options('group_id', $group_array, array('class' => 'chzn-select', 'selected' => $group_id_copy)) ?>
                </td>
            </tr>
            <tr>
                <th>Unit Cost</th>
                <td>
                    <?php build_select_options('currency_id', $currency_array, array('class' => 'chzn-select', 'selected' => $currency_id_copy)) ?>
                    <input type='text' name='unit_cost' value='<?php echo $unit_cost_copy ?>'>
                </td>
            </tr>
            <tr class='quantity'>
                <th>Quantity</th>
                <td>
                    <input type='text' name='quantity' value='<?php echo $quantity_copy ?>'>
                </td>
            </tr>
            <tr>
                <th>Image</th>
                <td>
                    <div class='fileupload'><span>Upload image</span></div>
                    <input class='fileinput' type='file' accept='image/*' name='imagefile'>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addpart' value='addpart'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>


<div id="ajaxdiv" class='table'>
</div>
