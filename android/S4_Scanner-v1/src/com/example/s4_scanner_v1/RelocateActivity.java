package com.example.s4_scanner_v1;

import MyContent.MyRelocationAdapter;
import MyContent.RelocateItemContent;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class RelocateActivity extends Activity {

	private ListView listview;
	private AutoCompleteTextView actv;
	private ProgressBar spinner;
	private String itemSelectedID;
	private RelativeLayout relativeLayout;
	private RelocateItemContent.RelocateItem currentItem;
	final Context context = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_relocate);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		RelocateItemContent.adapter = new MyRelocationAdapter(this,RelocateItemContent.ITEMS);
		listview = (ListView) findViewById(R.id.listViewRelocate);
		listview.setAdapter(RelocateItemContent.adapter);
		
    	actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewRelocateSort); 
    	RelocateItemContent.adapterOrders = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, RelocateItemContent.ORDERS);	
		actv.setAdapter(RelocateItemContent.adapterOrders);	
		
		itemSelectedID = "0";
		
	      spinner = (ProgressBar)findViewById(R.id.progressBarRelocate);
	      spinner.setVisibility(View.GONE);
	    
	    relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayoutRelocateContent);
	    
	    ((TextView) findViewById(R.id.textViewRelocateDetails1)).setText("Order Number :");
	    ((TextView) findViewById(R.id.textViewRelocateDetails2)).setText("Supplier Code :");
	    ((TextView) findViewById(R.id.textViewRelocateDetails3)).setText("Project Name :");
	    ((TextView) findViewById(R.id.textViewRelocateDetails4)).setText("Description :");
	    ((TextView) findViewById(R.id.textViewRelocateDetails5)).setText("Barcode :");
	    ((TextView) findViewById(R.id.textViewRelocateDetails6)).setText("Previous Shelf Barcode :");
	    ((TextView) findViewById(R.id.textViewRelocateDetails7)).setText("Previous Shelf Description :");
	    
		final TextView tvT_OrderNumber = (TextView) findViewById(R.id.textViewRelocateDetails1);
		final TextView tvT_SupplierCode = (TextView) findViewById(R.id.textViewRelocateDetails2);
		final TextView tvT_ProjectName = (TextView) findViewById(R.id.textViewRelocateDetails3);
		final TextView tvT_Description = (TextView) findViewById(R.id.textViewRelocateDetails4);
		final TextView tvT_Barcode = (TextView) findViewById(R.id.textViewRelocateDetails5);
		final TextView tvT_PreviousShelfBarcode = (TextView) findViewById(R.id.textViewRelocateDetails6);
		final TextView tvT_PreviousShelfDescription = (TextView) findViewById(R.id.textViewRelocateDetails7);
		final View v_PageSlitter2 = (View) findViewById(R.id.ViewRelocatePageSplitter2);
	    
		tvT_OrderNumber.setText("Order Number :");
		tvT_SupplierCode.setText("Supplier Code :");
		tvT_ProjectName.setText("Project Name :");
		tvT_Description.setText("Description :");
		tvT_Barcode.setText("Barcode :");
		tvT_PreviousShelfBarcode.setText("Previous Shelf Barcode :");
		tvT_PreviousShelfDescription.setText("Previous Shelf Description :");
		
	    
	final TextView tvOrderNumber = (TextView) findViewById(R.id.textViewRelocateDetails1desc);
	final TextView tvSupplierCode = (TextView) findViewById(R.id.textViewRelocateDetails2desc);
	final TextView tvProjectName = (TextView) findViewById(R.id.textViewRelocateDetails3desc);
	final TextView tvDescription = (TextView) findViewById(R.id.textViewRelocateDetails4desc);
	final TextView tvBarcode = (TextView) findViewById(R.id.textViewRelocateDetails5desc);
	final TextView tvPreviousShelfBarcode = (TextView) findViewById(R.id.textViewRelocateDetails6desc);
	final TextView tvPreviousShelfDescription = (TextView) findViewById(R.id.textViewRelocateDetails7desc);
	final CheckBox cbItemCheckout = (CheckBox) findViewById(R.id.checkBoxRelocate);
	
	Button buttonOrderSort = (Button) findViewById(R.id.buttonRelocateOrderSort);
	final Button buttonServerUpdate = (Button) findViewById(R.id.buttonRelocateServerUpdate);
	
	if (itemSelectedID.equals("0")) { relativeLayout.setVisibility(View.GONE); }
	else { relativeLayout.setVisibility(View.VISIBLE);}
	
	buttonOrderSort.setOnClickListener(new OnClickListener(){
		
		@Override
		public void onClick(View v) {
			RelocateItemContent.ReOrder(actv.getText().toString());	
			RelocateItemContent.adapter.notifyDataSetChanged();
			
			itemSelectedID = "0";
			relativeLayout.setVisibility(View.GONE);
		}
		
	});
	
	listview.setOnItemClickListener(new OnItemClickListener() {
		  @Override
		  public void onItemClick(AdapterView<?> parent, View view,
		    int position, long id) {

			  itemSelectedID = ((RelocateItemContent.RelocateItem) parent.getItemAtPosition(position)).id;
			  currentItem = RelocateItemContent.ITEM_MAP.get(itemSelectedID);

			  relativeLayout.setVisibility(View.VISIBLE);
			  
				tvOrderNumber.setText(currentItem.orderNumber);			
				tvSupplierCode.setText(currentItem.supplierCode);			
				tvProjectName.setText(currentItem.projectName);
				tvDescription.setText(currentItem.description);	
				tvBarcode.setText(currentItem.barcode);
				tvPreviousShelfBarcode.setText(currentItem.previousShelfBarcode);
				tvPreviousShelfDescription.setText(currentItem.previousShelfDescription);
				cbItemCheckout.setChecked(currentItem.itemSelected);
				
		  }
		}); 
	
	cbItemCheckout.setOnClickListener(new OnClickListener() {
		 
		  @Override
		  public void onClick(View v) {
			  
			if (((CheckBox) v).isChecked()) {
				currentItem.itemSelected = true;
			}
			else{
				currentItem.itemSelected = false;
			}	 
			RelocateItemContent.ITEM_MAP.put(itemSelectedID, currentItem);
			RelocateItemContent.adapter.notifyDataSetChanged();
		  }
		});
		
	buttonServerUpdate.setOnClickListener(new OnClickListener(){
		
		@Override
		public void onClick(View v) {
			
	        Intent intent = new Intent(context, DestinationActivity.class);
	        intent.putExtra("SOURCE", "Relocate");
	        startActivityForResult(intent, 2);
		}
		
	});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.relocate, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	 @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

	     if (requestCode == 2) {
	          if (resultCode == -1) {
	             finish();

	          }
	      }
	 }
}
