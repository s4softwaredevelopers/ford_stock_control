package com.example.s4_scanner_v1;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import MyContent.MyWebRequest;
import MyContent.UsersContent;
import MyContent.UsersContent.Users;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

public class LoginActivity extends Activity {

	private ProgressBar spinner;
	private AutoCompleteTextView actv;
	private String source;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		 this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
			
	      spinner = (ProgressBar)findViewById(R.id.progressLogin);
	      spinner.setVisibility(View.GONE);
	      
	      Intent intent = getIntent();
	      source = intent.getStringExtra("SOURCE");
	      
	      if (source.equals("QRelocate")){   	
	    	  ((EditText) findViewById(R.id.editTextLoginDescription)).setHint("Reason for items return");
	      }
	      if (source.equals("Capture")){   	
	    	  ((EditText) findViewById(R.id.editTextLoginDescription)).setHint("Reason for Capture of new items");
	      }
	      
	      UsersContent.bReturnUser = false;
	      
		new WebRequestUsersPopuate().execute("");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	class WebRequestUsersPopuate extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=login&fid=user";
			String result = "";
			try{
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				UsersPopulate(result);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return "";
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("")) 
	    	{
	    		UserDropdownCreate();
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	    public void UsersPopulate(String html){
	    	
	    	UsersContent.USERS.clear();
	    	UsersContent.USER_MAP.clear();
	    	
	    	Document doc = Jsoup.parse(html);
	    	Elements tableElements = doc.select("table");
	    	Elements tableRowElements = tableElements.select(":not(thead) tr");
	        for (int i = 0; i < tableRowElements.size(); i++) {
	            Element row = tableRowElements.get(i);
	            List<String> ColList = new ArrayList<String>();
	            Elements rowItems = row.select("td");
	            for (int j = 0; j < rowItems.size(); j++) {
	            	ColList.add(rowItems.get(j).text());
	            }
	            UsersContent.addItem( new Users(ColList.get(0), ColList.get(1)));
	        }
	    }

	}
	
    public void UserDropdownCreate(){
    	
    	actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewLogin); 
		UsersContent.adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, UsersContent.USERS_N);		
		actv.setAdapter(UsersContent.adapter);	
    }
    
	public void LoginUser(View view) {
		new WebRequestRelocateUserUpdate().execute("");
	}
    
	class WebRequestRelocateUserUpdate extends AsyncTask<String, Void, String> {

		EditText etPassword;
		EditText etDescription;
		String sPassword = ""; 
		String sDescription = ""; 
		String sUser = "";
		//String sUserName = "";
		
		@Override
		protected String doInBackground(String... params) {
			
			if (sUser.equals("Invalid User")){ return "Invalid User";}
			if (sUser.equals("")){ return "No User Selected";}
			if (sPassword.equals("")){ return "No Password";}
			if (sDescription.equals("")){ return "No Reasons";}
			
			String urlParameters = "?pid=login&fid=details";
			String result = "";
			try{
			
				urlParameters += SendParameters();
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return result;
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("Login Successfully")) 
	    	{
	    		Toast.makeText(getBaseContext(),"You have been successfully logged in",Toast.LENGTH_LONG).show();
	    		etPassword.setText("");
	    		UsersContent.returnUserid = sUser;
	    		UsersContent.returnDescription = sDescription;
	    		UsersContent.bReturnUser = true;
	    		LoginComplete();
	    	}
	    	else
	    	{
	    		etPassword.setText("");
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	try { sUser = UsersContent.USERS_ID.get(UsersContent.USERS_N.indexOf(actv.getText().toString())); }
	    	catch(Exception e) { sUser = "Invalid User"; }
	    	etPassword = (EditText) findViewById(R.id.editTextLoginPassword);
	    	sPassword = etPassword.getText().toString();
	    	etDescription = (EditText) findViewById(R.id.editTextLoginDescription);
	    	sDescription = etDescription.getText().toString();
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
	    
	    public String SendParameters(){
	    	String param = "";
	    	param += "&userid=" + sUser + "&password=" + sPassword;
	    	return param;
	    }

	}

	public void LoginComplete() {
		Intent intent = null;
		if (source.equals("QRelocate")){   	
			intent = new Intent(this, QRelocateActivity.class);
		}
		if (source.equals("Capture")){
			intent = new Intent(this, CaptureActivity.class);
		}
        startActivity(intent);
	}
    
}
