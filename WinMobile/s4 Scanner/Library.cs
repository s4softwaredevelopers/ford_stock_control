using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Xml;
using System.Reflection;
//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Text;

namespace s4_Scanner
{
    public class ScanItem //Item details 
    {
        private string id;
        private string orderId;
        private string supplierCode;
        private string projectName;
        private string description;
        private string barcode;
        private string database;
        private string shelf;
        private string previousShelf;
        private string previousShelfDescription;
        private string previousStoreeoomLocation;
        private bool userSelected;

        public ScanItem(string id, string orderId, string supplierCode, string projectName, string description, string barcode, string database, string shelf, string previousShelf, string previousShelfDescription, string previousStoreeoomLocation)
        {
            this.id = id;
            this.orderId = orderId;
            this.supplierCode = supplierCode;
            this.projectName = projectName;
            this.description = description;
            this.database = database;
            this.barcode = barcode;
            this.shelf = shelf;
            this.previousShelf = previousShelf;
            this.previousShelfDescription = previousShelfDescription;
            this.previousStoreeoomLocation = previousStoreeoomLocation;
            userSelected = false;
        }

        public string Id
        {
            get { return id; }
            set { id = value; }
        }
        public string OrderId
        {
            get { return orderId; }
            set { orderId = value; }
        }
        public string SupplierCode
        {
            get { return supplierCode; }
            set { supplierCode = value; }
        }
        public string ProjectName
        {
            get { return projectName; }
            set { projectName = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string Barcode
        {
            get { return barcode; }
            set { barcode = value; }
        }
        public string Database
        {
            get { return database; }
            set { database = value; }
        }
        public string Shelf
        {
            get { return shelf; }
            set { shelf = value; }
        }
        public string PreviousShelf
        {
            get { return previousShelf; }
            set { previousShelf = value; }
        }
        public string PreviousShelfDescription
        {
            get { return previousShelfDescription; }
            set { previousShelfDescription = value; }
        }
        public string PreviousStoreeoomLocation
        {
            get { return previousStoreeoomLocation; }
            set { previousStoreeoomLocation = value; }
        }
        public bool UserSelected
        {
            get { return userSelected; }
            set { userSelected = value; }
        }
    }

    public class Users // Users info
    {
        private string ID;
        private string Name;

        public Users(string ID, string Name)
        {
            this.ID = ID;
            this.Name = Name;
        }

        public string UserID
        {
            get { return ID; }
            set { ID = value; }
        }
        public string UserName
        {
            get { return Name; }
            set { Name = value; }
        }
    }

    public class MyLibrary // Server communication functions
    {
        public static void GetDataFromServer(out bool state, out string message, out List<ScanItem> ItemsList)// Marry Get data
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=marryItems&&fid=display";
            state = false;
            message = "";
            ItemsList = new List<ScanItem>();
            string DataString = "NONE";
            string ReceiveDataString = "";
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                return;
            }
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "No items found!")
            {
                state = false;
                message = "No items found!";
                return;
            }
            try
            {
                StringBuilder databuilder = new StringBuilder(ReceiveDataString);
                databuilder.Replace("</tr><tr>", '\u0232'.ToString());
                databuilder.Replace("<tr>", '\u0232'.ToString());
                databuilder.Replace("</tr>", '\u0232'.ToString());
                databuilder.Replace('\u0232'.ToString() + '\u0232'.ToString(), '\u0232'.ToString()); // remove empty row
                string[] rowValues = databuilder.ToString().Split(new char[] { '\u0232' });
                List<string> rowList = new List<string>(rowValues);
                rowList.RemoveAt(0);
                rowList.RemoveAt(rowList.Count - 1);
                if (rowList.Count > 0)
                {
                    foreach (string rowelement in rowList)
                    {
                        StringBuilder rowbuilder = new StringBuilder(rowelement);
                        rowbuilder.Replace("</td><td>", '\u0232'.ToString());
                        rowbuilder.Replace("<td>", '\u0232'.ToString());
                        rowbuilder.Replace("</td>", '\u0232'.ToString());
                        string[] colValues = rowbuilder.ToString().Split(new char[] { '\u0232' });
                        List<string> colList = new List<string>(colValues);
                        colList.RemoveAt(0);
                        colList.RemoveAt(colList.Count - 1);
                        if (colList[5] == "no") { colList[5] = ""; }
                        ItemsList.Add(new ScanItem(colList[1], colList[2], colList[3], colList[4], colList[5], colList[6], colList[7], "", "", "", ""));    
                    }
                    state = true;
                }
            }
            catch
            {
                state = false;
                message = "Data Format Error";
                return;
            }           
        }

        public static void SendDataToServer(out bool state, out string message, List<ScanItem> ItemsList, out string ReceiveDataString) // Marry Send data
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=marryItems&&fid=update";
            state = false;
            message = "";
            ReceiveDataString = "";
            if (ItemsList.Count == 0) 
            {
                state = true;
                message = "No Data to Update";
                return;
            }
            string DataString = "";
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].Barcode != "")
                {
                    DataString = DataString + "&&ID" + count + "=" + ItemsList[count].Id + "&&BC" + count + "=" + ItemsList[count].Barcode + "&&SC" + count + "=" + "0";
                }
            }
            if (DataString == "")
            {
                state = true;
                message = "No Data to Update";
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Assign Barcodes - Send data to server : " + DataString); 
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Assign Barcodes - Send data to server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Assign Barcodes - Send data to server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Updated";
                return;
            }
            state = true;
            message = "Updated";
            return;
            //Data.State = false;
            //Data.Message = Data.ReceiveDataString;
            //return Data;        
        }

        public static void RelocateBarcode(out bool state, out string message, List<string> BarcodeList, string Shelf, string ReturnName, string ReturnUser, string ReturnDescription) // Relocate list barcodes to shelf
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=setLocation&&fid=update";
            string DataString = "";
            string ReceiveDataString = "";
            message = "";
            if (BarcodeList.Count == 0) 
            {
                state = true;
                return; 
            }
            for (int count = 0; count < BarcodeList.Count; count++)
            {
                DataString += "&&barcode" + count + "=" + BarcodeList[count] + "&&shelf" + count + "=" + Shelf;
                             
            }
            if (ReturnName != "")
            {
                DataString += "&&returnUser=" + ReturnUser + "&&returnDescription=" + ReturnDescription;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Relocate - Send data to server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Send data to server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Updated";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        public static void RelocateLaterBarcode(out bool state, out string message, List<string> BarcodeList, string ReturnName, string ReturnUser, string ReturnDescription) // Relocate list barcodes for later
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=setLocation&&fid=empty";
            string DataString = "";
            string ReceiveDataString = "";
            //state = false;
            message = "";
            if (BarcodeList.Count == 0)
            {
                state = true;
                return;
            }
            for (int count = 0; count < BarcodeList.Count; count++)
            {
                DataString += "&&barcode" + count + "=" + BarcodeList[count];
            }
            if (ReturnName != "")
            {
                DataString += "&&returnUser=" + ReturnUser + "&&returnDescription=" + ReturnDescription;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate Later - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Relocate Later - Send data to server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate Later - Send data to server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Updated";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        public static void UserLoginRelocation(out bool state, out string message, List<string> BarcodeList, string UserName, string UserId, string UserPassword,string UserDescription, string ReturnName, string ReturnUser, string ReturnDescription) // Relocate list barcodes to person
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=setLocation&&fid=userUpdate";
            string DataString = "";
            string ReceiveDataString = "";
            message = "";
            if (BarcodeList.Count == 0)
            {
                state = true;
                return;
            }
            for (int count = 0; count < BarcodeList.Count; count++)
            {
                DataString += "&&barcode" + count + "=" + BarcodeList[count];
            }
            DataString += "&&username=" + UserName + "&&userid=" + UserId + "&&password=" + UserPassword + "&&description=" + UserDescription;
            if (ReturnName != "")
            {
                DataString += "&&returnUser=" + ReturnUser + "&&returnDescription=" + ReturnDescription;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Relocate - Send data to server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Send data to server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Updated";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;  

        }

        public static void UsersPopulate(out bool state, out string message, out List<Users> UsersList) //Get all Users Info 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=login&&fid=user&";
            string DataString = "NONE";
            string ReceiveDataString = "";
            state = false;
            message = "";
            UsersList = new List<Users>();
            MyLogFile.LogFile("CONNECT ", "Users Populate - Send data to server");
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Users Populate - Get data from server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Users Populate - Get data from server response"); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            try
            {
                StringBuilder databuilder = new StringBuilder(ReceiveDataString);
                databuilder.Replace("</tr><tr>", '\u0232'.ToString());
                databuilder.Replace("<tr>", '\u0232'.ToString());
                databuilder.Replace("</tr>", '\u0232'.ToString());
                databuilder.Replace('\u0232'.ToString() + '\u0232'.ToString(), '\u0232'.ToString()); // remove empty row
                string[] rowValues = databuilder.ToString().Split(new char[] { '\u0232' });
                List<string> rowList = new List<string>(rowValues);
                rowList.RemoveAt(0);
                rowList.RemoveAt(rowList.Count - 1);
                if (rowList.Count > 0)
                {
                    foreach (string rowelement in rowList)
                    {
                        StringBuilder rowbuilder = new StringBuilder(rowelement);
                        rowbuilder.Replace("</td><td>", '\u0232'.ToString());
                        rowbuilder.Replace("<td>", '\u0232'.ToString());
                        rowbuilder.Replace("</td>", '\u0232'.ToString());
                        string[] colValues = rowbuilder.ToString().Split(new char[] { '\u0232' });
                        List<string> colList = new List<string>(colValues);
                        colList.RemoveAt(0);
                        colList.RemoveAt(colList.Count - 1);
                        UsersList.Add(new Users(colList[0], colList[1]));
                    }
                    state = true;
                }
            }
            catch
            {
                state = false;
                message = "Data Format Error";
                return;
            }
        }

        public static void LoginDetails(out bool state, out string message, string UserName, string UserId, string UserPassword) //Login Check 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=login&&fid=details";
            string DataString = "";
            string ReceiveDataString = "";
            if (UserName == "")
            {
                state = false;
                message = "User Required";
                return;
            }
            if (UserPassword == "")
            {
                state = false;
                message = "Password Required";
                return;
            }
            DataString = "&&userid=" + UserId + "&&password=" + UserPassword;
            MyLogFile.LogFile("CONNECT ", "Login - Send data to server : " + UserId);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Login - Get data from server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Login - Get data from server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "Login Successfully")
            {
                state = true;
                message = ReceiveDataString;
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        public static void GetPendingDataFromServer(out bool state, out string message, out List<ScanItem> ItemsList) //Get Pending List 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=setLocation&&fid=display";
            string DataString = "NONE";
            string ReceiveDataString = "";
            state = false;
            message = "";
            ItemsList = new List<ScanItem>();
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                return;
            }
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "No items found!")
            {
                state = false;
                message = "No items found!";
                return;
            }
            try
            {
                StringBuilder databuilder = new StringBuilder(ReceiveDataString);
                databuilder.Replace("</tr><tr>", '\u0232'.ToString());
                databuilder.Replace("<tr>", '\u0232'.ToString());
                databuilder.Replace("</tr>", '\u0232'.ToString());
                databuilder.Replace('\u0232'.ToString() + '\u0232'.ToString(), '\u0232'.ToString()); // remove empty row
                string[] rowValues = databuilder.ToString().Split(new char[] { '\u0232' });
                List<string> rowList = new List<string>(rowValues);
                rowList.RemoveAt(0);
                rowList.RemoveAt(rowList.Count - 1);
                if (rowList.Count > 0)
                {
                    foreach (string rowelement in rowList)
                    {
                        StringBuilder rowbuilder = new StringBuilder(rowelement);
                        rowbuilder.Replace("</td><td>", '\u0232'.ToString());
                        rowbuilder.Replace("<td>", '\u0232'.ToString());
                        rowbuilder.Replace("</td>", '\u0232'.ToString());
                        string[] colValues = rowbuilder.ToString().Split(new char[] { '\u0232' });
                        List<string> colList = new List<string>(colValues);
                        colList.RemoveAt(0);
                        colList.RemoveAt(colList.Count - 1);
                        if (colList[5] == "no") { colList[5] = ""; }
                        ItemsList.Add(new ScanItem(colList[1], colList[2], colList[3], colList[4], colList[5], colList[6], colList[7], "", colList[8], colList[9], ""));
                    }
                    state = true;
                }
            }
            catch
            {
                state = false;
                message = "Data Format Error";
                return;
            }
        }

        public static void RelocatePendingBarcode(out bool state, out string message, List<ScanItem> ItemsList, string shelf) //Pending to Shelf 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=setLocation&&fid=update";
            string DataString = "";
            string ReceiveDataString = "";
            state = false;
            message = "";
            if (ItemsList.Count == 0)
            {
                state = true;
                return;
            }
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].UserSelected)
                {
                    DataString += "&&barcode" + count + "=" + ItemsList[count].Barcode + "&&shelf" + count + "=" + shelf;
                }
            }
            if ( DataString == "")
            {
                state = true;
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Relocate - Get data from server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Get data from server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Updated";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        public static void PendingUserLoginRelocation(out bool state, out string message, List<ScanItem> ItemsList, string UserName, string UserId, string UserPassword, string UserDescription) //Pending to Person 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=setLocation&&fid=userUpdate";
            string DataString = "";
            string ReceiveDataString = "";
            message = "";
            state = false;
            if (ItemsList.Count == 0)
            {
                state = true;
                return;
            }
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].UserSelected)
                {
                    DataString += "&&barcode" + count + "=" + ItemsList[count].Barcode;
                }
            }
            DataString += "&&username=" + UserName + "&&userid=" + UserId + "&&password=" + UserPassword + "&&description=" + UserDescription;
            MyLogFile.LogFile("CONNECT ", "Relocate - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Relocate - Get data from server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Relocate - Get data from server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Updated";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        public static void Print(out bool state, out string message, int Quantity) //Print Barcodes 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=untrackedReturn&&fid=print";
            string DataString = "";
            string ReceiveDataString = "";
            message = "";
            state = false;

            if (Quantity == 0)
            {
                state = false;
                message = "Zero Quantity";
                return;
            }
            DataString = "quantity=" + Quantity;
            MyLogFile.LogFile("CONNECT ", "Print - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Print - Get data from server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Print - Get data from server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Printed";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        public static void CaptureItems(out bool state, out string message, string description, int quantity) //Capture Items 
        {
            string URL = MobileConfiguration.Settings["URL"] + "pid=untrackedReturn&&fid=update";
            string DataString = "";
            string ReceiveDataString = "";
            message = "";
            state = false;

            if (description == "")
            {
                state = false;
                message = "Item information missing";
                return;
            }

            DataString = "&&description=" + description + "&&quantity=" + quantity;
            MyLogFile.LogFile("CONNECT ", "Capture - Send data to server : " + DataString);
            try
            {
                MyWebRequest myRequest = new MyWebRequest(URL, "POST", DataString);
                ReceiveDataString = myRequest.GetResponse();
            }
            catch
            {
                state = false;
                message = "Network can't connect";
                MyLogFile.LogFile("ERROR   ", "Print - Get data from server response");
                return;
            }
            MyLogFile.LogFile("CONNECT ", "Print - Get data from server response: " + ReceiveDataString); 
            if (ReceiveDataString == "")
            {
                state = false;
                message = "Connection Lost";
                return;
            }
            if (ReceiveDataString == "done")
            {
                state = true;
                message = "Items Added";
                return;
            }
            state = false;
            message = ReceiveDataString;
            return;
        }

        private const int BS_MULTILINE = 0x00002000;
        private const int GWL_STYLE = -16;

        [System.Runtime.InteropServices.DllImport("coredll")]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [System.Runtime.InteropServices.DllImport("coredll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        public static void MakeButtonMultiline(Button b)
        {
            IntPtr hwnd = b.Handle;
            int currentStyle = GetWindowLong(hwnd, GWL_STYLE);
            int newStyle = SetWindowLong(hwnd, GWL_STYLE, currentStyle | BS_MULTILINE);
        }
    }

    public class MyMenu // Menu Navigation
    {
        public static void Marry()
        {
            Marry MarryForm = new Marry();
            MarryForm.Show();
        }
        public static void Capture()
        {
            Capture CaptureForm = new Capture();
            CaptureForm.Show();
        }
        public static void RelocateMenu()
        {
            Relocation_Menu RelocateMenuForm = new Relocation_Menu();
            RelocateMenuForm.Show();
        }
        public static void Relocate(string sSource, string sDestination)
        {
            if (sSource == "Shelf" || sSource == "User")
            {
                Relocation RelocateForm = new Relocation(sSource, sDestination);
                RelocateForm.Show();
            }
            if (sSource == "Pending")
            {
                Relocate_Pending RelocatePendingForm = new Relocate_Pending(sDestination);
                RelocatePendingForm.Show();
            }
        }
    }

    public class MyWebRequest // Web Request
    {
        private WebRequest request;
        private Stream dataStream;

        private string status;

        public String Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public MyWebRequest(string url)
        {
            // Create a request using a URL that can receive a post.

            request = WebRequest.Create(url);
            request.Timeout = 15000;
        }

        public MyWebRequest(string url, string method)
            : this(url)
        {

            if (method.Equals("GET") || method.Equals("POST"))
            {
                // Set the Method property of the request to POST.
                request.Method = method;
            }
            else
            {
                throw new Exception("Invalid Method Type");
            }
        }

        public MyWebRequest(string url, string method, string data)
            : this(url, method)
        {

            // Create POST data and convert it to a byte array.
            string postData = data;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            // Close the Stream object.
            dataStream.Close();

        }

        public string GetResponse()
        {
            // Get the original response.
            WebResponse response = request.GetResponse();

            this.Status = ((HttpWebResponse)response).StatusDescription;

            // Get the stream containing all content returned by the requested server.
            dataStream = response.GetResponseStream();

            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);

            // Read the content fully up to the end.
            string responseFromServer = reader.ReadToEnd();

            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return responseFromServer;
        }
    }

    public static class MobileConfiguration // Config
    {
        public static NameValueCollection Settings;

        static MobileConfiguration()
        {
            string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
            string configFile = Path.Combine(appPath, "S4_Scanner_Config.config");

            if (!File.Exists(configFile))
            {
                throw new FileNotFoundException(string.Format("Application configuration file '{0}' not found.", configFile));
            }

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(configFile);
            XmlNodeList nodeList = xmlDocument.GetElementsByTagName("appSettings");
            Settings = new NameValueCollection();

            foreach (XmlNode node in nodeList)
            {
                foreach (XmlNode key in node.ChildNodes)
                {
                    Settings.Add(key.Attributes["key"].Value, key.Attributes["value"].Value);
                }
            }
        }

    }

    public class MyLogFile // Logger
    {
        public static string fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase), "S4_Scanner_Logger.txt");
        public static bool bLog = false;

        public static void LogClear()
        {
            if (bLog)
            {
                FileInfo f = new FileInfo(MyLogFile.fileName);
                if (f.Exists)
                {
                    long filesize = f.Length;
                    if (filesize > 1048576)
                    {
                        f.Delete();
                    }
                }
            }
        }

        public static void LoggerState()
        {
            string state = MobileConfiguration.Settings["Logger"];
            if (state == "true")
            {
                bLog = true;
            }
            else
            {
                bLog = false;
            }
        }

        public static void LogFile(string strCategory, string strMessage)
        {
            if (bLog)
            {
                using (StreamWriter writer = new StreamWriter(new FileStream(MyLogFile.fileName, FileMode.Append)))
                {
                    writer.WriteLine(DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss.fff tt") + " - {0} : {1}", strCategory, strMessage);
                }
            }
        }
    }


}
