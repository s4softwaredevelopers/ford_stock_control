using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Marry : Form
    {
        List<ScanItem> ItemsList = new List<ScanItem>();
        List<int> OrderList = new List<int>();
        List<string> OrderNumberList = new List<string>();
        int itemcount = -1;
        bool state = true;

        public Marry()
        {
            InitializeComponent();
        }

        private void NextItem()// Next Item in list 
        {
            if (state == false) { return; }
            if (itemcount == -1) { return; }

            labelMessage.Text = "";
            DetailsUpdate();

            if (itemcount < (OrderList.Count - 1)) { itemcount++; }
            else { itemcount = 0; }
            ItemDisplay(OrderList[itemcount]);
        }

        private void PreviousItem() // Previous Item in list
        {
            if (state == false) { return; }
            if (itemcount == -1) { return; }

            labelMessage.Text = "";
            DetailsUpdate();

            if (itemcount > 0) { itemcount--; }
            else { itemcount = OrderList.Count - 1; }
            ItemDisplay(OrderList[itemcount]);
        }

        private void ItemDisplay(int itemnum) // Current Item Selected
        {
            labelItemNumber.Text = (itemcount+1) + " / " + OrderList.Count;

            if (itemcount != -1)
            {
                //labelOrder.Text = ItemsList[itemnum].OrderID;
                labelProjData.Text = "                 " + ItemsList[itemnum].ProjectName;
                labelSupplierData.Text = ItemsList[itemnum].SupplierCode;
                labelDescriptionText.Text = "                 " + ItemsList[itemnum].Description;
                //labelDatabase.Text = ItemsList[itemnum].Database;
                textBoxBarcode.Text = ItemsList[itemnum].Barcode;
            }
        }

        private void DisableCheck() // Disable when errors
        {
            if (state)
            {
                panel_O_Order_D.Enabled = true;
                button_O_Selected.Enabled = true;
                panel_D_Barcode.Enabled = true;
            }
            else
            {
                panel_O_Order_D.Enabled = false;
                button_O_Selected.Enabled = false;
                panel_D_Barcode.Enabled = false;
            }
        }

        private void GetData()
        {
            Loader();
            string message = "";

            MyLibrary.GetDataFromServer(out state, out message, out ItemsList);

            DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void SendData()
        {
            Loader();
            string message = "";
            string ReceiveDataString = "";

            MyLibrary.SendDataToServer(out state, out message, ItemsList, out ReceiveDataString);

            DisableCheck();
            if (state)
            {
                if (message != "No Data to Update")
                {
                    MarryResults(ReceiveDataString);
                }
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void MarryResults(string DataString) //Display Marry Results 
        {
            bool results = false;
            string resultsList = "";
            try
            {
                StringBuilder databuilder = new StringBuilder(DataString);
                databuilder.Replace("</tr><tr>", '\u0232'.ToString());
                databuilder.Replace("<tr>", '\u0232'.ToString());
                databuilder.Replace("</tr>", '\u0232'.ToString());
                databuilder.Replace('\u0232'.ToString() + '\u0232'.ToString(), '\u0232'.ToString()); // remove empty row
                string[] rowValues = databuilder.ToString().Split(new char[] { '\u0232' });
                List<string> rowList = new List<string>(rowValues);
                rowList.RemoveAt(0);
                rowList.RemoveAt(rowList.Count - 1);
                if (rowList.Count > 0)
                {
                    foreach (string rowelement in rowList)
                    {
                        StringBuilder rowbuilder = new StringBuilder(rowelement);
                        rowbuilder.Replace("</td><td>", '\u0232'.ToString());
                        rowbuilder.Replace("<td>", '\u0232'.ToString());
                        rowbuilder.Replace("</td>", '\u0232'.ToString());
                        string[] colValues = rowbuilder.ToString().Split(new char[] { '\u0232' });
                        List<string> colList = new List<string>(colValues);
                        colList.RemoveAt(0);
                        colList.RemoveAt(colList.Count - 1);
                        if (colList[3] != "Barcode assigned successfully")
                        {
                            results = true;
                            resultsList += "Barcode : " + colList[1] + "\r\n";
                            resultsList += colList[2] + "\r\n";
                            resultsList += colList[3] + "\r\n" + "\r\n";
                        }
                    }
                }
            }
            catch
            {
                results = false;
            }
            if (results)
            {
                textBoxResultsList.Text = resultsList;
                panelResults.Show();
            }
        }

        private void ClearData()
        {
            ItemsList.Clear();
            OrderList.Clear();
            OrderNumberList.Clear();
            comboBoxOrder.Items.Clear();
            itemcount = -1;
            state = true;
            textBoxBarcode.Text = "";

            labelDescriptionText.Text = "";
            labelDatabase.Text = "";
            labelMessage.Text = "";
            labelSupplierData.Text = "";
            labelProjData.Text = "";
            textBoxBarcode.Text = "";

        }

        private void DetailsUpdate()
        {
            if (itemcount != -1)
            {
                if (ItemsList[OrderList[itemcount]].Barcode != textBoxBarcode.Text)
                {
                    BarcodeUpdate();
                }
            }
        }

        private void BarcodeUpdate() // Check and Update Barcode
        {
            if (Duplicate())
            {
                labelMessage.Text = "Barcode already scanned";
                labelMessage.ForeColor = System.Drawing.Color.Red;
                textBoxBarcode.Text = "";
                ItemsList[OrderList[itemcount]].Barcode = "";
                textBoxBarcode.Focus();
            }
            else
            {
                ItemsList[OrderList[itemcount]].Barcode = textBoxBarcode.Text;
            }
        }

        private bool Duplicate() // Check for Duplicate Barcode
        {
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].Barcode == textBoxBarcode.Text)
                {
                    return true;
                }
            }
            return false;
        }

        private void OrderSort(string Order) // Sort Order
        {
            OrderList.Clear();
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if ((ItemsList[count].OrderId == Order) || (Order == "All Orders"))
                {
                    OrderList.Add(count);
                }
            }
            if (OrderList.Count > 0) { itemcount = 0; } else { itemcount = -1; }
        }

        private void PopulateOrderNumbers() // Populate Orders 
        {
            OrderNumberList.Clear();
            List<string> temp = new List<string>();
            bool bfound;

            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].OrderId == "") { ItemsList[count].OrderId = "No Order"; }
                temp.Add(ItemsList[count].OrderId);
            }
            
            for (int count1 = 0; count1 < temp.Count; count1++)
            {
                bfound = false;
                for (int count2 = 0; count2 < OrderNumberList.Count; count2++)
                {
                    if (temp[count1] == OrderNumberList[count2]) { bfound = true; }
                }
                if (bfound == false) { OrderNumberList.Add(temp[count1]); }

            }

            comboBoxOrder.Items.Clear();
            comboBoxOrder.Items.Add("All Orders");
            foreach (string element in OrderNumberList)
            {
                comboBoxOrder.Items.Add(element);
            }
            if (comboBoxOrder.Items.Contains("No Order"))
            {
                comboBoxOrder.Items.Remove("No Order");
                comboBoxOrder.Items.Add("No Order");
            }
            comboBoxOrder.Text = "All Orders";            
        }

        private void Loader() // Loaders
        {
            panelLoader.Show();
            panelLoader.Refresh();
            buttonClear.Hide();
            buttonClear.Refresh();
            buttonClose.Hide();
            buttonClose.Refresh();
            Application.DoEvents();
            panelLoader.Hide();
            buttonClear.Show();
            buttonClose.Show();
        }

        private void button_O_Selected_Click(object sender, EventArgs e)
        {
            OrderSort(comboBoxOrder.Text);

            panelOptions.Hide();
            panelBase.Show();

            textBoxBarcode.Focus();

            ItemDisplay(OrderList[itemcount]);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            DetailsUpdate();
            SendData();
            DisableCheck();
            if (state)
            {
                this.Close();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            DetailsUpdate();
            SendData();
            DisableCheck();
            if (state)
            {
                ClearData();
                panelOptions.Show();
                panelBase.Hide();
                GetData();
                DisableCheck();
                if (state)
                {
                    PopulateOrderNumbers();                    
                }
            }
        }

        private void Marry_Load(object sender, EventArgs e)
        {
            GetData();
            DisableCheck();
            if (state)
            {
                PopulateOrderNumbers();
                MyLogFile.LogFile("OPEN    ", "Assign Barcodes - Connected to Server");
            }
            else
            {
                MyLogFile.LogFile("ERROR   ", "Assign Barcodes - Can't connect to Server");
            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearData();
            panelResults.Hide();
            panelBase.Hide();
            panelOptions.Show();
            GetData();
            DisableCheck();
            if (state)
            {
                PopulateOrderNumbers();
            }
        }

        private void textBoxBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                BarcodeUpdate();
            }
        }

        private void Marry_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Left) && (panelBase.Visible))
            {
                PreviousItem();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right) && (panelBase.Visible))
            {
                NextItem();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Escape))
            {
                DetailsUpdate();
                SendData();
                DisableCheck();
                if (state)
                {
                    this.Close();
                }
            }
        }

        private void buttonReslutsClose_Click(object sender, EventArgs e)
        {
            panelResults.Hide();
        }
    }
}