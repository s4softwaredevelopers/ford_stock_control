<?php

include 'Model.php';
include 'View.php';

//error_reporting(E_ERROR);
//Form Functions

function contrUserLogin() {
    try {
        echo viewUserAuthenticateForm(viewDropdownUsernames(modelGetUsernames()));
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

function contrUserLoginAuthenticate() {
    if (isset($_REQUEST["txtPwd"]) && isset($_REQUEST["staffcode"])) {
        $enteredpwd = $_REQUEST["txtPwd"];
        $staffCode = $_REQUEST["staffcode"];
        if ($pwd = '') {
            throw new Exception("Please enter password");
        } elseif ($staffCode == '') {
            throw new Exception("Please enter username");
        } else {
            //validate username and password
            $status = modelAuthenticateUser($staffCode, $enteredpwd);
            $_SESSION['status'] = $status[0][1];
            echo viewUserAuthenticateForm(viewDropdownUsernames(modelGetUsernames()));
        }
    }
}

function contrStoremanLogin() {
    echo viewStoremanAuthenticateForm('authenticate');
}

function contrStoremanLoginAuthenticate() {
    if (isset($_REQUEST["txtUname"]) && isset($_REQUEST["txtPwd"])) {
        $enteredPwd = $_REQUEST["txtPwd"];
        $enteredUname = $_REQUEST["txtUname"];
        $status = modelAuthenticateStoreman($enteredUname, $enteredPwd);
        if ($status[0][0] === true) {
            $_SESSION['uname'] = $status[0][1];
            $_SESSION['status'] = $status[0][2];
            $_SESSION['userId'] = $status[0][3];
            $_SESSION['isManager'] = $status[0][4];
            if (isset($status[0][5])) {
                if (!empty($status[0][5])) {
                    $_SESSION['isMaintenance'] = true;
                }
            }
            $_SESSION['msgType'] = 'success';
            contrStoremanMain();
        } else {
            $_SESSION['status'] = $status[0][1];
            $_SESSION['msgType'] = 'danger';
            echo viewStoremanAuthenticateForm('authenticate');
        }
    }
}

function contrStoremanMain() {
    echo viewStoremanMainForm();
    echo viewAllTrackedReturnItems(modelGetAllTrackedReturnItems(), viewDropdownUsernamesMulti(modelGetUsernames(), "drps4AwaitRelocateUser"));
}

function contrManageShelves() {
    if (isset($_GET["fid"])) {
        $action = $_GET["fid"];
        switch ($action) {
            case "add":
                if (isset($_REQUEST['barcode']) && isset($_REQUEST['description']) && isset($_REQUEST['location_id'])) {
                    $barcode = $_REQUEST['barcode'];
                    $description = $_REQUEST['description'];
                    $project = $_REQUEST['project'];
                    $location_id = $_REQUEST['location_id'];
                    $s4Project = $_REQUEST['s4ProjectsAdd'];
                    $mmProject = $_REQUEST['mmProjectsAdd'];
                    $status = modelAddShelf($barcode, $description, $location_id, $s4Project, $mmProject);
                    if ($status[0][0] === true) {
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['msgType'] = 'danger';
                    }
                    $_SESSION['status'] = $status[0][1];
                }
                break;
            case "update":
                if (isset($_REQUEST['shelfId']) && isset($_REQUEST['editBarcode']) && isset($_REQUEST['editDescription']) && isset($_REQUEST['editLocation'])) {
                    $id = $_REQUEST['shelfId'];
                    $barcode = $_REQUEST['editBarcode'];
                    $description = $_REQUEST['editDescription'];
                    $s4Project = $_REQUEST['s4ProjectsAdd'];
                    $mmProject = $_REQUEST['mmProjectsAdd'];
                    $location = $_REQUEST['editLocation'];
                    $status = modelUpdateShelves($id, $barcode, $description, $location, $s4Project, $mmProject);
                    if ($status[0][0] === true) {
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['msgType'] = 'danger';
                    }
                    $_SESSION['status'] = $status[0][1];
                }
                break;
            case "delete":
                if (isset($_REQUEST["rowId"])) {
                    $id = $_REQUEST["rowId"];
                    $status = modelRemoveShelf($id);
                    $_SESSION['status'] = $status[0][1];
                    if ($status[0][0] === true) {
                        $status = modelGetDeletedShelfDetails($id);
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['msgType'] = 'danger';
                    }
                }
                break;
            case "print":
                $status = modelUdpRequest($_REQUEST['qty'], 5);
                if ($status[0][0] === true) {
                    $_SESSION['msgType'] = 'success';
                } else {
                    $_SESSION['msgType'] = 'danger';
                }
                $_SESSION['status'] = $status[0][1];
                break;
        }

        //Display all Shelves
        echo viewAddShelfForm(viewDropdownLocation(modelGetLocations()), viewDropdownS4Projects(modelGetProjectsIntranet()), viewDropdownMMProjects(modelGetProjectsAutonet()));
        echo viewShelvesForm(modelGetShelves(), modelGetLocations(), modelGetProjectsIntranet(), modelGetProjectsAutonet());
    }
}

function contrManageUsers() {
    if (isset($_GET["fid"])) {
        $action = $_GET["fid"];
        switch ($action) {
            case "add":
                if (isset($_REQUEST['username']) && isset($_REQUEST['email']) && isset($_REQUEST['password']) && isset($_REQUEST['user_access_id'])) {
                    $username = trim($_REQUEST['username']);
                    $email = trim($_REQUEST['email']);
                    $password = $_REQUEST['password'];
                    $user_access_id = $_REQUEST['user_access_id'];
                    $status = modelAddUser($username, $email, $password, $user_access_id);
                    if ($status[0][0] === true) {
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['msgType'] = 'danger';
                    }
                    $_SESSION['status'] = $status[0][1];
                }
                break;
            case "update":
                if (isset($_REQUEST['UserId']) && isset($_REQUEST['editUsername']) && isset($_REQUEST['editEmail']) && isset($_REQUEST['editUserAccess'])) {
                    $id = $_REQUEST['UserId'];
                    $username = $_REQUEST['editUsername'];
                    $userAccess = $_REQUEST['editUserAccess'];
                    $email = $_REQUEST['editEmail'];
                    $status = modelUpdateUsers($id, $username, $userAccess, $email);
                    if ($status[0][0] === true) {
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['msgType'] = 'danger';
                    }
                    $_SESSION['status'] = $status[0][1];
                }
                break;
            case "delete":
                if (isset($_REQUEST["rowId"])) {
                    $id = $_REQUEST["rowId"];
                    $status = modelRemoveUser($id);
                    if ($status[0][0] === true) {
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['msgType'] = 'danger';
                    }
                    $_SESSION['status'] = $status[0][1];
                }
                break;
        }
    }
    //Show add user form
    echo viewAddUserForm(viewDropdownUserType(modelGetUserTypes()));
    //Get all users
    echo viewUsersForm(modelGetUsers());
//        }
}

function contrManageOrders() {
    //Get orders
    try {
        echo viewOrderForm();
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

function contrManageOrdersItems() {
    try {
        if (isset($_POST["orderDetails"])) {
            $orderNumber = $_POST["orderDetails"];
            $_SESSION['ProcessingOrder'] = $orderNumber;
        }
        if (isset($_SESSION['ProcessingOrder']) != "") {
            $orderNumber = $_SESSION['ProcessingOrder'];
            if (strlen($orderNumber) > 2) {
                $orderNumber = trim($orderNumber);
                $OrderPrefix = substr($orderNumber, 0, 2);

                if ($OrderPrefix == 'mm') {
                    $db = 'mm';
                    $_SESSION['dbName'] = $db;
                } elseif ($OrderPrefix == 'MM') {
                    $db = 'mm';
                    $_SESSION['dbName'] = $db;
                } else {
                    $db = 's4';
                    $_SESSION['dbName'] = $db;
                }
                echo viewOrderForm(viewReportOrderDetails(modelGetOrderDetails($orderNumber)));
                echo viewOrderDetailsTracked(modelGetSpecficOrderByTracked($orderNumber, $db));
                echo viewOrderDetailsTrackedResell(modelGetSpecficOrderByTrackedResell($orderNumber, $db));
                echo viewOrderDetailsUntracked(modelGetSpecficOrderByUntracked($orderNumber, $db), viewDropdownConsumableType(modelGetConsumableTypes()));
                echo viewOrderDetailsConsumable(modelGetSpecficOrderByConsumable($orderNumber, $db), viewDropdownConsumableType(modelGetConsumableTypes()));
            } else {
                echo viewOrderForm();
            }
        } else {
            echo viewOrderForm();
        }
    } catch (Exception $ex) {
        echo $ex->getMessage();
    }
}

function formStoremanMain() {
    if ($_REQUEST['hasOrderNum'] == 'yes') {
        echo viewCaptureOrderNumber();
    } else {
        echo viewStockFromSite();
    }
}

function UpdateOrderItem() {
    $emailContent = '';
    $emailSubject = '';
    $emailAddress = array();
    $projectCode = '';
    $itemsID = '';
    $itemDb = '';
    $qtyVal = '';
    $qtyPrint = '';
    $returnValue = array();
    $emailContent .= "<table style='border: 1px solid rgb(204,204,204); border-collapse: collapse;text-align: center;'><thead><th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Order No</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Supplier Code</th><th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Project</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Item Description</th><th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Ordered</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Total Quantity Received</th><th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Received by Storeman</th></thead><tbody>";
    if (isset($_SESSION['dbName'])) {
        $db = $_SESSION['dbName'];
    } else {
        $db = 's4';
    }

    foreach ($_REQUEST as $key => $value) {
        if (strstr($key, 'qtyReceived')) {
            $x = str_replace('qtyReceived', '', $key);
            $id = $x;
        }
        if (strstr($key, 'totalReceived')) {
            $x = str_replace('totalReceived', '', $key);
            $totalReceived = $x;
        }

        if ($id != "" && $totalReceived != "") {
            //modelUpdate
            $qtyVal = $_REQUEST['qtyReceived' . $id];
            $NumberOfTrackedItems = $_REQUEST['txtItemsCount' . $id];
            $qtyPrint = $qtyPrint + $qtyVal;
            $totalReceived = $_POST['totalReceived' . $totalReceived];
            $itemDesc = $_REQUEST['itemDescription' . $id];
            $project = $_REQUEST['project' . $id];
            $supplier = $_REQUEST['supplierCode' . $id];
            $qtyOrdered = $_REQUEST['qtyOrdered' . $id];
            if ((is_int($qtyVal) || ctype_digit($qtyVal)) && (int) $qtyVal > 0) {
                $printQuantityTotal = $qtyVal + $totalReceived;
                $qtyVal = round($qtyVal);
                $totalReceived = round($totalReceived);
                $printQuantityTotal = round($printQuantityTotal);
                $status = modelUpdateQuantityReceived($db, $id, $printQuantityTotal);

                if ($status[0][0] === true) {
                    if ($db == 's4') {
                        $status = modelGetProjectCodeByItemsId($id, "s4admin");
                        if ($status[0][0] == true) {
                            $projectCode = $status[0][1];
                        } else {
                            $projectCode = '';
                        }
                        if ($_POST['isTrackResell'] == 1) {
                            modelInsertSpecficAmountOfOrderItems($id, 1, $db, $projectCode);
                        } else {
                            modelInsertSpecficAmountOfOrderItems($id, $NumberOfTrackedItems, $db, $projectCode);
                        }
                    } elseif ($db == 'mm') {
                        $status = modelGetProjectCodeByItemsId($id, "MMAdmin");
                        if ($status[0][0] == true) {
                            $projectCode = $status[0][1];
                        } else {
                            $projectCode = '';
                        }
                        if ($_POST['isTrackResell'] == 1) {
                            modelInsertSpecficAmountOfOrderItems($id, 1, $db, $projectCode);
                        } else {
                            modelInsertSpecficAmountOfOrderItems($id, $NumberOfTrackedItems, $db, $projectCode);
                        }
                    } else {
                        modelInsertSpecficAmountOfItems($id, $NumberOfTrackedItems, $db);
                    }
                    $BuildStatusMsg .= "<Strong>Item:</Strong> $itemDesc <Strong>| Project:</Strong> $project <Strong>| Supplier Code:</Strong> $supplier <Strong>| Qty Ordered:</Strong> $qtyOrdered => total quantity received was $totalReceived, updated with $qtyVal successfully. New Total Quantity received = $printQuantityTotal.<br/>";
                    $status = modelGetTrackedItemDetails($id, $db);
                    $emailContent .= "<tr><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][1] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][2] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][9] . " - " . $status[0][3] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][4] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][5] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][6] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>$qtyVal</td></tr>";
                    array_push($emailAddress, $status[0][7]);
                    if ($status[0][10] < 13) {
                        array_push($emailAddress, $status[0][8]);
                    }
                    //send email
                    $itemsID = $id;
                    $itemDb = $db;
                } else {
                    $BuildStatusMsg .= "<Strong>Item:</Strong> $itemDesc <Strong>| Project:</Strong> $project <Strong>| Supplier Code:</Strong> $supplier <Strong>| Qty Ordered:</Strong> $qtyOrdered => total quantity received was $totalReceived, updated with $qtyVal failed." . $status[0][1] . "<br />";
                    $isErrorMsg = 'danger';
                }
            }
            $id = "";
            $totalReceived = '';
        }
    }
    $emailContent .="</tbody></table>";
    if (!empty($status)) {
        if (empty($isErrorMsg)) {
            if ($_POST['isTrackResell'] == 1) {
                $_SESSION['resell'] = "NB!!! Do not open the box!";
                $_SESSION['statusTrackedResell'] = $BuildStatusMsg;
            } else {
                $_SESSION['status'] = $BuildStatusMsg;
            }
            $_SESSION['msgType'] = "success";
            $status = modelGetTrackedItemDetails($itemsID, $itemDb);
            if ($_POST['isTrackResell'] == 1) { //Tracked resale items
                $emailSubject .="Tracked Resale Items Received for Project : " . $status[0][9] . " - " . $status[0][3] . " | Requested by: " . $status[0][11];
            } else { //Tracked items
                $emailSubject .="Tracked Items Received for Order No: " . $status[0][1] . " ; Project : " . $status[0][9] . " - " . $status[0][3] . " | Requested by: " . $status[0][11];
            }
            //Remove Duplicate entries(duplicate emails)
            $emailAddress = array_unique($emailAddress);
            foreach ($emailAddress as $personAddress) {
                modelSendEmail($personAddress, $emailSubject, $emailContent);
                //echo "<br/>Email Address: ".$personAddress."-Subject:".$emailSubject."-Body:".$emailContent;
            }
        } else {
            $_SESSION['msgType'] = $isErrorMsg;
            if ($_POST['isTrackResell'] == 1) {
                $_SESSION['statusTrackedResell'] = $BuildStatusMsg;
            } else {
                $_SESSION['status'] = $BuildStatusMsg;
            }
        }
        $returnValue[] = array($_SESSION['msgType'], $qtyPrint);
        return $returnValue;
    }
}

function contrManageOrdersPrint() {
    $id = '';
    $totalReceived = '';
    $printQuantityTotal = '';
    $BuildStatusMsg = '';
    $isErrorMsg = '';

    //modelUpdate
    //$qtyVal = $_REQUEST['qtyReceived'.$id];
    if (isset($_POST['SaveAndPrint'])) {
        $status = UpdateOrderItem();
        if ($status[0][0] == "success") {
            $qtyVal = $status[0][1];
            if ($_POST['isTrackResell'] == 1) {
                $status = modelUdpRequest(1, 5);
            } else {
                $status = modelUdpRequest($qtyVal, 5);
            }

            if ($status[0][0] === true) {
                $_SESSION['msgType'] = 'success';
            } else {
                $_SESSION['msgType'] = 'danger';
            }
            $_SESSION['status'] = $status[0][1];
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = $status[0][1];
        }
    }
    if (isset($_POST['Print'])) {
        foreach ($_REQUEST as $key => $value) {
            if (strstr($key, 'qtyReceived')) {
                $x = str_replace('qtyReceived', '', $key);
                $id = $x;
            }
            if ($id != "") {
                //modelUpdate
                $qtyVal = $_REQUEST['qtyReceived' . $id];
                if ($_POST['isTrackResell'] == 1) {
                    $status = modelUdpRequest(1, 5);
                } else {
                    $status = modelUdpRequest($qtyVal, 5);
                }

                if ($status[0][0] === true) {
                    $_SESSION['msgType'] = 'success';
                } else {
                    $_SESSION['msgType'] = 'danger';
                }
                $_SESSION['status'] = $status[0][1];
            }
        }
    }
    if (isset($_POST['update'])) {
        $status = UpdateOrderItem();
        if ($status[0][0] == "success") {
            $_SESSION['msgType'] = 'success';
            $_SESSION['status'] = 'Updated successfully';
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = 'Update Failed';
        }
    }

    unset($_POST);
    contrManageOrdersItems();
}

function contrManageOrdersUntracked() {
    $BuildStatusMsg = '';
    $isErrorMsg = '';
    $emailContent = '';
    $emailSubject = '';
    $emailAddress = array();
    $storemanId = '';
    $storemanFullName = '';
    $orderId = '';
    $orderNumber = '';
    $qtyVal = '';
    $description = '';
    $itemDescriptionId = '';
    $itemDescriptionArray = array();
    $Itmdescription = '';
    $project = array();
    $emailContent .= "<table style='border: 1px solid rgb(204,204,204); border-collapse: collapse;text-align: center;'><thead>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Order No</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Supplier Code</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Project</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Item Description</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Ordered</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Total Quantity Received</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Consumable Type</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Received by Storeman</th></thead><tbody>";
    if (isset($_SESSION['dbName'])) {
        $db = $_SESSION['dbName'];
    } else {
        $db = 's4';
    }
    if (isset($_SESSION['userId'])) {
        $storemanId = $_SESSION['userId'];
    }
    foreach ($_REQUEST as $key => $value) {
        if (strstr($key, 'qtyReceived')) {
            $x = str_replace('qtyReceived', '', $key);
            $id = $x;
        }
        if (strstr($key, 'totalReceived')) {
            $x = str_replace('totalReceived', '', $key);
            $totalReceived = $x;
        }

        if (strstr($key, 'txtOrderNumber')) {
            $x = str_replace('txtOrderNumber', '', $key);
            $orderId = $x;
        }
        if (strstr($key, 'itemDescription')) {
            $x = str_replace('itemDescription', '', $key);
            $itemDescriptionId = $x;
        }

        if ($id != "" && $totalReceived != "" && $orderId != "" && $itemDescriptionId != "") {
            //modelUpdate
            // $id;//contains item id
            $qtyVal = $_REQUEST['qtyReceived' . $id];
            $orderNumber = $_REQUEST['txtOrderNumber' . $orderId];
            $totalReceived = $_POST['totalReceived' . $totalReceived];
            $Itmdescription = $_POST['itemDescription' . $itemDescriptionId];
            $itemDesc = $_REQUEST['itemDescription' . $id];
            $projectName = $_REQUEST['project' . $id];
            $supplier = $_REQUEST['supplierCode' . $id];
            $qtyOrdered = $_REQUEST['qtyOrdered' . $id];
            if ((is_int($qtyVal) || ctype_digit($qtyVal)) && (int) $qtyVal > 0) {
                $printQuantityTotal = $qtyVal + $totalReceived;
                $qtyVal = round($qtyVal);
                $totalReceived = round($totalReceived);
                $printQuantityTotal = round($printQuantityTotal);
                $status = modelUpdateQuantityReceived($db, $id, $printQuantityTotal);

                if ($status[0][0] === true) {
                    $description = $status[0][4];
                    $_SESSION['msgType'] = "success";
                    $type = $_REQUEST['type'];
                    $userId = $_REQUEST['userId'];
                    $status = modelConsumableTypeId($qtyVal, $type, $userId);
                    $BuildStatusMsg .= "<Strong>Item:</Strong> $itemDesc <Strong>| Project:</Strong> $projectName <Strong>| Supplier Code:</Strong> $supplier <Strong>| Qty Ordered:</Strong> $qtyOrdered => total quantity received was $totalReceived, updated with $qtyVal successfully. New Total Quantity received = $printQuantityTotal.<br/>";
                    $status = modelGetConsumableItemDetails($id, $db);
                    $emailContent .= "<tr><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][1] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][2] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][10] . " - " . $status[0][3] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][4] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][5] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][6] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][8] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>$qtyVal</td></tr>";
                    array_push($emailAddress, $status[0][7]);
                    if ($status[0][11] < 13) {
                        array_push($emailAddress, $status[0][9]);
                    }
                    array_push($project, "Untracked Items Received for Project : " . $status[0][10] . " - " . $status[0][3] . " | Requested by: " . $status[0][12]);
                    array_push($itemDescriptionArray, $Itmdescription);
                } else {
                    $BuildStatusMsg .= "<Strong>Item:</Strong> $itemDesc <Strong>| Project:</Strong> $projectName <Strong>| Supplier Code:</Strong> $supplier <Strong>| Qty Ordered:</Strong> $qtyOrdered => total quantity received was $totalReceived, updated with $qtyVal failed." . $status[0][1] . "<br />";
                    $isErrorMsg = 'danger';
                }
            } else {
                echo 'Invalid input value';
            }
            $id = "";
            $totalReceived = '';
        }
    }
    $emailContent .="</tbody></table>";
    $project = array_unique($project);
    $itemDescriptionArray = array_unique($itemDescriptionArray);
    foreach ($project as $subject) {
        $emailSubject .= $subject;
    }
    if (!empty($isErrorMsg)) {
        $_SESSION['msgType'] = $isErrorMsg;
    } else {
        //Remove Duplicate entries(duplicate emails)
        $emailAddress = array_unique($emailAddress);
        foreach ($emailAddress as $personAddress) {
            modelSendEmail($personAddress, $emailSubject, $emailContent);
        }
        foreach ($itemDescriptionArray as $DescriptionValue) {
            modelLog('Item', 'Captured untracked items', null, $DescriptionValue, $orderNumber, "Items Received: " . $qtyVal, $sourceComment = null, "", "");
        }
    }
    $_SESSION['statusUntracked'] = $BuildStatusMsg;

    unset($_POST);
    contrManageOrdersItems();
}

function contrManageOrdersConsumable() {
    $BuildStatusMsg = '';
    $isErrorMsg = '';
    $emailContent = '';
    $emailSubject = '';
    $emailAddress = array();
    $storemanId = '';
    $storemanFullName = '';
    $orderId = '';
    $orderNumber = '';
    $qtyVal = '';
    $description = '';
    $itemDescriptionId = '';
    $itemDescriptionArray = array();
    $Itmdescription = '';
    $project = array();
    $emailContent .= "<table style='border: 1px solid rgb(204,204,204); border-collapse: collapse;text-align: center;'><thead>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Order No</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Supplier Code</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Project</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Item Description</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Ordered</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Total Quantity Received</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Consumable Type</th>"
            . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Received by Storeman</th></thead><tbody>";
    if (isset($_SESSION['dbName'])) {
        $db = $_SESSION['dbName'];
    } else {
        $db = 's4';
    }
    if (isset($_SESSION['userId'])) {
        $storemanId = $_SESSION['userId'];
    }
    foreach ($_REQUEST as $key => $value) {
        if (strstr($key, 'qtyReceived')) {
            $x = str_replace('qtyReceived', '', $key);
            $id = $x;
        }
        if (strstr($key, 'totalReceived')) {
            $x = str_replace('totalReceived', '', $key);
            $totalReceived = $x;
        }

        if (strstr($key, 'txtOrderNumber')) {
            $x = str_replace('txtOrderNumber', '', $key);
            $orderId = $x;
        }
        if (strstr($key, 'itemDescription')) {
            $x = str_replace('itemDescription', '', $key);
            $itemDescriptionId = $x;
        }

        if ($id != "" && $totalReceived != "" && $orderId != "" && $itemDescriptionId != "") {
            //modelUpdate
            $qtyVal = $_REQUEST['qtyReceived' . $id];
            $orderNumber = $_REQUEST['txtOrderNumber' . $orderId];
            $totalReceived = $_POST['totalReceived' . $totalReceived];
            $Itmdescription = $_POST['itemDescription' . $itemDescriptionId];
            $itemDesc = $_REQUEST['itemDescription' . $id];
            $projectName = $_REQUEST['project' . $id];
            $supplier = $_REQUEST['supplierCode' . $id];
            $qtyOrdered = $_REQUEST['qtyOrdered' . $id];
            if ((is_int($qtyVal) || ctype_digit($qtyVal)) && (int) $qtyVal > 0) {
                $printQuantityTotal = $qtyVal + $totalReceived;
                $qtyVal = round($qtyVal);
                $totalReceived = round($totalReceived);
                $printQuantityTotal = round($printQuantityTotal);
                $status = modelUpdateQuantityReceived($db, $id, $printQuantityTotal);

                if ($status[0][0] === true) {
                    $description = $status[0][4];
                    //modelCaptureUntrackedOrderedItem($orderNumber,$description,$qtyVal);
                    $_SESSION['msgType'] = "success";
                    $type = $_REQUEST['type'];
                    $userId = $_REQUEST['userId'];
                    $status = modelConsumableTypeId($qtyVal, $type, $userId);
                    $BuildStatusMsg .= "<Strong>Item:</Strong> $itemDesc <Strong>| Project:</Strong> $projectName <Strong>| Supplier Code:</Strong> $supplier <Strong>| Qty Ordered:</Strong> $qtyOrdered => total quantity received was $totalReceived, updated with $qtyVal successfully. New Total Quantity received = $printQuantityTotal.<br/>";
                    $status = modelGetConsumableItemDetails($id, $db);
                    $emailContent .= "<tr><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][1] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][2] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][10] . " - " . $status[0][3] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][4] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][5] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][6] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>" . $status[0][8] . "</td><td style='border: 1px solid rgb(204,204,204);text-align: center;'>$qtyVal</td></tr>";
                    array_push($emailAddress, $status[0][7]);
                    if ($status[0][11] < 13) {
                        array_push($emailAddress, $status[0][9]);
                    }
                    array_push($project, "Consumable Items Received for Project : " . $status[0][10] . " - " . $status[0][3] . " | Requested by: " . $status[0][12]);
                    array_push($itemDescriptionArray, $Itmdescription);
                } else {
                    $BuildStatusMsg .= "<Strong>Item:</Strong> $itemDesc <Strong>| Project:</Strong> $projectName <Strong>| Supplier Code:</Strong> $supplier <Strong>| Qty Ordered:</Strong> $qtyOrdered => total quantity received was $totalReceived, updated with $qtyVal failed." . $status[0][1] . "<br />";
                    $isErrorMsg = 'danger';
                }
            } else {
                echo 'Invalid input value';
            }
            $id = "";
            $totalReceived = '';
        }
    }
    $emailContent .="</tbody></table>";
    $project = array_unique($project);
    $itemDescriptionArray = array_unique($itemDescriptionArray);
    foreach ($project as $subject) {
        $emailSubject .= $subject;
    }
    if (!empty($isErrorMsg)) {
        $_SESSION['msgType'] = $isErrorMsg;
    } else {
        //Remove Duplicate entries(duplicate emails)
        $emailAddress = array_unique($emailAddress);
        foreach ($emailAddress as $personAddress) {
            modelSendEmail($personAddress, $emailSubject, $emailContent);
        }
        foreach ($itemDescriptionArray as $DescriptionValue) {
            modelLog('Item', 'Captured consumable item', null, $DescriptionValue, $orderNumber, "Items Received: " . $qtyVal, $sourceComment = null, "", "");
        }
    }
    $_SESSION['statusConsumable'] = $BuildStatusMsg;

    unset($_POST);
    contrManageOrdersItems();
}

function contrManageOrdersConsumableItem() {
    if (isset($_REQUEST['staffcode']) && isset($_REQUEST['pWord']) && isset($_REQUEST['type']) && isset($_REQUEST['qtyReceived'])) {
        $staffCode = $_REQUEST['staffcode'];
        $password = $_REQUEST['pWord'];
        $consumableType = $_REQUEST['type'];
        $qty = $_REQUEST['qtyReceived'];
        $userId = $_REQUEST['userId'];
        $status = modelAuthenticateUser($staffCode, $password);
        if ($status[0][0] === true) {
            //check if type exist in consumable table
            //update qty
            $status = modelConsumableTypeId($qty, $consumableType, $userId);
            if ($status[0][0] === true) {
                $_SESSION['msgTypeConsumable'] = 'success';
            } else {
                $_SESSION['msgTypeConsumable'] = 'danger';
            }
            $_SESSION['statusConsumable'] = $status[0][1];
        } else {
            $_SESSION['msgTypeConsumable'] = 'danger';
            $_SESSION['statusConsumable'] = $status[0][1];
        }
    }
    contrNoOrderItems();
}

function contrManageOrdersConsumableRelocate() {
    $status = array();
    $result = '';
    if (isset($_POST['consumableId']) && isset($_POST['qtyReceived']) && isset($_POST['userId'])) {
        $consumableId = $_POST['consumableId'];
        $userId = $_POST['userId'];
        $qty = $_POST['qtyReceived'];
        if (isset($_POST['in'])) {
            if ($_POST['in']) {
                //update quantity in store
                //check if positive number
                if (ctype_digit($qty)) {
                    $status = modelIncreaseQuantityInStore($consumableId, $qty, $userId);
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'success';
                } else {
                    $_SESSION['status'] = "Invalid quantity value.";
                    $_SESSION['msgType'] = 'danger';
                }
            }
        }
        if (isset($_POST['out'])) {
            if ($_POST['out']) {
                //decrease quantity
                //else display success
                if (ctype_digit($qty)) {
                    $status = modelDescreaseQuantityInStore($consumableId, $qty, $userId);
                    if ($status[0][0] === true) {
                        $_SESSION['status'] = $status[0][1];
                        $_SESSION['msgType'] = 'success';
                    } else {
                        $_SESSION['status'] = $status[0][1];
                        $_SESSION['msgType'] = 'danger';
                    }

                    //Check if quantity = minimum
                } else {
                    $_SESSION['status'] = "Invalid quantity value.";
                    $_SESSION['msgType'] = 'danger';
                }
            }
        }
    }
    contrManageConsumableOrder();
}

function contrManageOrdersMarry() {
    echo viewAllPrintedBarcodeItems(modelGetAllPrintedItems());
}

function contrManageReturns() {
    if (isset($_POST['barcodeVal']) && isset($_POST['staffcodeVal']) && isset($_POST['pWord'])) {
        if (isset($_POST['descriptionVal'])) {
            $description = $_POST['descriptionVal'];
        }
        //Description needs to be saved/log
        $staffCode = $_POST['staffcodeVal'];
        $password = $_POST['pWord'];
        $itmBarcode = $_POST['barcodeVal'];
        $status = modelAuthenticateUser($db, $staffCode, $password);
        if ($status[0][0] === true) {
            if (!empty($itmBarcode)) {
                $status = modelResetLocation($itmBarcode);
                if ($status[0][0] === true) {
                    $_SESSION['msgType'] = 'success';
                } else {
                    $_SESSION['msgType'] = 'danger';
                }
                $_SESSION['status'] = $status[0][1];
            } else {
                $_SESSION['msgType'] = 'danger';
                $_SESSION['status'] = "Barcode cannot be empty";
            }
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = $status[0][1];
        }
    }
    echo viewRelocateOptions();
    echo viewScanTrackedReturnItems(viewDropdownUsernamesMulti(modelGetUsernames(), "drps4u2u"), viewDropdownUsernamesMulti(modelGetUsernames(), "drps4s2u"), viewDropdownUsernamesMulti(modelGetUsernames(), "drps4u2u2"), viewDropdownUsernamesMulti(modelGetUsernames(), "drps4u2s"));
}

function contrManageLocation() {
    if (isset($_REQUEST['itemId']) && isset($_REQUEST['itembarcode']) && isset($_REQUEST['itemDescription']) &&
            isset($_REQUEST['shelfBarcode']) && isset($_REQUEST['shelfDescription'])) {
        $id = $_REQUEST['itemId'];
        $itemBarcode = $_REQUEST['itembarcode'];
        $itemDescription = $_REQUEST['itemDescription'];
        $shelfBarcode = $_REQUEST['shelfBarcode'];
        $shelfDescription = $_REQUEST['shelfDescription'];
        echo viewSetLocationPage($itemBarcode, $itemDescription, $shelfBarcode, $shelfDescription);
    }
}

function contrManageSetLocation() {
    //validate user and save new location(entered shelf barcode)
    if (isset($_REQUEST["itemId"]) && isset($_REQUEST["shelfBarcodeVal"])) {
        $id = $_REQUEST["itemId"];
        $barcode = $_REQUEST["shelfBarcodeVal"];
        if (!empty($barcode)) {
            $status = modelUpdateShelfBarcode($id, $barcode);
            if ($status[0][0] === true) {
                $_SESSION['msgType'] = 'success';
            } else {
                $_SESSION['msgType'] = 'danger';
            }
            $_SESSION['status'] = $status[0][1];
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = 'Shelf cannot be empty';
        }
    }
    contrStoremanMain();
}

function contrNoOrderItems() {
    echo viewUntrackedReturnItems(viewDropdownUsernamesMulti(modelGetUsernames(), "drpNoOrders4"));
    echo viewUpdateConsumables(viewDropdownUsernamesMulti(modelGetUsernames(), "drpConsumables4"), viewDropdownConsumableType(modelGetConsumableTypes()));
}

function contrNoOrderItemsUpdate() {
    if (isset($_REQUEST['staffcodeValue']) && isset($_REQUEST['pWord'])) {
        $partNumber = '';
        $description = $_REQUEST['txtDescription'];
        $qty = $_REQUEST['txtQty'];
        $staffCode = $_REQUEST['staffcode'];
        $password = $_REQUEST['pWord'];
        if (isset($_REQUEST['txtPartNumber'])) {
            $partNumber = $_REQUEST['txtPartNumber'];
        }

        if (isset($_REQUEST['update'])) {
            $command = 'update';
        }
        if (isset($_REQUEST['print'])) {
            $command = 'print';
        }
        if (isset($_REQUEST['UpdateAndPrint'])) {
            $command = 'UpdateAndPrint';
        }
        $status = modelAuthenticateUser($staffCode, $password);
        if ($status[0][0] === true) {
            if (isset($_REQUEST['txtQty'])) {
                switch ($command) {
                    case "update":
                        $status = modelInsertNoOrderItem($description, $partNumber, $qty);
                        if ($status[0][0] === true) {
                            $_SESSION['msgType'] = 'success';
                        } else {
                            $_SESSION['msgType'] = 'danger';
                        }
                        $_SESSION['status'] = $status[0][1];
                        break;
                    case "print":
                        $status = modelUdpRequest($qty, 5);
                        if ($status[0][0] === true) {
                            $_SESSION['msgType'] = 'success';
                        } else {
                            $_SESSION['msgType'] = 'danger';
                        }
                        $_SESSION['status'] = $status[0][1];
                        break;
                    case "UpdateAndPrint":
                        $status = modelInsertNoOrderItem($description, $partNumber, $qty);
                        if ($status[0][0] === true) {
                            $_SESSION['msgType'] = 'success';
                        } else {
                            $_SESSION['msgType'] = 'danger';
                        }
                        $_SESSION['status'] = $status[0][1];

                        $status = modelUdpRequest($qty, 5);
                        if ($status[0][0] === true) {
                            $_SESSION['msgType'] = 'success';
                        } else {
                            $_SESSION['msgType'] = 'danger';
                        }
                        $_SESSION['status'] = $status[0][1];
                        break;
                }
            }
        } else {
            $_SESSION['status'] = $status[0][1];
            $_SESSION['msgType'] = 'danger';
        }
    } else {
        $_SESSION['status'] = "Please complete all required fields.";
    }
    contrNoOrderItems();
}

function contrReportOrder() {
    echo viewReportOrderform();
    if (isset($_POST['orderNumber'])) {
        $orderNumber = $_REQUEST['orderNumber'];
        if (strlen($orderNumber) > 3) {
            echo viewReportOrderDetails(modelGetOrderDetails($orderNumber));
            echo viewReportsOrder(modelGetItemsByOrder($orderNumber), modelGetItemsByOrderItems($orderNumber));
        }
    }
}

function contrManageReturnStore() {
    if (isset($_POST['barcodeVal']) && isset($_POST['form_token'])) {
        if ($_POST['form_token'] == $_SESSION['form_token']) {//ensure valid submit and not refresh
            $itmBarcode = $_POST['barcodeVal'];
            if (!empty($itmBarcode)) {
                $status = modelResetLocation($itmBarcode);
                if ($status[0][0] === true) {
                    $_SESSION['msgType'] = 'success';
                } else {
                    $_SESSION['msgType'] = 'danger';
                }

                $_SESSION['status'] = $status[0][1];
            } else {
                $_SESSION['msgType'] = 'danger';
                $_SESSION['status'] = "Barcode cannot be empty";
            }
            unset($_SESSION['form_token']);
        }
    }

    echo viewScanTrackedReturnItems(viewDropdownUsernamesMulti(modelGetUsernames(), "drps4"));
    echo viewScanTrackedReturnStoreItems();
}

function contrReportSearchfilter() {
    if (isset($_POST['search'])) {
        if (empty($_POST['datefrom']) && empty($_POST['dateto'])) {
            echo viewItemHistory(modelGetAllLog());
        } else {
            $datefrom = $_POST['datefrom'] . ' 00:00:00';
            $dateto = $_POST['dateto'] . ' 23:59:59';
            $dateToDisplay = date('Y-m-d', strtotime($datefrom));
            $dateFromDisplay = date('Y-m-d', strtotime($dateto));
            echo viewItemHistory(modelGetLogDateRange($datefrom, $dateto), $dateToDisplay, $dateFromDisplay);
        }
    } else {
        echo viewItemHistory(modelGetAllLog());
    }
}

function contrReportSearchfilterUntracked() {
    if (isset($_POST['search'])) {
        if (empty($_POST['datefrom']) && empty($_POST['dateto'])) {
            echo viewItemHistory(modelGetAllUntrackedItemsLog());
        } else {
            $datefrom = $_POST['datefrom'] . ' 00:00:00';
            $dateto = $_POST['dateto'] . ' 23:59:59';
            $dateToDisplay = date('Y-m-d', strtotime($datefrom));
            $dateFromDisplay = date('Y-m-d', strtotime($dateto));
            echo viewItemHistory(modelGetLogDateRangeUntracked($datefrom, $dateto), $dateToDisplay, $dateFromDisplay);
        }
    } else {
        echo viewItemHistory(modelGetAllLog());
    }
}

function contrReportItemHistory() {
    echo viewItemHistory(modelGetLimitedLog());
}

function contrManageConsumableOrder() {
    echo viewRelocateConsumable(modelGetConsumables());
}

function contrAssignLocation() {
    $location = $_REQUEST['locationType'];
    if ($location == "shelf") {
        $shelfbarcode = $_POST['toShelfBarcode'];
        $itemBarcode = '';
        foreach ($_POST as $key => $value) {
            if (strstr($key, 'checkedItem')) {
                $x = str_replace('checkedItem', '', $key);
                $itemBarcode = $x;
            }
            if ($itemBarcode != "") {
                $status = modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfbarcode);
                if ($status[0][0] === true) {
                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", $sourceComment = null, "Shelf", "Shelf Barcode: " . $shelfbarcode);
                    $_SESSION['status'] = "Location changed succesfully!";
                    $_SESSION['msgType'] = 'success';
                } else {
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'danger';
                }
                $itemBarcode = "";
            }
        }
    }

    if ($location == "user") {
        if (isset($_POST['staffcode']) && isset($_POST['receivePassword'])) {
            $staffCode = $_POST['staffcode'];
            $password = $_POST['receivePassword'];
            $description = '';
            if (isset($_POST['receiveDescription'])) {
                $description = $_POST['receiveDescription'];
            }
            $username = $_POST['username'];
            $shelfId = '';
            $status = modelAuthenticateUser($staffCode, $password);
            if ($status[0][0] == true) {
                $status = modelShelfWithUser($username, $description);
                if ($status[0][0] == true) {
                    $shelfId = $status[0][1];
                    $itemBarcode = '';
                    foreach ($_POST as $key => $value) {
                        if (strstr($key, 'checkedItem')) {
                            $x = str_replace('checkedItem', '', $key);
                            $itemBarcode = $x;
                        }
                        if ($itemBarcode != "") {
                            $status = modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode);
                            if ($status[0][0] === true) {
                                if (isset($_POST['receiveDescription'])) {
                                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username . ",Reason for taking:" . $description);
                                } else {
                                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username);
                                }
                                $_SESSION['status'] = "items relocated succesfully";
                                //echo $status[0][1];
                                $_SESSION['msgType'] = 'success';
                            }
                            $itemBarcode = "";
                        }
                    }
                } else {
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'danger';
                    //echo $status[0][1];
                }
            } else {
                $_SESSION['status'] = 'Login failed';
                $_SESSION['msgType'] = 'danger';
                //echo 'Login failed';
            }
        }
    }
    contrStoremanMain();
}

function contrManageConsumables() {
    echo viewAddConsumable();
    echo viewConsumables(modelGetAllConsumables());
}

function contrAddConsumable() {
    $status = array();
    $name = $_REQUEST['name'];
    $description = $_REQUEST['description'];
    $detail = $_REQUEST['detail'];
    $supplier_code = $_REQUEST['supplier'];
    $minQty = $_REQUEST['minQty'];
    $maxQty = $_REQUEST['maxQty'];
    $qtyInStore = $_REQUEST['qtyInStore'];
    $nettValue = $_REQUEST['nettValue'];
    if (empty($name)) {
        echo 'Please enter the name of the consumable';
    } elseif (empty($description)) {
        echo 'Please enter the description of the consumable';
    } elseif (empty($detail)) {
        echo 'Please enter the details of the consumable';
    } elseif (empty($supplier_code)) {
        echo 'Please enter the supplier code of the consumable';
    } elseif (empty($minQty)) {
        echo 'Please enter the minimum quantity of the consumable';
    } elseif (empty($maxQty)) {
        echo 'Please enter the maximum quantity of the consumable';
    } elseif (empty($qtyInStore)) {
        echo 'Please enter the quantity in-store of the consumable';
    } else {
//            $status = modelAddConsumable($name, $description, $detail, $minQty, $maxQty, $qtyInStore);
        $status = modelAddConsumable($name, $description, $detail, $minQty, $maxQty, $qtyInStore, $supplier_code, $nettValue);
        echo $status[0][1];
    }
}

//    function contrUpdateConsumable()
//    {
//        $status = array();
//        $id = $_REQUEST['id'];
//        $name = $_REQUEST['name'];
//        $description = $_REQUEST['description'];
//        $detail = $_REQUEST['detail'];
//        $minQty = $_REQUEST['minQty'];
//        $maxQty = $_REQUEST['maxQty'];
//        $qtyInStore = $_REQUEST['qtyInStore'];
//        if (empty($name))
//        {
//            echo 'Please enter the name of the consumable';
//        }
//        elseif (empty ($description))
//        {
//            echo 'Please enter the description of the consumable';
//        }
//        elseif (empty ($detail))
//        {
//            echo 'Please enter the details of the consumable';
//        }
//        elseif (empty ($minQty))
//        {
//            echo 'Please enter the minimum quantity of the consumable';
//        }
//        elseif (empty ($maxQty))
//        {
//            echo 'Please enter the maximum quantity of the consumable';
//        }
//        elseif (empty ($qtyInStore))
//        {
//            echo 'Please enter the quantity in-store of the consumable';
//        }
//        else
//        {
//            $status = modelUpdateConsumable($id,$name, $description, $detail, $minQty, $maxQty, $qtyInStore);
//            echo $status[0][1];
//        }
//
//    }
//    function contrDeleteConsumable()
//    {
//        $id = $_REQUEST['id'];
//        $status = modelDeleteConsumable($id);
//        echo $status[0][1];
//
//    }

function contrMarryItemBarcode() {
    //$location = $_REQUEST['locationType'];

    $itemBarcode = '';
    foreach ($_POST as $key => $value) {
        if (strstr($key, 'txtBarcode')) {
            $x = str_replace('txtBarcode', '', $key);
            $itemBarcode = $value;
        }
        if ($itemBarcode != "") {
            $status = modelUpdateItemBarcode($x, $itemBarcode);
            if ($status[0][0] === true) {
                modelLog('Item', 'Assigned Barcode', null, $itemBarcode, $orderNo = null, "", $sourceComment = null, "", "");
                $_SESSION['status'] = "Item barcode assigned succesfully!";
                $_SESSION['msgType'] = 'success';
            } else {
                $_SESSION['status'] = $status[0][1];
                $_SESSION['msgType'] = 'danger';
                //echo $status[0][1];
            }
            $itemBarcode = "";
        }
    }

    contrManageOrdersMarry();
}

function contrAssignLocationMobile() {
    $location = $_REQUEST['locationType'];
    if ($location == "shelf") {
        $shelfbarcode = $_POST['toShelfBarcode'];
        $itemBarcode = '';
        foreach ($_POST as $key => $value) {
            if (strstr($key, 'checkedItem')) {
                $x = str_replace('checkedItem', '', $key);
                $itemBarcode = $x;
            }
            if ($itemBarcode != "") {
                $status = modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfbarcode);
                if ($status[0][0] === true) {
                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", $sourceComment = null, "Shelf", "Shelf Barcode: " . $shelfbarcode);
                    $_SESSION['status'] = "Location changed succesfully!";
                    $_SESSION['msgType'] = 'success';
                    echo 'Location changed succesfully!';
                } else {
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'danger';
                    echo $status[0][1];
                }
                $itemBarcode = "";
            }
        }
    }

    if ($location == "user") {
        if (isset($_POST['staffcode']) && isset($_POST['receivePassword'])) {
            $staffCode = $_POST['staffcode'];
            $password = $_POST['receivePassword'];
            $description = '';
            if (isset($_POST['receiveDescription'])) {
                $description = $_POST['receiveDescription'];
            }
            $username = $_POST['username'];
            $shelfId = '';
            $status = modelAuthenticateUser($staffCode, $password);
            if ($status[0][0] == true) {
                $status = modelShelfWithUser($username, $description);
                if ($status[0][0] == true) {
                    $shelfId = $status[0][1];
                    $itemBarcode = '';
                    foreach ($_POST as $key => $value) {
                        if (strstr($key, 'checkedItem')) {
                            $x = str_replace('checkedItem', '', $key);
                            $itemBarcode = $x;
                        }
                        if ($itemBarcode != "") {
                            $status = modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode);
                            if ($status[0][0] === true) {
                                if (isset($_POST['receiveDescription'])) {
                                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username . ",Reason for taking:" . $description);
                                } else {
                                    modelLog('Item', 'Relocate', null, $itemBarcode, $orderNo = null, "Shelf", "", "User", "Name: " . $username);
                                }
                                $_SESSION['status'] = "items relocated succesfully";
                                //echo $status[0][1];
                                $_SESSION['msgType'] = 'success';
                            }
                            $itemBarcode = "";
                        }
                    }
                } else {
                    $_SESSION['status'] = $status[0][1];
                    $_SESSION['msgType'] = 'danger';
                    //echo $status[0][1];
                }
            } else {
                $_SESSION['status'] = 'Login failed';
                $_SESSION['msgType'] = 'danger';
                //echo 'Login failed';
            }
        }
    }
    contrStoremanMain();
}

function contrDeleteAssignBarcode() {
    if (isset($_REQUEST['ItemsId'])) {
        $itemId = $_REQUEST['ItemsId'];
        $status = modelDeleteAssignBarcodeItems($itemId);
        if ($status[0][0] === true) {
            $_SESSION['msgType'] = 'success';
        } else {
            $_SESSION['msgType'] = 'danger';
        }
        $_SESSION['status'] = $status[0][1];
    }
    contrManageOrdersMarry();
}

function contrReportbarcode() {
    echo viewReportByBarcode();
    if (isset($_POST['barcode']) || isset($_GET['barcode'])) {
        $barcode = $_REQUEST['barcode'];
        if (isset($barcode)) {
            echo viewReportsBarcodeOrder(modelGetItemsByBarcode($barcode), modelGetItemsByBarcodeItems($barcode));
        }
    }
}

function contrReportUntrackedItemHistory() {
    echo viewUntrackedItemHistory(modelGetAllUntrackedItemsLog());
}

function contrReportLocation() {
    echo viewReportByLocation(viewDropdownShelfWithProject(modelGetShelvesWithProject()));
    if (isset($_POST['ddShelfProject'])) {
        $location = $_REQUEST['ddShelfProject'];
        if (isset($location)) {
            echo viewReportItemsByLocation(modelGetItemByLocation($location));
        }
    }
}

function contrReportProject() {
    
    echo viewReportByProject(viewDropdownS4ProjectsWithID(modelGetProjectsIntranet(), "ddReportProjectS4"), viewDropdownMMProjectsWithID(modelGetProjectsAutonet(), "ddReportProjectAuto"));
    if (isset($_POST['s4ProjectsAdd']) && !empty($_POST['s4ProjectsAdd']) && isset($_POST['txtOrderNumber'])) {       
        $project = $_POST['s4ProjectsAdd'];
        $orderNumber = $_POST['txtOrderNumber'];
        echo viewReportsBarcodeOrder(modelGetItemsByS4Project($project,$orderNumber), modelGetItemsByS4ProjectItems($project,$orderNumber));
        
    } elseif (isset($_POST['mmProjectsAdd']) && !empty($_POST['mmProjectsAdd']) && isset($_POST['txtOrderNumber'])) {
        
        $project = $_POST['mmProjectsAdd'];
        $orderNumber = $_POST['txtOrderNumber'];
        echo viewReportsBarcodeOrder(modelGetItemsByMMProject($project,$orderNumber), modelGetItemsByMMProjectItems($project,$orderNumber));
    }
}

function contrRelocateRemoveItem() {
    $id = $_REQUEST['ItemId'];
    $status = modelDeleteRelocateItem($id);
    if ($status[0][0]) {
        $_SESSION['msgType'] = 'success';
    } else {
        $_SESSION['msgType'] = 'danger';
    }
    $_SESSION['status'] = $status[0][1];
    contrStoremanMain();
}

function contrForgotPassword() {
    if (isset($_POST['email'])) {
        $id = '';
        $email = $_POST['email'];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $status = modelCheckEmail($email);
            if ($status[0][0] === true) {
                $id = $status[0][1];
                $subject = "Stock Control - Reset Password";
                $code = rand(100, 999);
                $status = modelUpdateActivationCode($id, $code);
                if ($status[0][0] === true) {
                    $body = "Your reset password link is : <a href='http://" . $_SERVER['SERVER_NAME'] . "/stockcontrol/src/index.php?pid=manageUsers&fid=changePassword&id=$id&code=$code'>here</a>";
                    $status = modelSendEmail($email, $subject, $body);
                    if ($status[0][0] === true) {
                        $_SESSION['msgType'] = 'success';
                        $_SESSION['status'] = "Your reset password link was sent to $email.Please view your email and click on the link.";
                    } else {
                        $_SESSION['msgType'] = 'danger';
                        $_SESSION['status'] = $status[0][1];
                    }
                } else {
                    $_SESSION['msgType'] = 'danger';
                    $_SESSION['status'] = $status[0][1];
                }
            } else {
                $_SESSION['msgType'] = 'danger';
                $_SESSION['status'] = $status[0][1];
            }
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = "$email is not a valid email address";
        }
    }
    echo viewforgotpassword();
}

function contrChangePassword() {
    if (isset($_GET['id']) && isset($_GET['code'])) {
        $id = $_GET['id'];
        $code = $_GET['code'];
        $status = modelCheckValidCode($id, $code);
        if ($status[0][0] === true) {
            echo viewResetPassword($id);
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = $status[0][1];
        }
    } else {
        echo viewInvalidPage();
    }
}

function contrNewPassword() {
    if (isset($_POST['password']) && isset($_POST['hidId'])) {
        $id = $_POST['hidId'];
        $password = $_POST['password'];
        $status = modelUpdatePassword($id, $password);
        if ($status[0][0] === true) {
            $_SESSION['msgType'] = 'success';
            $_SESSION['status'] = $status[0][1];
            echo contrStoremanLogin();
        } else {
            $_SESSION['msgType'] = 'danger';
            $_SESSION['status'] = $status[0][1];
        }
    } else {
        echo viewInvalidPage();
    }
}

function contrManageEmailNotification() {
    echo viewEmailNotificationSettings(modelGetNotificationEmailUsers(), viewDropdownnotificationUsers(modelGetnotificationUsers()));
}

function contrManageEmailNotificationAddUser() {
    if (isset($_POST['btnAdd'])) {
        if (isset($_POST['staffEmail'])) {
            $value = $_POST['staffEmail'];
            $array = explode("*", $value);
            $email = $array[0];
            $staffCode = $array[1];
            $db = $array[2];
            $status = modelAddNotificationUser($email, $staffCode, $db);
            if ($status[0][0] === true) {
                $_SESSION['msgType'] = 'success';
            } else {
                $_SESSION['msgType'] = 'danger';
            }
            $_SESSION['status'] = $status[0][1];
        }
    }
    echo viewEmailNotificationSettings(modelGetNotificationEmailUsers(), viewDropdownnotificationUsers(modelGetnotificationUsers()));
}

function contrManageEmailNotificationDeleteUser() {
    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $status = modelRemoveNotificationUser($id);
        if ($status[0][0] === true) {
            $_SESSION['msgType'] = 'success';
        } else {
            $_SESSION['msgType'] = 'danger';
        }
        $_SESSION['status'] = $status[0][1];
    }
    echo viewEmailNotificationSettings(modelGetNotificationEmailUsers(), viewDropdownnotificationUsers(modelGetnotificationUsers()));
}

function contrLogoff() {
    session_destroy();
    $_SESSION = array();
    contrStoremanLogin();
}

//Menu
function contrCreateMenu() {
    return viewStoremanMenu();
}

#State Settings

$stateArray = array();
$stateArray["userLogin"]["none"] = "contrUserLogin";
$stateArray["userLogin"]["authenticate"] = "contrUserLoginAuthenticate";
$stateArray["storemanLogin"]["none"] = "contrStoremanLogin";
$stateArray["storemanLogin"]["authenticate"] = "contrStoremanLoginAuthenticate";
$stateArray["storemanMain"]["none"] = "contrStoremanMain";
$stateArray["manageShelves"]["none"] = "contrManageShelves";
$stateArray["manageShelves"]["add"] = "contrManageShelves";
$stateArray["manageShelves"]["update"] = "contrManageShelves";
$stateArray["manageShelves"]["delete"] = "contrManageShelves";
$stateArray["manageShelves"]["print"] = "contrManageShelves";
$stateArray["manageUsers"]["none"] = "contrManageUsers";
$stateArray["manageUsers"]["add"] = "contrManageUsers";
$stateArray["manageUsers"]["update"] = "contrManageUsers";
$stateArray["manageUsers"]["delete"] = "contrManageUsers";
$stateArray["manageUsers"]["forgotPassword"] = "contrForgotPassword";
$stateArray["manageUsers"]["changePassword"] = "contrChangePassword";
$stateArray["manageUsers"]["newPassword"] = "contrNewPassword";
$stateArray["ManageOrders"]["none"] = "contrManageOrders";
$stateArray["manageConsumableOrder"]["none"] = "contrManageConsumableOrder";
$stateArray["ManageOrders"]["items"] = "contrManageOrdersItems";
$stateArray["ManageOrders"]["print"] = "contrManageOrdersPrint";
$stateArray["ManageOrders"]["consumable"] = "contrManageOrdersConsumable";
$stateArray["ManageOrders"]["untracked"] = "contrManageOrdersUntracked";
$stateArray["ManageOrders"]["consumableItem"] = "contrManageOrdersConsumableItem";
$stateArray["consumable"]["all"] = "contrManageConsumables";
$stateArray["ManageOrders"]["consumableRelocate"] = "contrManageOrdersConsumableRelocate";
$stateArray["marryItems"]["none"] = "contrManageOrdersMarry";
$stateArray["marry"]["item"] = "contrMarryItemBarcode";
$stateArray['ManageReturns']["none"] = "contrManageReturns";
$stateArray['ManageReturns']["store"] = "contrManageReturnStore";
$stateArray['ManageReturns']["relocateUserSpecific"] = "contrManageReturnRelocateUserSpecific";
$stateArray['ManageLocation']["none"] = "contrManageLocation";
$stateArray['ManageLocation']["change"] = "contrManageSetLocation";
$stateArray['AssignLocation']["change"] = "contrAssignLocation";
$stateArray['noOrderItems']["none"] = "contrNoOrderItems";
$stateArray['noOrderItems']["update"] = "contrNoOrderItemsUpdate";
$stateArray['report']["order"] = "contrReportOrder";
$stateArray['report']["barcode"] = "contrReportbarcode";
$stateArray['report']["itemHistory"] = "contrReportItemHistory";
$stateArray['report']["untrackedItemHistory"] = "contrReportUntrackedItemHistory";
$stateArray['report']["location"] = "contrReportLocation";
$stateArray['report']["project"] = "contrReportProject";
$stateArray['Relocate']["remove"] = "contrRelocateRemoveItem";
$stateArray['report']["searchfilter"] = "contrReportSearchfilter";
$stateArray['report']["searchfilterUntracked"] = "contrReportSearchfilterUntracked";
$stateArray["logoff"]["none"] = "contrLogoff";
$stateArray["AssignBarcode"]["delete"] = "contrDeleteAssignBarcode";
$stateArray["manageEmail"]["none"] = "contrManageEmailNotification";
$stateArray["manageEmail"]["add"] = "contrManageEmailNotificationAddUser";
$stateArray["manageEmail"]["delete"] = "contrManageEmailNotificationDeleteUser";
# End of State Settings

$mePid = isset($_GET["pid"]) ? $_GET["pid"] : "storemanMain";
$meFid = isset($_GET["fid"]) ? $_GET["fid"] : "none";


//navigate to relevant page
if (isset($stateArray[$mePid][$meFid])) {
    unset($_SESSION['status']); //Clear status msg
    unset($_SESSION['msgType']);
    unset($_SESSION['resell']);
    unset($_SESSION['locationHistory']);
    unset($_SESSION['statusTrackedResell']);
    unset($_SESSION['statusConsumable']);
    unset($_SESSION['statusUntracked']);
    unset($_SESSION['msgTypeConsumable']);

    modelGetAllTrackedReturnItems();
    modelGetAllPrintedItems();

    if ($meFid == 'none') {
        if (isset($_SESSION['userId'])) {
            call_user_func($stateArray[$mePid][$meFid]);
        } else {
            call_user_func('contrStoremanLogin');
        }
    } else {
        call_user_func($stateArray[$mePid][$meFid]);
    }
} else {
    echo viewInvalidPage();
}
