<?php
//function db_connect()
//{
//    return new PDO('mysql:host=192.168.0.203;dbname=stock_control;charset=utf8', 's4mysql', '', 
//            array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
//}
function build_statement($q, $p)
{
	$statement = $q;
	if (is_array($p))
		foreach($p as $key => $value)
		{
			$statement = str_replace($key, "'".$value."'",$statement);
			
		}

	return $statement;
}
function db_connect()
{
	
	$link = mysql_connect('localhost', 'root', '');
	if (!$link) 
	{
		die('Could not connect: ' . mysql_error());
	}
	
	$db_selected = mysql_select_db('stock_control', $link);
	if (!$db_selected) 
	{
		die ('Can\'t use stock_control : ' . mysql_error());
	}
	
	return $link;
	
}

function sql_select(&$result, $q = '', $params = null)
{
	$rows = 0;
	$result = array();
	
		$db = db_connect();
		$stmt = build_statement($q, $params);
		$dataset = mysql_query($stmt, $db);
		if (!$dataset) 
		{
			die('SQL select Invalid query: ' . mysql_error());
		}
		$rows = mysql_num_rows($dataset);
		while ($row = mysql_fetch_assoc($dataset))
		{
			$result[] = $row;
		}
	
	return $rows;
}

function sql_insert(&$id, $q = '', $params = null)
{
	$rows = 0;
	
		$db = db_connect();
		$stmt = build_statement($q, $params);
		$dataset = mysql_query($stmt, $db);
		if (!$dataset) 
		{
			die('SQL insert Invalid query: ' . mysql_error());
		}
		$rows = mysql_affected_rows($db);
		$id = mysql_insert_id($db);
	
	return $rows;
}

//function sql_select(&$result, $q = '', $params = null)
//{
//    $rows = 0;
//    try {
//        $db = db_connect();
//        $stmt = $db->prepare($q);
//        $stmt->execute($params);
//        $rows = $stmt->rowCount();
//        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
//    }
//    catch (PDOException $e) {
//        echo '<span class="error">' . $e->getMessage() . '</span>';
//    }
//    return $rows;
//}

//function sql_insert(&$id, $q = '', $params = null)
//{
//    $rows = 0;
//    try {
//        $db = db_connect();
//        $stmt = $db->prepare($q);
//        $stmt->execute($params);
//        $rows = $stmt->rowCount();
//        $id = $db->lastInsertId();
//    }
//    catch (PDOException $e) {
//        echo '<span class="error">' . $e->getMessage() . '</span>';
//    }
//    return $rows;
//}

function sql_update($q = '', $params = null)
{
	$rows = 0;
	
		$db = db_connect();
		$stmt = build_statement($q, $params);
		$dataset = mysql_query($stmt, $db);
		if (!$dataset) 
		{
			die('SQL update Invalid query: ' . mysql_error());
		}
		$rows = mysql_affected_rows($db);
	
	return $rows;
}

//function sql_update($q = '', $params = null)
//{
//    $rows = 0;
//    try {
//        $db = db_connect();
//        $stmt = $db->prepare($q);
//        $stmt->execute($params);
//        $rows = $stmt->rowCount();
//    }
//    catch (PDOException $e) {
//        echo '<span class="error">' . $e->getMessage() . '</span>';
//    }
//    return $rows;
//}

function sql_delete($q = '', $params = null)
{
	$rows = 0;
	
		$db = db_connect();
		$stmt = build_statement($q, $params);
		$dataset = mysql_query($stmt, $db);
		if (!$dataset) 
		{
			die('SQL delete Invalid query: ' . mysql_error());
		}
		$rows = mysql_affected_rows($db);
	
	return $rows;
}

//function sql_delete($q = '', $params = null)
//{
//    $rows = 0;
//    try {
//        $db = db_connect();
//        $stmt = $db->prepare($q);
//        $stmt->execute($params);
//        $rows = $stmt->rowCount();
//    }
//    catch (PDOException $e) {
//        echo '<span class="error">' . $e->getMessage() . '</span>';
//    }
//    return $rows;
//}

function sql_count($table)
{
    
        $db = db_connect();
        $q = 'SELECT COUNT(*) AS total FROM ' . $table;
        if ($result = mysql_query($q, $db)) 
		{
			$data = mysql_fetch_assoc($result);
            return $data["total"];
        }
    
    return 0;
}

//function sql_count($table)
//{
//    try {
//        $db = db_connect();
//        $q = 'SELECT COUNT(*) FROM ' . $table;
//        if ($result = $db->query($q)) {
//            return $result->fetchColumn();
//        }
//    }
//    catch (PDOException $e) {
//        echo '<span class="error">' . $e->getMessage() . '</span>';
//    }
//    return 0;
//}
?>