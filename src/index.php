<?php
//error_reporting(E_ERROR);
session_start();
ob_start();
    include 'mvc/Controller.php';
   
$main_output = ob_get_clean();

$menu_output = contrCreateMenu();


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        
        
        <link rel="stylesheet" href="css/bootstrap.css"  />
        <link rel="stylesheet" href="css/datepicker.css" />
        <link rel="stylesheet" href="css/chosen.css" />
        <link rel='stylesheet' href='css/style.css' />
        <link rel="stylesheet" href="css/prettify.css" />
        <link rel="stylesheet" href="css/bootstrap-switch.css"/>
        <link rel="stylesheet" href="css/awesome-bootstrap-checkbox.css"/>
        <link rel="stylesheet" href="css/font-awesome.css"/>        
        <link rel="stylesheet" href="css/build.css"/>
        <link rel="stylesheet" href="css/multiple-emails.css"/>
        
        <script type="text/javascript" src="js/jquery-1.8.3.js"></script> 
        <script type='text/javascript' src='js/picnet.table.filter.min.js'></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-datepicker.js"></script>
        <script type='text/javascript' src='js/scripts.js'></script>
        <script type="text/javascript" src="js/chosen.jquery.js"></script>
        <script type="text/javascript" src="js/jquery.bootstrap.wizard.js"></script>
        <script type="text/javascript" src="js/prettify.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/bootstrap-switch.js"></script>
        <script type="text/javascript" src="js/multiple-emails.js"></script>
        <title>Stock Room</title>
    </head>
    <body>
        <div id="header">  
        </div>
            <div id='nav'>
                <div class='navbar navbar-default navbar-fixed-top' role='navigation'>
                        <div class='container'>
                        <?php echo $menu_output ;?>
                        </div>
                </div>
             </div>
        <div id="main">
            
            <div id="content">
                <?php
                   echo $main_output;
                ?>
            </div>
             
        </div>
<!--        <div id="footer" class='navbar navbar-default navbar-fixed-bottom' role='navigation'>
            <div class="container">
                <h4 id='footerText'></h4>
            </div>
        </div>-->
       
    </body>
</html>    




