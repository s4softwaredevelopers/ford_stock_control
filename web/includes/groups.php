<h2>Groups</h2>

<?php
if (isset($_POST['addgroup'])) {
    $description = trim($_POST['group_description']);
    
    $q = 'INSERT INTO item_group (description) VALUES (:description)';
    $params = array(':description' => $description);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Group ' . $description . ' added.</p>';
        logger('add', 'Group :: ' . $description);
    }
    else {
        echo '<p class="error">Group not added.</p>';
    }
}
elseif (isset($_POST['updategroup'])) {
    $id = $_POST['updategroup'];
    $description = trim($_POST['group_description']);
    
    $q = 'UPDATE item_group SET description=:description WHERE id=:id';
    $params = array(':id' => $id, ':description' => $description);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>Group ' . $description . ' updated.</p>';
        logger('update', 'Group :: ' . $description);
    }
    else {
        echo '<p class="error">Group not updated.</p>';
    }
}
elseif (isset($_POST['removegroup'])) {
    $id = $_POST['removegroup'];
    $description = $_POST['group_description'];
    
    $q = 'DELETE FROM item_group WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>Group ' . $description . ' removed.</p>';
        logger('remove', 'Group :: ' . $description);
    }
    else {
        echo '<p class="error">Group not removed.</p>';
    }
} ?>
<div class='auxwrapper'>
    <input type='button' value='Add'>
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addgroupform' action='index.php?pid=groups' method='post'>
            <tr>
                <th>Name</th>
                <td>
                    <input id='focus' type='text' name='group_description' value=''>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addgroup' value='addgroup'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = "SELECT id, description FROM item_group WHERE description NOT IN ('Unassigned') ORDER BY id";

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='view first last w140'>Name</th>
                <th class='edit' filter='false'></th>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $description = $row['description']; 
            ?>
            <tr>
                <form name='updategroupform<?php echo $id ?>' action='index.php?pid=groups' method='post'>
                    <td class='view w140'>
                        <div><?php echo $description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='group_description' value='<?php echo $description; ?>'>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updategroup' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form description='removegroupform<?php echo $id ?>' action='index.php?pid=groups' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removegroup' value='<?php echo $id ?>'>
                        <input type='hidden' name='group_description' value='<?php echo $description; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No groups in database.</p>';
} ?>