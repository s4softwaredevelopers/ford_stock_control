package MyContent;

import java.util.List;

import com.example.s4_scanner_v1.R;

import MyContent.AssignedItemContent.AssignedItem;
import android.R.color;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAssignBarcodesAdapter extends ArrayAdapter<AssignedItemContent.AssignedItem> {
  private final Context context;
  private final  List<AssignedItem> ITEMS;

  public MyAssignBarcodesAdapter(Context context, List<AssignedItem> ITEMS) {
    super(context, R.layout.rowlayout, ITEMS);
    this.context = context;
    this.ITEMS = ITEMS;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    LayoutInflater inflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
    TextView textView = (TextView) rowView.findViewById(R.id.label);
    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
    textView.setText(AssignedItemContent.ITEM_MAP.get(ITEMS.get(position).id).description);
    if (AssignedItemContent.ITEM_MAP.get(ITEMS.get(position).id).barcode.equals(""))
    {
    	imageView.setImageResource(color.transparent);
    }
    else
    {
    	imageView.setImageResource(R.drawable.transparent_green_checkmark); 
	    if (AssignedItemContent.ITEM_MAP.get(ITEMS.get(position).id).finished == true)
	    {
	    	imageView.setImageResource(R.drawable.accept_round);
	    }
	    if (AssignedItemContent.ITEM_MAP.get(ITEMS.get(position).id).dublicateBarcode == true)
	    {
	    	imageView.setImageResource(R.drawable.cancel_round);
	    }
    }
    return rowView;
  }
} 