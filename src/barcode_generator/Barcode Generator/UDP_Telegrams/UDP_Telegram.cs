﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading; 

namespace UDP_Telegrams
{
    public partial class UDP_Telegram : Form
    {
        int ServerPort;
        int RemotePort;
        int RemoteReplyPort;
        int ServerReplyPort;
        Boolean listenDone = false;
        Boolean TalkerDone = false;
        Boolean exception_thrown1 = false;
        Boolean exception_thrown2 = false;
        IPAddress send_to_address;
        IPEndPoint sending_end_point;
        Socket sending_socket;
        Thread UdpTalkerThread;
        Thread UdpListenThread;

        public UDP_Telegram()
        {
            InitializeComponent();
        }

        public void AppendTextBox(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendTextBox), new object[] { value });
                return;
            }
            richTextBoxListen.Select(0, 0);
            richTextBoxListen.SelectedText = DateTime.Now + " - " + value;
        }

        public void ChangeLabelStatus(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChangeLabelStatus), new object[] { value });
                return;
            }
            labelStatus.Text = value;
        }

        public void ChangeLabelTalkerMessageStatus1(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChangeLabelTalkerMessageStatus1), new object[] { value });
                return;
            }
            labelTalkerMessageStatus1.Text = value;
        }

        public void ChangeLabelTalkerMessageStatus2(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChangeLabelTalkerMessageStatus2), new object[] { value });
                return;
            }
            labelTalkerMessageStatus2.Text = value;
        }

        public void ChangeLabelTalkerMessageStatus3(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChangeLabelTalkerMessageStatus3), new object[] { value });
                return;
            }
            labelTalkerMessageStatus3.Text = value;
        }

        public void ChangeTextBoxTalkerMessage(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(ChangeTextBoxTalkerMessage), new object[] { value });
                return;
            }
            textBoxTalkerMessage.Text = value;
        }

        private void buttonStartListen_Click(object sender, EventArgs e)
        {
            textBoxServerIPNumber.Enabled = false;
            textBoxServerPortNumber.Enabled = false;
            textBoxServerReplyPort.Enabled = false;

            if (textBoxServerPortNumber.Text == "")
            {
                ServerPort = 11000;
            }
            else
            {
                ServerPort = int.Parse(textBoxServerPortNumber.Text);
            }
            if (textBoxServerReplyPort.Text == "")
            {
                ServerReplyPort = 11001;
            }
            else
            {
                ServerReplyPort = int.Parse(textBoxServerReplyPort.Text);
            }
            buttonStartServer.Enabled = false;
            buttonStopServer.Enabled = true;
            ChangeLabelStatus("Waiting for broadcast");
            UdpListenThread = new Thread(() =>
            {
                UdpClient listener = new UdpClient(ServerPort);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, ServerPort);
                string received_data;
                byte[] receive_byte_array;
                try
                {
                    while (!listenDone)
                    {
                        receive_byte_array = listener.Receive(ref groupEP);
                        ChangeLabelStatus("Received a broadcast from " + groupEP.ToString());
                        received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                        AppendTextBox("\n" + groupEP.ToString() + "\n" + received_data + "\n\n");
                        Thread.Sleep(1000);
                        Reply(groupEP,"OK");
                    }
                }
                catch (Exception ex)
                {
                    AppendTextBox(ex.ToString());
                }
                listener.Close();
            });
            UdpListenThread.IsBackground = true;
            UdpListenThread.Start();
        }

        private void Reply(IPEndPoint Reply_sending_end_point, string Reply_text_to_send)
        {
            Socket Reply_sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            byte[] Reply_send_buffer = Encoding.ASCII.GetBytes(Reply_text_to_send);

            Reply_sending_end_point.Port = 11001;
            Reply_sending_socket.SendTo(Reply_send_buffer, Reply_sending_end_point);
            ChangeLabelTalkerMessageStatus3("OK");
            AppendTextBox("\n" + Reply_sending_end_point.ToString() + "\n" + Reply_text_to_send + "\n\n");          
        }

        private void UDP_Telegram_Load(object sender, EventArgs e)
        {
            buttonStopServer.Enabled = false;
            buttonStopRemote.Enabled = false;
            buttonRemoteSend.Enabled = false;
            ChangeLabelStatus("");
            ChangeLabelTalkerMessageStatus1("");
            ChangeLabelTalkerMessageStatus2("");
            ChangeLabelTalkerMessageStatus3("");
        }

        private void buttonStopListen_Click(object sender, EventArgs e)
        {
            textBoxServerIPNumber.Enabled = true;
            textBoxServerPortNumber.Enabled = true;
            textBoxServerReplyPort.Enabled = true;
            listenDone = true;
            //if (UdpListenThread.IsAlive)
            //{
            //    UdpListenThread.Abort();
            //}
            buttonStartServer.Enabled = true;
            buttonStopServer.Enabled = false;
            ChangeLabelStatus("");
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonTalkerSend_Click(object sender, EventArgs e)
        {
            string text_to_send = textBoxTalkerMessage.Text;
            byte[] send_buffer = Encoding.ASCII.GetBytes(text_to_send);

            ChangeLabelTalkerMessageStatus1("sending to address: " + sending_end_point.Address + " port: " + sending_end_point.Port);

            try
            {
                sending_socket.SendTo(send_buffer, sending_end_point);
            }
            catch (Exception send_exception)
            {
                exception_thrown1 = true;
                ChangeLabelTalkerMessageStatus2(" Exception " + send_exception.Message);
            }
            if (exception_thrown1 == false)
            {
                ChangeLabelTalkerMessageStatus2("Message has been sent to the broadcast address");
                ChangeTextBoxTalkerMessage("");
            }
            else
            {
                exception_thrown1 = false;
                //ChangeLabelTalkerMessageStatus2("The exception indicates the message was not sent.");
            }
            ChangeLabelTalkerMessageStatus3("Waiting for confirmation");
            ReplyConfirm(sending_end_point.Address);

        }

        private void ReplyConfirm(IPAddress IPAddress)
        {
            Thread ReplyConfirmThread = new Thread(() =>
            {           
                UdpClient ReplyConfirmlistener = new UdpClient(RemoteReplyPort);
                IPEndPoint R_groupEP = new IPEndPoint(IPAddress, RemoteReplyPort);
                string received_data;
                byte[] receive_byte_array;
                try
                {
                    //while (!listenDone)
                    //{
                    receive_byte_array = ReplyConfirmlistener.Receive(ref R_groupEP);
                    received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                    ChangeLabelTalkerMessageStatus3("Confirmed: " + received_data);
                    //}
                }
                catch (Exception ex)
                {
                    ChangeLabelTalkerMessageStatus3(ex.ToString());
                }
                ReplyConfirmlistener.Close();
            });            
            ReplyConfirmThread.IsBackground = true;
            ReplyConfirmThread.Start();
        }

        private void buttonStartTalking_Click(object sender, EventArgs e)
        {
            textBoxRemoteIPNumber.Enabled = false;
            textBoxRemotePortNumber.Enabled = false;
            textBoxRemoteReplyPort.Enabled = false;
            buttonStartRemote.Enabled = false;
            buttonStopRemote.Enabled = true;
            

            if (textBoxRemotePortNumber.Text == "")
            {
                RemotePort = 11000;
            }
            else
            {
                RemotePort = int.Parse(textBoxRemotePortNumber.Text);
            }
            
            if (textBoxRemoteReplyPort.Text == "")
            {
                RemoteReplyPort = 11001;
            }
            else
            {
                RemoteReplyPort = int.Parse(textBoxRemoteReplyPort.Text);
            }

            try
            {
                send_to_address = IPAddress.Parse(textBoxRemoteIPNumber.Text);
            }
            catch (ArgumentNullException te)
            {
                ChangeLabelTalkerMessageStatus1("Source : " + te.Source);
                ChangeLabelTalkerMessageStatus2("Message : " + te.Message);
                exception_thrown2 = true;
                return;
            }

            catch (FormatException te)
            {
                ChangeLabelTalkerMessageStatus1("Source : " + te.Source);
                ChangeLabelTalkerMessageStatus2("Message : " + te.Message);
                exception_thrown2 = true;
                return;
            }

            catch (Exception te)
            {
                ChangeLabelTalkerMessageStatus1("Source : " + te.Source);
                ChangeLabelTalkerMessageStatus2("Message : " + te.Message);
                exception_thrown2 = true;
                return;
            }
            buttonRemoteSend.Enabled = true;
            textBoxTalkerMessage.Focus();
            UdpTalkerThread = new Thread(() =>
            {
                sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                sending_end_point = new IPEndPoint(send_to_address, RemotePort);

                while (!TalkerDone)
                {

                }

            });
            UdpTalkerThread.IsBackground = true;
            UdpTalkerThread.Start();
        }

        private void buttonStopTalking_Click(object sender, EventArgs e)
        {
            textBoxRemoteIPNumber.Enabled = true;
            textBoxRemoteIPNumber.Focus();
            textBoxRemotePortNumber.Enabled = true;
            textBoxRemoteReplyPort.Enabled = true;
            buttonRemoteSend.Enabled = false;
            TalkerDone = true;
            if (exception_thrown2)
            {
                textBoxRemoteIPNumber.Clear();
                exception_thrown2 = false;
            }
            else
            {
                UdpTalkerThread.Abort();
            }
            buttonStartRemote.Enabled = true;
            buttonStopRemote.Enabled = false;
            ChangeLabelTalkerMessageStatus1("");
            ChangeLabelTalkerMessageStatus2("");
        }

        private void groupBoxTalker_Enter(object sender, EventArgs e)
        {

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ChangeLabelTalkerMessageStatus1("");
            ChangeLabelTalkerMessageStatus2("");
            ChangeLabelTalkerMessageStatus3("");
            textBoxRemoteIPNumber.Enabled = true;
            textBoxRemotePortNumber.Enabled = true;
            textBoxRemoteReplyPort.Enabled = true;
            buttonRemoteSend.Enabled = false;
            buttonStartRemote.Enabled = true;
            buttonStopRemote.Enabled = false;
        }

        private void labelStatus_Click(object sender, EventArgs e)
        {

        }
    }
}
