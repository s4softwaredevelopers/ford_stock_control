package com.example.s4_scanner_v1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import MyContent.HistoryContent;
import MyContent.MyWebRequest;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class HistoryActivity extends Activity {

	private ProgressBar spinner;
	EditText etBarcode;
	HistoryContent historyContent;
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		
	      spinner = (ProgressBar)findViewById(R.id.progressHistory);
	      spinner.setVisibility(View.GONE);

	      etBarcode = (EditText) findViewById(R.id.editTextHistoryBarcode);
			
	        // get the listview
	        expListView = (ExpandableListView) findViewById(R.id.expandableListViewHistoryDetails);
	        expListView.setVisibility(View.GONE);	
	 


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.history, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
    public void ScanBarcode(View view) {
    	// ScanBarcode Button Press
    	Intent intent = new Intent("com.google.zxing.client.android.SCAN");
    	intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
    	startActivityForResult(intent, 0);
    }
    
    public void HistorySearch(View view) {
    	// HistorySearch Button Press
    	new WebRequestHistorySearch().execute("");
    }
    
    class WebRequestHistorySearch extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=history&fid=search&barcode=" + etBarcode.getText().toString();
			String result = "";
			try{
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				
				PopulateDetails(result);				
				
				return "";
			} catch (Exception e) {
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);	    	
	    	if (result.equals("")) 
	    	{
	    		expListView.setVisibility(View.VISIBLE);
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	}
    
	public void PopulateDetails(String html)
	{
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
		
    	Document doc = Jsoup.parse(html);
    	Elements tableElements = doc.select("table");
    	Elements tableRowElements = tableElements.select(":not(thead) tr");
        for (int i = 0; i < tableRowElements.size(); i++) {
            Element row = tableRowElements.get(i);
            List<String> ColList = new ArrayList<String>();
            Elements rowItems = row.select("td");
            for (int j = 0; j < rowItems.size(); j++) {
            	ColList.add(rowItems.get(j).text());
            }
            listDataHeader.add("History: " + String.valueOf(i));
            listDataChild.put(listDataHeader.get(i), ColList);
        }
        
        listAdapter = new MyContent.ExpandableListAdapter(this, listDataHeader, listDataChild);
   	 
        // setting list adapter
        expListView.setAdapter(listAdapter);
        
       
	}
	
}
