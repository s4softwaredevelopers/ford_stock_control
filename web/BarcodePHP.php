 <?php

    #Ensure that the client has provided a value for "CodeToSearch"
    if (isset($_POST["CodeToSearch"]) && $_POST["CodeToSearch"] != ""){
        
        #Setup variables
        $code = $_POST["CodeToSearch"];
        
        #Connect to Database
        $con = mysqli_connect("localhost","root","", "test");
        
        #Check connection
        if (mysqli_connect_errno()) {
            echo 'Database connection error: ' . mysqli_connect_error();
            exit();
        }

        #Escape special characters to avoid SQL injection attacks
        $code = mysqli_real_escape_string($con, $code);
        
        #Query the database to get the code details.
        $codedetails = mysqli_query($con, "SELECT * FROM barcode WHERE SerialNumber = '$code'");

        #If no data was returned, check for any SQL errors
        if (!$codedetails) {
            echo 'Could not run query: ' . mysqli_error($con);
            exit;
        }

        #Get the first row of the results
        $row = mysqli_fetch_row($codedetails);

        #Build the result array (Assign keys to the values)
        $result_data = array(
            'SerialNumber' => $row[1],
            'Description' => $row[2],
            'Product' => $row[3],
            'Company' => $row[4],
			'Location' => $row[5],
			'Image' => $row[6],
            );

        #Output the JSON data
        echo json_encode($result_data); 
    }else{
        echo "Could not complete query. Missing parameter"; 
    }
?> 