<h2>Users</h2>

<?php
include 'includes/passwordHash.php';

if (isset($_POST['adduser'])) {
    $username = trim($_POST['username']);
    $password = $_POST['password'];
    $user_access_id = $_POST['user_access_id'];
	
    $hasher = new PasswordHash(8, false);
    $hash = $hasher->hash_password($password);
    $q = 'INSERT INTO user (username, password, user_access_id) VALUES (:username, :password, :user_access_id)';
    $params = array(':username' => $username, ':password' => $hash, ':user_access_id' => $user_access_id);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>User ' . $username . ' added.</p>';
        logger('add', 'Username :: ' . $username);
    }
    else {
        echo '<p class="error">User not added.</p>';
    }
}
elseif (isset($_POST['updateuser'])) {
    $id = $_POST['updateuser'];
    $username = trim($_POST['username']);
    
    $q = 'UPDATE user SET username=:username WHERE id=:id';
    $params = array(':id' => $id, ':username' => $username);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>User ' . $username . ' updated.</p>';
        logger('update', 'Username :: ' . $username);
    }
    else {
        echo '<p class="error">User not updated.</p>';
    }
}
elseif (isset($_POST['removeuser'])) {
    $id = $_POST['removeuser'];
    $username = $_POST['username'];
    
    $q = 'DELETE FROM user WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>User ' . $username . ' removed.</p>';
        logger('remove', 'Username :: ' . $username);
    }
    else {
        echo '<p class="error">User not removed.</p>';
    }
}

$q = "SELECT id, description FROM user_access WHERE description NOT IN ('None', 'Overlord')";
$user_access_array = build_result_array($q); ?>

<div class='auxwrapper'>
    <input type='button' value='Add'>
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='adduserform' action='index.php?pid=users' method='post' onsubmit='return passwordCheck();'>
            <tr>
                <th>Username</th>
                <td>
                    <input id='focus' type='text' name='username' value=''>
                </td>
            </tr>
            <tr>
                <th>Password</th>
                <td>
                    <input class='password' type='password' name='password' value=''>
                </td>
            </tr>
            <tr>
                <th>Confirm password</th>
                <td>
                    <input class='passwordconfirm' type='password' name='passwordconfirm' value=''>
                </td>
            </tr>
            <tr>
                <th>User type</th>
                <td>
                    <?php build_select_options('user_access_id', $user_access_array) ?>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='adduser' value='adduser'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = "SELECT id, username FROM user WHERE username NOT IN ('unassigned', 'superuser')";

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='view first last w140'>Username</th>
                <th class='edit' filter='false'></th>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $username = $row['username']; ?>
            <tr>
                <form name='updateuserform<?php echo $id ?>' action='index.php?pid=users' method='post'>
                    <td class='view w140'>
                        <div class='view'><?php echo $username; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input class='edit' type='text' name='username' value='<?php echo $username; ?>'>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updateuser' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form username='removeuserform<?php echo $id ?>' action='index.php?pid=users' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removeuser' value='<?php echo $id ?>'>
                        <input type='hidden' name='username' value='<?php echo $username; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No users in database.</p>';
} ?>