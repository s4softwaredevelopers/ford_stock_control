package MyContent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.widget.ArrayAdapter;

public class AssignedItemContent {

	public static List<AssignedItem> ITEMS = new ArrayList<AssignedItem>();
	
	public static List<AssignedItem> ITEMS_ALL = new ArrayList<AssignedItem>();

	public static Map<String, AssignedItem> ITEM_MAP = new HashMap<String, AssignedItem>();
	
	public static List<String> ORDERS = new ArrayList<String>();
	
	public static ArrayAdapter<AssignedItem> adapter;
	
	public static ArrayAdapter<String> adapterOrders;
	
	public static void addItem(AssignedItem item) {
		ITEMS_ALL.add(item);
		ITEM_MAP.put(item.id, item);
		
		if (!(ORDERS.contains(item.orderNumber))){
			ORDERS.add(item.orderNumber);
		}			
	}
	
	public static void OrderNumbersSort(){
        Collections.sort(ORDERS);
        ORDERS.add(0,"All Orders");
        if (ORDERS.contains(""))
        {
        	ORDERS.remove("");
        	ORDERS.add("No Order Number");
        }
	}
	
    public static void BarcodePopulate(String html){
    	// Get data from string table and add them
    	ITEMS_ALL.clear();
    	ITEM_MAP.clear();
    	ORDERS.clear();	    	
    	
    	Document doc = Jsoup.parse(html);
    	Elements tableElements = doc.select("table");
    	Elements tableRowElements = tableElements.select(":not(thead) tr");
        for (int i = 0; i < tableRowElements.size(); i++) {
            Element row = tableRowElements.get(i);
            List<String> ColList = new ArrayList<String>();
            Elements rowItems = row.select("td");
            for (int j = 0; j < rowItems.size(); j++) {
            	ColList.add(rowItems.get(j).text());
            }
            addItem( new AssignedItem(String.valueOf(i+1), ColList.get(1), ColList.get(2), ColList.get(3), ColList.get(4), ColList.get(5), ColList.get(6), ColList.get(7)));
        }
    }
    
    public static String FailedAssignedItems(String html){
    	// Get data from string table and add them  	
    	String message = "";
    	List<String> failedItems = new ArrayList<String>();
    	
    	
    	html = "<table>" + html + "</table>";
    	
    	Document doc = Jsoup.parse(html);
    	Elements tableElements = doc.select("table");
    	Elements tableRowElements = tableElements.select(":not(thead) tr");
        for (int i = 0; i < tableRowElements.size(); i++) {
            Element row = tableRowElements.get(i);
            List<String> ColList = new ArrayList<String>();
            Elements rowItems = row.select("td");
            for (int j = 0; j < rowItems.size(); j++) {
            	ColList.add(rowItems.get(j).text());
            }
            if (ColList.get(3).contains("Failed"))
            {
            	for (AssignedItem item : ITEMS)
            	{
            		if (item.itemId.equals(ColList.get(0)))
            		{
            			AssignedItemContent.ITEM_MAP.get(item.id).dublicateBarcode = true;
            			AssignedItemContent.ITEM_MAP.get(item.id).finished = false;
            		}
            	}
            	message += " =Failed= Barcode: " + ColList.get(1) + " - Description: " + ColList.get(2) + System.getProperty("line.separator");
            }
        }
        return message;
    }
	
	public static void ReOrder(String order) {
		// Get new list for display in Selection list according selected order
		ITEMS.clear();
		
			if (order.equals("No Order Number")){
				order = "";
			}
			
			if (order.equals("")){
				order = "All Orders";
			}
			
			for (AssignedItem item : ITEMS_ALL)
			{
				if (order.equals("All Orders")){
					ITEMS.add(item);
				}
				else{
					if (item.orderNumber.equals(order))
					{
						ITEMS.add(item);
					}
				}	
			}
	}

	public static class AssignedItem {
		public String id;
		public String itemId;
		public String orderNumber;
		public String supplierCode;
		public String projectName;
		public String description;
		public String barcode;
		public String database;
		public boolean dublicateBarcode = false;
		public boolean finished = false;
		

		public AssignedItem(String id, String itemId, String orderNumber, String supplierCode, String projectName, String description, String barcode, String database) {
			this.id = id;
			this.itemId = itemId;
			this.orderNumber = orderNumber;
			this.supplierCode = supplierCode;
			this.projectName = projectName;
			this.description = description;
			this.barcode = barcode;
			this.database = database;
		}

		@Override
		public String toString() {
			// Text Displaying in Selection List
			return description; 
		}
	}
	
}
