using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Relocation : Form
    {
        string LoginName, LoginUser, LoginPassword, LoginDescription, ReturnName, ReturnUser, ReturnPassword, ReturnDescription;
        List<string> BarcodeList = new List<string>();
        List<Users> UsersList = new List<Users>();
        string message = "";
        string sDestination;
        string sSource;
        bool state = true;

        public Relocation(string sSource, string sDestination)
        {
            this.sDestination = sDestination;
            this.sSource = sSource;
            InitializeComponent();
        }

        private void Relocation_Load(object sender, EventArgs e)
        {
            string stitle = " Relocate: ";
            if (sSource == "Shelf") { stitle += "S - "; }
            if (sSource == "User") { stitle += "U - "; }
            if (sDestination == "Shelf") { stitle += "S"; }
            if (sDestination == "User") { stitle += "U"; }
            labelTitle.Text = stitle;

            panelUser.Hide();
            panelShelf.Hide();

            if (sSource == "User")
            {
                panelUser.Show();
                panelUserLogin.Show();
                panelUserRelocate.Hide();
            }
            else
            {
                ReturnName = "";
                textBoxBarcode.Focus();
            }

            PopulateUsers();
            UserscomboboxPopulate();
        }

        private void PopulateUsers()
        {
            Loader();
            MyLibrary.UsersPopulate(out state, out message, out UsersList);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void UserscomboboxPopulate()
        {
            comboBoxUsers.Items.Clear();
            foreach (Users user in UsersList)
            {
                comboBoxUsers.Items.Add(user.UserName);
            }
        }

        private void buttonUserLogin_Click(object sender, EventArgs e)
        {
            if (comboBoxUsers.SelectedIndex >= 0)
            {
                Users user = UsersList[comboBoxUsers.SelectedIndex];
                ReturnUser = user.UserID;
                ReturnName = user.UserName;
            }
            else
            {
                ReturnUser = "";
                ReturnName = "";
            }
            ReturnPassword = textBoxPassword.Text;
            ReturnDescription = textBoxDescription.Text;

            Loader();
            
            MyLibrary.LoginDetails(out state, out message, ReturnName, ReturnUser, ReturnPassword);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
                textBoxPassword.Text = "";
                textBoxDescription.Text = "";
                UserscomboboxPopulate();

                panelUser.Hide();
                textBoxBarcode.Focus();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;

        }

        private void textBoxBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (textBoxBarcode.Text != "")
                {
                    textBoxListBarcodes.Text = textBoxBarcode.Text + "\r\n" + textBoxListBarcodes.Text;
                    BarcodeList.Add(textBoxBarcode.Text);
                    textBoxBarcode.Text = "";
                }
            }
        }

        private void CloseForm()
        {
            if (BarcodeList.Count > 0)
            {
                labelMessage.Text = "Unrelocated Items in List";
                labelMessage.ForeColor = System.Drawing.Color.Red;
                return;
            }
            this.Close();
        }

        private void buttonItemsListClear_Click(object sender, EventArgs e)
        {
            BarcodeList.Clear();
            textBoxListBarcodes.Text = "";
            textBoxBarcode.Focus();
            labelMessage.Text = "";
        }

        private void buttonRelocate_Click(object sender, EventArgs e)
        {
            if (BarcodeList.Count == 0)
            {
                labelMessage.Text = "No Items to Relocate";
                labelMessage.ForeColor = System.Drawing.Color.Red;
                textBoxBarcode.Focus();
                return;
            }
            if (sDestination == "Shelf")
            {
                panelShelf.Show();
                textBoxShelf.Focus();
            }
            if (sDestination == "User")
            {
                panelUser.Show();
                panelUserLogin.Hide();
                panelUserRelocate.Show();
            }
            labelMessage.Text = "";
        }

        private void buttonLater_Shelf_Click(object sender, EventArgs e)
        {
            if (BarcodeList.Count == 0)
            {
                labelMessage.Text = "No Items to Relocate";
                labelMessage.ForeColor = System.Drawing.Color.Red;
                textBoxBarcode.Focus();
                return;
            }
            Loader();

            MyLibrary.RelocateLaterBarcode(out state, out message, BarcodeList, ReturnName, ReturnUser, ReturnPassword);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
                BarcodeList.Clear();
                textBoxListBarcodes.Text = "";
                labelMessage.Text = message;
                CloseForm();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
                labelMessage.Text = message;
            }
            
        }

        private void buttonRelocate_Shelf_Click(object sender, EventArgs e)
        {
            if (textBoxShelf.Text != "")
            {
                string Shelf = textBoxShelf.Text;
                Loader();

                MyLibrary.RelocateBarcode(out state, out message, BarcodeList, Shelf, ReturnName, ReturnUser, ReturnDescription);
                //DisableCheck();
                if (state)
                {
                    labelMessage.ForeColor = System.Drawing.Color.Green;
                    panelShelf.Hide();
                    BarcodeList.Clear();
                    textBoxShelf.Text = "";
                    textBoxListBarcodes.Text = "";
                }
                else
                {
                    labelMessage.ForeColor = System.Drawing.Color.Red;
                }
                labelMessage.Text = message;
            }
            else
            {
                labelMessage.Text = "Enter Shelf Barcode";
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }   
        }

        private void buttonShelfCancel_Click(object sender, EventArgs e)
        {
            panelShelf.Hide();
            textBoxShelf.Text = "";
            labelMessage.Text = "";
        }

        private void buttonGoUser_Click(object sender, EventArgs e)
        {
            if (comboBoxUsers.SelectedIndex >= 0)
            {
                Users user = UsersList[comboBoxUsers.SelectedIndex];
                LoginUser = user.UserID;
                LoginName = user.UserName;
            }
            else
            {
                LoginUser = "";
                LoginName = "";
            }
            LoginPassword = textBoxPassword.Text;
            LoginDescription = textBoxDescription.Text;

            Loader();

            MyLibrary.UserLoginRelocation(out state, out message, BarcodeList, LoginName, LoginUser, LoginPassword, LoginDescription, ReturnName, ReturnUser, ReturnPassword);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
                textBoxPassword.Text = "";
                textBoxDescription.Text = "";
                UserscomboboxPopulate();

                BarcodeList.Clear();
                textBoxListBarcodes.Text = "";

                panelUser.Hide();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void buttonUserCancel_Click(object sender, EventArgs e)
        {
            panelUser.Hide();
        }

        private void buttonUserClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Loader()
        {
            panelLoader.Show();
            panelLoader.Refresh();
            buttonClear.Hide();
            buttonClear.Refresh();
            buttonClose.Hide();
            buttonClose.Refresh();
            Application.DoEvents();
            panelLoader.Hide();
            buttonClear.Show();
            buttonClose.Show();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            if (BarcodeList.Count == 0)
            {
                CloseForm();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
                labelMessage.Text = "Items in List for Relocation";
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            panelUser.Hide();
            panelShelf.Hide();

            if (sSource == "User")
            {
                panelUser.Show();
                panelUserLogin.Show();
                panelUserRelocate.Hide();
            }
            else
            {
                ReturnName = "";
            }

            PopulateUsers();
            UserscomboboxPopulate();

            textBoxListBarcodes.Text = "";
            BarcodeList.Clear();
            textBoxBarcode.Text = "";
            textBoxDescription.Text = "";
            textBoxPassword.Text = "";
            textBoxShelf.Text = "";
        }

        private void Relocation_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Escape))
            {
                if (BarcodeList.Count == 0)
                {
                    CloseForm();
                }
                else
                {
                    labelMessage.ForeColor = System.Drawing.Color.Red;
                    labelMessage.Text = "Items in List for Relocation";
                }
            }

        }
    }
}