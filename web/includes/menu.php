<?php 
if (isset($_SESSION['ua'])) {
    $ua = $_SESSION['ua'];
}
else {
    $ua = 0;
} ?>

<?php if ($ua > 0) { ?>
<ul>
    <li><a href='index.php?pid=inventory_tracking'>Inventory tracking</a></li>
    <?php if ($ua > 1) { ?>
        <li class='subnav'><a href='index.php?pid=parts'>Parts</a>
            <ul>
                <li><a href='index.php?pid=products'>Products</a></li>
                <li><a href='index.php?pid=groups'>Groups</a></li>
           </ul>
        </li>
        <li class='subnav'><a href='index.php?pid=storerooms'>Storerooms</a>
            <ul>
                <li><a href='index.php?pid=shelves'>Shelves</a></li>
           </ul>
        </li>
        <li class='subnav'><a href='index.php?pid=companies'>Companies</a>
            <ul>
                <li><a href='index.php?pid=projects'>Projects</a></li>
           </ul>
        </li>
        <li><a href='index.php?pid=users'>Users</a></li>
    <?php } ?>
    <li><a href='index.php?pid=logs'>Logs</a></li> 
</ul>
<?php } ?>