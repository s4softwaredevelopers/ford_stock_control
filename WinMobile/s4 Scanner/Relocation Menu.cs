using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Relocation_Menu : Form
    {
        bool bSource = false;
        bool bDestination = false;
        string sSource = "";
        string sDestination = "";

        public Relocation_Menu()
        {
            InitializeComponent();
        }

        private void buttonS_Shelf_Click(object sender, EventArgs e)
        {
            bSource = true;
            sSource = "Shelf";
            buttonS_Shelf.BackColor = System.Drawing.Color.RoyalBlue;
            buttonS_User.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonS_Pending.BackColor = System.Drawing.Color.LightSteelBlue;
            panelSource.Hide();
            panelDestination.Show();
            panelDestination.Focus();
            RelocationPath();
        }

        private void buttonS_User_Click(object sender, EventArgs e)
        {
            bSource = true;
            sSource = "User";
            buttonS_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonS_User.BackColor = System.Drawing.Color.RoyalBlue;
            buttonS_Pending.BackColor = System.Drawing.Color.LightSteelBlue;
            panelSource.Hide();
            panelDestination.Show();
            panelDestination.Focus();
            RelocationPath();
        }

        private void buttonS_Pending_Click(object sender, EventArgs e)
        {
            bSource = true;
            sSource = "Pending";
            buttonS_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonS_User.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonS_Pending.BackColor = System.Drawing.Color.RoyalBlue;
            panelSource.Hide();
            panelDestination.Show();
            panelDestination.Focus();
            RelocationPath();
        }

        private void buttonD_Shelf_Click(object sender, EventArgs e)
        {
            bDestination = true;
            sDestination = "Shelf";
            buttonD_Shelf.BackColor = System.Drawing.Color.RoyalBlue;
            buttonD_User.BackColor = System.Drawing.Color.LightSteelBlue;
            panelSource.Show();
            panelSource.Focus();
            panelDestination.Hide();
            RelocationPath();
        }

        private void buttonD_User_Click(object sender, EventArgs e)
        {
            bDestination = true;
            sDestination = "User";
            buttonD_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonD_User.BackColor = System.Drawing.Color.RoyalBlue;
            panelSource.Show();
            panelSource.Focus();
            panelDestination.Hide();
            RelocationPath();
        }

        private void buttonChangeSource_Click(object sender, EventArgs e)
        {
            panelSource.Show();
            panelSource.Focus();
            panelDestination.Hide();
        }

        private void RelocationPath()
        {
            if ((bSource) && (bDestination))
            {
                panelLoader.Show();
                panelLoader.Refresh();
                buttonClear.Hide();
                buttonClear.Refresh();
                buttonClose.Hide();
                buttonClose.Refresh();
                Application.DoEvents();
                MyMenu.Relocate(sSource, sDestination);
                this.Close();
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            bSource = false;
            bDestination = false;
            sSource = "";
            sDestination = "";
            panelSource.Show();
            panelDestination.Hide();
            buttonS_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonS_User.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonS_Pending.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonD_Shelf.BackColor = System.Drawing.Color.LightSteelBlue;
            buttonD_User.BackColor = System.Drawing.Color.LightSteelBlue;
        }
    }
}