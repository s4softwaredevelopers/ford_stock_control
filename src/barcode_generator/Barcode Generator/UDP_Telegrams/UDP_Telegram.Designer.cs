﻿namespace UDP_Telegrams
{
    partial class UDP_Telegram
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxServerIPNumber = new System.Windows.Forms.TextBox();
            this.labelServerIPNumber = new System.Windows.Forms.Label();
            this.buttonStopServer = new System.Windows.Forms.Button();
            this.buttonStartServer = new System.Windows.Forms.Button();
            this.labelServerPortNumber = new System.Windows.Forms.Label();
            this.textBoxServerPortNumber = new System.Windows.Forms.TextBox();
            this.groupBoxListen = new System.Windows.Forms.GroupBox();
            this.textBoxServerReplyPort = new System.Windows.Forms.TextBox();
            this.labelServerReplyPort = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.richTextBoxListen = new System.Windows.Forms.RichTextBox();
            this.groupBoxTalker = new System.Windows.Forms.GroupBox();
            this.textBoxRemoteReplyPort = new System.Windows.Forms.TextBox();
            this.labelRemoteReplyPort = new System.Windows.Forms.Label();
            this.labelTalkerMessageStatus3 = new System.Windows.Forms.Label();
            this.labelTalkerMessageStatus2 = new System.Windows.Forms.Label();
            this.labelTalkerMessageStatus1 = new System.Windows.Forms.Label();
            this.buttonStartRemote = new System.Windows.Forms.Button();
            this.buttonStopRemote = new System.Windows.Forms.Button();
            this.labelTalkerMessage = new System.Windows.Forms.Label();
            this.textBoxRemotePortNumber = new System.Windows.Forms.TextBox();
            this.textBoxRemoteIPNumber = new System.Windows.Forms.TextBox();
            this.textBoxTalkerMessage = new System.Windows.Forms.TextBox();
            this.buttonRemoteSend = new System.Windows.Forms.Button();
            this.labelRemotePortNumber = new System.Windows.Forms.Label();
            this.labelRemoteIPNumber = new System.Windows.Forms.Label();
            this.groupBoxNotes = new System.Windows.Forms.GroupBox();
            this.richTextBoxNotes = new System.Windows.Forms.RichTextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.groupBoxListen.SuspendLayout();
            this.groupBoxTalker.SuspendLayout();
            this.groupBoxNotes.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxServerIPNumber
            // 
            this.textBoxServerIPNumber.Location = new System.Drawing.Point(99, 60);
            this.textBoxServerIPNumber.Name = "textBoxServerIPNumber";
            this.textBoxServerIPNumber.Size = new System.Drawing.Size(185, 20);
            this.textBoxServerIPNumber.TabIndex = 11;
            // 
            // labelServerIPNumber
            // 
            this.labelServerIPNumber.AutoSize = true;
            this.labelServerIPNumber.Location = new System.Drawing.Point(24, 63);
            this.labelServerIPNumber.Name = "labelServerIPNumber";
            this.labelServerIPNumber.Size = new System.Drawing.Size(58, 13);
            this.labelServerIPNumber.TabIndex = 10;
            this.labelServerIPNumber.Text = "IP Address";
            // 
            // buttonStopServer
            // 
            this.buttonStopServer.Location = new System.Drawing.Point(169, 26);
            this.buttonStopServer.Name = "buttonStopServer";
            this.buttonStopServer.Size = new System.Drawing.Size(100, 23);
            this.buttonStopServer.TabIndex = 9;
            this.buttonStopServer.Text = "Stop Server";
            this.buttonStopServer.UseVisualStyleBackColor = true;
            this.buttonStopServer.Click += new System.EventHandler(this.buttonStopListen_Click);
            // 
            // buttonStartServer
            // 
            this.buttonStartServer.Location = new System.Drawing.Point(27, 26);
            this.buttonStartServer.Name = "buttonStartServer";
            this.buttonStartServer.Size = new System.Drawing.Size(100, 23);
            this.buttonStartServer.TabIndex = 8;
            this.buttonStartServer.Text = "Start Server";
            this.buttonStartServer.UseVisualStyleBackColor = true;
            this.buttonStartServer.Click += new System.EventHandler(this.buttonStartListen_Click);
            // 
            // labelServerPortNumber
            // 
            this.labelServerPortNumber.AutoSize = true;
            this.labelServerPortNumber.Location = new System.Drawing.Point(24, 85);
            this.labelServerPortNumber.Name = "labelServerPortNumber";
            this.labelServerPortNumber.Size = new System.Drawing.Size(66, 13);
            this.labelServerPortNumber.TabIndex = 12;
            this.labelServerPortNumber.Text = "Port Number";
            // 
            // textBoxServerPortNumber
            // 
            this.textBoxServerPortNumber.Location = new System.Drawing.Point(99, 82);
            this.textBoxServerPortNumber.Name = "textBoxServerPortNumber";
            this.textBoxServerPortNumber.Size = new System.Drawing.Size(185, 20);
            this.textBoxServerPortNumber.TabIndex = 13;
            // 
            // groupBoxListen
            // 
            this.groupBoxListen.Controls.Add(this.textBoxServerReplyPort);
            this.groupBoxListen.Controls.Add(this.labelServerReplyPort);
            this.groupBoxListen.Controls.Add(this.labelStatus);
            this.groupBoxListen.Controls.Add(this.richTextBoxListen);
            this.groupBoxListen.Controls.Add(this.buttonStartServer);
            this.groupBoxListen.Controls.Add(this.textBoxServerPortNumber);
            this.groupBoxListen.Controls.Add(this.buttonStopServer);
            this.groupBoxListen.Controls.Add(this.labelServerPortNumber);
            this.groupBoxListen.Controls.Add(this.labelServerIPNumber);
            this.groupBoxListen.Controls.Add(this.textBoxServerIPNumber);
            this.groupBoxListen.Location = new System.Drawing.Point(12, 12);
            this.groupBoxListen.Name = "groupBoxListen";
            this.groupBoxListen.Size = new System.Drawing.Size(300, 425);
            this.groupBoxListen.TabIndex = 14;
            this.groupBoxListen.TabStop = false;
            this.groupBoxListen.Text = "Server";
            // 
            // textBoxServerReplyPort
            // 
            this.textBoxServerReplyPort.Location = new System.Drawing.Point(99, 104);
            this.textBoxServerReplyPort.Name = "textBoxServerReplyPort";
            this.textBoxServerReplyPort.Size = new System.Drawing.Size(185, 20);
            this.textBoxServerReplyPort.TabIndex = 25;
            // 
            // labelServerReplyPort
            // 
            this.labelServerReplyPort.AutoSize = true;
            this.labelServerReplyPort.Location = new System.Drawing.Point(24, 107);
            this.labelServerReplyPort.Name = "labelServerReplyPort";
            this.labelServerReplyPort.Size = new System.Drawing.Size(56, 13);
            this.labelServerReplyPort.TabIndex = 24;
            this.labelServerReplyPort.Text = "Reply Port";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(24, 133);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(37, 13);
            this.labelStatus.TabIndex = 16;
            this.labelStatus.Text = "Status";
            this.labelStatus.Click += new System.EventHandler(this.labelStatus_Click);
            // 
            // richTextBoxListen
            // 
            this.richTextBoxListen.Location = new System.Drawing.Point(6, 149);
            this.richTextBoxListen.Name = "richTextBoxListen";
            this.richTextBoxListen.Size = new System.Drawing.Size(288, 270);
            this.richTextBoxListen.TabIndex = 15;
            this.richTextBoxListen.Text = "";
            // 
            // groupBoxTalker
            // 
            this.groupBoxTalker.Controls.Add(this.textBoxRemoteReplyPort);
            this.groupBoxTalker.Controls.Add(this.labelRemoteReplyPort);
            this.groupBoxTalker.Controls.Add(this.labelTalkerMessageStatus3);
            this.groupBoxTalker.Controls.Add(this.labelTalkerMessageStatus2);
            this.groupBoxTalker.Controls.Add(this.labelTalkerMessageStatus1);
            this.groupBoxTalker.Controls.Add(this.buttonStartRemote);
            this.groupBoxTalker.Controls.Add(this.buttonStopRemote);
            this.groupBoxTalker.Controls.Add(this.labelTalkerMessage);
            this.groupBoxTalker.Controls.Add(this.textBoxRemotePortNumber);
            this.groupBoxTalker.Controls.Add(this.textBoxRemoteIPNumber);
            this.groupBoxTalker.Controls.Add(this.textBoxTalkerMessage);
            this.groupBoxTalker.Controls.Add(this.buttonRemoteSend);
            this.groupBoxTalker.Controls.Add(this.labelRemotePortNumber);
            this.groupBoxTalker.Controls.Add(this.labelRemoteIPNumber);
            this.groupBoxTalker.Location = new System.Drawing.Point(325, 12);
            this.groupBoxTalker.Name = "groupBoxTalker";
            this.groupBoxTalker.Size = new System.Drawing.Size(300, 259);
            this.groupBoxTalker.TabIndex = 15;
            this.groupBoxTalker.TabStop = false;
            this.groupBoxTalker.Text = "Remote";
            this.groupBoxTalker.Enter += new System.EventHandler(this.groupBoxTalker_Enter);
            // 
            // textBoxRemoteReplyPort
            // 
            this.textBoxRemoteReplyPort.Location = new System.Drawing.Point(96, 104);
            this.textBoxRemoteReplyPort.Name = "textBoxRemoteReplyPort";
            this.textBoxRemoteReplyPort.Size = new System.Drawing.Size(185, 20);
            this.textBoxRemoteReplyPort.TabIndex = 24;
            // 
            // labelRemoteReplyPort
            // 
            this.labelRemoteReplyPort.AutoSize = true;
            this.labelRemoteReplyPort.Location = new System.Drawing.Point(19, 107);
            this.labelRemoteReplyPort.Name = "labelRemoteReplyPort";
            this.labelRemoteReplyPort.Size = new System.Drawing.Size(56, 13);
            this.labelRemoteReplyPort.TabIndex = 23;
            this.labelRemoteReplyPort.Text = "Reply Port";
            // 
            // labelTalkerMessageStatus3
            // 
            this.labelTalkerMessageStatus3.AutoSize = true;
            this.labelTalkerMessageStatus3.Location = new System.Drawing.Point(19, 233);
            this.labelTalkerMessageStatus3.Name = "labelTalkerMessageStatus3";
            this.labelTalkerMessageStatus3.Size = new System.Drawing.Size(37, 13);
            this.labelTalkerMessageStatus3.TabIndex = 22;
            this.labelTalkerMessageStatus3.Text = "Status";
            // 
            // labelTalkerMessageStatus2
            // 
            this.labelTalkerMessageStatus2.AutoSize = true;
            this.labelTalkerMessageStatus2.Location = new System.Drawing.Point(19, 220);
            this.labelTalkerMessageStatus2.Name = "labelTalkerMessageStatus2";
            this.labelTalkerMessageStatus2.Size = new System.Drawing.Size(37, 13);
            this.labelTalkerMessageStatus2.TabIndex = 21;
            this.labelTalkerMessageStatus2.Text = "Status";
            // 
            // labelTalkerMessageStatus1
            // 
            this.labelTalkerMessageStatus1.AutoSize = true;
            this.labelTalkerMessageStatus1.Location = new System.Drawing.Point(19, 207);
            this.labelTalkerMessageStatus1.Name = "labelTalkerMessageStatus1";
            this.labelTalkerMessageStatus1.Size = new System.Drawing.Size(37, 13);
            this.labelTalkerMessageStatus1.TabIndex = 20;
            this.labelTalkerMessageStatus1.Text = "Status";
            // 
            // buttonStartRemote
            // 
            this.buttonStartRemote.Location = new System.Drawing.Point(31, 26);
            this.buttonStartRemote.Name = "buttonStartRemote";
            this.buttonStartRemote.Size = new System.Drawing.Size(100, 23);
            this.buttonStartRemote.TabIndex = 18;
            this.buttonStartRemote.Text = "Start Remote";
            this.buttonStartRemote.UseVisualStyleBackColor = true;
            this.buttonStartRemote.Click += new System.EventHandler(this.buttonStartTalking_Click);
            // 
            // buttonStopRemote
            // 
            this.buttonStopRemote.Location = new System.Drawing.Point(171, 26);
            this.buttonStopRemote.Name = "buttonStopRemote";
            this.buttonStopRemote.Size = new System.Drawing.Size(100, 23);
            this.buttonStopRemote.TabIndex = 19;
            this.buttonStopRemote.Text = "Stop Remote";
            this.buttonStopRemote.UseVisualStyleBackColor = true;
            this.buttonStopRemote.Click += new System.EventHandler(this.buttonStopTalking_Click);
            // 
            // labelTalkerMessage
            // 
            this.labelTalkerMessage.AutoSize = true;
            this.labelTalkerMessage.Location = new System.Drawing.Point(109, 133);
            this.labelTalkerMessage.Name = "labelTalkerMessage";
            this.labelTalkerMessage.Size = new System.Drawing.Size(50, 13);
            this.labelTalkerMessage.TabIndex = 17;
            this.labelTalkerMessage.Text = "Message";
            // 
            // textBoxRemotePortNumber
            // 
            this.textBoxRemotePortNumber.Location = new System.Drawing.Point(96, 82);
            this.textBoxRemotePortNumber.Name = "textBoxRemotePortNumber";
            this.textBoxRemotePortNumber.Size = new System.Drawing.Size(185, 20);
            this.textBoxRemotePortNumber.TabIndex = 16;
            // 
            // textBoxRemoteIPNumber
            // 
            this.textBoxRemoteIPNumber.Location = new System.Drawing.Point(96, 60);
            this.textBoxRemoteIPNumber.Name = "textBoxRemoteIPNumber";
            this.textBoxRemoteIPNumber.Size = new System.Drawing.Size(185, 20);
            this.textBoxRemoteIPNumber.TabIndex = 15;
            // 
            // textBoxTalkerMessage
            // 
            this.textBoxTalkerMessage.Location = new System.Drawing.Point(6, 149);
            this.textBoxTalkerMessage.Name = "textBoxTalkerMessage";
            this.textBoxTalkerMessage.Size = new System.Drawing.Size(288, 20);
            this.textBoxTalkerMessage.TabIndex = 14;
            // 
            // buttonRemoteSend
            // 
            this.buttonRemoteSend.Location = new System.Drawing.Point(96, 175);
            this.buttonRemoteSend.Name = "buttonRemoteSend";
            this.buttonRemoteSend.Size = new System.Drawing.Size(75, 23);
            this.buttonRemoteSend.TabIndex = 8;
            this.buttonRemoteSend.Text = "Send Message";
            this.buttonRemoteSend.UseVisualStyleBackColor = true;
            this.buttonRemoteSend.Click += new System.EventHandler(this.buttonTalkerSend_Click);
            // 
            // labelRemotePortNumber
            // 
            this.labelRemotePortNumber.AutoSize = true;
            this.labelRemotePortNumber.Location = new System.Drawing.Point(19, 85);
            this.labelRemotePortNumber.Name = "labelRemotePortNumber";
            this.labelRemotePortNumber.Size = new System.Drawing.Size(66, 13);
            this.labelRemotePortNumber.TabIndex = 12;
            this.labelRemotePortNumber.Text = "Port Number";
            // 
            // labelRemoteIPNumber
            // 
            this.labelRemoteIPNumber.AutoSize = true;
            this.labelRemoteIPNumber.Location = new System.Drawing.Point(19, 63);
            this.labelRemoteIPNumber.Name = "labelRemoteIPNumber";
            this.labelRemoteIPNumber.Size = new System.Drawing.Size(58, 13);
            this.labelRemoteIPNumber.TabIndex = 10;
            this.labelRemoteIPNumber.Text = "IP Address";
            // 
            // groupBoxNotes
            // 
            this.groupBoxNotes.Controls.Add(this.richTextBoxNotes);
            this.groupBoxNotes.Location = new System.Drawing.Point(325, 321);
            this.groupBoxNotes.Name = "groupBoxNotes";
            this.groupBoxNotes.Size = new System.Drawing.Size(300, 116);
            this.groupBoxNotes.TabIndex = 16;
            this.groupBoxNotes.TabStop = false;
            this.groupBoxNotes.Text = "Notes";
            // 
            // richTextBoxNotes
            // 
            this.richTextBoxNotes.Location = new System.Drawing.Point(6, 19);
            this.richTextBoxNotes.Name = "richTextBoxNotes";
            this.richTextBoxNotes.Size = new System.Drawing.Size(288, 91);
            this.richTextBoxNotes.TabIndex = 0;
            this.richTextBoxNotes.Text = "Listener: Default IP Address - Listen for all IP\'s\nTalker: IP Address - Needs rec" +
    "eivers IP Address\t\nDefault Port Number - 11000\nDefault Reply Port Number - 11001" +
    "";
            this.richTextBoxNotes.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(335, 277);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 0;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // UDP_Telegram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 446);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.groupBoxNotes);
            this.Controls.Add(this.groupBoxTalker);
            this.Controls.Add(this.groupBoxListen);
            this.Name = "UDP_Telegram";
            this.Text = "UDP Telegrams";
            this.Load += new System.EventHandler(this.UDP_Telegram_Load);
            this.groupBoxListen.ResumeLayout(false);
            this.groupBoxListen.PerformLayout();
            this.groupBoxTalker.ResumeLayout(false);
            this.groupBoxTalker.PerformLayout();
            this.groupBoxNotes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxServerIPNumber;
        private System.Windows.Forms.Label labelServerIPNumber;
        private System.Windows.Forms.Button buttonStopServer;
        private System.Windows.Forms.Button buttonStartServer;
        private System.Windows.Forms.Label labelServerPortNumber;
        private System.Windows.Forms.TextBox textBoxServerPortNumber;
        private System.Windows.Forms.GroupBox groupBoxListen;
        private System.Windows.Forms.RichTextBox richTextBoxListen;
        private System.Windows.Forms.GroupBox groupBoxTalker;
        private System.Windows.Forms.Label labelTalkerMessage;
        private System.Windows.Forms.TextBox textBoxRemotePortNumber;
        private System.Windows.Forms.TextBox textBoxRemoteIPNumber;
        private System.Windows.Forms.TextBox textBoxTalkerMessage;
        private System.Windows.Forms.Button buttonRemoteSend;
        private System.Windows.Forms.Label labelRemotePortNumber;
        private System.Windows.Forms.Label labelRemoteIPNumber;
        private System.Windows.Forms.GroupBox groupBoxNotes;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.RichTextBox richTextBoxNotes;
        private System.Windows.Forms.Button buttonStartRemote;
        private System.Windows.Forms.Button buttonStopRemote;
        private System.Windows.Forms.Label labelTalkerMessageStatus1;
        private System.Windows.Forms.Label labelTalkerMessageStatus2;
        private System.Windows.Forms.Label labelTalkerMessageStatus3;
        private System.Windows.Forms.TextBox textBoxRemoteReplyPort;
        private System.Windows.Forms.Label labelRemoteReplyPort;
        private System.Windows.Forms.TextBox textBoxServerReplyPort;
        private System.Windows.Forms.Label labelServerReplyPort;
    }
}

