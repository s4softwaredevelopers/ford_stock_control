using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Relocate_Pending : Form
    {
        List<Users> UsersList = new List<Users>();
        List<ScanItem> ItemsList = new List<ScanItem>();
        List<int> OrderList = new List<int>();
        List<string> OrderNumberList = new List<string>();
        string LoginName, LoginUser, LoginPassword, LoginDescription;
        string message;
        string sDestination;
        int itemcount = -1;
        bool state = true;

        public Relocate_Pending(string sDestination)
        {
            this.sDestination = sDestination;
            InitializeComponent();
        }

        private void NextItem()// Next Item in list 
        {
            if (state == false) { return; }
            if (itemcount == -1) { return; }

            labelMessage.Text = "";
            DetailsUpdate();

            if (itemcount < (OrderList.Count - 1)) { itemcount++; }
            else { itemcount = 0; }
            ItemDisplay(OrderList[itemcount]);
        }

        private void PreviousItem() // Previous Item in list
        {
            if (state == false) { return; }
            if (itemcount == -1) { return; }

            labelMessage.Text = "";
            DetailsUpdate();

            if (itemcount > 0) { itemcount--; }
            else { itemcount = OrderList.Count - 1; }
            ItemDisplay(OrderList[itemcount]);
        }

        private void DetailsUpdate()
        {
            if (itemcount != -1)
            {
                if (ItemsList[OrderList[itemcount]].UserSelected != checkBoxSelected.Checked)
                {
                    ItemsList[OrderList[itemcount]].UserSelected = checkBoxSelected.Checked;
                }
            }
        }

        private void ItemDisplay(int itemnum) // Current Item Selected
        {
            labelItemNumber.Text = (itemcount + 1) + " / " + OrderList.Count;

            if (itemcount != -1)
            {
                //labelOrder.Text = ItemsList[itemnum].OrderID;
                labelProjData.Text = "                 " + ItemsList[itemnum].ProjectName;
                labelSupplierData.Text = ItemsList[itemnum].SupplierCode;
                labelDescriptionText.Text = "                 " + ItemsList[itemnum].Description;
                //labelDatabase.Text = ItemsList[itemnum].Database;
                labelBarcodeData.Text = ItemsList[itemnum].Barcode;
                PreviousLocationDisplay(itemnum);
                //textBoxShelf.Text = ItemsList[itemnum].Shelf;
                checkBoxSelected.Checked = ItemsList[itemnum].UserSelected;
            }
        }

        private void PreviousLocationDisplay(int itemnum)
        {
            if (ItemsList[itemnum].PreviousShelf == "")
            {
                labelOldShelfDesc.Text = "UNKNOWN";
                return;
            }
            for (int count = 0; count < UsersList.Count; count++)
            {
                if (ItemsList[itemnum].PreviousShelf == UsersList[count].UserName)
                {
                    labelOldShelfDesc.Text = ItemsList[itemnum].PreviousShelf;
                    return;
                }
            }
            labelOldShelfDesc.Text = ItemsList[itemnum].PreviousShelfDescription;
            if (ItemsList[itemnum].PreviousShelfDescription == "Auto")
            {
                labelOldShelfDesc.Text = ItemsList[itemnum].PreviousShelf + " (" + ItemsList[itemnum].PreviousShelfDescription + ")";
            }
        }

        private void DisableCheck()
        {
            if (state)
            {
                panel_O_Order_D.Enabled = true;
                button_O_Selected.Enabled = true;

                panel_D_Search.Enabled = true;
                panel_D_Marked.Enabled = true;
            }
            else
            {
                panel_O_Order_D.Enabled = false;
                button_O_Selected.Enabled = false;

                panel_D_Search.Enabled = false;
                panel_D_Marked.Enabled = false;
            }
        }

        private void GetData()
        {
            Loader();

            MyLibrary.GetPendingDataFromServer(out state, out message, out ItemsList);
            DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void PopulateUsers()
        {
            Loader();
            MyLibrary.UsersPopulate(out state, out message, out UsersList);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void UserscomboboxPopulate()
        {
            comboBoxUsers.Items.Clear();
            foreach (Users user in UsersList)
            {
                comboBoxUsers.Items.Add(user.UserName);
            }
        }

        private void UserSendData()
        {
            if (comboBoxUsers.SelectedIndex >= 0)
            {
                Users user = UsersList[comboBoxUsers.SelectedIndex];
                LoginUser = user.UserID;
                LoginName = user.UserName;
            }
            else
            {
                LoginUser = "";
                LoginName = "";
            }
            LoginPassword = textBoxPassword.Text;
            LoginDescription = textBoxDescription.Text;

            Loader();

            MyLibrary.PendingUserLoginRelocation(out state, out message, ItemsList, LoginName, LoginUser, LoginPassword, LoginDescription);
            DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
                textBoxPassword.Text = "";
                textBoxDescription.Text = "";
                UserscomboboxPopulate();

                panelUser.Hide();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void ClearData()
        {
            ItemsList.Clear();
            OrderList.Clear();
            OrderNumberList.Clear();
            comboBoxOrder.Items.Clear();
            itemcount = -1;
            state = true;
            textBoxSearchAuto.Text = "";

            panelUser.Hide();
            panelShelf.Hide();
            panelOptions.Show();

            textBoxPassword.Text = "";
            textBoxDescription.Text = "";

            labelDescriptionText.Text = "";
            labelDatabase.Text = "";
            labelMessage.Text = "";
            labelSupplierData.Text = "";
            labelProjData.Text = "";

            textBoxShelf.Text = "";
        }

        private void Relocate_P_User_Load(object sender, EventArgs e)
        {
            GetData();
            DisableCheck();
            if (state)
            {
                PopulateOrderNumbers();
                PopulateUsers();
                UserscomboboxPopulate();
            }

            if (sDestination == "Shelf") 
            { 
                labelTitle.Text = " Relocate to Shelf";
                buttonRelocate.Text = "Relocate to Shelf";
            }
            if (sDestination == "User") 
            { 
                labelTitle.Text = " Relocate to User";
                buttonRelocate.Text = "Relocate to User";
            }

            panelUser.Hide();
            panelShelf.Hide();
        }

        private void OrderSort(string Order) // Sort Order
        {
            OrderList.Clear();
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if ((ItemsList[count].OrderId == Order) || (Order == "All Orders"))
                {
                    OrderList.Add(count);
                }
            }
            if (OrderList.Count > 0) { itemcount = 0; } else { itemcount = -1; }
        }

        private void PopulateOrderNumbers() // Populate Orders 
        {
            OrderNumberList.Clear();
            List<string> temp = new List<string>();
            bool bfound;

            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].OrderId == "") { ItemsList[count].OrderId = "No Order"; }
                temp.Add(ItemsList[count].OrderId);
            }

            for (int count1 = 0; count1 < temp.Count; count1++)
            {
                bfound = false;
                for (int count2 = 0; count2 < OrderNumberList.Count; count2++)
                {
                    if (temp[count1] == OrderNumberList[count2]) { bfound = true; }
                }
                if (bfound == false) { OrderNumberList.Add(temp[count1]); }

            }

            comboBoxOrder.Items.Clear();
            comboBoxOrder.Items.Add("All Orders");
            foreach (string element in OrderNumberList)
            {
                comboBoxOrder.Items.Add(element);
            }
            if (comboBoxOrder.Items.Contains("No Order"))
            {
                comboBoxOrder.Items.Remove("No Order");
                comboBoxOrder.Items.Add("No Order");
            }
            comboBoxOrder.Text = "All Orders";
            comboBoxOrder.Focus();
        }




        private void button_O_Selected_Click(object sender, EventArgs e)
        {
            OrderSort(comboBoxOrder.Text);

            panelOptions.Hide();
            panelBase.Show();
            textBoxSearchAuto.Focus();
            ItemDisplay(OrderList[itemcount]);
        }

        private void checkBoxSelected_CheckStateChanged(object sender, EventArgs e)
        {
            if (itemcount != -1)
            {
                ItemsList[OrderList[itemcount]].UserSelected = checkBoxSelected.Checked;
            }
        }

        private void textBoxSearchAuto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                BarcodeSearch(textBoxSearchAuto.Text);
            }
        }

        private int ItemSearch(string barcode)
        {
            for (int count = 0; count < OrderList.Count; count++)
            {
                if (ItemsList[OrderList[count]].Barcode == barcode)
                {
                    return count;
                }
            }
            return -1;
        }

        private void BarcodeSearch(string barcode) // Search for barcode
        {
            DetailsUpdate();
            int oldItemNum = itemcount;
            itemcount = ItemSearch(barcode);
            if (itemcount >= 0)
            {
                ItemDisplay(OrderList[itemcount]);
                panelInfo.Show();
                panel_D_Marked.Enabled = true;

                ItemsList[OrderList[itemcount]].UserSelected = true;
                checkBoxSelected.Checked = true;

                labelMessage.Text = "";
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
                labelMessage.Text = "Barcode Not in List";
                itemcount = oldItemNum;
                ItemDisplay(OrderList[itemcount]);

            }
            textBoxSearchAuto.Text = "";
            textBoxSearchAuto.Focus();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            if (ItemsForRelocation())
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
                labelMessage.Text = "Marked Items in List";
            }
            else
            {
                this.Close();
            }

        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            DetailsUpdate();
            UserSendData();
            DisableCheck();
            if (state)
            {
                ClearData();
                panelOptions.Show();
                GetData();
                DisableCheck();
                if (state)
                {
                    PopulateOrderNumbers();
                    PopulateUsers();
                    UserscomboboxPopulate();
                }
            }
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            ClearData();
            panelOptions.Show();
            panelBase.Hide();
            GetData();
            DisableCheck();
            if (state)
            {
                PopulateOrderNumbers();
                PopulateUsers();
                UserscomboboxPopulate();
            }
        }

        private void buttonRelocate_Click(object sender, EventArgs e)
        {
            DetailsUpdate();
            if (ItemsForRelocation())
            {
                if (sDestination == "User")
                {
                    UserscomboboxPopulate();
                    textBoxPassword.Text = "";
                    textBoxDescription.Text = "";
                    panelUser.Show();
                }
                if (sDestination == "Shelf")
                {
                    panelShelf.Show();
                    textBoxShelf.Text = "";
                    textBoxShelf.Focus();
                }
                labelMessage.Text = "";
                textBoxSearchAuto.Text = "";
                panelBase.Hide();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
                labelMessage.Text = "Zero items for Relocation";
            }
        }

        private bool ItemsForRelocation()
        {
            for (int count = 0; count < ItemsList.Count; count++)
            {
                if (ItemsList[count].UserSelected)
                {
                    return true;
                }
            }
            return false;
        }

        private void buttonUserCancel_Click(object sender, EventArgs e)
        {
            panelUser.Hide();
            panelBase.Show();
            labelMessage.Text = "";
        }

        private void Loader()
        {
            panelLoader.Show();
            panelLoader.Refresh();
            buttonClear.Hide();
            buttonClear.Refresh();
            buttonClose.Hide();
            buttonClose.Refresh();
            Application.DoEvents();
            panelLoader.Hide();
            buttonClear.Show();
            buttonClose.Show();
        }

        private void Relocate_P_User_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == System.Windows.Forms.Keys.Left) && (panelBase.Visible))
            {
                PreviousItem();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Right) && (panelBase.Visible)) 
            {
                NextItem();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Up) && (panelBase.Visible))
            {
                textBoxSearchAuto.Focus();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Down) && (panelBase.Visible))
            {
                checkBoxSelected.Focus();
            }
            if ((e.KeyCode == System.Windows.Forms.Keys.Escape))
            {
                if (ItemsForRelocation())
                {
                    labelMessage.ForeColor = System.Drawing.Color.Red;
                    labelMessage.Text = "Marked Items in List";
                }
                else
                {
                    this.Close();
                }
            }

        }

        private void buttonShelfCancel_Click(object sender, EventArgs e)
        {
            panelShelf.Hide();
            panelBase.Show();
            labelMessage.Text = "";
        }

        private void buttonRelocate_Shelf_Click(object sender, EventArgs e)
        {
            if (textBoxShelf.Text != "")
            {
                Loader();

                MyLibrary.RelocatePendingBarcode(out state, out message, ItemsList, textBoxShelf.Text);

                DisableCheck();
                if (state)
                {
                    ClearData();
                    GetData();
                    panelOptions.Show();
                    DisableCheck();
                    if (state)
                    {
                        PopulateOrderNumbers();
                        PopulateUsers();
                        UserscomboboxPopulate();
                    }
                }
                else
                {
                    labelMessage.ForeColor = System.Drawing.Color.Red;
                }
                labelMessage.Text = message;
            }
            else
            {
                labelMessage.Text = "Enter Shelf Barcode";
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
        }

    }
}