namespace s4_Scanner
{
    partial class Marry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelOptions = new System.Windows.Forms.Panel();
            this.panel_O_Button = new System.Windows.Forms.Panel();
            this.button_O_Selected = new System.Windows.Forms.Button();
            this.panel_O_Order_D = new System.Windows.Forms.Panel();
            this.labelExtraOrders = new System.Windows.Forms.Label();
            this.comboBoxOrder = new System.Windows.Forms.ComboBox();
            this.panelFooter = new System.Windows.Forms.Panel();
            this.labelMessage = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelBase = new System.Windows.Forms.Panel();
            this.panelBarcodes = new System.Windows.Forms.Panel();
            this.panel_D_Barcode = new System.Windows.Forms.Panel();
            this.textBoxBarcode = new System.Windows.Forms.TextBox();
            this.labelBarcode = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.panelLine2 = new System.Windows.Forms.Panel();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelProj = new System.Windows.Forms.Label();
            this.labelProjData = new System.Windows.Forms.Label();
            this.labelSupplierData = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelDescriptionText = new System.Windows.Forms.Label();
            this.labelSupplier = new System.Windows.Forms.Label();
            this.labelItemNumber = new System.Windows.Forms.Label();
            this.labelDatabase = new System.Windows.Forms.Label();
            this.panelResults = new System.Windows.Forms.Panel();
            this.buttonReslutsClose = new System.Windows.Forms.Button();
            this.labelResultsTitle = new System.Windows.Forms.Label();
            this.textBoxResultsList = new System.Windows.Forms.TextBox();
            this.panelOptions.SuspendLayout();
            this.panel_O_Button.SuspendLayout();
            this.panel_O_Order_D.SuspendLayout();
            this.panelFooter.SuspendLayout();
            this.panelHeader.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.panelBase.SuspendLayout();
            this.panelBarcodes.SuspendLayout();
            this.panel_D_Barcode.SuspendLayout();
            this.panelInfo.SuspendLayout();
            this.panelResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelOptions
            // 
            this.panelOptions.Controls.Add(this.panel_O_Button);
            this.panelOptions.Controls.Add(this.panel_O_Order_D);
            this.panelOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelOptions.Location = new System.Drawing.Point(0, 20);
            this.panelOptions.Name = "panelOptions";
            this.panelOptions.Size = new System.Drawing.Size(240, 280);
            // 
            // panel_O_Button
            // 
            this.panel_O_Button.Controls.Add(this.button_O_Selected);
            this.panel_O_Button.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_O_Button.Location = new System.Drawing.Point(0, 246);
            this.panel_O_Button.Name = "panel_O_Button";
            this.panel_O_Button.Size = new System.Drawing.Size(240, 34);
            // 
            // button_O_Selected
            // 
            this.button_O_Selected.BackColor = System.Drawing.Color.LightGray;
            this.button_O_Selected.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.button_O_Selected.Location = new System.Drawing.Point(5, 2);
            this.button_O_Selected.Name = "button_O_Selected";
            this.button_O_Selected.Size = new System.Drawing.Size(230, 30);
            this.button_O_Selected.TabIndex = 73;
            this.button_O_Selected.Text = "Assign Barcodes";
            this.button_O_Selected.Click += new System.EventHandler(this.button_O_Selected_Click);
            // 
            // panel_O_Order_D
            // 
            this.panel_O_Order_D.Controls.Add(this.labelExtraOrders);
            this.panel_O_Order_D.Controls.Add(this.comboBoxOrder);
            this.panel_O_Order_D.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_O_Order_D.Location = new System.Drawing.Point(0, 0);
            this.panel_O_Order_D.Name = "panel_O_Order_D";
            this.panel_O_Order_D.Size = new System.Drawing.Size(240, 190);
            // 
            // labelExtraOrders
            // 
            this.labelExtraOrders.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelExtraOrders.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelExtraOrders.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelExtraOrders.Location = new System.Drawing.Point(20, 10);
            this.labelExtraOrders.Name = "labelExtraOrders";
            this.labelExtraOrders.Size = new System.Drawing.Size(200, 36);
            this.labelExtraOrders.Text = "Select Order Number to view all items of the selected Order";
            // 
            // comboBoxOrder
            // 
            this.comboBoxOrder.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.comboBoxOrder.Location = new System.Drawing.Point(20, 54);
            this.comboBoxOrder.Name = "comboBoxOrder";
            this.comboBoxOrder.Size = new System.Drawing.Size(200, 24);
            this.comboBoxOrder.TabIndex = 87;
            // 
            // panelFooter
            // 
            this.panelFooter.Controls.Add(this.labelMessage);
            this.panelFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelFooter.Location = new System.Drawing.Point(0, 300);
            this.panelFooter.Name = "panelFooter";
            this.panelFooter.Size = new System.Drawing.Size(240, 20);
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(5, 0);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(230, 20);
            this.labelMessage.Text = "Message";
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.buttonClear);
            this.panelHeader.Controls.Add(this.buttonClose);
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.BackColor = System.Drawing.Color.LightGray;
            this.buttonClear.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClear.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClear.Location = new System.Drawing.Point(161, 0);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(30, 20);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "R";
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(211, 0);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(30, 20);
            this.buttonClose.TabIndex = 10;
            this.buttonClose.Text = " x";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Assign Barcodes";
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label2);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 280);
            this.panelLoader.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(29, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 107);
            this.label2.Text = "Loading";
            // 
            // panelBase
            // 
            this.panelBase.Controls.Add(this.panelBarcodes);
            this.panelBase.Controls.Add(this.panelInfo);
            this.panelBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelBase.Location = new System.Drawing.Point(0, 20);
            this.panelBase.Name = "panelBase";
            this.panelBase.Size = new System.Drawing.Size(240, 280);
            this.panelBase.Visible = false;
            // 
            // panelBarcodes
            // 
            this.panelBarcodes.BackColor = System.Drawing.Color.White;
            this.panelBarcodes.Controls.Add(this.panel_D_Barcode);
            this.panelBarcodes.Controls.Add(this.buttonUpdate);
            this.panelBarcodes.Controls.Add(this.panelLine2);
            this.panelBarcodes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBarcodes.Location = new System.Drawing.Point(0, 120);
            this.panelBarcodes.Name = "panelBarcodes";
            this.panelBarcodes.Size = new System.Drawing.Size(240, 160);
            // 
            // panel_D_Barcode
            // 
            this.panel_D_Barcode.Controls.Add(this.textBoxBarcode);
            this.panel_D_Barcode.Controls.Add(this.labelBarcode);
            this.panel_D_Barcode.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_D_Barcode.Location = new System.Drawing.Point(0, 0);
            this.panel_D_Barcode.Name = "panel_D_Barcode";
            this.panel_D_Barcode.Size = new System.Drawing.Size(240, 119);
            // 
            // textBoxBarcode
            // 
            this.textBoxBarcode.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.textBoxBarcode.Location = new System.Drawing.Point(5, 44);
            this.textBoxBarcode.Name = "textBoxBarcode";
            this.textBoxBarcode.Size = new System.Drawing.Size(230, 29);
            this.textBoxBarcode.TabIndex = 2;
            this.textBoxBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxBarcode_KeyPress);
            // 
            // labelBarcode
            // 
            this.labelBarcode.BackColor = System.Drawing.SystemColors.Highlight;
            this.labelBarcode.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelBarcode.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelBarcode.Location = new System.Drawing.Point(5, 24);
            this.labelBarcode.Name = "labelBarcode";
            this.labelBarcode.Size = new System.Drawing.Size(230, 20);
            this.labelBarcode.Text = "ENTER BARCODE OF ITEM";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.BackColor = System.Drawing.Color.LightGray;
            this.buttonUpdate.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.buttonUpdate.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonUpdate.Location = new System.Drawing.Point(5, 126);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(230, 30);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Update!";
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // panelLine2
            // 
            this.panelLine2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panelLine2.Location = new System.Drawing.Point(0, 121);
            this.panelLine2.Name = "panelLine2";
            this.panelLine2.Size = new System.Drawing.Size(240, 1);
            // 
            // panelInfo
            // 
            this.panelInfo.Controls.Add(this.labelProj);
            this.panelInfo.Controls.Add(this.labelProjData);
            this.panelInfo.Controls.Add(this.labelSupplierData);
            this.panelInfo.Controls.Add(this.label1);
            this.panelInfo.Controls.Add(this.labelDescriptionText);
            this.panelInfo.Controls.Add(this.labelSupplier);
            this.panelInfo.Controls.Add(this.labelItemNumber);
            this.panelInfo.Controls.Add(this.labelDatabase);
            this.panelInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelInfo.Location = new System.Drawing.Point(0, 0);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(240, 126);
            // 
            // labelProj
            // 
            this.labelProj.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelProj.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelProj.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelProj.Location = new System.Drawing.Point(4, 25);
            this.labelProj.Name = "labelProj";
            this.labelProj.Size = new System.Drawing.Size(65, 17);
            this.labelProj.Text = "PROJECT";
            // 
            // labelProjData
            // 
            this.labelProjData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelProjData.Location = new System.Drawing.Point(4, 25);
            this.labelProjData.Name = "labelProjData";
            this.labelProjData.Size = new System.Drawing.Size(232, 35);
            this.labelProjData.Text = "               ProjData - Line 1\r\nLine 2";
            // 
            // labelSupplierData
            // 
            this.labelSupplierData.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSupplierData.Location = new System.Drawing.Point(71, 5);
            this.labelSupplierData.Name = "labelSupplierData";
            this.labelSupplierData.Size = new System.Drawing.Size(94, 17);
            this.labelSupplierData.Text = "SupData";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label1.Location = new System.Drawing.Point(4, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.Text = "ITEM INFO";
            // 
            // labelDescriptionText
            // 
            this.labelDescriptionText.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelDescriptionText.Location = new System.Drawing.Point(4, 65);
            this.labelDescriptionText.Name = "labelDescriptionText";
            this.labelDescriptionText.Size = new System.Drawing.Size(232, 50);
            this.labelDescriptionText.Text = "                  Desc Text  - Line 1\r\nLine 2\r\nLine 3";
            // 
            // labelSupplier
            // 
            this.labelSupplier.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelSupplier.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelSupplier.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelSupplier.Location = new System.Drawing.Point(4, 5);
            this.labelSupplier.Name = "labelSupplier";
            this.labelSupplier.Size = new System.Drawing.Size(65, 17);
            this.labelSupplier.Text = "SUPPLIER";
            // 
            // labelItemNumber
            // 
            this.labelItemNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelItemNumber.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular);
            this.labelItemNumber.Location = new System.Drawing.Point(161, 0);
            this.labelItemNumber.Name = "labelItemNumber";
            this.labelItemNumber.Size = new System.Drawing.Size(80, 20);
            this.labelItemNumber.Text = "999 / 999";
            this.labelItemNumber.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDatabase
            // 
            this.labelDatabase.Location = new System.Drawing.Point(120, 5);
            this.labelDatabase.Name = "labelDatabase";
            this.labelDatabase.Size = new System.Drawing.Size(98, 19);
            this.labelDatabase.Text = "Databse";
            this.labelDatabase.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panelResults
            // 
            this.panelResults.Controls.Add(this.buttonReslutsClose);
            this.panelResults.Controls.Add(this.labelResultsTitle);
            this.panelResults.Controls.Add(this.textBoxResultsList);
            this.panelResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelResults.Location = new System.Drawing.Point(0, 20);
            this.panelResults.Name = "panelResults";
            this.panelResults.Size = new System.Drawing.Size(240, 280);
            this.panelResults.Visible = false;
            // 
            // buttonReslutsClose
            // 
            this.buttonReslutsClose.BackColor = System.Drawing.Color.LightGray;
            this.buttonReslutsClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonReslutsClose.Location = new System.Drawing.Point(5, 245);
            this.buttonReslutsClose.Name = "buttonReslutsClose";
            this.buttonReslutsClose.Size = new System.Drawing.Size(230, 30);
            this.buttonReslutsClose.TabIndex = 74;
            this.buttonReslutsClose.Text = "Clsoe Resluts";
            this.buttonReslutsClose.Click += new System.EventHandler(this.buttonReslutsClose_Click);
            // 
            // labelResultsTitle
            // 
            this.labelResultsTitle.BackColor = System.Drawing.SystemColors.MenuText;
            this.labelResultsTitle.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelResultsTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelResultsTitle.Location = new System.Drawing.Point(5, 5);
            this.labelResultsTitle.Name = "labelResultsTitle";
            this.labelResultsTitle.Size = new System.Drawing.Size(230, 18);
            this.labelResultsTitle.Text = "Failed Resluts";
            this.labelResultsTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxResultsList
            // 
            this.textBoxResultsList.Location = new System.Drawing.Point(5, 25);
            this.textBoxResultsList.Multiline = true;
            this.textBoxResultsList.Name = "textBoxResultsList";
            this.textBoxResultsList.ReadOnly = true;
            this.textBoxResultsList.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxResultsList.Size = new System.Drawing.Size(230, 218);
            this.textBoxResultsList.TabIndex = 0;
            this.textBoxResultsList.WordWrap = false;
            // 
            // Marry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.panelResults);
            this.Controls.Add(this.panelOptions);
            this.Controls.Add(this.panelBase);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.panelFooter);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "Marry";
            this.Text = "Marry";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Marry_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Marry_KeyDown);
            this.panelOptions.ResumeLayout(false);
            this.panel_O_Button.ResumeLayout(false);
            this.panel_O_Order_D.ResumeLayout(false);
            this.panelFooter.ResumeLayout(false);
            this.panelHeader.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.panelBase.ResumeLayout(false);
            this.panelBarcodes.ResumeLayout(false);
            this.panel_D_Barcode.ResumeLayout(false);
            this.panelInfo.ResumeLayout(false);
            this.panelResults.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelOptions;
        private System.Windows.Forms.Panel panel_O_Button;
        private System.Windows.Forms.Button button_O_Selected;
        private System.Windows.Forms.Panel panel_O_Order_D;
        private System.Windows.Forms.ComboBox comboBoxOrder;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelBase;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label labelItemNumber;
        private System.Windows.Forms.Label labelDatabase;
        private System.Windows.Forms.Panel panelBarcodes;
        private System.Windows.Forms.Panel panel_D_Barcode;
        private System.Windows.Forms.TextBox textBoxBarcode;
        private System.Windows.Forms.Label labelBarcode;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Panel panelLine2;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Panel panelResults;
        private System.Windows.Forms.Label labelResultsTitle;
        private System.Windows.Forms.TextBox textBoxResultsList;
        private System.Windows.Forms.Button buttonReslutsClose;
        private System.Windows.Forms.Label labelExtraOrders;
        private System.Windows.Forms.Label labelProj;
        private System.Windows.Forms.Label labelProjData;
        private System.Windows.Forms.Label labelSupplierData;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelDescriptionText;
        private System.Windows.Forms.Label labelSupplier;
    }
}

