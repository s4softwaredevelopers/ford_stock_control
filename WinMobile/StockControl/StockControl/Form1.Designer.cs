namespace Stockroom
{
    partial class StockControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.webBrowserStockControl = new System.Windows.Forms.WebBrowser();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.buttonNavigate = new System.Windows.Forms.Button();
            this.textBoxURL = new System.Windows.Forms.TextBox();
            this.labelURL = new System.Windows.Forms.Label();
            this.panelSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "URL";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Close";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // webBrowserStockControl
            // 
            this.webBrowserStockControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowserStockControl.Location = new System.Drawing.Point(0, 0);
            this.webBrowserStockControl.Name = "webBrowserStockControl";
            this.webBrowserStockControl.Size = new System.Drawing.Size(240, 268);
            this.webBrowserStockControl.Visible = false;
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.buttonNavigate);
            this.panelSettings.Controls.Add(this.textBoxURL);
            this.panelSettings.Controls.Add(this.labelURL);
            this.panelSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSettings.Location = new System.Drawing.Point(0, 0);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(240, 268);
            // 
            // buttonNavigate
            // 
            this.buttonNavigate.Location = new System.Drawing.Point(21, 121);
            this.buttonNavigate.Name = "buttonNavigate";
            this.buttonNavigate.Size = new System.Drawing.Size(193, 20);
            this.buttonNavigate.TabIndex = 2;
            this.buttonNavigate.Text = "Navigate";
            this.buttonNavigate.Click += new System.EventHandler(this.buttonNavigate_Click);
            // 
            // textBoxURL
            // 
            this.textBoxURL.Location = new System.Drawing.Point(21, 67);
            this.textBoxURL.Name = "textBoxURL";
            this.textBoxURL.Size = new System.Drawing.Size(193, 21);
            this.textBoxURL.TabIndex = 1;
            this.textBoxURL.Text = "http://s4.servebeer.com/stockcontrol/src/index.php?";
            // 
            // labelURL
            // 
            this.labelURL.Location = new System.Drawing.Point(21, 43);
            this.labelURL.Name = "labelURL";
            this.labelURL.Size = new System.Drawing.Size(100, 20);
            this.labelURL.Text = "URL";
            // 
            // StockControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.panelSettings);
            this.Controls.Add(this.webBrowserStockControl);
            this.Menu = this.mainMenu1;
            this.Name = "StockControl";
            this.Text = "StockControl";
            this.panelSettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowserStockControl;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Label labelURL;
        private System.Windows.Forms.Button buttonNavigate;
        private System.Windows.Forms.TextBox textBoxURL;
    }
}

