<?php
if (isset($_POST['editlocation'])) {
    display_edit_location($_POST['editlocation']);
}
elseif (isset($_POST['updatelocation'])) {
    $rows = update_location($_POST['updatelocation'], $_POST['name'], $_POST['project']);
    $message = ($rows == 1 ? 'Update succeeded.' : 'Nothing to update.');
    
    display_edit_location($_POST['updatelocation'], $message, null);
}
else if (isset($_POST['updateshelf'])) {
    $rows = update_shelf($_POST['updateshelf'], $_POST['name'], $_POST['description']);
    $message = ($rows == 1 ? 'Update succeeded.' : 'Nothing to update.');
    
    display_edit_location($_POST['location'], null, $message);
}
else if (isset($_POST['removeshelf'])) {
    
}
elseif (isset($_POST['removelocation'])) {
     if (isset($_POST['checked'])) {
         // remove the location
         maintain_all_locations();
     }
     else {
         maintain_all_locations();
         echo '<div>Confirm the delete first.</div>';
     }
}
else {
    maintain_all_locations();
}

function maintain_all_locations()
{?>
    <h2>Maintain Locations</h2>
    
    <?php $q = 'SELECT * FROM stockroom_location';
    
    $result = null;
    $rows = sql_select($result, $q);
    
    if ($rows > 0) {
        if (isset($result)) { ?>
            <table cellspacing="5" cellpadding="0">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Project</th>
                        <th colspan='2'>Actions</th>
                        <th>Confirm</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($result as $row) {
                        $id = $row['StockRoom_ID'];
                        $name = $row['StockRoom_Description'];
                        $project = $row['StockRoom_Project']; ?>

                        <tr>
                            <td><?php echo $name ?></td>
                            <td><?php echo $project ?></td>
                            <td>
                                <form name='editlocationsform<?php echo $id ?>' action='index.php?pid=maintainlocations' method='post'>
                                    <input type='hidden' name='editlocation' value='<?php echo $id ?>'>
                                    <input type='submit' value='Edit'>
                                </form>
                            </td>
                            <form name='removelocationsform<?php echo $id ?>' action='index.php?pid=maintainlocations' method='post'>
                                <td>
                                    <input type='hidden' name='removelocation' value='<?php echo $id ?>'>
                                    <input type='submit' value='Remove'>
                                </td>
                                <td>
                                    <input type='checkbox' name='checked' value='checked'>
                                </td>
                            </form>
                        </tr>
                    <?php } ?>
                        
                </tbody>
            </table>
        <?php }
    }
}

function edit_location($id, $message = null)
{ ?>
    <h2>Location</h2>
    
    <?php echo $message;
    
    $q = 'SELECT * FROM stockroom_location WHERE StockRoom_ID=:id';
    $params = array(':id' => $id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows == 1) {
        if (isset($result)) { ?>
            <table cellspacing="5" cellpadding="0">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Project</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($result as $row) {
                        $id = $row['StockRoom_ID'];
                        $name = $row['StockRoom_Description'];
                        $project = $row['StockRoom_Project']; ?>
                        
                        <tr>
                            <form name='updatelocationform<?php echo $id ?>' action='index.php?pid=maintainlocations' method='post'>
                                <td><input type='text' name='name' value='<?php echo $name ?>'</td>
                                <td><input type='text' name='project' value='<?php echo $project ?>'</td>
                                <td>
                                    <input type='hidden' name='updatelocation' value='<?php echo $id ?>'>
                                    <input type='submit' value='Update'>
                                </td>
                            </form>
                        </tr>
                    <?php } ?>
                        
                </tbody>
            </table>
        <?php }
    }
}

function edit_location_shelves($id, $message = null)
{ ?>
    <h2>Shelves</h2>

    <?php echo $message;
    
    $q = 'SELECT * FROM stockroom_shelves WHERE StockRoom_LocationID=:id';
    $params = array(':id' => $id);
    
    $result = null;
    $rows = sql_select($result, $q, $params);
    
    if ($rows > 0) {
        if (isset($result)) { ?>
            <table cellspacing="5" cellpadding="0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th colspan='2'>Actions</th>
                        <th>Confirm</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <?php foreach ($result as $row) {
                        $id = $row['StockRoomShelves_ID'];
                        $name = $row['StockRoom_ShelfID'];
                        $description = $row['description'];
                        $location = $row['StockRoom_LocationID']?>
                    
                        <tr>
                            <form name='updateshelfform<?php echo $id ?>' action='index.php?pid=maintainlocations' method='post'>
                                <td><input type='text' name='name' value='<?php echo $name ?>'</td>
                                <td><input type='text' name='description' value='<?php echo $description ?>'</td>
                                <td>
                                    <input type='hidden' name='location' value='<?php echo $location ?>'>
                                    <input type='hidden' name='updateshelf' value='<?php echo $id ?>'>
                                    <input type='submit' value='Update'>
                                </td>
                            </form>
                            <form name='removeshelfform<?php echo $id ?>' action='index.php?pid=maintainlocations' method='post'>
                                <td>
                                    <input type='hidden' name='location' value='<?php echo $location ?>'>
                                    <input type='hidden' name='removeshelf' value='<?php echo $id ?>'>
                                    <input type='submit' value='Remove'>
                                </td>
                                <td>
                                    <input type='checkbox' name='checked' value='checked'>
                                </td>
                            </form>
                        </tr>
                    <?php } ?>
                        
                </tbody>
            </table>
        <?php }
    }
}

function update_location($id, $name, $project)
{
    $q = 'UPDATE stockroom_location SET StockRoom_Description=:name, StockRoom_Project=:project WHERE StockRoom_ID=:id';
    $params = array(':id' => $id, ':name' => $name,':project' => $project);
    
    $rows = sql_update($q, $params);
    
    return $rows;
}

function update_shelf($id, $name, $description)
{
    $q = 'UPDATE stockroom_shelves SET StockRoom_ShelfID=:name, description=:description WHERE StockRoomShelves_ID=:id';
    $params = array(':id' => $id, ':name' => $name,':description' => $description);
    
    $rows = sql_update($q, $params);
    
    return $rows;
}

function remove_shelf($id)
{
    $q = 'DELETE FROM WHERE';
    $params = array(':id' => $id, ':name' => $name,':description' => $description);
    
    $rows = sql_update($q, $params);
    
    return $rows;
}

function remove_location($id)
{
    
}

function display_edit_location($id, $locationmessage = null, $shelvesmessage =null)
{ ?>
    <div>
        <?php edit_location($id, $locationmessage); ?>
    </div>
    <div>
        <?php edit_location_shelves($id, $shelvesmessage); ?>
    </div>
<?php }
?>