namespace s4_Scanner
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAssignBarcodes = new System.Windows.Forms.Button();
            this.buttonCapture = new System.Windows.Forms.Button();
            this.buttonRelocate = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelLoader = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panelHeader.SuspendLayout();
            this.panelLoader.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonAssignBarcodes
            // 
            this.buttonAssignBarcodes.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonAssignBarcodes.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular);
            this.buttonAssignBarcodes.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonAssignBarcodes.Location = new System.Drawing.Point(0, 20);
            this.buttonAssignBarcodes.Name = "buttonAssignBarcodes";
            this.buttonAssignBarcodes.Size = new System.Drawing.Size(120, 150);
            this.buttonAssignBarcodes.TabIndex = 1;
            this.buttonAssignBarcodes.Text = "Assign\r\n Barcodes";
            this.buttonAssignBarcodes.Click += new System.EventHandler(this.buttonMarry_Click);
            // 
            // buttonCapture
            // 
            this.buttonCapture.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonCapture.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular);
            this.buttonCapture.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonCapture.Location = new System.Drawing.Point(120, 20);
            this.buttonCapture.Name = "buttonCapture";
            this.buttonCapture.Size = new System.Drawing.Size(120, 150);
            this.buttonCapture.TabIndex = 2;
            this.buttonCapture.Text = "Capture\r\nItems";
            this.buttonCapture.Click += new System.EventHandler(this.buttonCapture_Click);
            // 
            // buttonRelocate
            // 
            this.buttonRelocate.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonRelocate.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular);
            this.buttonRelocate.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonRelocate.Location = new System.Drawing.Point(0, 170);
            this.buttonRelocate.Name = "buttonRelocate";
            this.buttonRelocate.Size = new System.Drawing.Size(120, 150);
            this.buttonRelocate.TabIndex = 3;
            this.buttonRelocate.Text = "Relocate";
            this.buttonRelocate.Click += new System.EventHandler(this.buttonRelocate_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonClose.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular);
            this.buttonClose.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.buttonClose.Location = new System.Drawing.Point(120, 170);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(120, 150);
            this.buttonClose.TabIndex = 4;
            this.buttonClose.Text = "Close";
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panelHeader.Controls.Add(this.labelTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(240, 20);
            // 
            // labelTitle
            // 
            this.labelTitle.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.labelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelTitle.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.labelTitle.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labelTitle.Location = new System.Drawing.Point(0, 0);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(240, 20);
            this.labelTitle.Text = " Mobile Scanner - Menu";
            // 
            // panelLoader
            // 
            this.panelLoader.Controls.Add(this.label2);
            this.panelLoader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLoader.Location = new System.Drawing.Point(0, 20);
            this.panelLoader.Name = "panelLoader";
            this.panelLoader.Size = new System.Drawing.Size(240, 300);
            this.panelLoader.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Regular);
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label2.Location = new System.Drawing.Point(29, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 107);
            this.label2.Text = "Loading";
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 320);
            this.ControlBox = false;
            this.Controls.Add(this.panelLoader);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonRelocate);
            this.Controls.Add(this.buttonCapture);
            this.Controls.Add(this.buttonAssignBarcodes);
            this.Controls.Add(this.panelHeader);
            this.KeyPreview = true;
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimizeBox = false;
            this.Name = "Menu";
            this.Text = "Menu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Menu_Load);
            this.panelHeader.ResumeLayout(false);
            this.panelLoader.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonAssignBarcodes;
        private System.Windows.Forms.Button buttonCapture;
        private System.Windows.Forms.Button buttonRelocate;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelLoader;
        private System.Windows.Forms.Label label2;
    }
}