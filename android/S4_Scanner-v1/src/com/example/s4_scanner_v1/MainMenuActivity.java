package com.example.s4_scanner_v1;

import MyContent.AssignedItemContent;
import MyContent.MyWebRequest;
import MyContent.QRelocateContent;
import MyContent.RelocateItemContent;
import MyContent.UsersContent;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainMenuActivity extends Activity {

	private ProgressBar spinner;
	private LinearLayout layoutQRelocate;
	public static String Mobile_Server = "MobileServer";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
	      spinner = (ProgressBar)findViewById(R.id.progressBar1);
	      spinner.setVisibility(View.GONE);
	      layoutQRelocate = (LinearLayout)findViewById(R.id.linearLayoutQRelocate);
	      layoutQRelocate.setVisibility(View.GONE);
	      
	      ServerSettings();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			SettingsStart();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void ServerSettings()
	{
		// Get Server Settings
		SharedPreferences sharedpreferences = getSharedPreferences(ScannerSettingsActivity.SETTINGS_SERVER, Context.MODE_PRIVATE);
		
	      if (sharedpreferences.contains(ScannerSettingsActivity.Server))
	      {
	    	  Mobile_Server = sharedpreferences.getString(ScannerSettingsActivity.Server, "");
	      }
	}
	
	
    public void AssignBarcodesButton(View view) {
    		new WebRequestAssignBarcodes().execute("");
    }
    
    public void AssignBarcodesStart() {
    	// Open Assign Barcodes Page
        Intent intent = new Intent(this, AssignActivity.class);
        startActivity(intent);
    }
    
    public void SettingsStart() {
    	// Open Settings Page
        Intent intent = new Intent(this, ScannerSettingsActivity.class);
        startActivity(intent);
    }
    
    public void QRelocateButton(View view) {
    	// Change button flow on Main Page
    	if (layoutQRelocate.getVisibility() == View.VISIBLE){
    		layoutQRelocate.setVisibility(View.GONE);
    	}
    	else
    	{
    		layoutQRelocate.setVisibility(View.VISIBLE);
    	}
    }
    
    public void QRelocateShelfButton(View view) {
    	// Open Quick Relocation without login Page
    	layoutQRelocate.setVisibility(View.GONE);
    	UsersContent.bReturnUser = false;
    	QRelocateContent.ITEMS.clear();
        Intent intent = new Intent(this, QRelocateActivity.class);
        startActivity(intent);
    }
    
    public void QRelocateUserButton(View view) {
    	// Open Quick Relocation with login Page first
    	layoutQRelocate.setVisibility(View.GONE);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("SOURCE","QRelocate");
        startActivity(intent);
    }
    
    public void CaptureButton(View view) {
    	// Open Capture new item Page
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("SOURCE","Capture");
        startActivity(intent);
    }
    
    public void RelocateButton(View view) {
    	// Relocate Button Press
    	layoutQRelocate.setVisibility(View.GONE);
    	new WebRequestRelocate().execute("");
    }
    
    public void HistoryButton(View view) {
    	// History Button Press
    	Toast.makeText(getBaseContext(),"Reserved For Future Use",Toast.LENGTH_LONG).show();
//        Intent intent = new Intent(this, HistoryActivity.class);
//        startActivity(intent);
    }
    
    public void RelocateStart() {
    	// Open Relocate Page
    	UsersContent.bReturnUser = false;
        Intent intent = new Intent(this, RelocateActivity.class);
        startActivity(intent);
    }
    
	class WebRequestAssignBarcodes extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=marryItems&fid=display";
			String result = "";
			try{
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				AssignedItemContent.BarcodePopulate(result);
				AssignedItemContent.OrderNumbersSort();
				AssignedItemContent.ReOrder("All Orders");
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return "";
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("")) 
	    	{
	    		AssignBarcodesStart(); 
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }

	}
	
	class WebRequestRelocate extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=setLocation&fid=display";
			String result = "";
			try{
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				RelocateItemContent.BarcodePopulate(result);
				RelocateItemContent.OrderNumbersSort();
				RelocateItemContent.ReOrder("All Orders");
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return "";
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("")) 
	    	{
	    		RelocateStart(); 
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }

	}
	

}
