﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Configuration;
using S4_Barcode_Generator.Properties;
using System.Deployment.Application;

namespace S4_Barcode_Generator
{
    public partial class Barcode_Generator : Form
    {
        int ServerPort;
        int ServerReplyPort;
        List<String> templateList = new List<String>();
        int templateSelected;
        string lastBarcodeDatabase;
        Boolean ServerDone = false;
        PrintDialog pd = new PrintDialog();
        Boolean DefaultPrinter = true;
        List<String> PortsUsed = new List<String>();
        LogFile logger = new LogFile();


        public Barcode_Generator()
        {
            InitializeComponent();
        }

        #region General

            public void PosStatus(string value)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(PosStatus), new object[] { value });
                    return;
                }
                labelGeneral_Status.ForeColor = System.Drawing.Color.Green;
                labelGeneral_Status.Text = value;

                richTextBoxLog_Logger.Select(0, 0);
                richTextBoxLog_Logger.SelectionColor = Color.Green;
                richTextBoxLog_Logger.SelectedText = DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss.fff tt") + " - " + value + "\n";   
            }
            public void NegStatus(string value)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(NegStatus), new object[] { value });
                    return;
                }
                labelGeneral_NegStatus.ForeColor = System.Drawing.Color.Red;
                labelGeneral_NegStatus.Text = value;

                richTextBoxLog_Logger.Select(0, 0);
                richTextBoxLog_Logger.SelectionColor = Color.Red;
                richTextBoxLog_Logger.SelectedText = DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss.fff tt") + " - " + value + "\n";            
            }
            public void GenStatus(string value)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<string>(GenStatus), new object[] { value });
                    return;
                }

                richTextBoxLog_Logger.Select(0, 0);
                richTextBoxLog_Logger.SelectionColor = Color.Blue;
                richTextBoxLog_Logger.SelectedText = DateTime.Now.ToString("dd/MM/yyyy  hh:mm:ss.fff tt") + " - " + value + "\n";
            }
            public void ButtonServer(bool value)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new Action<bool>(ButtonServer), new object[] { value });
                    return;
                }
                if (value)
                {
                    buttonServer_Start.Enabled = false;
                    buttonServer_Stop.Enabled = true;
                    buttonGenerate_GenerateBarcodes.Enabled = true;
                    buttonPrint_Print.Enabled = true;
                    textBoxServer_PortNumber.Enabled = false;
                    textBoxServer_ReplyNumber.Enabled = false;
                }
                else
                {
                    buttonServer_Start.Enabled = true;
                    buttonServer_Stop.Enabled = false;
                    buttonGenerate_GenerateBarcodes.Enabled = false;
                    buttonPrint_Print.Enabled = false;
                    textBoxServer_PortNumber.Enabled = true;
                    textBoxServer_ReplyNumber.Enabled = true;
                }
            }
            private void Arguments() // Command line arguments
            {
                string[] arguments = Environment.GetCommandLineArgs();
                if (arguments.Length > 1)
                {
                    for (int count = 1; count < arguments.Count(); count++)
                    {
                        switch (arguments[count])
                        {
                            case ("/p"):
                                {
                                    textBoxServer_PortNumber.Text = arguments[count + 1];
                                    logger.MyLogFile("ARGUMENT", "Server Port: " + arguments[count + 1]);
                                    break;
                                }
                            case ("/rp"):
                                {
                                    textBoxServer_ReplyNumber.Text = arguments[count + 1];
                                    logger.MyLogFile("ARGUMENT", "Reply Port: " + arguments[count + 1]);
                                    break;
                                }
                        }
                    }
                }
            }
            private void ConfigSettings() // Configuration File Settings 
            {
                try
                {
                    textBoxServer_PortNumber.Text = ConfigurationManager.AppSettings["Server_Port"];
                    textBoxServer_ReplyNumber.Text = ConfigurationManager.AppSettings["Server_Reply_Port"];
                    templateList.Add(ConfigurationManager.AppSettings["Barcode_Template_0"]);
                    templateList.Add(ConfigurationManager.AppSettings["Barcode_Template_1"]);
                    templateList.Add(ConfigurationManager.AppSettings["Barcode_Template_2"]);
                    templateList.Add(ConfigurationManager.AppSettings["Barcode_Template_3"]);
                    templateList.Add(ConfigurationManager.AppSettings["Barcode_Template_4"]);
                    templateSelected = Int32.Parse(ConfigurationManager.AppSettings["Barcode_Template_Selected"]);
                    lastBarcodeDatabase = ConfigurationManager.AppSettings["Last_Barcode_Database"];
                    if (ConfigurationManager.AppSettings["Minimized_Application"] == "True")
                    {
                        checkBoxMini.Checked = true;
                    }
                    logger.MyLogFile("CONFIG  ", "Configuration settings loaded");
                }
                catch
                {
                    logger.MyLogFile("ERROR   ", "Error reading configuration settings");
                }
            }
            private void DatabaseLoad() // Load Database
            {
                try
                {
                    this.barcodesTableAdapter.Fill(this.databaseBarcodesDataSet.Barcodes);
                    logger.MyLogFile("DATABASE", "Database Connected");
                    StartServer();                
                }
                catch (Exception e)
                {
                    logger.MyLogFile("ERROR   ", "Database Down: " + e.Message);
                }
            }
            private void DatabaseSave() // Save Database
            {
                try
                {
                    this.Validate();
                    this.barcodesBindingSource.EndEdit();
                    this.barcodesTableAdapter.Update(this.databaseBarcodesDataSet.Barcodes);
                    logger.MyLogFile("DATABASE", "Database Updated");
                }
                catch (Exception e)
                {
                    NegStatus("Database Update Error");
                    logger.MyLogFile("ERROR   ", "Database Update Error : " + e.Message);
                }
            }   
            private void ZPLTemplateList()
            {
                for (int count = 0; count < 5; count++)
                    comboBoxZPL_Templates.Items.Add(count);
                comboBoxZPL_Templates.SelectedIndex = templateSelected;
            }


        #endregion

        private void Barcode_Generator_Load(object sender, EventArgs e)
        {
            this.Height = 328;
            this.Width = 469;
            tabControlGenerator.Height = 263;
            tabControlGenerator.Width = 455;
            labelGeneral_Status.Text = "";
            labelGeneral_NegStatus.Text = "";
            logger.LogClear();
            ConfigSettings();
            Arguments();
            DatabaseLoad();
            ZPLTemplateList();

            if (labelGeneral_NegStatus.Text != "")
            {
                tabControlGenerator.SelectedIndex = 3;
            }

            if (checkBoxMini.Checked)
            {
                this.WindowState = FormWindowState.Minimized;
            }
        }

        #region Generate Barcodes

            private void buttonGenerate_GenerateBarcodes_Click(object sender, EventArgs e) // Button to Generate Barcodes
            {
                int numBarcodes = Decimal.ToInt32(numericUpDownGenerate_Total.Value); // value selected
                GenerateBarcode(numBarcodes, checkBoxGenerate_Print.Checked); // Generate Barcode
            }

            private void GenerateBarcode(int number, bool bPrint) // Generate New Barcode
            {
                string sBarcode = lastBarcodeDatabase;
                int pos = databaseBarcodesDataSet.Barcodes.Count() - 1;
                if (pos != -1)
                {
                    try
                    {
                        int iLastBarcode = (Int32.Parse(databaseBarcodesDataSet.Barcodes[pos].Barcode, System.Globalization.NumberStyles.HexNumber)); // last barcode    
                        if (iLastBarcode > (Int32.Parse(sBarcode, System.Globalization.NumberStyles.HexNumber)))
                        {
                            sBarcode = databaseBarcodesDataSet.Barcodes[pos].Barcode;
                        }
                    }
                    catch (Exception e)
                    {
                        NegStatus("Error - Last Barcode in db or Config is in wrong format");
                        logger.MyLogFile("ERROR  ", "Last Barcode in db or Config is in wrong format :" + e.Message);
                        return;
                    }
                }


                for (int count = 0; count < number; count++)
                {
                    sBarcode = (Int32.Parse(sBarcode, System.Globalization.NumberStyles.HexNumber) + 1).ToString("X10"); // new barcode
                    databaseBarcodesDataSet.Barcodes.AddBarcodesRow(sBarcode, 0);
                }
                PosStatus("Barcodes Generated : " + number);

                if (bPrint) // Print when generated
                {
                    PrintBarcode(number, pos + 1, 1);
                    PosStatus("Barcodes Generated and Printed : " + number);
                }
            }

        #endregion

        #region Print Barcodes

            private void buttonPrint_Print_Click(object sender, EventArgs e)
            {
                int numBarcodes = Decimal.ToInt32(numericUpDownPrint_NumberBarcodes.Value);
                int numCopiesPrint = Decimal.ToInt32(numericUpDownPrint_CopiesBarcodes.Value);
                int iBarcodeSelected = comboBoxPrint_Barcodes.SelectedIndex;
                if (iBarcodeSelected == -1) { return; }

                PrintBarcode(numBarcodes, iBarcodeSelected , numCopiesPrint);
            }

            private void PrintBarcode(int NumberPrint, int pos, int quantity)
            {
                string strDocDate = DateTime.Today.ToString("dd/MM/yyyy");
                List<String> printList = new List<String>();

                int max = databaseBarcodesDataSet.Barcodes.Count();
                bool bExtraPrint = false;

                for (int count = 0; count < NumberPrint; count++)
                {
                    if (max > (pos + count))
                    {
                        printList.Add(databaseBarcodesDataSet.Barcodes[pos + count].Barcode);
                        databaseBarcodesDataSet.Barcodes[pos + count].Copies_Printed++;
                    }
                    else { bExtraPrint = true; }
                }

                if (bExtraPrint) { NegStatus("Barcodes to be Printed does not exists"); }

                PosStatus("Barcodes to Print : " + printList.Count);

                // Printing Thread
                Thread PrintThread = new Thread(() =>
                    {
                        try
                        {
                            foreach (string strBarCode in printList)
                            {
                                string data = templateList[templateSelected].Replace("@BARCODE@", strBarCode).
                                    Replace("@DATE@", strDocDate).
                                    Replace("@PRNQTY@", quantity.ToString()).
                                    Replace("\n", "");

                                if (DefaultPrinter)
                                {
                                    RawPrinterHelper.SendStringToPrinter(new PrinterSettings().PrinterName, data); // Send to default printer
                                }
                                else
                                {
                                    RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, data); // Send a printer-specific to the printer.
                                }
                                logger.MyLogFile("PRINT   ", "Barcode: " + strBarCode + " - ZPL: " + data);
                                GenStatus("Barcode: " + strBarCode + " Printed");
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.MyLogFile("ERROR   ", ex.Message);
                            NegStatus("Print Error - " + ex.Message);
                        }                        
                    });
                PrintThread.IsBackground = true;
                PrintThread.Start();
            }

            private void RePrint(string strBarCode)
            {
                string strDocDate = DateTime.Today.ToString("dd/MM/yyyy");

                // Printing Thread
                Thread PrintThread = new Thread(() =>
                {
                    try
                    {
                        string data = templateList[templateSelected].Replace("@BARCODE@", strBarCode).
                            Replace("@DATE@", strDocDate).
                            Replace("@PRNQTY@", "1").
                            Replace("\n", "");

                        if (DefaultPrinter)
                        {
                            RawPrinterHelper.SendStringToPrinter(new PrinterSettings().PrinterName, data); // Send to default printer
                        }
                        else
                        {
                            RawPrinterHelper.SendStringToPrinter(pd.PrinterSettings.PrinterName, data); // Send a printer-specific to the printer.
                        }
                        logger.MyLogFile("PRINT   ", "Barcode: " + strBarCode + " - ZPL: " + data);
                        GenStatus("Barcode: " + strBarCode + " Printed");

                    }
                    catch (Exception ex)
                    {
                        logger.MyLogFile("ERROR   ", ex.Message);
                        NegStatus("Print Error - " + ex.Message);
                    }
                });
                PrintThread.IsBackground = true;
                PrintThread.Start();
            }

            private void buttonPrint_ChangePrinter_Click(object sender, EventArgs e)
            {
                pd.PrinterSettings = new PrinterSettings();
                if (DialogResult.OK == pd.ShowDialog(this))
                {
                    DefaultPrinter = false;
                    logger.MyLogFile("PRINTER ", "Printer changed - " + pd.PrinterSettings.PrinterName);
                    PosStatus("Printer changed - " + pd.PrinterSettings.PrinterName);
                }
            }

        #endregion

        #region Server

            private void buttonServer_Start_Click(object sender, EventArgs e)
            {
                labelGeneral_Status.Text = "";
                labelGeneral_NegStatus.Text = "";

                if (PortsUsed.Contains(textBoxServer_PortNumber.Text))
                {
                    textBoxServer_PortNumber.Focus();
                    NegStatus("Port is in use, try new port");
                }
                else
                {
                    StartServer();
                }
            }

            private void buttonServer_Stop_Click(object sender, EventArgs e)
            {
                labelGeneral_Status.Text = "";
                labelGeneral_NegStatus.Text = "";

                StopServer();
            }

            private void StartServer()
            {
                try
                {
                    ServerPort = int.Parse(textBoxServer_PortNumber.Text);
                    PortsUsed.Add(textBoxServer_PortNumber.Text);
                    ServerReplyPort = int.Parse(textBoxServer_ReplyNumber.Text);
                }
                catch
                {
                    NegStatus("Invalid Port");
                    return;
                }
                ServerDone = false;
                ButtonServer(true);

                PosStatus("Server Start");

                // Server Thread
                Thread UdpServerThread = new Thread(() =>
                {
                    try
                    {
                        UdpClient Server = new UdpClient(ServerPort);
                        IPEndPoint ServerEP = new IPEndPoint(IPAddress.Any, ServerPort);
                        string received_data;
                        byte[] receive_byte_array;
                        while (!ServerDone)
                        {
                            try
                            {
                                receive_byte_array = Server.Receive(ref ServerEP);
                                received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                                logger.MyLogFile("RECEIVED", "Received a broadcast from " + ServerEP.ToString() + " - Data: " + received_data);
                                PosStatus("Received a broadcast from " + ServerEP.ToString() + " - Data: " + received_data);

                                if (received_data[0].Equals('B'))
                                {
                                    RePrint(received_data.Substring(1));
                                }
                                else
                                {
                                    GenerateBarcode(int.Parse(received_data), true);
                                }
                                Thread.Sleep(100);
                                Reply(ServerEP, "OK");
                            }
                            catch (Exception ex)
                            {
                                logger.MyLogFile("ERROR   ", ex.Message);
                                NegStatus("Server Error - " + ex.Message);
                                Reply(ServerEP, "NOK:" + ex.Message);
                            }
                        }
                        Server.Close();
                    }
                    catch (Exception e)
                    {
                        StopServer();
                        logger.MyLogFile("ERROR  ", "Port is in use - " + e.Message);
                        NegStatus("Server Error - " + e.Message);
                    }
                });
                UdpServerThread.IsBackground = true;
                UdpServerThread.Start();
            }

            private void StopServer()
            {
                ServerDone = true;
                ButtonServer(false);
                NegStatus("Server Stop");
                logger.MyLogFile("SERVER  ", "Server Stopped");
            }

            private void Reply(IPEndPoint Reply_sending_end_point, string Reply_text_to_send) // Reply to Message received
            {
                try
                {
                    Socket Reply_sending_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    byte[] Reply_send_buffer = Encoding.ASCII.GetBytes(Reply_text_to_send);
                    Reply_sending_end_point.Port = ServerReplyPort;
                    Reply_sending_socket.SendTo(Reply_send_buffer, Reply_sending_end_point);
                    logger.MyLogFile("REPLIED ", "Replied to " + Reply_sending_end_point.ToString() + " - Data: " + Reply_text_to_send);
                    PosStatus("Replied to " + Reply_sending_end_point.ToString() + " - Data: " + Reply_text_to_send);
                }
                catch (Exception ex)
                {
                    logger.MyLogFile("ERROR   ", "Reply Error: " + ex.Message);
                    NegStatus("Reply Error - " + ex.Message);
                }
            }
        
        #endregion

        #region ZPL Code

            private void buttonZPL_Update_Click(object sender, EventArgs e)
            {
                UpdateZPLCode(comboBoxZPL_Templates.SelectedIndex);
            }

            private void comboBoxZPL_Templates_SelectedIndexChanged(object sender, EventArgs e)
            {
                LoadZPLCode(comboBoxZPL_Templates.SelectedIndex);
                templateSelected = comboBoxZPL_Templates.SelectedIndex;
            }

            private void UpdateZPLCode(int index)
            {
 
                templateList[index] = richTextBoxZPL_ZPLCode.Text;
                PosStatus("ZPL Code " + index + " Updated");

                try
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["Barcode_Template_" + index].Value = templateList[index];
                    config.Save(ConfigurationSaveMode.Minimal);
                }
                catch
                {
                    logger.MyLogFile("ERROR   ", "Error writing ZPL Code " + index + " to configuration settings");
                    NegStatus("Error writing ZPL Code " + index + " to configuration settings");
                }
            }

            private void LoadZPLCode(int index)
            {
                richTextBoxZPL_ZPLCode.Clear();

                richTextBoxZPL_ZPLCode.Text = templateList[index];
                PosStatus("ZPL Code " + index + " loaded");
            }

            private void buttonZPL_Start_Click(object sender, EventArgs e)
            {
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^XA{0}", Environment.NewLine));
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^PQ{0}{1}", "@PRNQTY@", Environment.NewLine));
            }
            private void buttonZPL_Finish_Click(object sender, EventArgs e)
            {
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^XZ{0}", Environment.NewLine));
            }
            private void buttonZPL_InsetText_Click(object sender, EventArgs e)
            {
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^CF0,{0}{1}", numericUpDownZPL_FontSize.Value.ToString(), Environment.NewLine));
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^FO{0},{1}^FD{2}^FS{3}",numericUpDownZPL_TextX.Value.ToString(),numericUpDownZPL_TextY.Value.ToString(),textBoxZPL_Text.Text,Environment.NewLine));
            }
            private void buttonZPL_CreateGraphicBox_Click(object sender, EventArgs e)
            {
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^FO{0},{1}^GB{2},{3},{4}^FS{5}", numericUpDownZPL_GraphicX.Value.ToString(), numericUpDownZPL_GraphicY.Value.ToString(), numericUpDownZPL_GraphicW.Value.ToString(), numericUpDownZPL_GraphicH.Value.ToString(), numericUpDownZPL_GraphicT.Value.ToString(), Environment.NewLine));
            }
            private void buttonZPL_CreateQRBarcode_Click(object sender, EventArgs e)
            {
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^FO{0},{1}^BQN,2,10^FDMA,{2}^FS{3}", numericUpDownZPL_QRX.Value.ToString(), numericUpDownZPL_QRY.Value.ToString(), textBoxZPL_QRText.Text, Environment.NewLine)); 
            }
            private void buttonZPL_BarCreate_Click(object sender, EventArgs e)
            {
                richTextBoxZPL_ZPLCode.AppendText(String.Format("^FO{0},{1}^BY3^B3N,N,50,Y,N^FD{2}^FS{3}", numericUpDownZPL_BarX.Value.ToString(), numericUpDownZPL_BarY.Value.ToString(), textBoxZPL_BarText.Text, Environment.NewLine)); 
            }

        #endregion

        #region Settings

            private void buttonSettings_ConfigUpdate_Click(object sender, EventArgs e)
            {
                lastBarcodeDatabase = textBoxSettings_Config.Text;

                try
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["Last_Barcode_Database"].Value = lastBarcodeDatabase;
                    config.Save(ConfigurationSaveMode.Minimal);
                    PosStatus("Last_Barcode_Database has been updated with : " + lastBarcodeDatabase);
                    logger.MyLogFile("UPDATE   ", "Last_Barcode_Database Settings Updated to configuration settings");
                }
                catch
                {
                    logger.MyLogFile("ERROR   ", "Error writing Last_Barcode_Database Settings to configuration settings");
                    NegStatus("Error writing Last_Barcode_Database Settings to configuration settings");
                }
            }

            private void SettingsDisplay()
            {
                int pos = databaseBarcodesDataSet.Barcodes.Count() - 1;
                if (pos != -1)
                {
                    labelSettings_LastDB.Text = databaseBarcodesDataSet.Barcodes[pos].Barcode;
                }
                else
                {
                    labelSettings_LastDB.Text = "Database empty";
                }
                textBoxSettings_Config.Text = lastBarcodeDatabase;
            }

        #endregion

        private void Barcode_Generator_FormClosing(object sender, FormClosingEventArgs e)
            {
                DatabaseSave();
                try
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    config.AppSettings.Settings["Barcode_Template_Selected"].Value = comboBoxZPL_Templates.SelectedIndex.ToString();
                    config.AppSettings.Settings["Server_Port"].Value = textBoxServer_PortNumber.Text;
                    config.AppSettings.Settings["Server_Reply_Port"].Value = textBoxServer_ReplyNumber.Text;
                    config.AppSettings.Settings["Minimized_Application"].Value = checkBoxMini.Checked.ToString();
                    config.Save(ConfigurationSaveMode.Minimal);
                }
                catch
                {
                    logger.MyLogFile("ERROR   ", "Error writing to configuration settings");
                    NegStatus("Error writing to configuration settings");
                }
            }

        private void tabControlGenerator_SelectedIndexChanged(object sender, EventArgs e)
            {
                labelGeneral_Status.Text = "";
                labelGeneral_NegStatus.Text = "";
                switch (tabControlGenerator.SelectedIndex)
                {
                    case (3):
                        {
                            this.Height = 583;
                            this.Width = 469;
                            tabControlGenerator.Height = 518;
                            tabControlGenerator.Width = 455;
                            break;
                        }
                    case (4):
                        {
                            this.Height = 583;
                            this.Width = 585;
                            tabControlGenerator.Height = 518;
                            tabControlGenerator.Width = 571;
                            break;
                        }
                    case (5):
                        {
                            this.Height = 328;
                            this.Width = 469;
                            tabControlGenerator.Height = 263;
                            tabControlGenerator.Width = 455;
                            SettingsDisplay();
                            break;
                        }
                    default:
                        {
                            this.Height = 328;
                            this.Width = 469;
                            tabControlGenerator.Height = 263;
                            tabControlGenerator.Width = 455;
                            break;
                        }
                }
                dataGridViewLog.Hide();
            }

        private void buttonLog_Switch_Click(object sender, EventArgs e)
        {
            dataGridViewLog.Show();
        }

        private void Barcode_Generator_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }






    }
}
