<?php

//error_reporting(E_ERROR);
//include 'email/sendEmail.php';
include 'email/Mailing.php';

function modelLoginDB($dbname, &$errormsg) {
    $link = mysql_connect('localhost', 's4mysql', 'mysql');
    if (!$link) {
        throw new Exception("Could not connect to Server");
    }
    $db_selected = mysql_select_db($dbname, $link);
    if (!$db_selected) {
        throw new Exception("Could not connect to database");
    }
    return $link;
}

function modelLoginLocalDB($dbName, &$errormsg) {
    $link = mysql_connect('localhost', 's4mysql', 'mysql');
    if (!$link) {
        throw new Exception("Could not connect to Server");
    }
    $db_selected = mysql_select_db($dbName, $link);
    if (!$db_selected) {
        throw new Exception("Could not connect to database");
    }
    return $link;
}

function modelConnectToServer(&$errormsg) {
    $link = mysql_connect('localhost', 's4mysql', 'mysql');
    if (!$link) {
        throw new Exception("Could not connect to Server");
    }
    return $link;
}

function modelUdpRequest($msg, $timeout, &$result) {
    $status = array();
    $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    $len = strlen($msg);
    $receiverIP = '255.255.255.255';
    $receiverPort = 11000;
    socket_set_option($sock, SOL_SOCKET, SO_BROADCAST, 1);
    socket_sendto($sock, $msg, $len, 0, $receiverIP, $receiverPort);
    $from = '';
    $port = 0;
    $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    socket_bind($socket, '0.0.0.0', 11001);
    socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array("sec" => $timeout, "usec" => 0));
    socket_recvfrom($socket, $buf, 4096, 0, $from, $port);
    if ($buf != "") { //timeout data received
        if ($buf == 'OK') {
            $status[] = array(true, 'Barcodes Printed successfully: Please collect Barcodes at printer.');
            closeSocket($sock);
            closeSocket($socket);
            return $status;
        } else {
            $errormsg = substr($buf, strpos($buf, ':') + 1);
            $status[] = array(false, $errormsg);
            closeSocket($sock);
            closeSocket($socket);
            return $status;
        }
    } else { //timeout no data received
        $status[] = array(false, 'Print Failed: Network Error: Unable to connect to printer.');
        closeSocket($sock);
        closeSocket($socket);
        return $status;
    }
}

function closeSocket($socket) {
    socket_close($socket);
}

function modelGetUsernames() {
    $users = array();
    $con = modelConnectToServer();
    $q = 'SELECT Staff_First_Name, Staff_Last_Name FROM S4Admin.Staff WHERE Staff_IsEmployee <> 0 UNION SELECT Staff_First_Name, Staff_Last_Name FROM MMAdmin.Staff WHERE Staff_IsEmployee <> 0 Order By Staff_First_Name,Staff_Last_Name';
    $result = mysql_query($q, $con);
    if (!$result) {
        $users[] = array('An error occured while retrieving users.' . mysql_error());
        return $users;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $users[] = array(utf8_encode($row['Staff_First_Name']), utf8_encode($row['Staff_First_Name']) . " " . utf8_encode($row['Staff_Last_Name']));
            }
        } else {
            $users[] = array("No users found!");
        }
    }
    return $users;
}

function modelAuthenticateUser($firstname, $password) {

    $status = array();
    $con = modelConnectToServer();
    $q = "SELECT Staff_Password FROM S4Admin.Staff WHERE Staff_First_Name = '$firstname' AND Staff_Password = old_password('$password')";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'An error occured while validating user.' . mysql_error());
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, "Login Successfully");
                return $status;
            }
        } else {
            $q = "SELECT Staff_Password FROM MMAdmin.Staff WHERE Staff_First_Name = '$firstname' AND Staff_Password = old_password('$password')";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'An error occured while validating user.' . mysql_error());
            } else {

                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_assoc($result)) {
                        $status[] = array(true, "Login Successfully");
                        return $status;
                    }
                } else {
                    $status[] = array(false, "Login Failed");
                    return $status;
                }
            }
        }
    }
}

function modelAuthenticateStoreman($uname, $password) {
    $status = array();
    $con = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `username`,user_access_id,`id` FROM `user` WHERE `username` = '$uname' AND `password` = old_password('$password') AND `user_access_id` = 3 OR `username` = '$uname' AND `password` = old_password('$password') AND `user_access_id` = 2 OR `username` = '$uname' AND `password` = old_password('$password') AND `user_access_id` = 4";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array('An error occured while validating storeman.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, $row['username'], "Logged in Successfully!", $row['id'], false, $row['user_access_id']);
                return $status;
            }
        } else {
            //check in
            $con = modelConnectToServer();
            $q = "SELECT Staff_Auth_Level,Staff_Code,Staff_First_Name,Staff_Password FROM S4Admin.Staff WHERE Staff_First_Name = '$uname' AND Staff_Password = old_password('$password') AND Staff_Code = '030' OR Staff_First_Name = '$uname' AND Staff_Password = old_password('$password') AND Staff_Code = '258' OR Staff_First_Name = '$uname' AND Staff_Password = old_password('$password') AND Staff_Code = '276'";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'An error occured while validating user.' . mysql_error());
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_assoc($result)) {
                        $status[] = array(true, $row['Staff_First_Name'] . " " . $row['Staff_Last_Name'], "Logged in Successfully!", $row['Staff_Code'], true);
                        return $status;
                    }
                } else {
                    $q = "SELECT Staff_Auth_Level,Staff_First_Name,Staff_Password FROM MMAdmin.Staff WHERE Staff_First_Name = '$firstname' AND Staff_Password = old_password('$password') AND Staff_Code = '030'";
                    $result = mysql_query($q, $con);
                    if (!$result) {
                        $status[] = array(false, 'An error occured while validating user.' . mysql_error());
                    } else {

                        $num_results = mysql_num_rows($result);
                        if ($num_results > 0) {
                            while ($row = mysql_fetch_assoc($result)) {
                                $status[] = array(true, $row['Staff_First_Name'] . " " . $row['Staff_Last_Name'], "Logged in Successfully!", $row['Staff_Auth_Level'], true);
                                return $status;
                            }
                        } else {
                            $status[] = array(false, "Login Failed");
                            return $status;
                        }
                    }
                }
            }
        }
    }
}

function modelAddUser($uname, $email, $pword, $user_access_id) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO `user` (username,email,password,user_access_id) VALUES('$uname','$email',old_password('$pword'),'$user_access_id')";

    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to add user.' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, 'User added successfully!');
        return $status;
    }
}

function modelRemoveUser($userId) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "DELETE FROM `user` WHERE id = $userId";

    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to remove user!' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, 'User removed successfully!');
        return $status;
    }
}

function modelGetUsers() {
    $users = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT user.id,user.username,user_access.description,user_access.id AS AccessID,email FROM `user` LEFT JOIN `user_access` ON user.user_access_id = user_access.id WHERE user_access.id <> 1 order by user.username"; #
    $result = mysql_query($q, $con);
    if (!$result) {
        $users[] = array(false, 'An error occured while retrieving users.' . mysql_error());
        return $users;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $users[] = array(true, $row['id'], $row['username'], $row['description'], $row['AccessID'], $row['email']);
            }
            return $users;
        } else {
            $users[] = array(false, 'No users found!');
            return $users;
        }
    }
}

function modelGetUserTypes() {
    $usersTypes = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT user_access.id,user_access.description FROM `user_access` WHERE user_access.id <> 0 AND user_access.id <> 1";
    $result = mysql_query($q, $con);
    if (!$result) {
        $usersTypes[] = array(false, 'An error occured while retrieving user types.' . mysql_error());
        return $usersTypes;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $usersTypes[] = array(true, $row['id'], $row['description']);
            }
            return $usersTypes;
        } else {
            $usersTypes[] = array(false, 'No user types found!');
            return $usersTypes;
        }
    }
}

function modelGetShelves() {
    $shelves = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT s.id,s.barcode,s.description,s4.Project_Description AS s4Projects,mm.Project_Description AS mmProjects,l.name,l.id AS location_id,s4.Project_Code AS s4Code,mm.Project_Code AS mmCode FROM `shelf` s LEFT JOIN S4Admin.Project s4 ON (s4.Project_Code = s.s4project) LEFT JOIN MMAdmin.Project mm ON (mm.Project_Code = s.mmproject) LEFT JOIN `location` l ON s.location_id = l.id WHERE l.id <> '3' order by s.id";
    $result = mysql_query($q, $con);
    if (!$result) {
        $shelves[] = array(false, 'An error occured while retrieving shelves.' . mysql_error());
        return $shelves;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $shelves[] = array(true, $row['id'], $row['barcode'], $row['description'], $row['s4Projects'], $row['mmProjects'], $row['name'], $row['location_id'], $row['s4Code'], $row['mmCode']);
            }
            return $shelves;
        } else {
            $shelves[] = array(false, 'No shelves found!');
            return $shelves;
        }
    }
}

function modelRemoveShelf($shelfId) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "DELETE FROM `shelf` WHERE id = $shelfId";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Unable to remove shelf!' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, 'Shelf removed successfully!');
        return $status;
    }
}

function modelAddShelf($barcode, $description, $location_id, $s4Project, $mmProject) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO shelf (barcode, description, location_id,s4project,mmproject) VALUES ('$barcode','$description',$location_id,'$s4Project','$mmProject')";

    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Shelf not added!' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, 'Shelf added successfully!');
        return $status;
    }
}

function modelGetLocations() {
    $locations = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT location.id,location.name FROM `location` WHERE `id` != '3'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $locations[] = array('An error occured while retrieving locations.' . mysql_error());
        return $locations;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $locations[] = array($row['id'], $row['name']);
            }
            return $locations;
        } else {
            $locations[] = array('No locations found!');
            return $locations;
        }
    }
}

function modelUpdateShelves($id, $barcode, $description, $location, $s4Project, $mmProject) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "UPDATE shelf SET barcode='$barcode', description='$description', s4project='$s4Project',mmproject='$mmProject', location_id=$location WHERE id=$id";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Shelf not updated!' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, 'Shelf updated successfully!');
        return $status;
    }
}

function modelUpdateUsers($id, $username, $userAccess, $email) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "UPDATE user SET username='$username', user_access_id='$userAccess',email='$email' WHERE id=$id";
    $result = mysql_query("SELECT username FROM user WHERE id=$id", $con);
    $prev = mysql_fetch_array($result);
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'User not updated!' . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($prev['username'] == $_SESSION['uname'])
            $_SESSION['uname'] = $username;
        $status[] = array(true, 'User updated successfully!');
        return $status;
    }
}

function modelGetOrders($db) {
    $orders = array();
    if ($db == 's4') {
        $con = modelLoginDB('S4Admin', $errormsg); //changed to 200
    } else if ($db == 'mm') {
        $con = modelLoginDB('MMAdmin', $errormsg);
    }
    $q = "SELECT OrderNo_Number, OrderNo_Comments FROM `OrderNo` ORDER BY OrderNo_Number DESC LIMIT 30";
    $result = mysql_query($q, $con);
    if (!$result) {
        $orders[] = array('An error occured while retrieving orders.' . mysql_error());
        return $orders;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $orders[] = array($row['OrderNo_Number'], $row['OrderNo_Comments']);
            }
            return $orders;
        } else {
            $orders[] = array('No orders found!');
            return $orders;
        }
    }
}

function modelGetSpecficOrderByTracked($orderNumber, $db) {
    $OldOrderNumber = '';
    $orderItems = array();
    if ($db == 's4') {
        $con = modelLoginDB('S4Admin', $errormsg);
    } else if ($db == 'mm') {
        $con = modelLoginDB('MMAdmin', $errormsg);
    }
    $q = "SELECT i.Items_ID,i.Items_Order_Number,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity,Items_Storeman_Received,p.Project_Description FROM `Items` i INNER JOIN OrderNo o ON (o.OrderNo_Number =i.Items_Order_Number) INNER JOIN Project p ON (p.Project_Code=o.OrderNo_Project)  WHERE Items_Order_Number = '$orderNumber' AND Items_StorageType = '1'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $orderItems[] = array(false, 'An error occured while retrieving order items.' . mysql_error());
        return $orderItems;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $orderItems[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], round($row['Items_Quantity']), $row['Items_ID'], $row['Items_Storeman_Received'], $row['Items_Supplier_Code'], $row['Project_Description']);
            }
            return $orderItems;
        } else {
            $orderItems[] = array(false, 'No order items found!');
            return $orderItems;
        }
    }
}

function modelGetSpecficOrderByTrackedResell($orderNumber, $db) {
    $orderItems = array();
    if ($db == 's4') {
        $con = modelLoginDB('S4Admin', $errormsg);
    } else if ($db == 'mm') {
        $con = modelLoginDB('MMAdmin', $errormsg);
    }
    $q = "SELECT i.Items_ID,i.Items_Order_Number,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity,Items_Storeman_Received,p.Project_Description FROM `Items` i "
            . "INNER JOIN OrderNo o ON (o.OrderNo_Number =i.Items_Order_Number)"
            . " INNER JOIN Project p ON (p.Project_Code=o.OrderNo_Project) WHERE Items_Order_Number = '$orderNumber' AND Items_StorageType = '2'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $orderItems[] = array(false, 'An error occured while retrieving order items.' . mysql_error());
        return $orderItems;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $orderItems[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], round($row['Items_Quantity']), $row['Items_ID'], $row['Items_Storeman_Received'], $row['Items_Supplier_Code'], $row['Project_Description']);
            }
            return $orderItems;
        } else {
            $orderItems[] = array(false, 'No order items found!');
            return $orderItems;
        }
    }
}

function modelUpdateQuantityReceived($db, $id, $value) {
    $status = array();
    $qtyOrdered = '';
    if ($db == 's4') {
        $con = modelLoginDB('S4Admin', $errormsg);
    } else if ($db == 'mm') {
        $con = modelLoginDB('MMAdmin', $errormsg);
    }
    $q = "SELECT i.Items_Quantity FROM `Items` i INNER JOIN OrderNo o ON (o.OrderNo_Number =i.Items_Order_Number) WHERE i.Items_ID = '$id'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'An error occured while retrieving quantity ordered value.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $qtyOrdered = round($row['Items_Quantity']);
            }
        }
    }
    if ($value > $qtyOrdered) {
        $status [] = array(false, "Quantity received cannot be greater than quantity ordered.");
        return $status;
    } else {
        if (is_numeric($value)) {
            $q = "UPDATE `Items` SET Items_Storeman_Received= '$value' WHERE Items_ID=$id";
            if (!mysql_query($q, $con)) {
                $status[] = array(false, 'Quantity not updated!' . mysql_error());
                return $status;
            } else {
                $status[] = array(true, 'Quantity updated successfully!');
                return $status;
            }
        } else {
            $status[] = array(false, 'Please enter a valid quantity value.');
            return $status;
        }
    }
}

function modelInsertSpecficAmountOfItems($itemId, $numberOfItems, $db) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $i = 0;
    while ($i < $numberOfItems) {
        $q = "INSERT INTO `items_barcode` (items_id,db) VALUES ('$itemId','$db')";
        $i = $i + 1;
        if (!mysql_query($q, $con)) {
            $status[] = array(false, 'Item not added' . mysql_error());
            return $status;
        }
    }
    $status[] = array(true, 'Items added successfully');
    return $status;
}

function modelInsertSpecficAmountOfOrderItems($itemId, $numberOfItems, $db, $ProjectCode) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $i = 0;
    while ($i < $numberOfItems) {
        $q = "INSERT INTO `items_barcode` (items_id,db,Project_code) VALUES ('$itemId','$db','$ProjectCode')";
        $i = $i + 1;
        if (!mysql_query($q, $con)) {
            $status[] = array(false, 'Item not added' . mysql_error());
            return $status;
        }
    }
    $status[] = array(true, 'Items added successfully');
    return $status;
}

function modelGetProjectCodeByItemsId($id, $dbName) {
    $status = array();
    $con = '';
    $con = modelConnectToServer($errormsg);
    $q = "SELECT o.OrderNo_Project
            FROM $dbName.`OrderNo` o
            INNER JOIN $dbName.Items i ON (i.Items_Order_Number = o.OrderNo_Number)
             WHERE i.Items_ID = '$id'";

    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'An error occured while retrieving barcode details.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, $row['OrderNo_Project']);
                return $status;
            }
        } else {
            $status[] = array(false, "No Item found");
            return $status;
        }
    }
}

function modelGetAllBarcodeDetails() {
    $status = array();
    $con = '';
    $con = modelConnectToServer($errormsg);
    $q = "SELECT i.`Items_Order_Number`,
               i.`Items_Description`,
               b.`barcode_value`,
               s.barcode,
               s.`description`
            FROM `S4Admin`.`Items` i
            JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
            JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
            JOIN `stock_control`.`shelf` s ON (b.shelf_id= s.id)";

    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array('An error occured while retrieving barcode details.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], $row['barcode_value'], $row['barcode'], $row['description']);
            }
            return $status;
        } else {
            $status[] = array('No barcodes found!');
            return $status;
        }
    }
}

function modelGetBarcodeScannedItems() {
    $status = array();
    $con = '';
    $con = modelConnectToServer($errormsg);
    $q = "SELECT i.`Items_Order_Number`,
        i.`Items_Description`,
        b.`barcode_value`
        FROM `S4Admin`.`Items` i
        JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
        JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)";

    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array('An error occured while retrieving items.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], $row['barcode_value']);
            }
            return $status;
        } else {
            $status[] = array('No items found!');
            return $status;
        }
    }
}

function modelGetAllPrintedItems() {
    $printItems = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT 	ib.`items_barcode_id`,
        i.`Items_Order_Number`,
        i.`Items_Description`,
        b.`barcode_value`,
        i.Items_Supplier_Code,
        ib.`db`
        FROM `S4Admin`.`Items` i
        LEFT JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
        LEFT JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
        WHERE ib.status='0' ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $printItems[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $printItems;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {

            while ($row = mysql_fetch_array($result)) {
                $printItems[] = array(true, $row['items_barcode_id'], $row['Items_Order_Number'], utf8_encode($row['Items_Description']), $row['barcode_value'], $row['db'], $row['Items_Supplier_Code']);
            }
        }
        $q = "SELECT ib.`items_barcode_id`,
                    i.`Items_Order_Number`,
                    i.`Items_Description`,
                    b.`barcode_value`,
                    ib.`db`,
                    i.Items_Supplier_Code
                    FROM `MMAdmin`.`Items` i
                    LEFT JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
                    LEFT JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                    WHERE ib.status='0'";
        $result = mysql_query($q, $con);
        if (!$result) {
            $printItems[] = array(false, 'An error occured while retrieving items.' . mysql_error());
            return $printItems;
        } else {

            $num_results = mysql_num_rows($result);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $printItems[] = array(true, $row['items_barcode_id'], $row['Items_Order_Number'], utf8_encode($row['Items_Description']), $row['barcode_value'], $row['db'], $row['Items_Supplier_Code']);
                }
            }
            $q = "SELECT ib.`items_barcode_id`,
                                i.`Order_Number`,
                                i.`description`,
                                b.`barcode_value`,
                                ib.`db`,
                                i.part_number
                                FROM `stock_control`.`no_order_items` i
                                LEFT JOIN `stock_control`.`items_barcode` ib ON (i.items_id = ib.items_id)
                                LEFT JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                                WHERE  ib.status='0' "; //AND i.Order_Number IS NULL
            $result = mysql_query($q, $con);
            if (!$result) {
                $printItems[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $printItems;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $printItems[] = array(true, $row['items_barcode_id'], $row['Order_Number'], utf8_encode($row['description']), $row['barcode_value'], $row['db'], $row['part_number']);
                    }
                }
            }
        }
    }
    if (empty($printItems)) {
        $printItems[] = array(false, 'No items found!');
        $_SESSION['AssignBarcodeAmount'] = '';
        return $printItems;
    } else {
        //Return items result
        $_SESSION['AssignBarcodeAmount'] = count($printItems);
        ;
        return $printItems;
    }
}

function modelMobileGetAllPrintedItems() {
    $printItems = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT distinct ib.`items_barcode_id`,
        i.`Items_Order_Number`,
        i.`Items_Description`,
        b.`barcode_value`,
        ib.`db`,
        i.Items_Supplier_Code,
        p.Project_Description
        FROM `S4Admin`.`Items` i
        LEFT JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
        LEFT JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
         LEFT JOIN `S4Admin`.`OrderNo` o ON (o.OrderNo_Number=i.Items_Order_Number)
        LEFT JOIN `S4Admin`.`Project` p ON (p.Project_Code=o.OrderNo_Project)
        WHERE ib.status='0' ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $printItems[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $printItems;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $printItems[] = array(true, $row['items_barcode_id'], $row['Items_Order_Number'], $row['Items_Description'], $row['barcode_value'], $row['db'], $row['Items_Supplier_Code'], $row['Project_Description']);
            }
        }

        $q = "SELECT distinct	ib.`items_barcode_id`,
                 i.`Items_Order_Number`,
                 i.`Items_Description`,
                 b.`barcode_value`,
                 ib.`db`,
                 i.Items_Supplier_Code,
                 Project_Description
                 FROM `MMAdmin`.`Items` i
                 LEFT JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
                 LEFT JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                 LEFT JOIN `MMAdmin`.`OrderNo` o ON (o.OrderNo_Number=i.Items_Order_Number)
                 LEFT JOIN `MMAdmin`.`Project` p ON (p.Project_Code=o.OrderNo_Project)
                 WHERE ib.status='0'";
        $result = mysql_query($q, $con);
        if (!$result) {
            $printItems[] = array(false, 'An error occured while retrieving items.' . mysql_error());
            return $printItems;
        } else {

            $num_results = mysql_num_rows($result);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $printItems[] = array(true, $row['items_barcode_id'], $row['Items_Order_Number'], $row['Items_Description'], $row['barcode_value'], $row['db'], $row['Items_Supplier_Code'], $row['Project_Description']);
                }
            }
            $q = "SELECT distinct ib.`items_barcode_id`,
                         i.`Order_Number`,
                         i.`description`,
                         b.`barcode_value`,
                         ib.`db`
                          FROM `stock_control`.`no_order_items` i
                         LEFT JOIN `stock_control`.`items_barcode` ib ON (i.items_id = ib.items_id)
                         LEFT JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                         WHERE ib.status='0'";
            $result = mysql_query($q, $con);
            if (!$result) {
                $printItems[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $printItems;
            } else {

                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $printItems[] = array(true, $row['items_barcode_id'], $row['Order_Number'], $row['description'], $row['barcode_value'], $row['db'], '', '');
                    }
                }
            }
        }
    }//end of everything
    if (empty($printItems)) {
        $printItems[] = array(false, 'No items found!');
        return $printItems;
        ;
    } else {
        return $printItems;
    }
}

function modelGetSpecficMarryItem($itemId) {
    $marryItem = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT stock_control.items_barcode.db, stock_control.items_barcode.items_barcode_id,S4Admin.Items.Items_Order_Number,S4Admin.Items.Items_Description FROM
            S4Admin.Items INNER JOIN stock_control.items_barcode ON
            S4Admin.Items.Items_ID = stock_control.items_barcode.items_id WHERE  stock_control.items_barcode.items_barcode_id =$itemId ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $marryItem[] = array(false, 'An error occured while retrieving item.' . mysql_error());
        return $marryItem;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $marryItem[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], $row['items_barcode_id'], $row['db']);
            }
            return $marryItem;
        } else {
            $marryItem[] = array(false, 'item not found!');
            return $marryItem;
        }
    }
}

function modelUpdateBarcodes($id, $itemBarcode, $shelfbarcode) {
    ######TRANSACTIONS TO BE USED HERE
    $status = array();
    $newShelfId = "";
    $barcodeId = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `id` FROM shelf WHERE barcode = '$shelfbarcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to get data from shelf table' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $newShelfId = $row['id'];
            }
        } else {
            $q = "INSERT INTO `shelf` (barcode,description,location_id) VALUES ('$shelfbarcode','Auto','1')";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Unable to add shelf barcode value to shelf table.' . mysql_error());
                return $status;
            } else {
                $q = "SELECT `id` FROM shelf WHERE barcode = '$shelfbarcode'";
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to find shelf barcode in the shelf table.' . mysql_error());
                    return $status;
                } else {

                    $num_results = mysql_num_rows($result);
                    if ($num_results > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            $newShelfId = $row['id'];
                        }
                    }
                }
            }
        }
    }
    //got shelf id
    $q = "SELECT `barcode_id` FROM barcode WHERE barcode_value = '$itemBarcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to get data from barcode table' . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $barcodeId = $row['barcode_id'];
            }
        } else {
            $q = "INSERT INTO barcode (barcode_value) VALUES ('$itemBarcode')";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, "Unable to add barcode value to barcode table." . mysql_error());
                return $status;
            } else {
                $barcodeId = mysql_insert_id();
            }
        }
    }
    //got item id an shelf id

    $q = "UPDATE items_barcode SET barcode_id='$barcodeId',shelf_id='$newShelfId',`status`='1' WHERE items_barcode_id=$id";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to add item barcode to database.' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, "done");
        return $status;
    }
}

function modelUpdateItemAndShelfBarcodes($itemBarcode, $shelfbarcode) {
    ######TRANSACTIONS TO BE USED HERE
    $status = array();
    $newShelfId = "";
    $barcodeId = '';
    $items_barcode_id = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `id` FROM shelf WHERE barcode = '$shelfbarcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to get data from shelf table' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $newShelfId = $row['id'];
            }
        } else {
            $q = "INSERT INTO `shelf` (barcode,description,location_id) VALUES ('$shelfbarcode','Auto','1')";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Unable to add shelf barcode value to shelf table.' . mysql_error());
                return $status;
            } else {
                $q = "SELECT `id` FROM shelf WHERE barcode = '$shelfbarcode'";
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to find shelf barcode in the shelf table.' . mysql_error());
                    return $status;
                } else {

                    $num_results = mysql_num_rows($result);
                    if ($num_results > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            $newShelfId = $row['id'];
                        }
                    }
                }
            }
        }
    }
    //got shelf id
    $q = "SELECT b.barcode_id,ib.items_barcode_id FROM barcode b INNER JOIN items_barcode ib ON(ib.barcode_id = b.barcode_id) WHERE barcode_value = '$itemBarcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to get data from barcode table' . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $barcodeId = $row['barcode_id'];
                $items_barcode_id = $row['items_barcode_id'];
            }
        } else {
            $status[] = array(false, "$itemBarcode -> Item Barcode does not exist.");
            return $status;
        }
    }
    //got item id and shelf id
    $q = "SELECT ib.shelf_id,s.location_id FROM shelf s INNER JOIN items_barcode ib on (ib.old_shelf= s.`id`) WHERE ib.barcode_id='$barcodeId' ";
    $isUser = '';
    $oldShelf = '';
    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {
        while ($row = mysql_fetch_array($result)) {
            $isUser = $row['location_id'];
            $oldShelf = $row['shelf_id'];
        }
    }
    if (empty($oldShelf)) {
        $q = "UPDATE items_barcode SET shelf_id='$newShelfId',`status`='1' WHERE barcode_id='$barcodeId'";
    } else {
        $q = "UPDATE items_barcode SET old_shelf = shelf_id, shelf_id='$newShelfId',`status`='1' WHERE barcode_id='$barcodeId'";
    }

    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to add item barcode to database.' . mysql_error());
        return $status;
    } else {
        if ($isUser == '3') { //is user not shelf
            $q = "DELETE shelf where `id` = '$oldShelf'";
            mysql_query($q, $con);
        }

        $status[] = array(true, "done");
        return $status;
    }
}

function modelShelfWithUser($username, $description) {
    $status = array();
    $userId = '';
    //assign item to user
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO shelf (barcode,description,location_id) VALUES ('$username','$description','3')";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to add user to location.' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, mysql_insert_id());
        return $status;
    }
}

function modelUpdateItemBarcodeWithUser($shelfId, $itemBarcode) {
    $status = array();
    $barcodeId = "";
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `barcode_id` FROM barcode WHERE barcode_value = '$itemBarcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to get id from barcode table.' . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $barcodeId = $row['barcode_id'];
            }
            $q = "SELECT ib.shelf_id,s.location_id FROM shelf s INNER JOIN items_barcode ib on (ib.old_shelf= s.`id`) WHERE ib.barcode_id=$barcodeId ";
            $isUser = '';
            $oldShelf = '';
            $result = mysql_query($q, $con);
            $num_results = mysql_num_rows($result);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $isUser = $row['location_id'];
                    $oldShelf = $row['shelf_id'];
                }
            }
            if (empty($oldShelf)) {
                $q = "UPDATE items_barcode SET shelf_id='$shelfId' WHERE barcode_id = '$barcodeId'";
            } else {
                $q = "UPDATE items_barcode SET old_shelf=shelf_id,shelf_id='$shelfId' WHERE barcode_id = '$barcodeId'";
            }

            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Unable to assign item to user.' . mysql_error());
                return $status;
            } else {
                if ($isUser == '3') { //is user not shelf
                    $q = "DELETE shelf where `id` = $oldShelf";
                    mysql_query($q, $con);
                }

                $status[] = array(true, 'done');
                return $status;
            }
        } else {
            $status[] = array(false, "$itemBarcode -> Item Barcode does not exist.");
            return $status;
        }
    }
}

function modelUpdateItemBarcode($id, $itemBarcode) {
    ######TRANSACTIONS TO BE USED HERE
    $status = array();
    $itemId = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `barcode_id` FROM barcode WHERE barcode_value = '$itemBarcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to get data from item table' . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $itemId = $row['barcode_id'];
            }
            $q = "UPDATE items_barcode SET barcode_id='$itemId',status='1' WHERE items_barcode_id=$id";
            if (!mysql_query($q, $con)) {
                $status[] = array(false, "Failed to add item barcode to database. Barcode ($itemBarcode) already in use. Please enter another item barcode");
                return $status;
            } else {
                $status[] = array(true, "Barcode assigned successfully");
            }
        } else {
            $q = "INSERT INTO barcode (barcode_value) VALUES ('$itemBarcode')";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Failed to add barcode value to barcode table.' . mysql_error());
                return $status;
            } else {
                $barId = mysql_insert_id();
                $q = "UPDATE items_barcode SET barcode_id='$barId',status='1' WHERE items_barcode_id=$id";
                if (!mysql_query($q, $con)) {
                    $status[] = array(false, 'Failed to add item barcode  to database.' . mysql_error());
                    return $status;
                } else {
                    $status[] = array(true, "Barcode assigned successfully");
                    return $status;
                }
            }
        }
    }
}

function modelMobileGetItemDescription($id) {
    $status = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT stock_control.items_barcode.items_id,stock_control.items_barcode.db FROM stock_control.items_barcode WHERE  stock_control.items_barcode.items_barcode_id ='$id' ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to retrieve item information.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['db'] == 's4') {
                    $q = "SELECT S4Admin.Items.Items_Description FROM S4Admin.Items WHERE  S4Admin.Items.Items_ID = " . $row['items_id'];
                } elseif ($row['db'] == 'mm') {
                    $q = "SELECT MMAdmin.Items.Items_Description FROM MMAdmin.Items WHERE  MMAdmin.Items.Items_ID = " . $row['items_id'];
                } else {
                    $q = "SELECT stock_control.no_order_items.description AS Items_Description FROM stock_control.no_order_items WHERE  stock_control.no_order_items.items_id = " . $row['items_id'];
                }
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to retrieve item description.' . mysql_error());
                    return $status;
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        $status[] = array(true, $row['Items_Description']);
                        return $status;
                    }
                }
            }
            return $status;
        } else {
            $status[] = array(false, "No item description found!");
            return $status;
        }
    }
}

function modelResetLocation($barcode) {
    $status = array();
    $db = '';
    $shelfId = '';
    $con = modelConnectToServer($errormsg);
    $q = "SELECT a.shelf_id FROM stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id            WHERE b.barcode_value = '$barcode' ";
    $result = mysql_query($q, $con);
    if ($result) {
        while ($row = mysql_fetch_array($result)) {
            $shelfId = $row['shelf_id'];
        }
    }
    if (empty($shelfId)) {
        $status[] = array(false, 'Barcode does not exist!');
        return $status;
    } else {
        $q = "SELECT ib.old_shelf,s.location_id FROM shelf s INNER JOIN items_barcode ib on (ib.old_shelf= s.`id`) INNER JOIN barcode b ON (ib.barcode_id = b.barcode_id)  WHERE b.barcode_value = '$barcode' ";
        $isUser = '';
        $oldShelf = '';
        $result = mysql_query($q, $con);
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $isUser = $row['location_id'];
                $oldShelf = $row['old_shelf'];
            }
        }

        $q = "Update stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id SET a.old_shelf= a.shelf_id WHERE  b.barcode_value = '$barcode' ";

        $result = mysql_query($q, $con);
        if (!$result) {
            $status = array(false, 'Unable to relocate of item.' . mysql_error());
            return $status;
        } else {
            if ($isUser == '3') { //is user not shelf
                $q = "DELETE shelf where `id` = $oldShelf";
                mysql_query($q, $con);
            }
            $q = "Update stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id SET a.`shelf_id` = NULL WHERE  b.barcode_value = '$barcode' ";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Unable to reset location of item.' . mysql_error());
                return $status;
            } else {
                $q = "SELECT ib.db FROM stock_control.`items_barcode`ib INNER JOIN stock_control.`barcode` b ON ib.barcode_id = b.barcode_id WHERE b.barcode_value = '$barcode'";
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to get database of item.' . mysql_error());
                    return $status;
                } else {
                    $num_results = mysql_num_rows($result);
                    if ($num_results > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            $db = $row['db'];
                        }
                    } else {
                        $status[] = array(false, 'Barcode does not exist!');
                        return $status;
                    }
                }
                if ($db == 's4') {
                    $q = "SELECT
                    i.`Items_Order_Number`,
                    b.`barcode_value`,
                    i.`Items_Description`,
                    s.`barcode`,
                    s.`description`,
                    l.`name`
                    FROM `S4Admin`.`Items` i
                    INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
                    INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                    INNER JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf)
                    INNER JOIN `stock_control`.`location` l ON (l.id =s.location_id)
                    WHERE b.barcode_value = '$barcode'";
                } else {
                    $q = "SELECT
                    i.`Items_Order_Number`,
                    b.`barcode_value`,
                    i.`Items_Description`,
                    s.`barcode`,
                    s.`description`,
                    l.`name`
                    FROM `MMAdmin`.`Items` i
                    INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
                    INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                    INNER JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf)
                    INNER JOIN `stock_control`.`location` l ON (l.id =s.location_id)
                    WHERE b.barcode_value = '$barcode'";
                }
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to retrieve items.' . mysql_error());
                    return $status;
                } else {
                    $num_results = mysql_num_rows($result);
                    if ($num_results > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            $status[] = array(true, $row['Items_Order_Number'], $row['barcode_value'], $row['Items_Description'], $row['barcode'], $row['description'], $row['name']);
                            return $status;
                        }
                    } else {
                        $status[] = array(false, 'No items found!');
                        return $status;
                    }
                }
            }
        }
    }
}

function modelMobileResetLocation($barcode) {
    $status = array();
    $db = '';
    $shelfId = '';
    $con = modelConnectToServer($errormsg);
    $q = "SELECT a.shelf_id FROM stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id            WHERE b.barcode_value = '$barcode' ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status = array(false, 'Unable to relocate of item.' . mysql_error());
        return $status;
    } else {
        while ($row = mysql_fetch_array($result)) {
            $shelfId = $row['shelf_id'];
        }
    }
    if (empty($shelfId)) {
        $status[] = array(false, 'Barcode does not exist!');
        return $status;
    } else {
        $q = "SELECT ib.old_shelf,s.location_id FROM shelf s INNER JOIN items_barcode ib on (ib.old_shelf= s.`id`) "
                . "INNER JOIN barcode ib on (ib.barcode_id= b.barcode_id) WHERE b.barcode_value = '$barcode'";
        $isUser = '';
        $oldShelf = '';
        $result = mysql_query($q, $con);
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $isUser = $row['location_id'];
                $oldShelf = $row['old_shelf'];
            }
        }
        $q = "Update stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id SET a.old_shelf= a.shelf_id WHERE  b.barcode_value = '$barcode' ";

        $result = mysql_query($q, $con);
        if (!$result) {
            $status = array(false, 'Unable to relocate of item.' . mysql_error());
            return $status;
        } else {
            if ($isUser == '3') { //is user not shelf
                $q = "DELETE shelf where `id` = $oldShelf";
                mysql_query($q, $con);
            }
            $q = "Update stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id SET a.`shelf_id` = NULL WHERE  b.barcode_value = '$barcode' ";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Unable to reset location of item.' . mysql_error());
                return $status;
            } else {
                $q = "SELECT ib.db FROM stock_control.`items_barcode`ib INNER JOIN stock_control.`barcode` b ON ib.barcode_id = b.barcode_id WHERE b.barcode_value = '$barcode'";
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to get database of item.' . mysql_error());
                    return $status;
                } else {
                    $num_results = mysql_num_rows($result);
                    if ($num_results > 0) {
                        while ($row = mysql_fetch_array($result)) {
                            $db = $row['db'];
                        }
                    } else {
                        $status[] = array(false, 'Barcode does not exist!');
                        return $status;
                    }
                }
                if ($db == 's4') {
                    $q = "SELECT
                    i.`Items_Order_Number`,
                    b.`barcode_value`,
                    i.`Items_Description`,
                    s.`barcode`,
                    s.`description`,
                    l.`name`
                    FROM `S4Admin`.`Items` i
                    INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
                    INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                    INNER JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf)
                    INNER JOIN `stock_control`.`location` l ON (l.id =s.location_id)
                    WHERE b.barcode_value = '$barcode'";
                } else {
                    $q = "SELECT
                    i.`Items_Order_Number`,
                    b.`barcode_value`,
                    i.`Items_Description`,
                    s.`barcode`,
                    s.`description`,
                    l.`name`
                    FROM `MMAdmin`.`Items` i
                    INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id)
                    INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)
                    INNER JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf)
                    INNER JOIN `stock_control`.`location` l ON (l.id =s.location_id)
                    WHERE b.barcode_value = '$barcode'";
                }
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Unable to retrieve items.' . mysql_error());
                    return $status;
                } else {
                    $num_results = mysql_num_rows($result);
                    if ($num_results > 0) {
                        return "done";
                    } else {
                        $status[] = array(false, 'No items found!');
                        return $status;
                    }
                }
            }
        }
    }
}

function modelSetLocationEmpty($barcode) {
    $status = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.old_shelf, s.location_id FROM stock_control.shelf s INNER JOIN stock_control.items_barcode ib on (ib.old_shelf= s.`id`) INNER JOIN stock_control.barcode b on (ib.barcode_id= b.barcode_id) WHERE b.barcode_value = '$barcode'";
    $oldShelf = '';
    $isUser = '';
    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {
        while ($row = mysql_fetch_array($result)) {
            $isUser = $row['location_id'];
            $oldShelf = $row['old_shelf'];
        }
    }
    $q = "Update stock_control.`items_barcode` a INNER JOIN stock_control.`barcode` b ON a.barcode_id = b.barcode_id SET"
            . " a.old_shelf= a.shelf_id, shelf_id = null WHERE b.barcode_value = '$barcode' ";

    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to reset item location' . mysql_error());
        return $status;
    } else {
        if ($isUser == '3') { //is user not shelf
            $q = "DELETE shelf where `id` = $oldShelf";
            mysql_query($q, $con);
        }
        $status[] = array(true, 'done');
        return $status;
    }
}

function modelGetAllTrackedReturnItems() {
    $status = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.`items_barcode_id`, i.`Items_Order_Number`,i.Items_Supplier_Code,i.`Items_Description`, b.`barcode_value`,p.Project_Description, s.`barcode`, s.`description`, l.`name`"
            . " FROM `S4Admin`.`Items` i INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id) INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id)"
            . " LEFT JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf) Left JOIN `stock_control`.`location` l ON (l.id =s.location_id)"
            . "INNER JOIN `S4Admin`.`OrderNo` o ON (o.OrderNo_Number = i.Items_Order_Number)"
            . "INNER JOIN `S4Admin`.`Project` p ON (p.Project_Code =o.OrderNo_Project)"
            . " WHERE ib.shelf_id IS NULL AND ib.status = '1' AND ib.db = 's4'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to retrieve items.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $status[] = array(true, $row['items_barcode_id'], $row['Items_Order_Number'], $row['Items_Supplier_Code'], utf8_encode(trim($row['Items_Description'], '"')), $row['Project_Description'], $row['barcode_value'], 's4', $row['barcode'], $row['description'], $row['name'], $row['Items_Supplier_Code']);
            }
        }
        $q = "SELECT ib.`items_barcode_id`, i.`Items_Order_Number`,i.Items_Supplier_Code,i.`Items_Description`, b.`barcode_value`,p.Project_Description, s.`barcode`, s.`description`, l.`name` "
                . "FROM `MMAdmin`.`Items` i INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id) INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id) "
                . "LEFT JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf) Left JOIN `stock_control`.`location` l ON (l.id =s.location_id) "
                . "INNER JOIN `MMAdmin`.`OrderNo` o ON (o.OrderNo_Number = i.Items_Order_Number)"
                . "INNER JOIN `MMAdmin`.`Project` p ON (p.Project_Code =o.OrderNo_Project)"
                . "WHERE ib.shelf_id IS NULL AND ib.status = '1' AND ib.db = 'mm'";
        $result = mysql_query($q, $con);
        if (!$result) {
            $status[] = array(false, 'Unable to retrieve items.' . mysql_error());
            return $status;
        } else {

            $num_results = mysql_num_rows($result);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $status[] = array(true, $row['items_barcode_id'], $row['Items_Order_Number'], $row['Items_Supplier_Code'], utf8_encode(trim($row['Items_Description'])), $row['Project_Description'], $row['barcode_value'], 'mm', $row['barcode'], $row['description'], $row['name'], $row['Items_Supplier_Code']);
                }
            }
            $q = "SELECT ib.`items_barcode_id`, i.`Order_Number`, b.`barcode_value`, i.`description` AS `item description`, s.`barcode`, s.`description`, l.`name` "
                    . "FROM stock_control.`no_order_items` i "
                    . "INNER JOIN `stock_control`.`items_barcode` ib ON (i.Items_ID = ib.items_id) "
                    . "INNER JOIN `stock_control`.`barcode` b ON (b.barcode_id=ib.barcode_id) "
                    . "LEFT JOIN `stock_control`.`shelf` s ON (s.id = ib.old_shelf) "
                    . "Left JOIN `stock_control`.`location` l ON (l.id =s.location_id) "
                    . "WHERE ib.shelf_id IS NULL AND ib.status = '1' AND ib.db = 'no'";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Unable to retrieve items.' . mysql_error());
                return $status;
            } else {

                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $status[] = array(true, $row['items_barcode_id'], "No Order Number", "", utf8_encode(trim($row['item description'])), "", $row['barcode_value'], 'mm', $row['barcode'], $row['description'], $row['name'], $row['Items_Supplier_Code']);
                    }
                }
            }
        }
    }
    if (empty($status)) {
        $status[] = array(false, 'No items found!');
        $_SESSION['relocationAmount'] = "";
        return $status;
    } else {
        $_SESSION['relocationAmount'] = count($status);
        return $status;
    }
}

function modelUpdateShelfBarcode($id, $barcode) {
    $status = array();
    $itemID = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `id` FROM `shelf` WHERE `barcode` = '$barcode'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to retrieve shelf barcode.');
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $itemID = $row['id'];
            }
            $q = "UPDATE `items_barcode` SET `shelf_id`= $itemID , `status`= '1' WHERE items_barcode_id='$id'";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Failed to save the new shelf barcode.' . mysql_error());
                return $status;
            } else {
                $status[] = array(true, 'Shelf barcode saved successfully.');
                return $status;
            }
        } else {
            $q = "INSERT INTO `shelf` (barcode,description,location_id) VALUES ('$barcode','Auto','1')";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, 'Failed to add the new shelf barcode.' . mysql_error());
                return $status;
            } else {
                $barId = mysql_insert_id();
                $q = "UPDATE `items_barcode` SET `shelf_id` = $barId, `status`= '1' WHERE items_barcode_id='$id'";
                $result = mysql_query($q, $con);
                if (!$result) {
                    $status[] = array(false, 'Failed to save the new shelf barcode.' . mysql_error());
                    return $status;
                } else {
                    $status[] = array(true, 'Shelf barcode saved successfully.');
                    return $status;
                }
            }
        }
    }
}

function modelInsertNoOrderItem($description, $partNumber, $qty) {
    $status = array();
    $Itemid = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO `no_order_items` (description,part_number,Items_storeman_Received) VALUES ('$description','$partNumber','$qty')";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to add item!' . mysql_error());
        return $status;
    } else {
        $Itemid = mysql_insert_id();
        $i = 0;
        while ($i < $qty) {
            $q = "INSERT INTO `items_barcode` (items_id,db) VALUES ('$Itemid','no')";
            $i = $i + 1;
            if (!mysql_query($q, $con)) {
                $status[] = array(false, 'Failed to add item!' . mysql_error());
                return $status;
            }
        }
        $status[] = array(true, 'Item Captured successfully!');
        return $status;
    }
}

function modelCaptureUntrackedOrderedItem($orderNumber, $description, $qty) {
    $status = array();
    $Itemid = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO `no_order_items` (Order_Number,description,Items_storeman_Received) VALUES ('$orderNumber','$description','$qty')";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to add item!' . mysql_error());
        return $status;
    } else {
        $Itemid = mysql_insert_id();
        $i = 0;
        while ($i < $qty) {
            $q = "INSERT INTO `items_barcode` (items_id,db) VALUES ('$Itemid','no')";
            $i = $i + 1;
            if (!mysql_query($q, $con)) {
                $status[] = array(false, 'Failed to add item!' . mysql_error());
                return $status;
            }
        }
        $status[] = array(true, 'Item Captured successfully!');
        return $status;
    }
}

function modelGetNoOrderItems() {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT no_order_items_id, description FROM `no_order_items` WHERE shelf_id IS NULL AND shelf_id IS NULL";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to retrieve items.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, $row['no_order_items_id'], $row['description']);
            }
            return $status;
        } else {
            $status[] = array(false, "No items found!");
            return $status;
        }
    }
}

function modeMobilelGetNoOrderItems() {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `no_order_items_id`, `description`,b.barcode_value FROM `no_order_items` n LEFT JOIN `barcode` b ON (b.barcode_id=n.barcode_id) WHERE n.shelf_id IS NULL ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to retrieve items!' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                $status[] = array(true, $row['no_order_items_id'], $row['description']);
            }
            return $status;
        } else {
            $status[] = array(false, "No items found!");
            return $status;
        }
    }
}

function modelMobileUdpRequest($msg, $timeout, &$result) {
    $status = array();
    $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    $len = strlen($msg);
    $receiverIP = '192.168.0.168';
    $receiverPort = 11000;
    socket_sendto($sock, $msg, $len, 0, $receiverIP, $receiverPort);
    $from = '';
    $port = 0;
    $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
    socket_bind($socket, '0.0.0.0', 11001);
    socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array("sec" => $timeout, "usec" => 0));
    socket_recvfrom($socket, $buf, 4096, 0, $from, $port);
    if ($buf != "") { //timeout data received
        if ($buf == 'OK') {
            $status[] = array(true, 'Barcodes Printed.');
            closeSocket($sock);
            closeSocket($socket);
            return $status;
        } else {
            $errormsg = substr($buf, strpos($buf, ':') + 1);
            $status[] = array(false, $errormsg);
            closeSocket($sock);
            closeSocket($socket);
            return $status;
        }
    } else { //timeout no data received
        $status[] = array(false, 'Network Error: No data was received in the given time frame.');
        closeSocket($sock);
        closeSocket($socket);
        return $status;
    }
}

function modelGetSpecficOrderByUntracked($orderNumber, $db) {
    $orderItems = array();
    if ($db == 's4') {
        $con = modelLoginDB('S4Admin', $errormsg);
    } else if ($db == 'mm') {
        $con = modelLoginDB('MMAdmin', $errormsg);
    }
    $q = "SELECT i.Items_ID,i.Items_Order_Number,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity,i.Items_Storeman_Received,p.Project_Description FROM `Items` i "
            . "INNER JOIN OrderNo o ON (o.OrderNo_Number =i.Items_Order_Number)"
            . " INNER JOIN Project p ON (p.Project_Code=o.OrderNo_Project) WHERE Items_Order_Number = '$orderNumber' AND Items_StorageType = '3'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $orderItems[] = array(false, 'Unable to retrieve items.' . mysql_error());
        return $orderItems;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $orderItems[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], round($row['Items_Quantity']), $row['Items_ID'], $row['Items_Storeman_Received'], $row['Items_Supplier_Code'], $row['Project_Description']);
            }
            return $orderItems;
        } else {
            $status[] = array(false, "No items found!");
            return $status;
        }
    }
}

function modelGetSpecficOrderByConsumable($orderNumber, $db) {
    $orderItems = array();
    if ($db == 's4') {
        $con = modelLoginDB('S4Admin', $errormsg);
    } else if ($db == 'mm') {
        $con = modelLoginDB('MMAdmin', $errormsg);
    }
    $q = "SELECT i.Items_ID,i.Items_Order_Number,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity,i.Items_Storeman_Received,p.Project_Description FROM `Items` i "
            . "INNER JOIN OrderNo o ON (o.OrderNo_Number =i.Items_Order_Number)"
            . " INNER JOIN Project p ON (p.Project_Code=o.OrderNo_Project) WHERE Items_Order_Number = '$orderNumber' AND Items_StorageType = '4'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $orderItems[] = array(false, 'Unable to retrieve items.' . mysql_error());
        return $orderItems;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $orderItems[] = array(true, $row['Items_Order_Number'], $row['Items_Description'], round($row['Items_Quantity']), $row['Items_ID'], $row['Items_Storeman_Received'], $row['Items_Supplier_Code'], $row['Project_Description']);
            }
            return $orderItems;
        } else {
            $status[] = array(false, "No items found!");
            return $status;
        }
    }
}

function modelInsertConsumable($description, $qty, $type, $userid) {
    $status = array();
    $Itemid = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO `consumable` (`description`,consumable_type_id,user_id) VALUES ('$description','$type','$userid')";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Consumable not added!' . mysql_error());
        return $status;
    } else {
        $Itemid = mysql_insert_id();
        $q = "UPDATE `consumable` SET `quantity_in_store`= `quantity_in_store` + $qty WHERE consumable_id='$Itemid'";
        $result = mysql_query($q, $con);
        if (!$result) {
            $status[] = array(false, ' Failed to update quantity  in store.' . mysql_error());
            return $status;
        } else {
            $status[] = array(true, 'Consumable added successfully!');
            return $status;
        }
    }
}

function modelGetConsumableTypes() {
    $usersTypes = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT consumable_type_id, name,details FROM `consumable_type`"; #
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, 'Unable to retrieve consumble types.' . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $usersTypes[] = array(true, $row['consumable_type_id'], $row['name'], $row['details']);
            }
            return $usersTypes;
        } else {
            $status[] = array(false, "No items found!");
            return $status;
        }
    }
}

function modelConsumableTypeId($qty, $typeId, $userId) {
    $consumable = array();
    $qtyInStore = '';
    $consumableId = '';
    $max_quantity = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT consumable_id,quantity_in_store FROM consumable WHERE consumable_type_id='$typeId'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $consumable[] = array(false, 'Unable to retrieve consumable details.' . mysql_error());
        return $consumable;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $consumableId = $row['consumable_id'];
                $qtyInStore = $row['quantity_in_store'];
            }
            //update existing row in consumable table
            $q = "UPDATE `consumable` SET `quantity_in_store`= `quantity_in_store`+ $qty WHERE consumable_id = $consumableId";
            $result = mysql_query($q, $con);
            if (!$result) {
                $consumable[] = array(false, 'Unable to update quantity in store.' . mysql_error());
                return $consumable;
            } else {
                $q = "SELECT c.quantity_in_store, t.max_qty FROM consumable c INNER JOIN consumable_type t ON c.consumable_type_id = t.consumable_type_id WHERE consumable_id='$consumableId'";
                $result = mysql_query($q, $con);
                if (!$result) {
                    $consumable[] = array(false, 'Unable to get quantity in store.');
                    return $consumable;
                } else {
                    while ($row = mysql_fetch_array($result)) {
                        $qtyInStore = $row['quantity_in_store'];
                        $max_quantity = $row['max_qty'];
                        if ($qtyInStore > $max_quantity) {
                            //send email to manager
                        }
                        $consumable[] = array(true, 'consumable added successfully');
                        return $consumable;
                    }
                }
            }
        } else {
            //insert new row in consumable table
            $q = "INSERT INTO `consumable` (quantity_in_store,consumable_type_id,user_id) VALUES ('$qty','$typeId','$userId')";
            if (!mysql_query($q, $con)) {
                $consumable[] = array(false, 'Consumable not added!' . mysql_error());
                return $consumable;
            } else {
                $consumable[] = array(true, 'consumable added successfully');
                return $consumable;
            }
        }
    }
}

function modelGetConsumables() {
    $consumable = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT c.consumable_id,t.name,t.details,t.description,t.nett_value, c.quantity_in_store,t.supplier_code FROM consumable c INNER JOIN consumable_type t ON c.consumable_type_id = t.consumable_type_id ORDER BY t.name,t.supplier_code";
    $result = mysql_query($q, $con);
    if (!$result) {
        $consumable[] = array(false, "Unable to retrieve consumables" . mysql_error());
        return$consumable;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $consumable[] = array(true, $row['consumable_id'], $row['name'], $row['description'], $row['details'], $row['supplier_code'], $row['quantity_in_store'], $row['nett_value']);
            }
            return $consumable;
        } else {
            $consumable[] = array(false, "No items found!");
            return $consumable;
        }
    }
}

function modelIncreaseQuantityInStore($consumableId, $qty, $userId) {
    $status = array();
    $qty_in_store = '';
    $maxQty = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT c.quantity_in_store,ct.supplier_code,ct.nett_value, ct.min_qty,ct.name,ct.description,ct.details optimal_qty,ct.max_qty FROM consumable c INNER JOIN consumable_type ct ON (c.consumable_type_id= ct.consumable_type_id) WHERE consumable_id= $consumableId";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to get quantity in store." . mysql_error());
        return $status;
    } else {
        while ($row = mysql_fetch_array($result)) {
            $qty_in_store = $row['quantity_in_store'];
            $consumableName = $row['name'];
            $consumableDescription = $row['description'];
            $supplierCode = $row['supplier_code'];
        }
        $totalQtyInStore = $qty_in_store + $qty;

        $q = "UPDATE `consumable` SET user_id=$userId ,`quantity_in_store`= `quantity_in_store`+ $qty WHERE consumable_id = $consumableId";
        $result = mysql_query($q, $con);
        if (!$result) {
            $status[] = array(false, "Unable to update quantity in store." . mysql_error());
            return $status;
        } else {
            $status[] = array(true, "<Strong>Category:</Strong> $consumableName | <Strong>Supplier Code:</Strong> $supplierCode | <Strong>Description:</Strong> $consumableDescription =>  Total quantity in store was $qty_in_store,increased by $qty succesfully. The new Total Quantity = $totalQtyInStore.");
            return $status;
        }
    }
}

function modelDescreaseQuantityInStore($consumableId, $qty, $userId) {
    $status = array();
    $qty_in_store = '';
    $totalQtyInStore = '';
    $minQty = '';
    $StaffEmail = '';
    $consumableName = '';
    $consumableDescription = '';
    $consumableDetails = '';
    $maxQty = '';
    $optimalQty = '';
    $supplierCode = '';
    $netValue = '';
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT c.quantity_in_store,ct.supplier_code,ct.nett_value, ct.min_qty,ct.name,ct.description,ct.details optimal_qty,ct.max_qty FROM consumable c INNER JOIN consumable_type ct ON (c.consumable_type_id= ct.consumable_type_id) WHERE consumable_id= $consumableId";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to get quantity in store." . mysql_error());
        return $status;
    } else {
        while ($row = mysql_fetch_array($result)) {
            $qty_in_store = $row['quantity_in_store'];
            $minQty = (int) $row['min_qty'];
            $consumableName = $row['name'];
            $consumableDescription = $row['description'];
            $consumableDetails = $row['details'];
            $maxQty = $row['max_qty'];
            $optimalQty = $row['optimal_qty'];
            $supplierCode = $row['supplier_code'];
            $netValue = $row['nett_value'];
        }

        $totalQtyInStore = $qty_in_store - $qty;
        if ($qty_in_store >= $qty) {
            $q = "UPDATE `consumable` SET user_id='$userId' ,`quantity_in_store`= `quantity_in_store`- $qty WHERE consumable_id = $consumableId";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, "Unable to update quantity in store." . mysql_error());
                return $status;
            } else {
                $status[] = array(true, "<Strong>Category:</Strong> $consumableName | <Strong>Supplier Code:</Strong> $supplierCode | <Strong>Description:</Strong> $consumableDescription =>  Total quantity in store was $qty_in_store,decreased by $qty succesfully. The new Total Quantity = $totalQtyInStore.");
                if ($totalQtyInStore <= $minQty) {
                    //notify users of low level
                    $email = modelGetNotificationEmails();
                    if ($email[0][0] === true) {
                        foreach ($email as $mailAddress) {
                            modelSendEmail($mailAddress[1], "Consumable Item Low Stock Levels", "<table style='border: 1px solid rgb(204,204,204); border-collapse: collapse;text-align: center;'><thead>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Category</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Description</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Details</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Supplier Code</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Net Value</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Min Quantity</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Optimal Quantity</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Max Quantity</th>"
                                    . "<th style='border: 1px solid rgb(204,204,204);text-align: center;background-color: rgb(20, 70, 120);color:#fff;'>Quantity Level</th>"
                                    . "</thead>"
                                    . "<tbody><tr>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$consumableName</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$consumableDescription</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$consumableDetails</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$supplierCode</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$netValue</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$minQty</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$optimalQty</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$maxQty</td>"
                                    . "<td style='border: 1px solid rgb(204,204,204);text-align: center;'>$totalQtyInStore</td>"
                                    . "</tr></tbody></tbody></table>");
                        }
                    }
                }
                return $status;
            }
        } else {
            $status[] = array(false, "<Strong>Category:</Strong> $consumableName | <Strong>Supplier Code:</Strong> $supplierCode | <Strong>Description:</Strong> $consumableDescription => Entered quantity exceeds the minimum quantity in store.Please order from Supplier.");
            return $status;
            //notify pm to order from Supplier
        }
    }
}

function modelGetItemsByOrder($OrderNo) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type` FROM S4Admin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
WHERE i.Items_Order_Number = '$OrderNo' ORDER BY i.Items_ID ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['Storage Type'] == "1") {
                    $row['Storage Type'] = "Tracked";
                } elseif ($row['Storage Type'] == "2") {
                    $row['Storage Type'] = "Resale Tracked";
                } elseif ($row['Storage Type'] == "3") {
                    $row['Storage Type'] = "Untracked/Resale Untracked";
                }
                $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo);
            }
            return $items;
        } else {

            $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
                    i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type` FROM MMAdmin.Items i
                    LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
                    LEFT JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
                    LEFT JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
                    WHERE i.Items_Order_Number = '$OrderNo' ORDER BY i.Items_ID ";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {

                    while ($row = mysql_fetch_array($result)) {

                        if ($row['Storage Type'] == "1") {
                            $row['Storage Type'] = "Tracked";
                        } elseif ($row['Storage Type'] == "2") {
                            $row['Storage Type'] = "Resale Tracked";
                        } elseif ($row['Storage Type'] == "3") {
                            $row['Storage Type'] = "Untracked/Resale Untracked";
                        }
                        $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo);
                    }

                    return $items;
                } else {

                    $items[] = array(false, "No items found!");
                    return $items;
                }
            }
        }
    }
}

function modelGetItemsByOrderNotReceived($OrderNo) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type` FROM S4Admin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
WHERE i.Items_Order_Number = '$OrderNo' AND i.Items_Quantity != i.Items_Storeman_Received ORDER BY i.Items_ID ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['Storage Type'] == "1") {
                    $row['Storage Type'] = "Tracked";
                } elseif ($row['Storage Type'] == "2") {
                    $row['Storage Type'] = "Resale Tracked";
                } elseif ($row['Storage Type'] == "3") {
                    $row['Storage Type'] = "Untracked/Resale Untracked";
                }
                $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo);
            }
            return $items;
        } else {

            $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
                    i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type` FROM MMAdmin.Items i
                    LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
                    LEFT JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
                    LEFT JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
                    WHERE i.Items_Order_Number = '$OrderNo' AND i.Items_Quantity != i.Items_Storeman_Received ORDER BY i.Items_ID ";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {

                    while ($row = mysql_fetch_array($result)) {

                        if ($row['Storage Type'] == "1") {
                            $row['Storage Type'] = "Tracked";
                        } elseif ($row['Storage Type'] == "2") {
                            $row['Storage Type'] = "Resale Tracked";
                        } elseif ($row['Storage Type'] == "3") {
                            $row['Storage Type'] = "Untracked/Resale Untracked";
                        }
                        $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo);
                    }

                    return $items;
                } else {

                    $items[] = array(false, "No items found!");
                    return $items;
                }
            }
        }
    }
}

function modelGetItemsByOrderItems($orderItemId) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
sh.description AS Shelf,l.description AS Storeroom,i.Items_ID FROM S4Admin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.shelf sh ON (sh.id = ib.shelf_id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.id = sh.location_id)
WHERE i.Items_Order_Number = '$orderItemId' ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'], $row['Storeroom'], $row['Items_ID']);
            }
            return $items;
        } else {
            $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
sh.description AS Shelf,l.description AS Storeroom,i.Items_ID FROM MMAdmin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.shelf sh ON (sh.id = ib.shelf_id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.id = sh.location_id)
WHERE i.Items_Order_Number = '$orderItemId' ";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'], $row['Storeroom'], $row['Items_ID']);
                    }
                    return $items;
                } else {
                    $items[] = array(false, "No items found!");
                    return $items;
                }
            }
        }
    }
}

function modelGetItemsByOrderItemsNotReceived($orderItemId) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
sh.description AS Shelf,l.description AS Storeroom,i.Items_ID FROM S4Admin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.shelf sh ON (sh.id = ib.shelf_id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.id = sh.location_id)
WHERE i.Items_Order_Number = '$orderItemId' AND i.Items_Quantity != i.Items_Storeman_Received";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'], $row['Storeroom'], $row['Items_ID']);
            }
            return $items;
        } else {
            $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
sh.description AS Shelf,l.description AS Storeroom,i.Items_ID FROM MMAdmin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.shelf sh ON (sh.id = ib.shelf_id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.id = sh.location_id)
WHERE i.Items_Order_Number = '$orderItemId' AND i.Items_Quantity != i.Items_Storeman_Received ";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'], $row['Storeroom'], $row['Items_ID']);
                    }
                    return $items;
                } else {
                    $items[] = array(false, "No items found!");
                    return $items;
                }
            }
        }
    }
}

function modelGetItemsByBarcode($barcode) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type`,p.Project_Pastel_Prefix,p.Project_Description,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name,b.barcode_value FROM S4Admin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN S4Admin.Project p ON (o.OrderNo_Project = p.Project_Code)
LEFT JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
INNER JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
WHERE b.barcode_value='$barcode' ORDER BY i.Items_ID ";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['Storage Type'] == "1") {
                    $row['Storage Type'] = "Tracked";
                } elseif ($row['Storage Type'] == "2") {
                    $row['Storage Type'] = "Resale Tracked";
                } elseif ($row['Storage Type'] == "3") {
                    $row['Storage Type'] = "Untracked/Resale Untracked";
                }
                $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo, $row['Project_Pastel_Prefix'] . " - " . $row['Project_Description'], $row['OrderNo_Number'], $row['Staff_First_Name'] . " " . $row['Staff_Last_Name'], $row['barcode_value']);
            }
            return $items;
        } else {
            $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type`,p.Project_Pastel_Prefix,p.Project_Description,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name,b.barcode_value FROM MMAdmin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN MMAdmin.Project p ON (o.OrderNo_Project = p.Project_Code)
LEFT JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
INNER JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
WHERE b.barcode_value='$barcode' ORDER BY i.Items_ID";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {

                    while ($row = mysql_fetch_array($result)) {

                        if ($row['Storage Type'] == "1") {
                            $row['Storage Type'] = "Tracked";
                        } elseif ($row['Storage Type'] == "2") {
                            $row['Storage Type'] = "Resale Tracked";
                        } elseif ($row['Storage Type'] == "3") {
                            $row['Storage Type'] = "Untracked/Resale Untracked";
                        }
                        $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo, $row['Project_Pastel_Prefix'] . " - " . $row['Project_Description'], $row['OrderNo_Number'], $row['Staff_First_Name'] . " " . $row['Staff_Last_Name'], $row['barcode_value']);
                    }

                    return $items;
                } else {

                    $items[] = array(false, "No items found!");
                    return $items;
                }
            }
        }
    }
}

function modelGetItemsByS4Project($project,$orderNumber) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type`,p.Project_Pastel_Prefix,p.Project_Description,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name
FROM  S4Admin.`Project` p
INNER JOIN S4Admin.OrderNo o ON (o.OrderNo_Project = p.Project_Code)
INNER JOIN S4Admin.Items i ON (o.OrderNo_Number = i.Items_Order_Number)
INNER JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN stock_control.shelf sh ON (ib.shelf_id = sh.id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
WHERE sh.mmproject = '$project' OR sh.s4project = '$project' AND i.Items_Order_Number = '$orderNumber' ORDER BY i.Items_ID";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['Storage Type'] == "1") {
                    $row['Storage Type'] = "Tracked";
                } elseif ($row['Storage Type'] == "2") {
                    $row['Storage Type'] = "Resale Tracked";
                } elseif ($row['Storage Type'] == "3") {
                    $row['Storage Type'] = "Untracked/Resale Untracked";
                }
                $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo, $row['Project_Pastel_Prefix'] . " - " . $row['Project_Description'], $row['OrderNo_Number'], $row['Staff_First_Name'] . " " . $row['Staff_Last_Name']);
            }
            return $items;
        } else {
            $items[] = array(false, "No items found!");
            return $items;
        }
    }
}

function modelGetItemsByMMProject($project,$orderNumber) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Distinct i.Items_ID,i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity AS `Quantity Ordered`,
i.Items_Storeman_Received AS `Quantity Received`,i.Items_StorageType AS `Storage Type`,p.Project_Pastel_Prefix,p.Project_Description,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name
FROM  MMAdmin.`Project` p
INNER JOIN MMAdmin.OrderNo o ON (o.OrderNo_Project = p.Project_Code)
INNER JOIN MMAdmin.Items i ON (o.OrderNo_Number = i.Items_Order_Number)
INNER JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN stock_control.shelf sh ON (ib.shelf_id = sh.id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
WHERE  sh.s4project = '$project' OR sh.mmproject = '$project' AND i.Items_Order_Number = '$orderNumber' ORDER BY i.Items_ID";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['Storage Type'] == "1") {
                    $row['Storage Type'] = "Tracked";
                } elseif ($row['Storage Type'] == "2") {
                    $row['Storage Type'] = "Resale Tracked";
                } elseif ($row['Storage Type'] == "3") {
                    $row['Storage Type'] = "Untracked/Resale Untracked";
                }
                $items[] = array(true, $row['Items_ID'], $row['Items_Supplier_Code'], $row['Items_Description'], $row['Quantity Ordered'], $row['Quantity Received'], $row['Storage Type'], $OrderNo, $row['Project_Pastel_Prefix'] . " - " . $row['Project_Description'], $row['OrderNo_Number'], $row['Staff_First_Name'] . " " . $row['Staff_Last_Name']);
            }           
            return $items;
        } else {
            $items[] = array(false, "No items found!");
            return $items;
        }
    }
}

function modelGetItemsByBarcodeItems($barcode) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
lo.destination AS Shelf,l.name AS Storeroom,i.Items_ID,lo.destination_comment,lo.date_time,sh.location_id,sh.description FROM S4Admin.Items i
INNER JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.shelf sh ON (sh.id = ib.shelf_id)
INNER JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.`id` = sh.location_id)
LEFT JOIN stock_control.log lo ON (b.barcode_value=lo.item)
WHERE b.barcode_value= '$barcode'  order by lo.date_time desc";//AND lo.action ='Relocate'
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['location_id'] == 3) {
                    $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], 'Reason for taking: ' . $row['destination_comment'], '', $row['Items_ID'], $row['date_time']);
                } else {
                    $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'] . ' ' . $row['description'], $row['Storeroom'], $row['Items_ID'], $row['date_time']);
                }
            }
            return $items;
        } else {
            $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
lo.destination AS Shelf,l.name AS Storeroom,i.Items_ID,lo.destination_comment,lo.date_time,sh.location_id,sh.description FROM MMAdmin.Items i
INNER JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
LEFT JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.shelf sh ON (sh.id = ib.shelf_id)
INNER JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.`id` = sh.location_id)
LEFT JOIN stock_control.log lo ON (b.barcode_value=lo.item)
WHERE b.barcode_value= '$barcode' AND lo.action ='Relocate' order by lo.date_time desc";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        if ($row['location_id'] == 3) {
                            $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], 'Reason for taking: ' . $row['destination_comment'], '', $row['Items_ID'], $row['date_time']);
                        } else {
                            $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'] . ' ' . $row['description'], $row['Storeroom'], $row['Items_ID'], $row['date_time']);
                        }
}
                    return $items;
                } else {
                    $items[] = array(false, "No items found!");
                    return $items;
                }
            }
        }
    }
}

function modelGetItemsByS4ProjectItems($project,$orderNumber) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
sh.description AS Shelf,l.description AS Storeroom,i.Items_ID
FROM  S4Admin.`Project` p
INNER JOIN S4Admin.OrderNo o ON (o.OrderNo_Project = p.Project_Code)
INNER JOIN S4Admin.Items i ON (o.OrderNo_Number = i.Items_Order_Number)
INNER JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN stock_control.shelf sh ON (ib.shelf_id = sh.id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.id = sh.location_id)
WHERE  sh.mmproject = '$project' OR sh.s4project = '$project' AND i.Items_Order_Number = '$orderNumber'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'], $row['Storeroom'], $row['Items_ID']);
            }
            return $items;
        } else {
            $items[] = array(false, "No items found!");
            return $items;
        }
    }
}

function modelGetItemsByMMProjectItems($project,$orderNumber) {
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.items_barcode_id,i.Items_Description,b.barcode_value,sh.barcode,
sh.description AS Shelf,l.description AS Storeroom,i.Items_ID
FROM  MMAdmin.`Project` p
INNER JOIN MMAdmin.OrderNo o ON (o.OrderNo_Project = p.Project_Code)
INNER JOIN MMAdmin.Items i ON (o.OrderNo_Number = i.Items_Order_Number)
INNER JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
LEFT JOIN stock_control.shelf sh ON (ib.shelf_id = sh.id)
LEFT JOIN stock_control.barcode b ON (b.barcode_id = ib.barcode_id)
LEFT JOIN stock_control.location l ON (l.id = sh.location_id)
WHERE  sh.mmproject = '$project' OR sh.s4project = '$project' AND i.Items_Order_Number = '$orderNumber'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['items_barcode_id'], $row['Items_Description'], $row['barcode'], $row['barcode_value'], $row['Shelf'], $row['Storeroom'], $row['Items_ID']);
            }
            return $items;
        } else {
            $items[] = array(false, "No items found!");
            return $items;
        }
    }
}

function modelGetOrderDetails($OrderNo) {

    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT i.Items_Order_Number,s.Staff_First_Name,s.Staff_Last_Name,sm.Staff_First_Name AS PMName,sm.Staff_Last_Name AS PMLName,o.OrderNo_Date_Time,p.Project_Description AS Project,p.Project_Pastel_Prefix FROM S4Admin.Items i
LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
INNER JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
INNER JOIN S4Admin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
INNER JOIN S4Admin.Staff sm ON (sm.Staff_Code = s.Staff_Reports_To)
INNER JOIN S4Admin.Project p ON (p.Project_Code = o.OrderNo_Project)
WHERE i.Items_Order_Number = '$OrderNo'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['Items_Order_Number'], $row['Staff_First_Name'] . ' ' . $row['Staff_Last_Name'], $row['OrderNo_Date_Time'], $row['Project'], $row['PMName'] . ' ' . $row['PMLName'], $row['Project_Pastel_Prefix']);
                return $items;
            }
        } else {
            $q = "SELECT i.Items_Order_Number,s.Staff_First_Name,s.Staff_Last_Name,sm.Staff_First_Name AS PMName,sm.Staff_Last_Name AS PMLName,o.OrderNo_Date_Time,p.Project_Description AS Project,p.Project_Pastel_Prefix FROM MMAdmin.Items i
            LEFT JOIN stock_control.items_barcode ib ON (ib.items_id = i.Items_ID)
            INNER JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)
            INNER JOIN MMAdmin.Staff s ON (s.Staff_Code = o.OrderNo_Requested)
            INNER JOIN S4Admin.Staff sm ON (sm.Staff_Code = s.Staff_Reports_To)
            INNER JOIN MMAdmin.Project p ON (p.Project_Code = o.OrderNo_Project)
            WHERE i.Items_Order_Number = '$OrderNo'";
            $result = mysql_query($q, $con);
            if (!$result) {
                $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
                return $items;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $items[] = array(true, $row['Items_Order_Number'], $row['Staff_First_Name'] . ' ' . $row['Staff_Last_Name'], $row['OrderNo_Date_Time'], $row['Project'], $row['PMName'] . ' ' . $row['PMLName'], $row['Project_Pastel_Prefix']);
                        return $items;
                    }
                } else {
                    return $items[] = array(false, "No items found!");
                }
            }
        }
    }
}

function modelLog($process, $action, $user, $itemId, $OrderNo = null, $source = null, $sourceComment = null, $destination = null, $destinationComment = null) {
    $storeman = '';
    if (empty($user)) {
        $storeman = $_SESSION['userId'];
        $con = modelConnectToServer($errormsg);
        $q = "SELECT username FROM stock_control.user WHERE `id`= '$storeman'";
        $result = mysql_query($q, $con);
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $storeman = $row['username'];
            }
        } else {
            $q = "SELECT Staff_First_Name,Staff_Last_Name FROM S4Admin.Staff WHERE `Staff_Code`= '$storeman'";
            $result = mysql_query($q, $con);
            $num_results = mysql_num_rows($result);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $storeman = $row['Staff_First_Name'] . " " . $row['Staff_Last_Name'];
                }
            } else {
                $q = "SELECT Staff_First_Name,Staff_Last_Name FROM MMAdmin.Staff WHERE `Staff_Code`= '$storeman'";
                $result = mysql_query($q, $con);
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $storeman = $row['Staff_First_Name'] . " " . $row['Staff_Last_Name'];
                    }
                }
            }
        }
    } else {
        $storeman = $user;
    }
    $columns = 'process,action,storeman,item';
    $values = "'$process','$action','$storeman','$itemId'";
    if (is_null($OrderNo) == false) {
        $columns .= ",order_number";
        $values .= ",'$OrderNo'";
    }
    if (is_null($source) == false) {
        $columns .= ",source";
        $values .= ",'$source'";
    }
    if (is_null($sourceComment) == false) {
        $columns .= ",source_comment";
        $values .= ",'$sourceComment'";
    }
    if (is_null($destination) == false) {
        $columns .= ",destination";
        $values .= ",'$destination'";
    }
    if (is_null($destinationComment) == false) {
        $columns .= ",destination_comment";
        $values .= ",'$destinationComment'";
    }
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO log ($columns) VALUES ($values)";
    $result = mysql_query($q, $con);
}
function modelGetLimitedLog() {
    $status = array();
    $db = "";
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.db FROM stock_control.log l LEFT JOIN stock_control.barcode b ON (b.barcode_value =l.item ) LEFT JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) GROUP BY ib.db ORDER BY date_time DESC";

    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {
        while ($row = mysql_fetch_array($result)) {
            $db = $row['db'];
            if ($db == "s4") {
                $q = "SELECT DISTINCT i.Items_Description,i.Items_Supplier_Code,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name,l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment FROM stock_control.log l "
                        . "LEFT JOIN stock_control.`user` u ON (u.`id` = l.storeman)"
                        . "INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item )"
                        . "INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id)"
                        . "INNER JOIN S4Admin.Items i ON (i.Items_ID = ib.items_id)"
                        . "LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)"
                        . "LEFT JOIN S4Admin.Project p ON (p.Project_Code = o.OrderNo_Project)"
                        . "LEFT JOIN S4Admin.Staff s ON (s.Staff_code = p.Project_Responsible) WHERE date_time >= NOW() - INTERVAL 6 MONTH ORDER BY date_time DESC";
            } elseif ($db == "mm") {
                $q = "SELECT DISTINCT i.Items_Description,i.Items_Supplier_Code,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name,l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment FROM stock_control.log l "
                        . "LEFT JOIN stock_control.`user` u ON (u.`id` = l.storeman)"
                        . "INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item )"
                        . "INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id)"
                        . "INNER JOIN MMAdmin.Items i ON (i.Items_ID = ib.items_id)"
                        . "INNER JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)"
                        . "INNER JOIN MMAdmin.Project p ON (p.Project_Code = o.OrderNo_Project)"
                        . "INNER JOIN MMAdmin.Staff s ON (s.Staff_code = p.Project_Responsible) WHERE date_time >= NOW() - INTERVAL 6 MONTH ORDER BY date_time DESC";
            } else {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item, l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,no.description AS Items_Description FROM stock_control.log l INNER JOIN stock_control.`barcode` b ON (b.`barcode_value` = l.item) INNER JOIN stock_control.`items_barcode` ib ON (ib.`barcode_id` = b.barcode_id)INNER JOIN stock_control.`no_order_items` no ON (no.`items_id` = ib.items_id) WHERE date_time >= NOW() - INTERVAL 6 MONTH ORDER BY date_time DESC";
            }

            $result2 = mysql_query($q, $con);
            $num_results = mysql_num_rows($result2);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result2)) {
                    $status[] = array(true, $row['date_time'], $row['process'], $row['action'], $row['storeman'], $row['item'], $row['Items_Description'], $row['OrderNo_Number'], $row['source'], $row['source_comment'], $row['destination'], $row['destination_comment'], $row['Staff_First_Name'], $row['Items_Supplier_Code']);
                }
            }
        }
        if (empty($status)) {
            $status[] = array(false, "No items found");
        }
    } else {
        $status[] = array(false, "No items found");
    }

    foreach ($status as $key => $row) {
        $hasItems[$key] = $row[0];
        $dates[$key] = $row[1];
        $process[$key] = $row[2];
        $action[$key] = $row[3];
        $storeman[$key] = $row[4];
        $item[$key] = $row[5];
        $Items_Description[$key] = $row[6];
        $OrderNo_Number[$key] = $row[7];
        $source[$key] = $row[8];
        $source_comment[$key] = $row[9];
        $destination[$key] = $row[10];
        $destination_comment[$key] = $row[11];
        $Staff_First_Name[$key] = $row[12];
        $Items_Supplier_Code[$key] = $row[13];
    }
    array_multisort($hasItems, SORT_DESC, $dates, SORT_DESC, $process, SORT_DESC, $action, SORT_DESC, $storeman, SORT_DESC, $item, SORT_DESC, $Items_Description, SORT_DESC, $OrderNo_Number, SORT_DESC, $source, SORT_DESC, $source_comment, SORT_DESC, $destination, SORT_DESC, $destination_comment, SORT_DESC, $Staff_First_Name, SORT_DESC, $Items_Supplier_Code, SORT_DESC, $status); //Sort array in descending order
    return $status;
}

function modelGetAllLog() {
    $status = array();
    $db = "";
    $con = modelConnectToServer($errormsg);
    $q = "SELECT ib.db FROM stock_control.log l LEFT JOIN stock_control.barcode b ON (b.barcode_value =l.item ) LEFT JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) GROUP BY ib.db ORDER BY date_time DESC";

    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {
        while ($row = mysql_fetch_array($result)) {
            $db = $row['db'];
            if ($db == "s4") {
                $q = "SELECT DISTINCT i.Items_Description,i.Items_Supplier_Code,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name,l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment FROM stock_control.log l "
                        . "LEFT JOIN stock_control.`user` u ON (u.`id` = l.storeman)"
                        . "INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item )"
                        . "INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id)"
                        . "INNER JOIN S4Admin.Items i ON (i.Items_ID = ib.items_id)"
                        . "LEFT JOIN S4Admin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)"
                        . "LEFT JOIN S4Admin.Project p ON (p.Project_Code = o.OrderNo_Project)"
                        . "LEFT JOIN S4Admin.Staff s ON (s.Staff_code = p.Project_Responsible) ORDER BY date_time DESC";
            } elseif ($db == "mm") {
                $q = "SELECT DISTINCT i.Items_Description,i.Items_Supplier_Code,o.OrderNo_Number,s.Staff_First_Name,s.Staff_Last_Name,l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment FROM stock_control.log l "
                        . "LEFT JOIN stock_control.`user` u ON (u.`id` = l.storeman)"
                        . "INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item )"
                        . "INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id)"
                        . "INNER JOIN MMAdmin.Items i ON (i.Items_ID = ib.items_id)"
                        . "INNER JOIN MMAdmin.OrderNo o ON (o.OrderNo_Number = i.Items_Order_Number)"
                        . "INNER JOIN MMAdmin.Project p ON (p.Project_Code = o.OrderNo_Project)"
                        . "INNER JOIN MMAdmin.Staff s ON (s.Staff_code = p.Project_Responsible) WHERE date_time >= NOW() - INTERVAL 6 MONTH ORDER BY date_time DESC";
            } else {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item, l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,no.description AS Items_Description FROM stock_control.log l INNER JOIN stock_control.`barcode` b ON (b.`barcode_value` = l.item) INNER JOIN stock_control.`items_barcode` ib ON (ib.`barcode_id` = b.barcode_id)INNER JOIN stock_control.`no_order_items` no ON (no.`items_id` = ib.items_id) ORDER BY date_time DESC";
            }

            $result2 = mysql_query($q, $con);
            $num_results = mysql_num_rows($result2);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result2)) {
                    $status[] = array(true, $row['date_time'], $row['process'], $row['action'], $row['storeman'], $row['item'], $row['Items_Description'], $row['OrderNo_Number'], $row['source'], $row['source_comment'], $row['destination'], $row['destination_comment'], $row['Staff_First_Name'], $row['Items_Supplier_Code']);
                }
            }
        }
        if (empty($status)) {
            $status[] = array(false, "No items found");
        }
    } else {
        $status[] = array(false, "No items found");
    }

    foreach ($status as $key => $row) {
        $hasItems[$key] = $row[0];
        $dates[$key] = $row[1];
        $process[$key] = $row[2];
        $action[$key] = $row[3];
        $storeman[$key] = $row[4];
        $item[$key] = $row[5];
        $Items_Description[$key] = $row[6];
        $OrderNo_Number[$key] = $row[7];
        $source[$key] = $row[8];
        $source_comment[$key] = $row[9];
        $destination[$key] = $row[10];
        $destination_comment[$key] = $row[11];
        $Staff_First_Name[$key] = $row[12];
        $Items_Supplier_Code[$key] = $row[13];
    }
    array_multisort($hasItems, SORT_DESC, $dates, SORT_DESC, $process, SORT_DESC, $action, SORT_DESC, $storeman, SORT_DESC, $item, SORT_DESC, $Items_Description, SORT_DESC, $OrderNo_Number, SORT_DESC, $source, SORT_DESC, $source_comment, SORT_DESC, $destination, SORT_DESC, $destination_comment, SORT_DESC, $Staff_First_Name, SORT_DESC, $Items_Supplier_Code, SORT_DESC, $status); //Sort array in descending order
    return $status;
}

function modelGetAllUntrackedItemsLog() {
    $status = array();
    $db = "";
    $con = modelConnectToServer($errormsg);
    $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item, l.order_number,l.source,l.source_comment,l.destination,l.destination_comment FROM stock_control.log l WHERE action = 'Captured Consumable/Untracked' OR action = 'Captured untracked items' OR action = 'Captured consumable item' ORDER BY date_time DESC";

    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {
        while ($row = mysql_fetch_array($result)) {
            $status[] = array(true, $row['date_time'], $row['process'], $row['action'], $row['storeman'], $row['item'], $row['item'], $row['order_number'], $row['source'], $row['source_comment'], $row['destination'], $row['destination_comment'], $row['Staff_First_Name'], $row['Items_Supplier_Code']);
        }
        if (empty($status)) {
            $status[] = array(false, "No items found");
        }
    } else {
        $status[] = array(false, "No items found");
    }
    return $status;
}

function modelGetLogDateRange($datefrom, $dateto) {
    $status = array();
    $id = "";
    $con = modelConnectToServer($errormsg);
    $q = "SELECT DISTINCT ib.db,ib.items_id FROM stock_control.log l LEFT JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) WHERE date_time BETWEEN '$datefrom' AND '$dateto'  ORDER BY date_time DESC;";
    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {

        while ($row = mysql_fetch_array($result)) {
            $id = $row['items_id'];
            if ($row['db'] == 's4') {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,i.Items_Description,i.Items_Order_Number FROM stock_control.log l INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) INNER JOIN S4Admin.Items i ON (i.Items_ID = ib.items_id) WHERE i.Items_ID = '$id' AND date_time BETWEEN '$datefrom' AND '$dateto' ORDER BY date_time DESC";
            }
            if ($row['db'] == 'mm') {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,i.Items_Description,i.Items_Order_Number FROM stock_control.log l INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) INNER JOIN MMAdmin.Items i ON (i.Items_ID = ib.items_id) WHERE i.Items_ID = '$id' AND date_time BETWEEN '$datefrom' AND '$dateto' ORDER BY date_time DESC";
            }
            if ($row['db'] == 'no') {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,i.description AS Items_Description,i.Order_Number AS Items_Order_Number  FROM stock_control.log l INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) INNER JOIN stock_control.no_order_items i ON (i.items_id = ib.items_id) WHERE i.Items_ID = '$id' AND date_time BETWEEN '$datefrom' AND '$dateto' ORDER BY date_time DESC";
            }
            $result2 = mysql_query($q, $con);
            $num_results = mysql_num_rows($result2);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result2)) {
                    $status[] = array(true, $row['date_time'], $row['process'], $row['action'], $row['storeman'], $row['item'], $row['Items_Description'], $row['Items_Order_Number'], $row['source'], $row['source_comment'], $row['destination'], $row['destination_comment']);
                }
            }
        }
        if (empty($status)) {
            $status[] = array(false, "No items found");
        }
    } else {
        $status[] = array(false, "No items found!");
    }
    return $status;
}

function modelGetLogDateRangeUntracked($datefrom, $dateto) {
    $status = array();
    $id = "";
    $con = modelConnectToServer($errormsg);
    $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item, l.order_number,l.source,l.source_comment,l.destination,l.destination_comment FROM stock_control.log l WHERE action = 'Captured Consumable/Untracked' OR action = 'Captured untracked items' OR action = 'Captured consumable item' AND date_time BETWEEN '$datefrom' AND '$dateto'  ORDER BY date_time DESC;";
    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {

        while ($row = mysql_fetch_array($result)) {
            $id = $row['items_id'];
            if ($row['db'] == 's4') {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,i.Items_Description,i.Items_Order_Number FROM stock_control.log l INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) INNER JOIN S4Admin.Items i ON (i.Items_ID = ib.items_id) WHERE i.Items_ID = '$id' AND date_time BETWEEN '$datefrom' AND '$dateto' ORDER BY date_time DESC";
            }
            if ($row['db'] == 'mm') {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,i.Items_Description,i.Items_Order_Number FROM stock_control.log l INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) INNER JOIN MMAdmin.Items i ON (i.Items_ID = ib.items_id) WHERE i.Items_ID = '$id' AND date_time BETWEEN '$datefrom' AND '$dateto' ORDER BY date_time DESC";
            }
            if ($row['db'] == 'no') {
                $q = "SELECT DISTINCT l.date_time,l.process,l.action,l.storeman,l.item,l.order_number,l.source,l.source_comment,l.destination,l.destination_comment,i.description AS Items_Description,i.Order_Number AS Items_Order_Number  FROM stock_control.log l INNER JOIN stock_control.barcode b ON (b.barcode_value =l.item) INNER JOIN stock_control.items_barcode ib ON (ib.barcode_id = b.barcode_id) INNER JOIN stock_control.no_order_items i ON (i.items_id = ib.items_id) WHERE i.Items_ID = '$id' AND date_time BETWEEN '$datefrom' AND '$dateto' ORDER BY date_time DESC";
            }
            $result2 = mysql_query($q, $con);
            $num_results = mysql_num_rows($result2);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result2)) {
                    $status[] = array(true, $row['date_time'], $row['process'], $row['action'], $row['storeman'], $row['item'], $row['Items_Description'], $row['Items_Order_Number'], $row['source'], $row['source_comment'], $row['destination'], $row['destination_comment']);
                }
            }
        }
        if (empty($status)) {
            $status[] = array(false, "No items found");
        }
    } else {
        $status[] = array(false, "No items found!");
    }
    return $status;
}

function modelSendEmail($toAddress, $subject, $body) {
    $status = array();
    try {
        //SendMailHTML($toAddress, $subject, $body);
		print_r($toAddress);
		SendMailHTML("reagan.gallant@s4.co.za", $subject, $body);
        $status[] = array(true, "success");
    } catch (Exception $ex) {
        $status[] = array(false, "Unable to sent email" . $ex->getMessage());
    }

    return $status;
}

function modelGetDeletedShelfDetails($id) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT barcode, description FROM shelf WHERE `id`= '$id'";
    $result = mysql_query($q, $con);
    $num_results = mysql_num_rows($result);
    if ($num_results > 0) {
        while ($row = mysql_fetch_array($result)) {
            $status[] = array(true, $row['barcode'], $row['description']);
        }
        return $status;
    }
}

function modelGetTrackedItemDetails($id, $db) {
    if ($db == "mm") {
        $db = "MMAdmin";
    } else {
        $db = "S4Admin";
    }
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT i.Items_Order_Number,i.Items_Supplier_Code,p.Project_Pastel_Prefix,p.Project_Description,i.Items_Description,i.Items_Quantity,"
            . "i.Items_Storeman_Received,s.Staff_email,sm.Staff_email AS PM_Email,s.Staff_Auth_Level,s.Staff_First_Name,s.Staff_Last_Name FROM $db.Items i INNER JOIN $db.OrderNo o ON "
            . "(o.OrderNo_Number = i.Items_Order_Number) INNER JOIN $db.Project p ON(p.Project_Code = o.OrderNo_Project)"
            . "INNER JOIN $db.Staff s ON(s.Staff_Code = o.OrderNo_Requested) INNER JOIN S4Admin.Staff sm ON (sm.Staff_Code = s.Staff_Reports_To) WHERE i.Items_ID = '$id'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['Items_Order_Number'], $row['Items_Supplier_Code'], $row['Project_Description'], $row['Items_Description'], round($row['Items_Quantity']), $row['Items_Storeman_Received'], $row['Staff_email'], $row['PM_Email'], $row['Project_Pastel_Prefix'], $row['Staff_Auth_Level'], $row['Staff_First_Name'] . ' ' . $row['Staff_Last_Name']);
            }
            return $items;
        }
    }
}

function modelGetConsumableItemDetails($id, $db) {
    if ($db == "mm") {
        $db = "MMAdmin";
    } else {
        $db = "S4Admin";
    }
    $items = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT i.Items_Order_Number,i.Items_Supplier_Code,p.Project_Pastel_Prefix,p.Project_Description,i.Items_Description,i.Items_Quantity,"
            . "i.Items_Storeman_Received,s.Staff_email, CONCAT(ct.description ,'-', ct.name ,',', ct.details) AS Details,sm.Staff_email AS PM_Email,s.Staff_Auth_Level,s.Staff_First_Name,s.Staff_Last_Name"
            . " FROM $db.Items i INNER JOIN $db.OrderNo o ON "
            . "(o.OrderNo_Number = i.Items_Order_Number) INNER JOIN $db.Project p ON(p.Project_Code = o.OrderNo_Project)"
            . "INNER JOIN $db.Staff s ON(s.Staff_Code = o.OrderNo_Requested)"
            . "INNER JOIN $db.Staff sm ON(sm.Staff_Code = s.Staff_Reports_To)"
            . "LEFT JOIN stock_control.consumable_type ct ON (ct.consumable_type_id = i.Items_StorageType) WHERE i.Items_ID = '$id'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $items[] = array(true, $row['Items_Order_Number'], $row['Items_Supplier_Code'], $row['Project_Description'], $row['Items_Description'], round($row['Items_Quantity']), $row['Items_Storeman_Received'], $row['Staff_email'], $row["Details"], $row['PM_Email'], $row['Project_Pastel_Prefix'], $row['Staff_Auth_Level'], $row['Staff_First_Name'] . ' ' . $row['Staff_Last_Name']);
            }
            return $items;
        }
    }
}

function modelGetAllConsumables() {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT ct.consumable_type_id, ct.name, ct.description,ct.details,ct.min_qty,ct.max_qty,c.quantity_in_store,ct.supplier_code,ct.nett_value "
            . "FROM consumable_type ct INNER JOIN consumable c ON (ct.consumable_type_id = c.consumable_type_id) ";

    $result = mysql_query($q, $con);
    if (!$result) {
        $items[] = array(false, 'An error occured while retrieving items.' . mysql_error());
        return $items;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $status[] = array(true, $row['consumable_type_id'], $row['name'], $row['description'], $row['details'], $row['min_qty'], $row['max_qty'], $row['quantity_in_store'], $row['supplier_code'], $row['nett_value']);
            }
            return $status;
        } else {
            $status[] = array(false, "No consumable items found");
        }
    }
}

function modelAddConsumable($name, $description, $details, $minQty, $maxQty, $qtyInStore, $supplier_code, $nettValue) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);

    $q = "INSERT INTO `consumable_type` (supplier_code,name,description,details,min_qty,max_qty,nett_value) VALUES('$supplier_code','$name','$description','$details','$minQty','$maxQty','$nettValue')";
    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to add consumable.' . mysql_error());
        return $status;
    } else {
        $newId = mysql_insert_id();
        $q = "INSERT INTO `consumable` (quantity_in_store,consumable_type_id) VALUES('$qtyInStore','$newId')";
        if (!mysql_query($q, $con)) {
            $status[] = array(false, 'Failed to add consumable quantity in store.' . mysql_error());
            return $status;
        } else {
            $status[] = array(true, 'Consumable added successfully!');
            return $status;
        }
    }
}

function modelUpdateConsumable($id, $name, $description, $detail, $minQty, $maxQty, $qtyInStore, $supplier_code, $nettValue) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "UPDATE consumable_type SET name = '$name', description = '$description',details='$detail',supplier_code='$supplier_code',min_qty='$minQty',max_qty='$maxQty',nett_value='$nettValue' WHERE consumable_type_id = $id";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to update consumable." . mysql_error());
        return $status;
    } else {
        $q = "UPDATE consumable SET quantity_in_store = '$qtyInStore' WHERE consumable_type_id = $id";
        $result = mysql_query($q, $con);
        if (!$result) {
            $status[] = array(false, "Unable to update consumable." . mysql_error());
            return $status;
        } else {
            echo 'Consumable updated successfully';
        }
    }
}

function modelDeleteConsumable($id) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "DELETE FROM consumable_type WHERE`consumable_type_id` = '$id'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to remove consumable." . mysql_error());
        return $status;
    } else {
        $status[] = array(true, "Consumable removed succesfully!");
        return $status;
    }
}

function modelDeleteAssignBarcodeItems($id) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "DELETE FROM items_barcode WHERE`items_barcode_id` = '$id'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to remove item." . mysql_error());
        return $status;
    } else {
        $status[] = array(true, "Item removed succesfully!");
        return $status;
    }
}

function modelGetProjectsIntranet() {
    $projects = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Project_Code,Project_Pastel_Prefix,Project_Description FROM S4Admin.`Project` WHERE Project_Closed = '0'";
    $result = mysql_query($q, $con);

    if (!$result) {
        $projects[] = array(false, 'An error occured while retrieving projects.' . mysql_error());
        return $projects;
    } else {
        $num_results = mysql_num_rows($result);

        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $projects[] = array(true, utf8_encode($row['Project_Code']), utf8_encode($row['Project_Pastel_Prefix'] . " - " . $row['Project_Description']));
            }
            return $projects;
        } else {
            $projects[] = array(false, 'No projects found!');
            return $projects;
        }
    }
}

function modelGetProjectsAutonet() {
    $projects = array();
    $con = modelConnectToServer($errormsg);
    $q = "SELECT Project_Code,Project_Pastel_Prefix, Project_Description FROM MMAdmin.`Project` WHERE Project_Closed = '0'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $projects[] = array(false, 'An error occured while retrieving projects.' . mysql_error());
        return $projects;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $projects[] = array(true, utf8_encode($row['Project_Code']), utf8_encode($row['Project_Pastel_Prefix'] . " - " . $row['Project_Description']));
            }
            return $projects;
        } else {
            $projects[] = array(false, 'No projects found!');
            return $projects;
        }
    }
}

function modelGetUserAccess($userID) {
    $usersTypes = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT user_access_id FROM `user` WHERE id = '$userID'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $usersTypes[] = array(false, 'An error occurred while retrieving user access id.' . mysql_error());
        return $usersTypes;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $usersTypes[] = array(true, $row['user_access_id']);
            }
            return $usersTypes;
        } else {
            $usersTypes[] = array(false, 'No user found!');
            return $usersTypes;
        }
    }
}

function modelGetShelvesWithProject() {
    $shelves = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT s.id,s.description,s4.Project_Description AS s4Projects,mm.Project_Description AS mmProjects,l.name,s4.Project_Pastel_Prefix AS s4ProjectPrefix,mm.Project_Pastel_Prefix AS mmProjectPrefix,s.barcode FROM `shelf` s LEFT JOIN S4Admin.Project s4 ON (s4.Project_Code = s.s4project) LEFT JOIN MMAdmin.Project mm ON (mm.Project_Code = s.mmproject) LEFT JOIN `location` l ON s.location_id = l.id WHERE l.id <> '3' order by s.id";
    $result = mysql_query($q, $con);
    if (!$result) {
        $shelves[] = array(false, 'An error occured while retrieving shelves.' . mysql_error());
        return $shelves;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if (empty($row['s4Projects']) && empty($row['mmProjects'])) {
                    $shelves[] = array(true, $row['id'], "Shelf:" . $row['description']." (" . $row['barcode'].") - Storeroom:" . $row['name']);
                } elseif (empty($row['s4Projects'])) {
                    $shelves[] = array(true, $row['id'], "Shelf: " . $row['description'] ." (" . $row['barcode'].") - Auto Project: " . $row['mmProjectPrefix'] . " - " . $row['mmProjects'] . ' - Storeroom:' . $row['name']);
                } elseif (empty($row['mmProjects'])) {
                    $shelves[] = array(true, $row['id'], "Shelf: " . $row['description'] ." (" . $row['barcode'].") - S4 Project: " . $row['s4ProjectPrefix'] . " - " . $row['s4Projects'] . ' - Storeroom:' . $row['name']);
                } else {
                    $shelves[] = array(true, $row['id'], "Shelf:" . $row['description'] . " (" . $row['barcode'].") - S4 Project: " . $row['s4ProjectPrefix'] . " - " . $row['s4Projects'] . " - Auto Project: " . $row['mmProjectPrefix'] . " - " . $row['mmProjects'] . ' - Storeroom:' . $row['name']);
                }
            }
            return $shelves;
        } else {
            $shelves[] = array(false, 'No shelves found!');
            return $shelves;
        }
    }
}

function modelGetItemByLocation($shelfId) {
    $shelves = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT ib.db,items_id,ib.items_barcode_id FROM `shelf` s INNER JOIN items_barcode ib ON (s.id = ib.shelf_id) WHERE `id` = '$shelfId' Group by ib.items_id";
    $result = mysql_query($q, $con);
    if (!$result) {
        $shelves[] = array(false, 'An error occured while retrieving shelves.' . mysql_error());
        return $shelves;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $db = $row['db'];
                $itemId = $row['items_id'];
				$items_barcode_id = $row['items_barcode_id'];
                if ($db == 's4') {
                    $q = "SELECT DISTINCT i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity,i.Items_Storeman_Received,i.Items_StorageType,b.barcode_value,s.barcode,s.description,loc.name FROM `S4Admin`.Items i
INNER JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id)
LEFT JOIN stock_control.barcode b ON (ib.barcode_id = b.barcode_id)
LEFT JOIN stock_control.log l ON (b.barcode_value = l.item)
INNER JOIN stock_control.shelf s ON (ib.shelf_id = s.id)
INNER JOIN stock_control.location loc ON (s.location_id = loc.id)
WHERE ib.items_barcode_id = '$items_barcode_id'";
                } elseif ($db == 'mm') {
                    $q = "SELECT DISTINCT i.Items_Supplier_Code,i.Items_Description,i.Items_Quantity,i.Items_Storeman_Received,i.Items_StorageType,b.barcode_value,s.barcode,s.description,loc.name FROM `MMAdmin`.Items i
INNER JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id)
LEFT JOIN stock_control.barcode b ON (ib.barcode_id = b.barcode_id)
LEFT JOIN stock_control.log l ON (b.barcode_value = l.item)
INNER JOIN stock_control.shelf s ON (ib.shelf_id = s.id)
INNER JOIN stock_control.location loc ON (s.location_id = loc.id)
WHERE ib.items_barcode_id = '$items_barcode_id'";
                } else {
                    $q = "SELECT DISTINCT i.description AS Items_Description,i.Items_Storeman_Received,b.barcode_value ,s.barcode,s.description,loc.name,'No order item' As 'Items_StorageType' FROM `stock_control`.no_order_items i
LEFT JOIN stock_control.items_barcode ib ON (i.Items_ID = ib.items_id)
LEFT JOIN stock_control.barcode b ON (ib.barcode_id = b.barcode_id)
LEFT JOIN stock_control.log l ON (b.barcode_value = l.item)
INNER JOIN stock_control.shelf s ON (ib.shelf_id = s.id)
INNER JOIN stock_control.location loc ON (s.location_id = loc.id)
WHERE ib.items_barcode_id = '$items_barcode_id' ";
                }
                $result2 = mysql_query($q, $con);
                if (!$result2) {
                    $shelves[] = array(false, 'An error occured while retrieving shelves.' . mysql_error());
                    return $shelves;
                } else {
                    $num_results2 = mysql_num_rows($result2);
                    if ($num_results2 > 0) {
                        while ($row = mysql_fetch_array($result2)) {
                            if ($row['Items_StorageType'] == "1") {
                                $row['Items_StorageType'] = "Tracked";
                            } elseif ($row['Items_StorageType'] == "2") {
                                $row['Items_StorageType'] = "Resale Tracked";
                            } elseif ($row['Items_StorageType'] == "3") {
                                $row['Items_StorageType'] = "Untracked/Resale Untracked";
                            }
                            $shelves[] = array(true, $row['Items_Supplier_Code'], $row['Items_Description'], $row['Items_Quantity'], $row['Items_Storeman_Received'], $row['Items_StorageType'], $row['barcode_value'], $row['barcode'], $row['description'], $row['name']);
                        }
                    }
                }
            }
            if (empty($shelves)) {
                $shelves[] = array(false, 'No items found!');
            }
            return $shelves;
        } else {
            $shelves[] = array(false, 'No items found!');
            return $shelves;
        }
    }
}


function modelDeleteRelocateItem($id) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "DELETE FROM items_barcode WHERE `items_barcode_id` = '$id'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to remove unrelocate item." . mysql_error());
        return $status;
    } else {
        $status[] = array(true, " unrelocate item removed succesfully!");
        return $status;
    }
}

function modelCheckEmail($email) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `id` FROM `user` WHERE `email` = '$email'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to retrieve email." . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $status[] = array(true, $row['id']);
            }
        } else {
            $status[] = array(false, "Email not found. Only stockroom user can change their passwords.");
        }
        return $status;
    }
}

function modelUpdateActivationCode($id, $code) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "UPDATE `user` SET `activation_code` = '$code' WHERE `id` = $id";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable save activation code." . mysql_error());
    } else {
        $status[] = array(true, "Activation code saved successfully");
    }
    return $status;
}

function modelCheckValidCode($id, $code) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `id` FROM `user` WHERE `id` = '$id' AND `activation_code` = '$code'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to validate recovery code." . mysql_error());
        return $status;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $status[] = array(true, $row['id']);
            }
        } else {
            $status[] = array(false, "Invalid recover code provided.");
        }
        return $status;
    }
}

function modelUpdatePassword($id, $password) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "UPDATE `user` SET `password` = old_password('$password') WHERE `id` = $id";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable save new password." . mysql_error());
    } else {
        $status[] = array(true, "New password saved successfully");
    }
    return $status;
}

function modelGetNotificationEmails() {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT `email_address` FROM `email_notification`";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to retrieve email." . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $status[] = array(true, $row['email_address']);
            }
        } else {
            $status[] = array(false, "No emails found.");
        }
        return $status;
    }
}

function modelGetNotificationEmailUsers() {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "SELECT en.email_address,u.username,en.id FROM `email_notification` en INNER JOIN `user` u ON (en.user_id=u.id) WHERE en.db = 'no'";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to retrieve email." . mysql_error());
        return $status;
    } else {
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $status[] = array(true, $row['username'], $row['email_address'], $row['id']);
            }
        }
        $q = "SELECT en.email_address,CONCAT(u.Staff_First_Name,' ',u.Staff_Last_Name) AS username,en.id FROM `email_notification` en INNER JOIN S4Admin.Staff u ON (en.user_id=u.Staff_Code) WHERE en.db = 's4'";
        $result = mysql_query($q, $con);
        if (!$result) {
            $status[] = array(false, "Unable to retrieve email." . mysql_error());
            return $status;
        } else {
            $num_results = mysql_num_rows($result);
            if ($num_results > 0) {
                while ($row = mysql_fetch_array($result)) {
                    $status[] = array(true, $row['username'], $row['email_address'], $row['id']);
                }
            }
            $q = "SELECT en.email_address,CONCAT(u.Staff_First_Name,' ',u.Staff_Last_Name) AS username,en.id FROM `email_notification` en INNER JOIN MMAdmin.Staff u ON (en.user_id=u.Staff_Code)  WHERE en.db = 'mm'";
            $result = mysql_query($q, $con);
            if (!$result) {
                $status[] = array(false, "Unable to retrieve email." . mysql_error());
                return $status;
            } else {
                $num_results = mysql_num_rows($result);
                if ($num_results > 0) {
                    while ($row = mysql_fetch_array($result)) {
                        $status[] = array(true, $row['username'], $row['email_address'], $row['id']);
                    }
                }
            }
        }
    }
    if (empty($status)) {
        $status[] = array(false, "No emails found.");
    }
    return $status;
}

function modelGetnotificationUsers() {
    $users = array();
    $con = modelConnectToServer();
    $q = "(SELECT Staff_email, CONCAT(Staff_First_Name,' ',Staff_Last_Name) AS Fullname,Staff_Code,'s4' FROM S4Admin.Staff WHERE Staff_email <> '' AND Staff_email <> 'NULL' AND Staff_email <> '0' AND Staff_IsEmployee <> '0') UNION (SELECT Staff_email, CONCAT(Staff_First_Name,' ',Staff_Last_Name) AS Fullname,Staff_Code,'mm' FROM MMAdmin.Staff WHERE Staff_email <> '' AND Staff_email <> 'NULL' AND Staff_email <> '0' AND Staff_IsEmployee <> '0') UNION (SELECT email AS Staff_email,username AS Fullname,id AS Staff_Code,'no' FROM stock_control.user) Order By Fullname";
    $result = mysql_query($q, $con);
    if (!$result) {
        $users[] = array('An error occured while retrieving users.' . mysql_error());
        return $users;
    } else {

        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                $users[] = array(true, $row['Staff_email'], utf8_encode($row['Fullname']), $row['Staff_Code'], $row['s4']);
            }
        } else {
            $users[] = array(false, "No users found!");
        }
    }
    return $users;
}

function modelAddNotificationUser($email, $staffCode, $db) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "INSERT INTO email_notification (email_address,user_id,db) VALUES('$email','$staffCode','$db')";
    $result = mysql_query($q, $con);
    if (!$result) {
        $status[] = array(false, "Unable to add email to database." . mysql_error());
        return $status;
    } else {
        $status[] = array(true, "Email address added succesfully!");
        return $status;
    }
}

function modelRemoveNotificationUser($id) {
    $status = array();
    $con = modelLoginLocalDB('stock_control', $errormsg);
    $q = "DELETE FROM `email_notification` WHERE `id` = '$id'";

    if (!mysql_query($q, $con)) {
        $status[] = array(false, 'Failed to remove notification user!' . mysql_error());
        return $status;
    } else {
        $status[] = array(true, 'Notification user removed successfully!');
        return $status;
    }
}