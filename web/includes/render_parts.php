<?php
ob_start();
header('Content-Encoding: gzip');

$path = 'uploads/';
?>
<table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
				<th class='view first'>Date created</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Serial Number</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Description</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Product</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Company</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Location</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Project</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Group</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Currency</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Unit Cost</th>
                <th class='edit' filter='false'></th>
                <th class='view'>Quantity</th>
                <th class='edit' filter='false'></th>
                <th class='view last'>Image</th>
                <th class='edit' filter='false'></th>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        </thead>
        <tbody class="ajax">
<?php 
include "sql.php";
include "functions.php";
$keyword = "";
if (isset($_GET["keyword"]))
	$keyword = "%".$_GET["keyword"]."%";
	$q = 'SELECT id, description FROM company ORDER BY description ASC';
	$company_array = build_result_array($q);

	$q = 'SELECT id, description FROM currency ORDER BY id ASC';
	$currency_array = build_result_array($q);

	$q = $q = "(SELECT id, description FROM item_group WHERE description='Unassigned') UNION (SELECT id, description FROM item_group WHERE description NOT IN ('Unassigned') ORDER BY description ASC)";
	$group_array = build_result_array($q);

	$q = "SELECT id, CONCAT(product_code, ' &ndash; ', item_type.description) AS description FROM item_type ORDER BY item_type.description ASC";
	$product_array = build_result_array($q);

	$q = $q = "(SELECT id, description FROM project WHERE description='Unassigned') UNION (SELECT id, description FROM project WHERE description NOT IN ('Unassigned') ORDER BY description ASC)";
	$project_array = build_result_array($q);

	$q = "SELECT shelf.id, CONCAT(shelf.code, ' @ ', storeroom.description) AS description FROM shelf INNER JOIN storeroom ON shelf.storeroom_id=storeroom.id ORDER BY storeroom.description ASC, shelf.description ASC";
	$location_array = build_result_array($q);
	
	$q = "SELECT item.id, item.serial_number, item.description AS item_description, item.date_added, item.image, item.quantity, item.unit_cost, item.company_id, item.currency_id, item.item_group_id, item.item_type_id, item.project_id, item.shelf_id, 
            company.description AS company, 
            currency.description AS currency, 
            item_group.description AS item_group, 
            CONCAT(item_type.product_code, ' &ndash; ' , item_type.description) AS product, 
            project.description AS project, 
            CONCAT(shelf.code, ' @ ' ,storeroom.description) AS location
            FROM item
			
            INNER JOIN company ON item.company_id=company.id
            INNER JOIN currency ON item.currency_id=currency.id
            INNER JOIN item_group ON item.item_group_id=item_group.id
            INNER JOIN item_type ON item.item_type_id=item_type.id
            INNER JOIN project ON item.project_id=project.id
            INNER JOIN shelf ON item.shelf_id=shelf.id
            INNER JOIN storeroom ON storeroom.id=shelf.storeroom_id
			WHERE item.serial_number LIKE :keyword OR item.description LIKE :keyword OR item.date_added LIKE :keyword OR  item.quantity LIKE :keyword OR item.unit_cost LIKE :keyword OR company.description LIKE :keyword OR currency.description LIKE :keyword OR item_group.description LIKE :keyword OR CONCAT(item_type.product_code, ' &ndash; ' , item_type.description) LIKE :keyword OR project.description LIKE :keyword OR CONCAT(shelf.code, ' @ ' ,storeroom.description) LIKE :keyword
            ORDER BY item.id ASC";
			
$params = array(':keyword' => $keyword);
$result = null;
$rows = sql_select($result, $q, $params);

         foreach ($result as $row) {
            $id = $row['id'];
            $serial_number = $row['serial_number'];
            $description = $row['item_description'];
            $image = $row['image'];
            $quantity = $row['quantity'];
            $unit_cost = $row['unit_cost'];
            $company_id = $row['company_id'];
            $currency_id = $row['currency_id'];
            $group_id = $row['item_group_id'];
            $product_id = $row['item_type_id'];
            $project_id = $row['project_id'];
            $shelf_id = $row['shelf_id'];
            $company = $row['company'];
            $currency = $row['currency'];
            $group = $row['item_group'];
            $product = $row['product'];
            $project = $row['project'];
            $location = $row['location'];
			$date_added = $row['date_added'];
			$tc = "tmp_".$id;
			$fc = "fin_".$id;
		?>
		<tr>
		<td class='view'>
                        <div><?php echo $date_added; ?></div>
                    </td>
                    <td class='edit'>
                        <div><?php echo $date_added; ?></div>
                    </td>
					<td class='view'>
                        <?php if ($serial_number == '') {
                            echo '<div>n/a</div>';
                        }
                        else {
                            echo '<div>' . $serial_number . '</div>';
                        } ?>
                    </td>
                    <td class='edit'>
                        <?php if ($serial_number == '') {
                            echo '<input class="'.$tc.'" type="hidden" name="serial_number" value="' . $serial_number . '">';
                            echo '<div>n/a</div>';
                        }
                        else {
                            echo '<input class="'.$tc.'" type="text" name="serial_number" value="' . $serial_number . '">';
                        } ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $description; ?></div>
                    </td>
                    <td class='edit'>
                        <input <?php echo 'class="'.$tc.'" ' ;?> type='text' name='part_description' value='<?php echo $description; ?>'>
                    </td>
                    <td class='view'>
                        <div><?php echo $product; ?></div>
                    </td>
                    <td class='edit'>
                          <?php build_select_options('product_id', $product_array, array('selected' => $product_id, 'class' => $tc.' chzn-select')) ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $company; ?></div>
                    </td>
                    <td class='edit'>
                        <?php build_select_options('company_id', $company_array, array('selected' => $company_id, 'class' => $tc.' chzn-select')) ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $location; ?></div>
                    </td>
                    <td class='edit'>
                        <?php build_select_options('shelf_id', $location_array, array('selected' => $shelf_id, 'class' => $tc.' chzn-select')) ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $project; ?></div>
                    </td>
                    <td class='edit'>
                        <?php build_select_options('project_id', $project_array, array('selected' => $project_id, 'class' => $tc.' chzn-select')) ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $group; ?></div>
                    </td>
                    <td class='edit'>
                        <?php build_select_options('group_id', $group_array, array('selected' => $group_id, 'class' => $tc.' chzn-select')) ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $currency; ?></div>
                    </td>
                    <td class='edit'>
                        <?php build_select_options('currency_id', $currency_array, array('selected' => $currency_id, 'class' => $tc.' chzn-select')) ?>
                    </td>
                    <td class='view'>
                        <div><?php echo $unit_cost; ?></div>
                    </td>
                    <td class='edit'>
                        <input <?php echo 'class="'.$tc.'" ' ;?> type='text' name='unit_cost' value='<?php echo $unit_cost; ?>'>
                    </td>
                    <td class='view'>
                        <div><?php echo $quantity; ?></div>
                    </td>
                    <td class='edit'>
                        <?php if ($serial_number == '') {
                            echo '<input class="'.$tc.'" type="text" name="quantity" value="' . $quantity . '">';
                        }
                        else {
                            echo '<input class="'.$tc.'" type="hidden" name="quantity" value="' . $quantity . '">';
                            echo '<div>' . $quantity . '</div>';
                        } ?>
                    </td>
                    <td class='view'>
                        <div class='mw100'><a class='preview fancybox' href='<?php echo $path . $image; ?>'><?php echo $image; ?></a></div>
                    </td>
                    <td class='edit'>
                        <div class='fileupload'><span>Upload image</span></div>
                        <input class='fileinput <?php echo $tc; ?>' type='file' accept='image/*' name='imagefile'>
                    </td>
                    <td class='button'>
					<form name='updatepartform<?php echo $id ?>' action='index.php?pid=parts' method='post' enctype="multipart/form-data">
						<input type="hidden" class="<?php echo $fc; ?>" name="serial_number" />
						<input type="hidden" class="<?php echo $fc; ?>" name="part_description" />
						<input type="hidden" class="<?php echo $fc; ?>" name="product_id" />
						<input type="hidden" class="<?php echo $fc; ?>" name="company_id" />
						<input type="hidden" class="<?php echo $fc; ?>" name="shelf_id" />
						<input type="hidden" class="<?php echo $fc; ?>" name="project_id" />
						<input type="hidden" class="<?php echo $fc; ?>" name="group_id" />
						<input type="hidden" class="<?php echo $fc; ?>" name="currency_id" />
						<input type="hidden" class="<?php echo $fc; ?>" name="unit_cost" />
						<input type="hidden" class="<?php echo $fc; ?>" name="quantity" />	
						<!-- file will be cloned by JQuery -->		
					
					
                        <input type='hidden' name='updatepart' value='<?php echo $id ?>'>
                        <input type='hidden' name='image' value='<?php echo $image; ?>'>
                        
						</form>
						<input type="button" name="<?php echo str_replace("fin_", "", $fc);?>" class="edit <?php echo $fc; ?>" value="UP" />	
                    </td>
                
                    <td class='button'>
						<input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='R'>
					</td>
                    <td class='button'>
					<form name='removepartform<?php echo $id ?>' action='index.php?pid=parts' method='post'>
						<input type='hidden' name='removepart' value='<?php echo $id ?>'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='C'>
					</form>
                    </td>
                
            </tr>
<?php
}
?>
</tbody>
    </table>
	
<?php
$gzip_contents = ob_get_contents();
    ob_end_clean();

    $gzip_size = strlen($gzip_contents);
    $gzip_crc = crc32($gzip_contents);

    $gzip_contents = gzcompress($gzip_contents, 9);
    $gzip_contents = substr($gzip_contents, 0, strlen($gzip_contents) - 4);

    echo "\x1f\x8b\x08\x00\x00\x00\x00\x00";
    echo $gzip_contents;
    echo pack('V', $gzip_crc);
    echo pack('V', $gzip_size);



?>