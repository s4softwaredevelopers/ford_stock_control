using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace s4_Scanner
{
    public partial class Capture : Form
    {
        List<Users> UsersList = new List<Users>();
        string LoginName, LoginUser, LoginPassword;
        string message;
        bool state = true;

        public Capture()
        {
            InitializeComponent();
        }

        private void PopulateUsers()
        {
            Loader();
            MyLibrary.UsersPopulate(out state, out message, out UsersList);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void UserscomboboxPopulate()
        {
            comboBoxUsers.Items.Clear();
            foreach (Users user in UsersList)
            {
                comboBoxUsers.Items.Add(user.UserName);
            }
        }

        private void buttonUserLogin_Click(object sender, EventArgs e)
        {
            if (comboBoxUsers.SelectedIndex >= 0)
            {
                Users user = UsersList[comboBoxUsers.SelectedIndex];
                LoginUser = user.UserID;
                LoginName = user.UserName;
            }
            else
            {
                LoginUser = "";
                LoginName = "";
            }
            LoginPassword = textBoxPassword.Text;
            //LoginDescription = textBoxDescription.Text;

            Loader();

            MyLibrary.LoginDetails(out state, out message, LoginName, LoginUser, LoginPassword);
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
                textBoxPassword.Text = "";
                textBoxDescription.Text = "";
                UserscomboboxPopulate();

                panelUser.Hide();
                panelCapture.Show();
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;

        }

        private void Capture_Load(object sender, EventArgs e)
        {
            panelUser.Show();
            panelCapture.Hide();
            PopulateUsers();
            UserscomboboxPopulate();
        }

        private void Loader()
        {
            panelLoader.Show();
            panelLoader.Refresh();
            buttonClear.Hide();
            buttonClear.Refresh();
            buttonClose.Hide();
            buttonClose.Refresh();
            Application.DoEvents();
            panelLoader.Hide();
            buttonClear.Show();
            buttonClose.Show();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            panelUser.Show();
            panelCapture.Hide();

            PopulateUsers();
            UserscomboboxPopulate();
            textBoxPassword.Text = "";

            textBoxDescription.Text = "";
            numericUpDownQuantity.Value = 0;
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            Loader();
            MyLibrary.Print(out state,out message, Decimal.ToInt32(numericUpDownQuantity.Value));
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            Loader();
            MyLibrary.CaptureItems(out state,out message, textBoxDescription.Text, Decimal.ToInt32(numericUpDownQuantity.Value));
            //DisableCheck();
            if (state)
            {
                labelMessage.ForeColor = System.Drawing.Color.Green;

                textBoxDescription.Text = "";
                numericUpDownQuantity.Value = 0;
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }

        private void buttonUandP_Click(object sender, EventArgs e)
        {
            Loader();
            MyLibrary.CaptureItems(out state,out message, textBoxDescription.Text, Decimal.ToInt32(numericUpDownQuantity.Value));
            //DisableCheck();
            if (state)
            {
                MyLibrary.Print(out state,out message, Decimal.ToInt32(numericUpDownQuantity.Value));
                //DisableCheck();
                if (state)
                {
                    labelMessage.ForeColor = System.Drawing.Color.Green;
                    message = "Printed and Updated";
                    textBoxDescription.Text = "";
                    numericUpDownQuantity.Value = 0;
                }
                else
                {
                    labelMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                labelMessage.ForeColor = System.Drawing.Color.Red;
            }
            labelMessage.Text = message;
        }
    }
}