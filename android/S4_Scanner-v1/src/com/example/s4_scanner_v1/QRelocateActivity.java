package com.example.s4_scanner_v1;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import MyContent.MyWebRequest;
import MyContent.QRelocateContent;
import MyContent.UsersContent;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class QRelocateActivity extends Activity {

	private ListView listview;
	private ProgressBar spinner;
	private EditText etBC;
	private TextView tvCounter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_qrelocate);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
	      spinner = (ProgressBar)findViewById(R.id.progressQRelocate);
	      spinner.setVisibility(View.GONE);
		
		listview = (ListView) findViewById(R.id.listViewQRelocate);
		etBC = (EditText) findViewById(R.id.editTextQRelocateBarcode);
		tvCounter = (TextView) findViewById(R.id.textViewQRelocateCounter);
		
		QRelocateContent.adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, QRelocateContent.ITEMS);
		listview.setAdapter(QRelocateContent.adapter);
		registerForContextMenu(listview);
		
		tvCounter.setText("Items for Relocation: " + listview.getCount());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
	if (v.getId() == R.id.listViewQRelocate) {
	    ListView lv = (ListView) v;
	    AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
	    menu.setHeaderTitle(QRelocateContent.ITEMS.get(info.position));

	    menu.add("Delete");
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	  AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
	  //int menuItemIndex = item.getItemId();	  
	  QRelocateContent.deleteItem(info.position);
	  QRelocateContent.adapter.notifyDataSetChanged();
	  tvCounter.setText("Items for Relocation: " + listview.getCount());
	  return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.qrelocate, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public void AddRelocateItem(View view) {
		if (etBC.getText().toString().equals("")) { return;}
		QRelocateContent.addItem(etBC.getText().toString());
		etBC.setText("");
		QRelocateContent.adapter.notifyDataSetChanged();
		tvCounter.setText("Items for Relocation: " + listview.getCount());
		
	}
	
	public void ClearList(View view) {
		QRelocateContent.ITEMS.clear();
		QRelocateContent.adapter.notifyDataSetChanged();
	}
	
	public void ScanBarcode(View view) {
    	Intent intent = new Intent("com.google.zxing.client.android.SCAN");
    	intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
    	startActivityForResult(intent, 0);
	}
	 
		public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		    if (requestCode == 0) {	    
		      if (resultCode == -1) {    	
		    	((EditText) findViewById(R.id.editTextQRelocateBarcode)).setText(intent.getStringExtra("SCAN_RESULT"));
		      } else if (resultCode == 0) {
		        //etScanner.setText("Scan cancelled.");
		      }
		    }
		    if (requestCode == 3) {
		          if (resultCode == -1) {
		        	  tvCounter.setText("Items for Relocation: " + listview.getCount());
		          }
		      }
		}
	
	public void QRelocateServerUpdateEmpty(View view) {
		new WebRequestQRelocateEmpty().execute("");
	}
	
	public void QRelocateServerUpdate(View view) {
        Intent intent = new Intent(this, DestinationActivity.class);
        intent.putExtra("SOURCE", "QRelocate");
        startActivityForResult(intent, 3);
	}
	
	
	class WebRequestQRelocateEmpty extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=setLocation&fid=empty";
			String result = "";
			try{
				
				urlParameters += SendParameters();
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result.equals("")){ return "Connection Lost / Return result empty";}
				return result;
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	if (result.equals("done")) 
	    	{
	    		Toast.makeText(getBaseContext(),"Items Relocate for later",Toast.LENGTH_LONG).show();
	    		QRelocateContent.ITEMS.clear();
	    		QRelocateContent.adapter.notifyDataSetChanged();
	    		tvCounter.setText("Items for Relocation: " + listview.getCount());
	    	}
	    	else
	    	{
	    		Toast.makeText(getBaseContext(),result,Toast.LENGTH_LONG).show();
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	    public String SendParameters() throws UnsupportedEncodingException{
	    	String param = "";
	    	int k = 0;
	    	for (String rItem : QRelocateContent.ITEMS){
	    		if (rItem != ""){
	    			param += "&barcode" + k + "=" + rItem;
	    			k++;
	    		}
	    	}	 
	    	if (UsersContent.bReturnUser == true)
	    	{
	    		param += "&returnUser=" + UsersContent.returnUserid + "&returnDescription=" + URLEncoder.encode(UsersContent.returnDescription, "utf-8");
	    	}
	    	return param;
	    }

	}
	
	
}
