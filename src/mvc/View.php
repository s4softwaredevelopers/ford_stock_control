<?php

error_reporting(E_ERROR);

//dropdown usersmenu
function viewDropdownUsernames($userNames) {
    try {
        $userData = $userNames;
        $dropdown = "<select style='min-width:200px;' class='chosen-select' class=' form-control' name='staffcode'>";
        foreach ($userData as $row) {
            $dropdown .="<option value='$row[0]'>$row[1]</option>\n";
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownProjectsMulti($projects, $id, $company) {
    try {
        $userData = $projects;
        $dropdown = "<select style='width:240px;' class='chosen-select $company' name='$company'>";
        foreach ($userData as $row) {
            if ($row[1] == $id) {
                $dropdown .="<option value='$row[1]' selected>$row[2]</option>\n";
            }
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownProjectsMultiAjax($projects, $id, $company) {
    try {
        $userData = $projects;
        //$dropdown = "<select style='width:240px;' class='chosen-select' name='$company'>";
        $dropdown = "<option value=''>No Project</option>\n";
        foreach ($userData as $row) {
            if ($row[1] == $id) {
                $dropdown .="<option value='$row[1]' selected>$row[2]</option>\n";
            } else {
                $dropdown .="<option value='$row[1]'>$row[2]</option>\n";
            }
        }
        //$dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//Display user authentication form
function viewUserAuthenticateForm($userDropdown) {
    $html = "<form method='post' action='index.php?pid=userLogin&fid=authenticate'>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<br>Please select your name: ";
    $html .= $userDropdown;
    $html .= '<br>';
    $html .= "Please enter password:&nbsp; <input type='password' name='txtPwd' required /><br>";
    $html .= "<input type='submit' value='Submit'/>";
    $html .= "</form>";
    return $html;
}

//Display storeman authentication form
function viewStoremanAuthenticateForm($fid) {
    $html = "<div class='container'><form method='post' class='form-signin' action='index.php?pid=storemanLogin&fid=$fid'>";
    $html .= "<h2 class='form-signin-heading'>Login Details</h2>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<div style='margin-top:15px;'><label for='txtUname'>Username</label></div>";
    $html .= "<input type='text' id='txtUname' name='txtUname' class='form-control' style='width:100%' placeholder='Enter username' required autofocus />";
    $html .= "<div style='margin-top:15px;'><label for='inputPassword'>Password</label></div>";
    $html .= "<input type='password' id='txtPwd' name='txtPwd' class='form-control' style='width:100%' placeholder='Enter password' required />";
    $html .= "<div class='divforgotPassword'><a href='index.php?pid=manageUsers&fid=forgotPassword'>Forgot Password?</a></div>";
    $html .= "<input class='btn btn-lg btn-success ' type='submit' value='Login'/>";
    $html .= "</form></div>"; //
    return $html;
}

//Display storeman authentication form
function viewUsersForm($usersArray) {
    $html = "<form method='post' action='index.php?pid=manageUsers&fid=add'>";
    $html .= "<h2>All Users</h2>";
    $html .= "<table id='tblUsers' class='bordered' style='width:70%'><thead><tr><th filter-type='text'>Username</th><th filter='false'>User Access</th><th filter='false'>Email Address</th><th filter='false'>Action</th><th filter='false'></th></tr></thead><tbody id='userTblBody'>";
    if ($usersArray[0][0] === true) {
        foreach ($usersArray as $value) {
            $html .= "<tr id='$value[1]'><td><input style='width:95%'  class='form-control' type='text' name='editUsername' value='" . $value[2] . "' /></td><td>" . viewDropdownUserTypeBySelected($value[4]) . "</td><td><input style='width:95%' placeholder='Enter email address' class='form-control' type='text' name='editEmail' value='" . $value[5] . "'/></td><td><a href='#' class='btn btn-warning' onclick='UpdateUsers(this);'>Update</a></td><td><a class='btn btn-danger' href='index.php?pid=manageUsers&fid=delete&rowId=$value[1]' onclick='return DeleteConfirmation(\"user\")'>Del</a></td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='4'>" . $usersArray[0][1] . "</td></tr>";
    }
    $html .= "</tbody></table><div class='col-md-12 text-center'><ul class='pagination pagination-lg pager' id='UserPager'></ul>
      </div></form>";
    return $html;
}

//Display add user Form
function viewAddUserForm($usertypesDropdown) {

    $html = "<h2>Manage Users</h2>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<h3>Add Users</h3>";
    $html .= "<table class='tblForm'>";
    $html .= "<form method='get' onsubmit='return isValidForm();' action='index.php?pid='manageUsers&fid=add'>";
    $html .= "<input type='hidden' name='pid' value='manageUsers' />";
    $html .= "<input type='hidden' name='fid' value='add' />";
    $html .= "<tr><th>Username</th><td><input class='form-control' id='focus' type='text' name='username' placeholder='Enter Username'></td></tr>";
    $html .= "<tr><th>Email</th><td><input class='form-control' id='focus' type='email' name='email' placeholder='Enter Email'></td>";
    $html .= "<tr><th>Password</th><td><input class='form-control' class='password' type='password' name='password' placeholder='Enter Password'/><b>minimum of 8 characters</b></td></tr>";
    $html .= "<tr><th>Confirm password</th><td><input class='passwordconfirm form-control' type='password' name='passwordconfirm' placeholder='Repeat Password'></td></tr>";
    $html .= "<tr><th>User type</th><td>$usertypesDropdown</td></tr>";
    $html .= "<tr><td></td><td><input class='btn btn-success' type='submit' value='Add User' /> <input class='btn btn-primary' class='reset' type='reset' value='Cancel' /></td></tr>";
    $html .= "</form>";
    $html .= "</table>";
    return $html;
}

//DropDown usertypes
function viewDropdownUserType($userTypes) {
    try {
        $userData = $userTypes;
        if ($userTypes[0][0] === true) {
            $dropdown = "<select style='min-width:200px;' style=min-width:150px;'' class='chosen-select' name='user_access_id'>";
            foreach ($userData as $row) {
                $dropdown .="<option value=$row[1]>$row[2]</option>\n";
            }
            $dropdown .= "</select>\n";
        } else {
            $dropdown = $userTypes[0][1];
        }

        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//Display Shelves form
function viewShelvesForm($shelvesArray, $locations, $intranetProjects, $autonetProjects) {
    $userId = $_SESSION['userId'];
    $isManager = $_SESSION['isManager'];
    $html = "<form method='post' action='index.php?pid=manageShelves&fid=update'>";
    $html .= "<h3>All Shelves</h3>";
    if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
        $html .= "<table class='bordered' style='width:100%'><thead><tr><th>Barcode</th><th>Description</th><th>Intranet Project</th><th>Autonet Project</th><th>Location</th><th colspan='2'>Action</th></tr></thead><tbody id='shelfTblBody'>";
    } else {
        $html .= "<table class='bordered' style='width:100%'><thead><tr><th>Barcode</th><th>Description</th><th>Intranet Project</th><th>Autonet Project</th><th>Location</th><th>Action</th></tr></thead><tbody id='shelfTblBody'>";
    }
    if ($shelvesArray[0][0] === true) {
        foreach ($shelvesArray as $value) {
            if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
                $html .= "<tr id='$value[1]'><td><input style='width:100%' class='form-control' type='text' name='editBarcode' value='" . $value[2] . "' /><input type='hidden' name='shelfId' value='" . $value[1] . "' /></td><td><input style='width:100%'  class='form-control' type='text' name='editDescription' value='" . $value[3] . "' /></td><td>" . viewDropdownProjectsMulti($intranetProjects, $value[8], 'ddIntranetProjects') . "</td><td>" . viewDropdownProjectsMulti($autonetProjects, $value[9], 'ddAutonetProjects') . "</td><td>" . viewDropdownLocationBySelected($locations, $value[7]) . "</td><td><a class='btn btn-warning' href='#' onclick='UpdateShelf(this);'>Update <a/></td><td><a class='btn btn-danger' href='index.php?pid=manageShelves&fid=delete&rowId=$value[1]' onclick='return DeleteConfirmation(\"shelft\")'>Del<a/></td></tr>";
            } else {
                $html .= "<tr id='$value[1]'><td><input style='width:100%' class='form-control' type='text' name='editBarcode' value='" . $value[2] . "' /><input type='hidden' name='shelfId' value='" . $value[1] . "' /></td><td><input style='width:100%'  class='form-control' type='text' name='editDescription' value='" . $value[3] . "' /></td><td>" . viewDropdownProjectsMulti($intranetProjects, $value[8], 'ddIntranetProjects') . "</td><td>" . viewDropdownProjectsMulti($autonetProjects, $value[9], 'ddAutonetProjects') . "</td><td>" . viewDropdownLocationBySelected($locations, $value[7]) . "</td><td><a class='btn btn-warning' href='#' onclick='UpdateShelf(this);'>Update <a/></td></tr>";
            }
        }
    } else {
        if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
            $html .= "<tr><td colspan='7'>" . $shelvesArray[0][1] . "</td></tr>";
        } else {
            $html .= "<tr><td colspan='6'>" . $shelvesArray[0][1] . "</td></tr>";
        }
    }

    $html .= "</tbody></table><div class='col-md-12 text-center'><ul class='pagination pagination-lg pager' id='shelfPager'></ul>
      </div></form>";
    return $html;
}

//Display add shelf form
function viewAddShelfForm($locationDropdown, $s4ProjectDropDown, $mmProjectDropDown) {

    $html = "<h2>Manage Shelves</h2>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<h3>Print Shelf Barcodes</h3>";
    $html .= "<p>Enter number of barcodes: <input style='width:10%'  class='form-control' type='text' name='txtBarcodeQty' /> <a class='btn btn-primary' style='margin-bottom:3px;' href='#' onclick='PrintShelf(this);'>Print</a></p>";
    $html .= "<h3>Add Shelf</h3>";
    $html .= "<table class='tblForm'>";
    $html .= "<form method='get' action='index.php'>";
    $html .= "<tr><th>Barcode</th><td><input class='form-control' id='focus' type='text' name='barcode' value=''></td></tr>";
    $html .= "<tr><th>Description</th><td><input class='form-control' type='text' name='description' value=''></td></tr>";
    $html .= "<tr><th>S4 Integration - Project</th><td>$s4ProjectDropDown</td></tr>";
    $html .= "<tr><th>S4 Automation - Project</th><td>$mmProjectDropDown</td></tr>";
    $html .= "<tr><th>Location</th><td>$locationDropdown</td></tr>";
    $html .= "<input type='hidden' name='pid' value='manageShelves' />";
    $html .= "<input type='hidden' name='fid' value='add' />";
    $html .= "<tr><td></td><td><input class='btn btn-success' type='submit' value='Add Shelf' /> <input class='btn btn-primary' type='reset' value='Cancel' /></td></tr>";
    $html .= "</form>";
    $html .= "</table>";
    return $html;
}

//Dropdown locations
function viewDropdownLocation($locations) {
    try {
        $userData = $locations;
        $dropdown = "<select style='min-width:200px;' class='chosen-select' name='location_id'>";
        foreach ($userData as $row) {
            $dropdown .="<option value=$row[0]>$row[1]</option>\n";
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//Dropdown by edit shelf
function viewDropdownLocationBySelected($locations, $id) {
    try {
        $userData = $locations;
        $dropdown = "<select style='min-width:200px;' class='chosen-select' name='editLocation'>";
        foreach ($userData as $row) {
            if ($row[0] == $id) {
                $dropdown .="<option value=$row[0] selected>$row[1]</option>\n";
            } else {
                $dropdown .="<option value=$row[0]>$row[1]</option>\n";
            }
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//Dropdown by edit user
function viewDropdownUserTypeBySelected($AccessId) {
    try {
        $con = modelLoginLocalDB('stock_control', $errormsg);
        $q = "SELECT user_access.id,user_access.description FROM `user_access` WHERE user_access.id <> 0 AND user_access.id <> 1";
        $result = mysql_query($q, $con);
        $dropdown = "<select style='min-width:200px;' class='chosen-select' name='editUserAccess'>";
        $num_results = mysql_num_rows($result);
        if ($num_results > 0) {
            while ($row = mysql_fetch_array($result)) {
                if ($row['id'] == $AccessId) {
                    $dropdown .="<option value=" . $row['id'] . " selected>" . $row['description'] . "</option>\n";
                } else {
                    $dropdown .="<option value=" . $row['id'] . ">" . $row['description'] . "</option>\n";
                }
            }
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//Display Storeman main form
function viewStoremanMainForm() {
    $html = "<div class='StoremanContent'>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<h3>Storeman Dashboard</h3>";
    $html .= "</div>";
    return $html;
}

//Display capture stock form
function viewCaptureStock() {
    $html = "<form method='post' action='index.php?fid=5'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Incoming Stock</h1>";
    $html .= "<div class='txtSpaceStoreman' />";
    $html .= "<H3>Do you have an order number?</h3>";
    $html .= "<input type='radio' name='hasOrderNum' value='yes'>Yes<br>";
    $html .= "<input type='radio' name='hasOrderNum' value='no'>No<br>";
    $html .= "</div></form>";
    return $html;
}

function viewNewOrderStock($content) {
    $html = "<form method='post' action='index.php?fid=6'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Incoming New Stock</h1>";
    $html .= "<div id='TrackContent'>";
    $html .= "<div class='tabButtons'>";
    $html .= "<div id='divUntrackedContent'>";
    $html .= "<input type='button' class='btnTrackContent' onclick='CallCheckinTable();' value='Untracked' />";
    $html .= "</div>";

    $html .= "<div id='divTrackedContent'>";
    $html .= "<input type='button' class='btnTrackContent' value='Tracked' />";
    $html .= "</div>";

    $html .= "<div id='divResellTrackedContent'>";
    $html .= "<input type='button' class='btnTrackContent' value='Resell Tracked' />";
    $html .= "</div>";
    $html .= "</div>"; // end of tabButtons div
    $html .= "<div id='newStockMainContent'>";
    if (isset($content)) {
        $html .= $content;
    };
    $html .= "</div>";
    $html .= "</div>"; //end of TrackContent div
    $html .= "</div>"; //end of StoremanContent div
    $html .= "</form>";
    return $html;
}

function viewStockFromSite() {
    $html = "<form method='post' action='index.php?fid=6'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Incoming Stock From Site</h1>";
    $html .= "<div class='txtSpaceStoreman' />";
    $html .= "<H3>Do the item have a barcode?</h3>";
    $html .= "<input type='radio' name='requireBarcode' value='yes'>Yes<br>";
    $html .= "<input type='radio' name='requireBarcode' value='no'>No<br>";
    $html .= "</div></form>";
    return $html;
}

function viewStockFromSiteUntracked($content) {
    $html = "<form method='post' action='index.php?fid=7'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Incoming Stock From Site</h1>";
    $html .= "<div id='UntrackedReturnedContent'>";
    $html .= "<div class='tabButtons'>";
    $html .= "<div id='divUntrackedReturnContent'>";
    $html .= "<input type='button' class='btnTrackContent'  value='Untracked' />";
    $html .= "</div>";

    $html .= "<div id='divTrackedReturnContent'>";
    $html .= "<input type='button' class='btnTrackContent' onclick='BarcodePrint();' value='Tracked' />";
    $html .= "</div>";

    $html .= "</div>"; // end of tabButtons div
    $html .= "<div id='returnStockMainContent'>";
    if (isset($content)) {
        $html .= $content;
    };
    $html .= "</div>";
    $html .= "</div>"; //end of TrackContent div
    $html .= "</div>"; //end of StoremanContent div
    $html .= "</form>";
    return $html;
}

function viewStockCheckin() {
    $html = "<table class='bordered'><thead><th>Consumable Type</th><th>Detail</th><th>Description</th><th>Quantity in Store</th><th>Quantity in/out</th><th><th></thead><tbody>";
    $html .= "<td>terminal</td><td>1mm</td><td>Murr</td><td>10</td><td>2</td><td>In</td><td>Out</td>";
    $html .= "</table>";
    return $html;
}

function viewCaptureOrderNumber() {
    $html = "<form method='post' action='index.php?fid=9'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Incoming Stock</h1>";
    $html .= "<div class='txtSpaceStoreman' />";
    $html .= "<H3>Please enter the order number:</h3>";
    $html .= "<input type='text' name='txtOrderNumber' />";
    $html .= "<input type='submit' value='Enter' />";
    $html .= "</div></form>";
    return $html;
}

function viewScanIn() {
    $html = "<form method='post' action='index.php?fid=10'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Incoming Stock</h1>";
    $html .= "<div class='txtSpaceStoreman' />";
    $html .= "<H3>Please scan Barcode:</h3>";
    $html .= "<input type='text' class='BarcodetextboxLength' name='txtBarcodeNumber' placeholder='Scan Barcode'/>";
    $html .= "<H3>Please enter the details of the responsible person/ origin:</h3>";
    $html .= "<input type='text' name='txtResponsiblePerson' placeholder='Person or origin'/>";
    $html .= "<input type='submit' value='Enter' />";
    $html .= "</div></form>";
    return $html;
}

function viewPrintBarcode() {
    $html = "<form method='post' action='index.php?fid=11'>";
    $html .= "<div class='StoremanContent'>";
    $html .= "<h1>Print Barcode</h1>";
    $html .= "<div class='txtSpaceStoreman' />";
    $html .= "<H3>Please enter the amount of barcodes to print:</h3>";
    $html .= "<input type='text' name='txtNumberOfBarcodes' placeholder='Enter Barcode amount' />";
    $html .= "<input type='submit' value='Print' />";
    $html .= "</div></form>";
    return $html;
}

function viewEnterPartDetails($userDropdown) {
    $tablename = 'tablePartDetails';
    $html = "<form method='post' id='formPartDetails' action='index.php?fid=12'>";
    $html .= "<div class='StoremanPartDetailsContent'>";
    $html .= "<h1>Incoming Stock</h1>";
    $html .= "<div class='txtSpaceStoreman' />";
    $html .= "<H3>Please Enter Part Details below:</h3>";
    $html .= "<input type='button' class='btnPartDetails' value='Add new part details' onclick='AddNewPartDetailsRow(\"$tablename\");' /><br>";
    $html .= "<table class='bordered' id='tablePartDetails'><thead><th>Name</th><th>Description</th></thead><tbody>";
    $html .= "<tr class='trPartDetails'><td><input class='txtboxPartdetails' type='text' id='txtFieldRow1Cell1' "
            . " placeholder='Enter text here...'/></td><td><textarea form='formPartDetails' class='txtareaPartdetails'"
            . " id='txtAreaRow1Cell2' placeholder='Enter text here...'></textarea></td></tr></tbody></table>";

    $html .= "<div class='textboxPartDetailsResponsiblePerson'>Please enter the  responsible person and origin: </div>";
    $html .= "<input type='text' class='txtboxPartdetails' name='txtResponsiblePerson' placeholder='Person or origin'/><br>";

    $html .= "<br><input type='submit' class='btnPartDetails' value='Marry Parts' />";
    $html .= "</div></form>";
    return $html;
}

function viewStoremanMenu() {
    if (isset($_SESSION['userId'])) {
        $uID = '';
        $userId = $_SESSION['userId'];
        $isManager = $_SESSION['isManager'];
        $isMaintenance = $_SESSION['isMaintenance'];
        $status = modelGetUserAccess($userId); //Check access level
        if ($status[0][0] === true) {
            $uID = $status[0][1];
        }
        if ($userId == "30" && $isManager == true || $userId == "030" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true || $uID == '2') {
            $html = "<div class='navbar-collapse collapse'><ul class='nav navbar-nav' style='font-size:14px'><div class='navbar-header'><img class='imgLogo' src='images/s4logo.png' width='50px' height='50px' class='img'/></div>";
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Management<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='index.php?pid=manageUsers&fid=none'>Manage User Accounts</a></li>";
            $html .= "<li><a href='index.php?pid=manageShelves&fid=none'>Manage Shelves</a><a href='index.php?pid=manageEmail&fid=none'>Low Stock Notification Settings</a><a href='index.php?pid=consumable&fid=all' >Manage Consumables</a></ul></li>"; //<a href='index.php?pid=manageShelves&fid=none' >Manage Email Notifications</a>
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Capture<b class='caret'></b></a><ul class='dropdown-menu'><li><li><a href='index.php?pid=ManageOrders&fid=none'>Capture Order</a><ul class='dropdown-menu'><li><a href='index.php?pid=report&fid=order'>View Items By Order Number</a></li><li><a href='index.php?pid=report&fid=itemHistory'>View Item History</a><li></ul></li>";
            $html .= "<li><a href='index.php?pid=noOrderItems&fid=none' >Capture Item (No Order Number)<a/></li></ul></li>";
            $html .= "<li><a href='index.php?pid=marryItems&fid=none'><span class='badge pull-right'>" . $_SESSION['AssignBarcodeAmount'] . "</span>Assign Barcodes&nbsp;</a></li>"; //<span class='badge pull-right'>".$_SESSION['relocationAmount']."</span>
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Relocate<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='index.php?pid=ManageReturns&fid=none'>Tracked Item</a></li><li><a href='index.php?pid=manageConsumableOrder&fid=none'>Consumable Item</a><li></ul></li>";
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Reports<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='index.php?pid=report&fid=order'>View Items By Order Number</a></li><li><a href='index.php?pid=report&fid=itemHistory'>View Tracked Item History</a><li><li><a href='index.php?pid=report&fid=untrackedItemHistory'>View Untracked Item History</a><li><li><a href='index.php?pid=report&fid=barcode'>View Items By Item Barcode</a><li><li><a href='index.php?pid=report&fid=location'>View Items On Shelf</a><li><li><a href='index.php?pid=report&fid=project'>View Item Locations By Project</a><li></ul></li>";
            $html .= "</ul><ul class='nav navbar-nav navbar-right'>";
            $html .= "<li><a href='index.php?pid=storemanMain&fid=none'><span class='badge pull-right'>" . $_SESSION['relocationAmount'] . "</span>Storeman Dashboard&nbsp; </a></li>";
            $html .= "<li><a href='#' >Welcome " . $_SESSION['uname'] . " </a></li><li><a href='index.php?pid=logoff&fid=none'>Logout</a></li></ul></div>";
            return $html;
        } elseif ($isMaintenance === 1) {
            $html = "<div class='navbar-collapse collapse'><ul class='nav navbar-nav' style='font-size:14px'><div class='navbar-header'><img class='imgLogo' src='images/s4logo.png' width='50px' height='50px' class='img'/></div>";
            $html .= "<li><a href='index.php?pid=consumable&fid=all' >Manage Consumables</a></li>";
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Reports<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='index.php?pid=report&fid=order'>View Items By Order Number</a></li><li><a href='index.php?pid=report&fid=itemHistory'>View Tracked Item History</a><li><li><a href='index.php?pid=report&fid=untrackedItemHistory'>View Untracked Item History</a><li><li><a href='index.php?pid=report&fid=barcode'>View Items By Item Barcode</a><li><li><a href='index.php?pid=report&fid=location'>View Items On Shelf</a><li><li><a href='index.php?pid=report&fid=project'>View Item Locations By Project</a><li></ul></li>"; //<li><a href='index.php?pid=report&fid=project'>View Items By Project</a><li>
            $html .= "</ul><ul class='nav navbar-nav navbar-right'>";
            $html .= "<li><a href='#' >Welcome " . $_SESSION['uname'] . " </a></li><li><a href='index.php?pid=logoff&fid=none'>Logout</a></li></ul></div>";
            return $html;
        } elseif ($_SESSION['userId'] != "0") {
            $html = "<div class='navbar-collapse collapse'><ul class='nav navbar-nav' style='font-size:14px'><div class='navbar-header'><img class='imgLogo' src='images/s4logo.png' width='50px' height='50px' class='img'/></div>";
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Management<b class='caret'></b></a><ul class='dropdown-menu'>";
            $html .= "<li><a href='index.php?pid=manageShelves&fid=none' >Manage Shelves</a></ul></li>";
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Capture<b class='caret'></b></a><ul class='dropdown-menu'><li><li><a href='index.php?pid=ManageOrders&fid=none'>Capture Order</a><ul class='dropdown-menu'><li><a href='index.php?pid=report&fid=order'>View Items By Order Number</a></li><li><a href='index.php?pid=report&fid=itemHistory'>View Item History</a><li></ul></li>";
            $html .= "<li><a href='index.php?pid=noOrderItems&fid=none' >Capture Item (No Order Number)<a/></li></ul></li>";
            $html .= "<li><a href='index.php?pid=marryItems&fid=none'><span class='badge pull-right'>" . $_SESSION['AssignBarcodeAmount'] . "</span>Assign Barcodes&nbsp;</a></li>"; //<span class='badge pull-right'>".$_SESSION['relocationAmount']."</span>
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Relocate<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='index.php?pid=ManageReturns&fid=none'>Tracked Item</a></li><li><a href='index.php?pid=manageConsumableOrder&fid=none'>Consumable Item</a><li></ul></li>";
            $html .= "<li class='dropdown'><a data-toggle='dropdown' class='dropdown-toggle'>Reports<b class='caret'></b></a><ul class='dropdown-menu'><li><a href='index.php?pid=report&fid=order'>View Items By Order Number</a></li><li><a href='index.php?pid=report&fid=itemHistory'>View Tracked Item History</a><li><li><a href='index.php?pid=report&fid=untrackedItemHistory'>View Untracked Item History</a><li><li><a href='index.php?pid=report&fid=barcode'>View Items By Item Barcode</a><li><li><a href='index.php?pid=report&fid=location'>View Items On Shelf</a><li><li><a href='index.php?pid=report&fid=project'>View Item Locations By Project</a><li></ul></li>";
            $html .= "</ul><ul class='nav navbar-nav navbar-right'>";
            $html .= "<li><a href='index.php?pid=storemanMain&fid=none'><span class='badge pull-right'>" . $_SESSION['relocationAmount'] . "</span>Storeman Dashboard&nbsp; </a></li>";
            $html .= "<li><a href='#' >Welcome " . $_SESSION['uname'] . " </a></li><li><a href='index.php?pid=logoff&fid=none'>Logout</a></li></ul></div>";
            return $html;
        }
    } else {
        if ($_SESSION['userId'] == "0") {
            // return "<div class='navbar-collapse collapse'><ul class='nav navbar-nav' style='font-size:14px'><div class='navbar-header'><img class='imgLogo' src='images/s4logo.png' width='50px' height='50px' class='img'/></div>";
        } else {
            return "<ul class='nav navbar-nav'><div class='navbar-header'><img src='images/s4logo.png' width='50px' height='50px' class='img'/></div><ul class='nav navbar-nav navbar-right'><li><a href='index.php'>Login</a></li></ul></ul>";
        }
    }
}

function viewInvalidPage() {
    $html = "<h1>Page not found!</h1>";
    $html .= "Please click <a href='index.php'>here<a/> to login.";
    return $html;
}

//dropdown Orders
function viewDropdownOrders($orders) {
    try {
        $userData = $orders;
        $dropdown = "<select style='min-width:200px;' class='chosen-select' name='orderDetails' style='width:300px;'>";
        foreach ($userData as $row) {
            if (empty($row[1])) {
                $dropdown .="<option value=$row[0]>$row[0]</option>\n";
            } else {
                $dropdown .="<option value=$row[0]>$row[0]-$row[1]</option>\n";
            }
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//view Order Page
function viewOrderForm($orderDetails) {
    $html = "<form method='post' action='index.php?pid=ManageOrders&fid=items'>";
    $html .= "<div style='width:50%;float:left'><h2>Capture Order</h2>";
    $html .= "<label>Please enter an order number :</label>&nbsp;";
    if (isset($_SESSION['ProcessingOrder'])) {
        $input = $_SESSION['ProcessingOrder'];
        $html .= "<input class='form-control' placeholder='Enter Order Number' type='text' name='orderDetails' value='$input' />";
    } else {
        $html .= "<input class='form-control' type='text' placeholder='Enter Order Number' name='orderDetails'/>";
    }
    $html .= " <button style='margin-bottom:3px;' class='btn btn-primary' type='submit' value='submit'>View Order Items</button>";
    $html .= "</div><div style='width:45%;float:left'>$orderDetails</div></form>";
    return $html;
}

function viewOrderDetails($orderDetails) {
    $html = "<div>";
    $html .= "<h2>Order details</h2>";
    $html .= "<table class='bordered'><thead><th>Order No</th><th>Description</th><th>Quantity In/Out</th></thead><tbody>";
    foreach ($orderDetails as $value) {
        $html .= "<tr><td>" . $value[0] . "</td><td>" . $value[1] . "</td><td>" . $value[2] . "</td></tr>";
    }
    $html .= "</tbody></table></div>";
    return $html;
}

function viewOrderDetailsTracked($orderDetails) {
    $html = "<form method='post' action='index.php?pid=ManageOrders&fid=print'>";
    $html .= "<hr/><h2>Ordered Items - Tracked</h2>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<table class='bordered'><thead><th>Order No</th><th>Supplier Code</th><th>Project</th><th>Description</th><th>Quantity Ordered</th><th>Total Quantity Received</th><th>Quantity Received</th><th>Notify People</th><th colspan='4'>Number Of Items To Track</th></thead><tbody>";
    if ($orderDetails[0][0] == true) {
        foreach ($orderDetails as $value) {
            $html .= "<tr><td>$value[1]</td><td>$value[6]<input type='hidden' name='supplierCode$value[4]' value='$value[6]'/></td><td>$value[7]<input type='hidden' name='project$value[4]' value='$value[7]'/></td>"
                    . "<td>$value[2]<input type='hidden' name='itemDescription$value[4]' value='$value[2]'/></td><td>$value[3]<input type='hidden' name='qtyOrdered$value[4]' value='$value[3]'/></td>"
                    . "<td name='totalReceived'>$value[5]<input type='hidden' name='totalReceived$value[4]' value='$value[5]'/></td><td><input type='text' placeholder='Enter quantity' style='width:95%' name='qtyReceived$value[4]' class='qtyOrderTrackedValues txtQtyReceivedAmount'/><td><input type='text' id='example_emailBS$value[4]' class='example_emailBS' name='example_emailBS' class='form-control' value=''></td><input type='hidden' name='isTrackResell' value='0' /><td><input type='text' placeholder='Enter amount' name='txtItemsCount$value[4]' class='txtItemsCount' /></td></td>"
                    . "<td><input type='submit' name='Print' class='btn btn-primary' value='Print'/></td></tr>";
        }
        $html .= "<tr><td style='text-align:right' colspan='8'><input type='submit' class='btn btn-success' name='update'  value='Save All' /></td><td><input  type='submit' class='btn btn-info' name='SaveAndPrint' value='S&P'  /></td></tr>";
    } else {
        $html .= "<tr><td colspan='8' style='text-align:center'>" . $orderDetails[0][1] . "</td></tr>";
    }

    $html .= "</tbody></table></form>";
    return $html;
}

function viewOrderDetailsTrackedResell($orderDetails) {
    $html = "<form method='post' action='index.php?pid=ManageOrders&fid=print'>";
    $html .= "<hr/><h2>Ordered Items - Tracked Resale</h2>";
    if (isset($_SESSION['statusTrackedResell'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['statusTrackedResell'] . "</div>";
    }
    if (isset($_SESSION['resell'])) {
        $html .= "<h2 style='color:red;font-weight:bold'>" . $_SESSION['resell'] . "</h2>";
        $html .= "<table class='bordered' style='border:3px solid red !important'>";
    } else {
        $html .= "<table class='bordered'>";
    }
    $html .= "<thead><th>Order No</th><th>Supplier Code</th><th>Project</th><th>Description</th><th>Quantity Ordered</th><th>Total Quantity Received</th><th colspan='4'>Quantity Received</th></thead><tbody>";
    if ($orderDetails[0][0] == true) {
        foreach ($orderDetails as $value) {
            $html .= "<tr><td>$value[1]</td><td>$value[6]<input type='hidden' name='supplierCode$value[4]' value='$value[6]'/></td><td>$value[7]<input type='hidden' name='project$value[4]' value='$value[7]'/></td>"
                    . "<td>$value[2]<input type='hidden' name='itemDescription$value[4]' value='$value[2]'/></td><td>$value[3]<input type='hidden' name='qtyOrdered$value[4]' value='$value[3]'/></td>"
                    . "<td name='totalReceived'>$value[5]<input type='hidden' name='totalReceived$value[4]' value='$value[5]'/></td><td><input placeholder='Enter quantity' style='width:95%' type='text' name='qtyReceived$value[4]' class='qtyOrderTrackedValues'/><input type='hidden' name='isTrackResell' value='1' /></td>"
                    . "<td><input type='submit' name='Print' class='btn btn-primary' value='Print'/></td></tr>";
        }
        $html .= "<tr><td style='text-align:right' colspan='7'><input type='submit' class='btn btn-success' name='update'  value='Save All' /></td><td><input  type='submit' class='btn btn-info' name='SaveAndPrint' value='Save & Print'  /></td></tr>";
    } else {
        $html .= "<tr><td colspan='8' style='text-align:center'>" . $orderDetails[0][1] . "</td></tr>";
    }
    $html .= "</tbody></table></form>";
    return $html;
}

function viewOrderDetailsUntracked($orderDetails, $types) {
    $html = "<form method='post' action='index.php?pid=ManageOrders&fid=untracked'>";
    $html .= "<hr/><h2>Ordered Items - Untracked</h2>";
    if (isset($_SESSION['statusUntracked'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['statusUntracked'] . "</div>";
    }
    $html .= "<table class='bordered'><thead><th>Order No</th><th>Supplier Code</th><th>Project</th><th>Description</th><th>Quantity Ordered</th><th>Total Quantity Received</th><th>Quantity Received</th></thead><tbody>";
    if (isset($_SESSION['userId'])) {
        $userID = $_SESSION['userId'];
    }
    if ($orderDetails[0][0] == true) {
        foreach ($orderDetails as $value) {

            $html .= "<tr id='$value[4]'><td>" . $value[1] . "<input type='hidden' name='txtOrderNumber$value[4]' value='$value[1]'/></td><td>$value[6]<input type='hidden' name='supplierCode$value[4]' value='$value[6]'/></td><td>$value[7]<input type='hidden' name='project$value[4]' value='$value[7]'/></td>"
                    . "<td>$value[2]<input type='hidden' name='itemDescription$value[4]' value='$value[2]'/></td><td>$value[3]<input type='hidden' name='qtyOrdered$value[4]' value='$value[3]'/></td>"
                    . "<td name='totalReceived'>$value[5]<input type='hidden' name='totalReceived$value[4]' value='$value[5]' /><input type='hidden' value='$value[4]' name='itemId' /></td><td><input placeholder='Enter quantity' style='width:95%' type='text' name='qtyReceived$value[4]'/></td>"
                    . "<input type='hidden' value='$userID' name='userId'/></tr>";
        }
        $html .= "<tr><td colspan='7' style='text-align:right;'><input type='submit' class='btn btn-primary' name='update' value='Save All' /></td></tr>";
    } else {
        $html .= "<tr><td colspan='7' style='text-align:center'>" . $orderDetails[0][1] . "</td></tr>";
    }
    $html .= "</tbody></table></form>";
    return $html;
}

function viewOrderDetailsConsumable($orderDetails, $types) {
    $html = "<form method='post' action='index.php?pid=ManageOrders&fid=consumable'>";
    $html .= "<hr/><h2>Ordered Items - Consumables</h2>";
    if (isset($_SESSION['statusConsumable'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['statusConsumable'] . "</div>";
    }
    $html .= "<table class='bordered'><thead><th>Order No</th><th>Supplier Code</th><th>Project</th><th>Description</th><th>Quantity Ordered</th><th>Total Quantity Received</th><th>Consumable Type</th><th>Quantity Received</th></thead><tbody>";
    if (isset($_SESSION['userId'])) {
        $userID = $_SESSION['userId'];
    }
    if ($orderDetails[0][0] == true) {
        foreach ($orderDetails as $value) {

            $html .= "<tr id='$value[4]'><td>" . $value[1] . "<input type='hidden' name='txtOrderNumber$value[4]' value='$value[1]'/></td><td>$value[6]<input type='hidden' name='supplierCode$value[4]' value='$value[6]'/></td><td>$value[7]<input type='hidden' name='project$value[4]' value='$value[7]'/></td>"
                    . "<td>$value[2]<input type='hidden' name='itemDescription$value[4]' value='$value[2]'/></td><td>$value[3]<input type='hidden' name='qtyOrdered$value[4]' value='$value[3]'/></td>"
                    . "<td name='totalReceived'>$value[5]<input type='hidden' name='totalReceived$value[4]' value='$value[5]' /><input type='hidden' value='$value[4]' name='itemId' /></td><td>$types</td><td><input placeholder='Enter quantity' style='width:95%' type='text' name='qtyReceived$value[4]'/></td>"
                    . "<input type='hidden' value='$userID' name='userId' /></tr>";
        }
        $html .= "<tr><td colspan='8' style='text-align:right;'><input type='submit' class='btn btn-success' name='update' value='Save All'/></td></tr>";
    } else {
        $html .= "<tr><td colspan='8' style='text-align:center'>" . $orderDetails[0][1] . "</td></tr>";
    }
    $html .= "</tbody></table></form>";
    return $html;
}

function viewAllPrintedBarcodeItems($items) {
    $userId = $_SESSION['userId'];
    $isManager = $_SESSION['isManager'];
    $html = "<form name='FormMarryItem' method='post' action='index.php?pid=marry&fid=item'>";
    $html .= "<h2>Assign Barcodes To Items...</h2>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    if ($items[0][0] == true) {
        if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
            $html .= "<table id='marryTable' data-pagination='true' class='bordered table table-hover'  cellspacing='0' width='100%'><thead><th filter-type='text'>Order Number</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Description</th><th filter='false'>Item Barcode</th><th filter='false'>Action</th></thead><tbody id='marryTblBody'>";
        } else {
            $html .= "<table id='marryTable' data-pagination='true' class='bordered table table-hover'  cellspacing='0' width='100%'><thead><th filter-type='text'>Order Number</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Description</th><th filter='false'>Item Barcode</th></thead><tbody id='marryTblBody'>";
        }
        foreach ($items as $value) {
            if (empty($value[2])) {
                $value[2] = "No Order Number";
            }
            if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
                $html .= "<tr><td>" . $value[2] . "</td><td>" . $value[6] . "</td><td>" . $value[3] . "</td><td><input style='width:95%' class='form-control ScanBarcode' placeholder='Scan item barcode' type='text' name='txtBarcode$value[1]' value='$value[4]' /></td><td><a class='btn btn-danger' href='index.php?pid=AssignBarcode&fid=delete&ItemsId=$value[1]' onclick='return DeleteConfirmation(\"item\")'>Del</a></td></tr>";
            } else {
                $html .= "<tr><td>" . $value[2] . "</td><td>" . $value[6] . "</td><td>" . $value[3] . "</td><td><input style='width:95%' placeholder='Scan item barcode' class='form-control ScanBarcode' type='text' name='txtBarcode$value[1]' value='$value[4]' /></td></tr>";
            }
        }

        $html .= "</tbody>";
        if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
            $html .= "<tfoot><tr><td colspan='4'></td><td style='text-align:center' ><input class='btn btn-lg btn-success ' type='submit' name='save' value='Save All' /></td></tr><tfoot>";
        } else {
            $html .= "<tfoot><tr><td colspan='3'></td><td style='text-align:center' ><input class='btn btn-lg btn-success ' type='submit' name='save' value='Save All' /></td></tr><tfoot>";
        }
        $html .= "</table><div class='col-md-12 text-center'><ul class='pagination pagination-lg pager' id='marryPager'></ul>
      </div></form></hr>";
    } else {
        $html .= "<table id='marryTable' class='bordered'  cellspacing='0' width='100%'><thead><th filter='false'>Order Number</th><th filter-type='false'>Description</th><th filter='false'>Item Barcode</th><th filter='false'>Shelf Barcode</th></thead><tbody>";
        $html .= "<tr><td colspan='4' style='text-align:center;'>" . $items[0][1] . "</td></tr>";
        $html .= "</tbody></table></hr>";
    }

    return $html;
}

function viewAllMarryItems($items) {
    $html = "<h1>Completed Items</h1>";
    $html .= "<table class='bordered'><thead><th>Order Number</th><th>Description</th><th>Item Barcode</th><th>Shelf Barcode</th><th>Shelf Description</th></thead><tbody>";
    foreach ($items as $value) {
        $html .= "<tr><td>" . $value[1] . "</td><td>" . $value[2] . "</td><td>" . $value[3] . "</td><td>" . $value[4] . "</td><td>" . $value[5] . "</td></tr>";
    }
    $html .= "</tbody></table>";
    return $html;
}

function viewAllBarcodeScannedItems($items) {
    $html = "<h1>Barcode Scanned Items</h1>";
    $html .= "<table class='bordered'><thead><th>Order Number</th><th>Description</th><th>Item Barcode</th></thead><tbody>";
    foreach ($items as $value) {
        $html .= "<tr><td>" . $value[1] . "</td><td>" . $value[2] . "</td><td>" . $value[3] . "</td></tr>";
    }
    $html .= "</tbody></table>";
    return $html;
}

function viewMobileBarcodeItems($items) {
    if ($items[0][0] == true) {
        $html = "<table class='bordered'>";
        foreach ($items as $value) {
            $html .= "<tr><td>$value[0]</td><td>$value[1]</td><td>$value[2]</td><td>$value[6]</td><td>$value[7]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td></tr>";
        }
        $html .= "</table>";
    } else {
        $html .= 'No items found!';
    }
    return $html;
}

function viewTableMarryItem($orderNumber, $description) {
    $html = "<div>Order Number:$orderNumber</div>";
    $html .= "<div>Description:$description</div>";
    $html .= "<div>Item Barcode:<input type='text' style='width=150px;' name='itemBarcodeValue'/></div>";
    $html .= "<div>Shelf Barcode:<input type='text' style='width =150px;' name='shelfBarcodeValue'/></div>";
    $html .= "<input type='hidden' name='itemId' value=''/>";
    $html .= "<input type='button' value='Save' onClick='closeWindow();' style='Width:150px;' />";
    return $html;
}

function viewScanTrackedReturnItems($userToUserS4, $shelfToUserS4, $userToReceiveUserS4, $userToShelfS4) {
    $html = '';
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<div style='width:100%;float:left;'>";
    $html .= "<table style='border-color:#428BCA;' id='tableRelocateWizard'>";
    $html .= "<tr><td id='tdStepOne' class='q'>Where does the item come from? </td><td id='tdTarget' rowspan='11' style='width:60%;'>";
    $html .= "</td></tr>";
    $html .= "<tr style='border-collapse: separate;'><td id='tdStepOneResult' class='a'>?</td></tr>";
    $html .= "<tr><td id='tdStepTwo' class='q'>Login/Scan Shelf</td></tr>";
    $html .= "<tr><td id='tdStepTwoResult' class='a'>?</td></tr>";
    $html .= "<tr><td id='tdStepThree' class='q'>Scan items</td></tr>";
    $html .= "<tr><td id='tdStepThreeResult' class='a'>?</td></tr>";
    $html .= "<tr><td id='tdStepFour' class='q'>Where are these items going to?</td</tr>";
    $html .= "<tr><td id='tdStepFourResult' class='a'>?</td></tr>";
    $html .= "<tr><td id='tdStepFive' class='q'>Login/Scan Shelf/Pending</td</tr>";
    $html .= "<tr><td id='tdStepFiveResult' class='a'>?</td</tr>";
    $html .= "<tr><td id='tdStepSix' class='q'>Finish</td</tr>";
    $html .= "</table></div>";
    return $html;
}

function viewScanTrackedReturnStoreItems() {
    $form_token = uniqid();
    $_SESSION['form_token'] = $form_token;
    $html = "<hr /><div id='divRelocateLater'><div style='width:60%;float:left'><form method='post' action='index.php?pid=ManageReturns&fid=store'>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "";
    $html .= "<h3>Relocate later</h3>";
    $html .= "<table class='tblForm'><tr><td><label>Scan Barcode:</label></td><td><input  style='width:50%' class='form-control' type='text' name='barcodeVal' />";
    $html .= " <input type='submit' class='btn btn-primary' value='Save' onClick='submitAndClose();return:true;' /></td><td><input type='hidden' name='staffcodeVal' value='' /><input type='hidden' name='myDb' value='' /></td></tr></table>";
    $html .= "<input type='hidden' name='form_token' value='$form_token' /></form></div></div>";
    return $html;
}

function viewAllTrackedReturnItems($items, $users) {
    $userId = $_SESSION['userId'];
    $isManager = $_SESSION['isManager'];
    $html = "<h3>Awaiting Relocation...</h3>";

    if ($items[0][0] == true) {
        if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
            $html .= "<form method='post' action='index.php?pid=AssignLocation&fid=change'><table id='tblAwaitingRelocation' class='bordered' style='width:100%'><thead><th filter-type='text'>Order Number</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Item Barcode</th><th filter-type='text'>Description</th><th filter-type='text'>Previous location</th><th filter='false'>Relocate</th><th filter='false'>Action</th></thead><tbody>";
        } else {
            $html .= "<form method='post' action='index.php?pid=AssignLocation&fid=change'><table id='tblAwaitingRelocation' class='bordered' style='width:100%'><thead><th filter-type='text'>Order Number</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Item Barcode</th><th filter-type='text'>Description</th><th filter-type='text'>Previous location</th><th filter='false'>Relocate</th></thead><tbody>";
        }

        foreach ($items as $value) {
            if (empty($value[9]) === false) {
                $value[9] = "::" . $value[9];
            }
            if (empty($value[10]) === false) {
                $value[10] = "::" . $value[10];
            }
            if (empty($value[8]) === false) {
                $value[8] = "Location: " . $value[8];
            }
            if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
                $html .= "<tr  class='tblAwaitingRelocationsRow'>"
                        . "<td>$value[2]</td><td>$value[11]</td><td><input type='hidden' name='ItemBarcode' value='checkedItem$value[6]'/>$value[6]</td>"
                        . "<td><input type='hidden' name='itemDescription' value='$value[4]'/>$value[4]</td>"
                        . "<td><input type='hidden' name='prevShelfBarcode' value='$value[8]'/>$value[8] $value[9] $value[10]</td>"
                        . "<input type='hidden' name='prevShelfDescription' value='$value[8]'/><td>"
                        . "<div class='checkbox checkbox-primary'><input type='checkbox' name='checkedItem$value[6]' id='$value[1]'/><label for='$value[1]'>relocate?</label></div></td><td><a class='btn btn-danger' href='index.php?pid=Relocate&fid=remove&ItemId=$value[1]' onclick='return DeleteConfirmation(\"Item\");'>Del</a></td></tr>";
            } else {
                $html .= "<tr  class='tblAwaitingRelocationsRow'>"
                        . "<td>$value[2]</td><td>$value[11]</td><td><input type='hidden' name='ItemBarcode' value='checkedItem$value[6]'/>$value[6]</td>"
                        . "<td><input type='hidden' name='itemDescription' value='$value[4]'/>$value[4]</td>"
                        . "<td><input type='hidden' name='prevShelfBarcode' value='$value[8]'/>$value[8] $value[9] $value[10]</td>"
                        . "<input type='hidden' name='prevShelfDescription' value='$value[8]'/><td>"
                        . "<div class='checkbox checkbox-primary'><input type='checkbox' name='checkedItem$value[6]' id='$value[1]'/><label for='$value[1]'>relocate?</label></div></td></tr>";
            }
        }
        if ($userId == "30" && $isManager == true || $userId == "258" && $isManager == true || $userId == "276" && $isManager == true) {
            $html .= "<tr><td colspan=5' style='text-align:right;'></td> "//<input  style='width:20%' class='form-control' type='text' name='shelfBarcodeVal' value='' />
                    . "</td><td colspan='2'><input type='button' id='btnContinueRelocate' class='btn btn-success' data-toggle='modal' data-target='#myModal' value='Checkout items'></input><div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                        <h4 class='modal-title' id='myModalLabel'>Please complete below:</h4>
                      </div>
                      <div class='modal-body'>
                      <div class='divChooseLocation'>
                      <p>Please select where these items are going to: <input type='button' class='btn btn-primary' name='btnToShelfLocation' value='To Shelf'/><input name='locationType' type='hidden' value=''/>&nbsp;<input type='button' class='btn btn-primary' name='btnToUserLocation' value='To User'/></p>
                      </div>
                      <div style='display:none;' class='divToShelf'>
                      <p>Scan Shelf : <input type='text' placeholder='Scan shelf barcode' style='width:50%' class='form-control' name='toShelfBarcode'/></p>
                      </div>
                      <div style='display:none;' class='divToUser'>
                        <p id='awaitingRelocateUserPleaseSelectText'>Please select :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $users </p>
                        <p>Please enter password:&nbsp; <input type='password' placeholder='Enter password' style='width:40%' class='form-control' name='receivePassword' /></p>
                        <p>Reason for taking:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' placeholder='Enter reason for taking item' style='width:40%' class='form-control' name='receiveDescription'/></p>
                        </div>
                      </div>
                      <div class='modal-footer'>
                      <div id='btnCancelLocationSelection'>
                      <input type='button' class='btn btn-primary' data-dismiss='modal' value='Cancel'></input>
                      </div>
                      <div style='display:none' id='btnSaveToUser'>
                        <input type='button' class='btn btn-primary' data-dismiss='modal' value='Cancel'></input>
                        <input type='submit' class='btn btn-success' value='Save changes'></input>
                        </div>
                      </div>
                    </div>
                  </div>
                </div></td></tr></tbody></table><input type='hidden' name='username' value=''/></form>";
        } else {
            $html .= "<tr><td colspan='5' style='text-align:right;'></td> "//<input  style='width:20%' class='form-control' type='text' name='shelfBarcodeVal' value='' />
                    . "</td><td><input type='button' id='btnContinueRelocate' class='btn btn-primary' data-toggle='modal' data-target='#myModal' value='Continue'></input><div class='modal fade' id='myModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                  <div class='modal-dialog'>
                    <div class='modal-content'>
                      <div class='modal-header'>
                        <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
                        <h4 class='modal-title' id='myModalLabel'>Please complete below:</h4>
                      </div>
                      <div class='modal-body'>
                      <div class='divChooseLocation'>
                      <p>Please select where these items are going to: <input type='button' class='btn btn-primary' name='btnToShelfLocation' value='To Shelf'/><input name='locationType' type='hidden' value=''/>&nbsp;<input type='button' class='btn btn-primary' name='btnToUserLocation' value='To User'/></p>
                      </div>
                      <div style='display:none;' class='divToShelf'>
                      <p>Scan Shelf : <input type='text' placeholder='Scan Shelf Barcode' style='width:50%' class='form-control' name='toShelfBarcode'/></p>
                      </div>
                      <div style='display:none;' class='divToUser'>
                        <p id='awaitingRelocateUserPleaseSelectText'>Please select :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $users </p>
                        <p>Please enter password:&nbsp; <input type='password' style='width:40%' placeholder='Enter password' class='form-control' name='receivePassword' /></p>
                        <p>Reason for taking:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input placeholder='Enter reason for taking item' type='text' style='width:40%' class='form-control' name='receiveDescription'/></p>
                        </div>
                      </div>
                      <div class='modal-footer'>
                      <div id='btnCancelLocationSelection'>
                      <input type='button' class='btn btn-primary' data-dismiss='modal' value='Cancel'></input>
                      </div>
                      <div style='display:none' id='btnSaveToUser'>
                        <input type='button' class='btn btn-primary' data-dismiss='modal' value='Cancel'></input>
                        <input type='submit' class='btn btn-success' value='Save changes'></input>
                        </div>
                      </div>
                    </div>
                  </div>
                </div></td></tr></tbody></table><input type='hidden' name='username' value=''/></form>";
        }
    } else {
        $html .= "<table class='bordered'><thead><th>Database</th><th>Order Number</th><th>Item Barcode</th><th>Description</th><th>Previous Shelf Barcode</th><th>Previous Shelf Description</th><th>Previous Storeroom Location</th><th>Scan Shelf Barcode</th></thead><tbody>";

        $html .= "<tr><td colspan='8' style='text-align:center;'>" . $items[0][1] . "</td></tr>";
        $html .= "</tbody></table></hr>";
    }

    return $html;
}

function viewSetLocationPage($users, $id, $itemBarcode, $itemDescription, $shelfBarcode, $shelfDescription) {
    $html = "<form method='post' action='index.php?pid=ManageLocation&fid=change'>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<h1>Set Location</h1>";
    $html .= "<b>Item History</b><table class='bordered'>";
    $html .= "<th >Item Barcode </th><th>Description </th><th>Shelf Barcode </th><th>Shelf Description</th>";
    $html .= "<tr><td>$itemBarcode</td><td>$itemDescription</td>";
    $html .= "<td>$shelfBarcode  </td><td> $shelfDescription</td><tr></table>";

    $html .= "</form>";
    return $html;
}

function viewMobileGetUsers($users) {
    $html = "<table>";
    foreach ($users as $row) {
        $html .="<tr><td>$row[0]</td><td>$row[1]</td></tr>";
    }
    $html .= "</table>";
    return $html;
}

//dropdown usersmenu
function viewDropdownUsernamesMulti($userNames, $id) {
    try {
        $userData = $userNames;
        $dropdown = "<select style='min-width:240px;' class='chosen-select' id='$id' name='staffcode'>";
        foreach ($userData as $row) {
            $dropdown .="<option value='$row[0]'>$row[1]</option>\n";
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownUsernamesMultiRelocate($userNames, $id) {
    try {
        $userData = $userNames;
        $dropdown = "<select style='min-width:180px;' class='chosen-select' id='$id' name='staffcode'>";
        foreach ($userData as $row) {
            $dropdown .="<option value='$row[0]'>$row[1]</option>\n";
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

//view for untracked return items
function viewUntrackedReturnItems($user) {
    $html = "<div class='divCaptureItem'><form method='post' action='index.php?pid=noOrderItems&fid=update'>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<h2>Capture item</h2>";
    $html .= "<table class='tblForm'><tr><td>Description: </td><td><input placeholder='Enter description' style='width:67%' class='form-control' type='text' name='txtDescription' required/></td></tr>";
    $html .= "<tr><td>Quantity:</td><td><input style='width:37%' placeholder='Enter quantity' class='form-control' type='text' name='txtQty' required/></td></tr>";
    $html .= "<tr><td>Part number:</td><td><input placeholder='Enter part number' style='width:67%' class='form-control' type='text' name='txtPartNumber' /></td></tr>";
    $html .= "<tr id='txtSelectUsername'><td>Please select return user:</td><td>$user</td></tr>";
    $html .= "<tr id='txtEnterPwd'><td>Please enter password:</td><td><input placeholder='Enter password' style='width:67%' class='form-control' type='password' name='pWord' required/></td></tr></table>";
    $html .= "<div><input type='hidden' name='staffcodeValue' value='' /></div>";
    $html .= "<p><input class='btn btn-success' type='submit' name='update' value='Save' /> <input class='btn btn-info' type='submit' name='UpdateAndPrint' value='Save and Print' /> <input class='btn btn-primary' type='submit' name='print' value='Print' /></p>";
    $html .= "</form></div>";
    return $html;
}

function viewGetAllUntrackedReturns($items) {
    $html = "<form name='FormMarryNoOrderItem' method='post' action='index.php?pid=marryItems&fid=none'>";
    $html .= "<table class='bordered' id='tblNoOrderMarry'><thead><th>OrderNumber</th><th>Description</th><th>Barcode</th></thead><tbody>"; //
    foreach ($items as $value) {
        $html .= "<tr id='$value[1]'><td></td><td>" . $value[2] . "</td><td><input type='text' name='txtBarcodeValue'/></td><td>"
                . "<input type='button' name='save' onclick='btnMarryNoOrderItem(this);'  value='S' /></td></tr>";
    }
    $html .= "</tbody></table></form></hr>";
    return $html;
}

function viewUpdateConsumables($user, $types) {
    $html = "<div class='divCaptureConsumable'><form method='post' action='index.php?pid=ManageOrders&fid=consumableItem'>";
    if (isset($_SESSION['statusConsumable'])) {
        $type = $_SESSION['msgTypeConsumable'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['statusConsumable'] . "</div>";
    }
    if (isset($_SESSION['userId'])) {
        $userID = $_SESSION['userId'];
    }
    $html .= "<h2>Capture Consumable</h2>";
    $html .= "<table class='tblForm'><tr><td>Quantity:</td><td><input style='width:45%' placeholder='Enter quantity' class='form-control' type='text' name='qtyReceived' required/></td></tr>";
    $html .= "<tr><td>Select type:</td><td> $types</td></tr>";
//         $html .= "<tr><td>Please choose:</td><td><input type='radio' name='radDb2' value='s4'>S4 Intergration <input type='radio' name='radDb2' value='mm'>S4 Automation</td></tr>";
    $html .= "<tr id='SelectUsername2'><td>Please select return user:</td><td>$user</td></tr>";
    $html .= "<tr id='EnterPwd2'><td>Please enter password:</td><td><input style='width:67%' placeholder='Enter password' class='form-control' type='password' name='pWord' required/></td></tr><table>";
    $html .= "<div><input type='hidden' name='staffcodeValue' value='' /></div>";
    $html .= "<p><input type='hidden' value='$userID' name='userId' /><input class='btn btn-success' type='submit' name='update' value='Save' />"; //<input type='submit' name='UpdateAndPrint' value='UP' /><input type='submit' name='print' value='P' /></p>
    $html .= "</form></div>";
    return $html;
}

//dropdown consumable types
function viewDropdownConsumableType($types) {
    if ($types[0][0] === true) {
        try {
            $userData = $types;
            $dropdown = "<select style='min-width:200px;' class='chosen-select' name='type'>";
            foreach ($userData as $row) {
                $dropdown .="<option value=$row[1]>$row[2]-$row[3]</option>\n";
            }
            $dropdown .= "</select>\n";
            return $dropdown;
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    } else {
        $dropdown = "No Consumables found!";
        return $dropdown;
    }
}

//view relocate consumables
function viewRelocateConsumable($items) {
    $html = "<div style='float:left'>";
    $html .= "<hr/><h3> Relocation - Consumables</h3>";
    if (isset($_SESSION['userId'])) {
        $userID = $_SESSION['userId'];
    }
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<table class='bordered' id='tblRelocateConsumable'><thead><th filter-type='text'>Category</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Description</th><th filter-type='text'>Detail</th><th filter-type='text'>Total Quantity</th><th filter-type='text'>Net Value</th><th filter='false'>Quantity Received</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($items as $value) {
            $html .= "<tr id='$value[1]' class='tblAwaitingRelocationConsumableRow'>"
                    . "<td>$value[2]</td>"
                    . "<td>$value[5]</td>"
                    . "<td>$value[3]</td>"
                    . "<td>$value[4]</td>"
                    . "<td>$value[6]</td>"
                    . "<td>$value[7]</td>"
                    . "<td><form method='post' action='index.php?pid=ManageOrders&fid=consumableRelocate'><input size='10' type='text' class='.input-group-lg form-control' name='qtyReceived' value='0' />"
                    . "<input type='hidden' value='$userID' name='userId' /><input type='hidden' value='$value[1]' name='consumableId' /> <input type='submit' class='btn btn-success' name='in'  value='IN'/> <input class='btn btn-warning' type='submit' name='out' value='OUT'/></form></td></tr>"; /*
             * <div class='modal-dialog'>
              <div class='modal-content'>
              <div class='modal-header'>
              <button type='button' class='close' data-dismiss='modal'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button>
              <h4 class='modal-title' id='myModalLabel'>Please complete below:</h4>
              </div>
              <div class='modal-body'>
              <p>Please enter your details: </p>
              <p>Please select :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $users </p>
              <p>Please enter password:&nbsp; <input type='password' style='width:40%' class='form-control' name='receivePassword' /></p>
              <p>Reason for taking:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' style='width:40%' class='form-control' name='receiveDescription'/></p>
              </div>
              <div class='modal-footer'>
              <input type='button' class='btn btn-primary' data-dismiss='modal' value='Cancel'></input>
              <input type='submit' class='btn btn-primary' value='OUT'></input>
              </div>
              </div>
              </div>
              </div> */
        }
    } else {
        $html .= "<tr><td colspan='7' style='text-align:center'>No items found</td></tr>";
    }
    $html .= "</tbody></table></div>";
    return $html;
}

function viewReportsOrder($orderItems, $items) {
    $html = "";
    $accordion = "";
    $html .= "<table id='tblReportByOrder' class='bordered'><thead><th>Supplier Code</th><th>Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($orderItems as $keys => $value) {
            $i = 0;
            $html .= "<tr id='$value[1]' class='rowOrderItems'><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><tr>";
            foreach ($items as $key => $val) {
                if ($value[1] == $val[7]) {
                    reset($items);
                    if ($key === key($items)) {
                        
                    }
                    $i++;
                    if ($i == 1) {
                        $html .= "<tr class='$val[7] tbOrderItemsHeading'><td colspan='5' class='tbColInvisible'><table class='innerTable'><thead><th>Item Barcode</th><th>Shelf Barcode / Current Location</th><th>Shelf Description / Check In-Out Purpose</th><th>Shelf Storeroom</th></thead><tbody>";
                    }
                    if (empty($val[3])) {
                        $val[3] = "No barcode";
                    }
                    if (empty($val[4])) {
                        $val[4] = "No barcode";
                    }
                    $html .= "<tr class='$val[7] tbOrderItemsContents'><td><a href='index.php?pid=report&fid=barcode&barcode=$val[4]'>$val[4]</a></td><td>$val[3]</td><td>$val[5]</td><td>$val[6]</td></tr>";
                }
                end($items);
                if ($key === key($items)) {
                    $html .= "</tbody></table></td></tr>";
                }
            }
        }
    } else {
        $html .= "<tr><td colspan='5' style='text-align:center'>No items found</td></tr>";
    }


    $html .="</tbody></table>";
    return $html;
}

function viewReportsBarcodeOrder($orderItems, $items) {
    $html = "";
    $accordion = "";
    $html .= "<table class='bordered'><thead><th>Order Number</th><th>Supplier Code</th><th>Project</th><th>Item Barcode</th><th>Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th><th>Requested By</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($orderItems as $keys => $value) {
            $i = 0;
            $html .= "<tr id='$value[1]' class='rowOrderItems'><td>$value[9]</td><td>$value[2]</td><td>$value[8]</td><td>$value[11]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><td>$value[10]</td><tr>";
            foreach ($items as $key => $val) {
                if ($value[1] == $val[7]) {
                    reset($items);
                    if ($key === key($items)) {
                        
                    }
                    $i++;
                    if ($i == 1) {
                        $html .= "<tr class='$val[7] tbOrderItemsHeading'><td colspan='6' class='tbColInvisible'><table class='innerTable' ><thead><th>Date Time</th><th>Shelf Barcode / Current Location</th><th>Shelf Description / Check In-Out Purpose</th><th>Shelf Storeroom (If Applicable)</th><th>Action</th><th>Storeman / Device</th></thead><tbody>";
                    }
                    if (empty($val[3])) {
                        $val[3] = "No barcode";
                    }
                    if (empty($val[4])) {
                        $val[4] = "No barcode";
                    }
                    if ($val[9] == "Assigned Barcode") {
                        $html .= "<tr class='$val[7] tbOrderItemsContents'><td>$val[8]</td><td>$val[3]</td><td></td><td></td><td>$val[9]</td><td>$val[10]</td></tr>";
                    }
                    else
                    {
                        $html .= "<tr class='$val[7] tbOrderItemsContents'><td>$val[8]</td><td>$val[3]</td><td>$val[5]</td><td>$val[6]</td><td>$val[9]</td><td>$val[10]</td></tr>";
                    }
                }
                end($items);
                if ($key === key($items)) {
                    $html .= "</tbody></table></td></tr>";
                }
            }
        }
    } else {
        $html .= "<tr><td colspan='8' style='text-align:center'>No items found</td></tr>";
    }
    $html .="</tbody></table>";
    return $html;
}
function viewReportsBarcodeOrderMobile($orderItems, $items) {
    $html = "";
    $accordion = "";
    
    if ($orderItems[0][0] === true) {
        $html .= "<table class='bordered'><thead><th>Order Number</th><th>Project</th><th>Item Barcode</th><th>Supplier Code</th><th>Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th><th>Requested By</th></thead><tbody>";
        foreach($orderItems as $value)
        {
            $html .= "<tr id='$value[1]' class='rowOrderItems'><td>$value[9]</td><td>$value[8]</td><td>$value[11]</td><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><td>$value[10]</td><tr>";
        }
        $html .="</tbody></table>";
    }
    else {
        $html .= "<table class='bordered'><thead><th>Order Number</th><th>Project</th><th>Item Barcode</th><th>Supplier Code</th><th>Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th><th>Requested By</th></thead><tbody>";
        $html .= "<tr><td colspan='8' style='text-align:center'>No items found</td></tr></tbody></table>";
    }
    
    if ($items[0][0] === true) {
        foreach ($orderItems as $keys => $value) {
            $i = 0;
            //$html .= "<tr id='$value[1]' class='rowOrderItems'><td>$value[9]</td><td>$value[2]</td><td>$value[8]</td><td>$value[11]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><td>$value[10]</td><tr>";
            foreach ($items as $key => $val) {
                if ($value[1] == $val[7]) {
                    reset($items);
                    if ($key === key($items)) {
                    }
                    $i++;
                    if ($i == 1) {
                        $html .= "<table class='innerTable' ><thead><th>Date Time</th><th>Shelf Barcode / Current Location</th><th>Shelf Description / Check In-Out Purpose</th><th>Shelf Storeroom (If Applicable)</th></thead><tbody>";
                    }
                    if (empty($val[3])) {
                        $val[3] = "No barcode";
                    }
                    if (empty($val[4])) {
                        $val[4] = "No barcode";
                    }
                    $html .= "<tr class='$val[7] tbOrderItemsContents'><td>$val[8]</td><td>$val[3]</td><td>$val[5]</td><td>$val[6]</td></tr>";
                }
                end($items);
                if ($key === key($items)) {
                    $html .= "</tbody></table>";
                }
            }
        }
    }
    else {
        if($orderItems[0][0] === true)
        {
            $html .= "<table class='innerTable' ><thead><th>Date Time</th><th>Shelf Barcode / Current Location</th><th>Shelf Description / Check In-Out Purpose</th><th>Shelf Storeroom (If Applicable)</th></thead><tbody>";
            $html .= "<tr><td colspan='4' style='text-align:center'>No history found</td></tr></tbody></table>";
        }
    }
    
    return $html;
}

function viewReportsOrderItems($items) {
    $html = "";
    $html .= "<h2>Report by order - items</h2>";
    $html .= "<table class='bordered'><thead><th>Description</th><th>Barcode</th><th>Shelf</th><th>Storeroom</th></thead><tbody>";
    foreach ($items as $value) {
        $html .= "<tr id='$value[1]'><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td></tr>";
    }
    $html .= "</tbody></table>";
    return $html;
}

function viewReportOrderDetails($items) {
    $html = "<div class='divReportDetails'><h3>Order Details</h3><table class='bordered'><thead><th>Order Number</th><th>Requested By</th><th>Date Ordered</th><th>Project</th><th>Project Manager</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($items as $value) {
            $html .= "<tr><td id='orderNumVal'>$value[1]</td><td>$value[2]</td><td>$value[3]</td><td>$value[6] - $value[4]</td><td>$value[5]</td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='5' style='text-align:center'>No items found</td></tr>";
    }
    $html .= "</table></div><hr />";
    $html .= "<p><input type='checkbox' name='chkUnreceived' value='unreceived'>Not Received items</p>";
    //$html .= "<p>Reprint Barcode : <input type='text' class='form-control' id='InputReprintBarcode' placeholder='Enter barcode'/> <input type='button' id='BtnReprintBarcode' value='Print' class='btn btn-primary'/></p>";
    return $html;
}

function viewReportOrderform() {
    $html = "<div class='divReportSearch'><form  method='post' action='index.php?pid=report&fid=order'>";
    $html .= "<h3>View Items By Order Number</h3>";
    $html .="<p>Enter Order Number : <input style='width:50%' placeholder='Enter order number' class='form-control' type='text' name='orderNumber'/> <input type='submit' class='btn btn-primary' value='find'/></p>";
    $html .= "</form></div>";
    return $html;
}

function viewReportByBarcode() {
    $html = "<div class='divReportSearch'><form  method='post' action='index.php?pid=report&fid=barcode'>";
    $html .= "<h3>View Items By Item Barcode</h3>";
    $html .="<p>Scan / Enter Barcode : <input style='width:50%' placeholder='Scan item barcode' class='form-control' type='text' name='barcode'/> <input type='submit' class='btn btn-primary' value='find'/></p>";
    $html .= "</form></div>";
    return $html;
}

function viewItemHistory($items, $datefrom, $dateto) {
    $html = "<form method='post' action='index.php?pid=report&fid=searchfilter'><div style='margin:15px;'><h3>View Tracked Items History</h3>Please select a date range: ";
    if (isset($datefrom) && isset($dateto)) {
        $html .= "From: <input type='text' style='width:20%' name='datefrom' value='$datefrom' class='from_date form-control' placeholder='Select start date' contenteditable='false'>";
        $html .= " To: <input style='width:20%' type='text' name='dateto' value='$dateto' class='to_date form-control' placeholder='Select end date' contenteditable='false'> <input id='btnSearchLog' type='submit' name='search' class='btn btn-primary' value='Search'/></div></form>";
    } else {
        $html .= "From: <input type='text' style='width:20%' name='datefrom' class='from_date form-control' placeholder='Select start date' contenteditable='false'>";
        $html .= " To: <input style='width:20%' type='text' name='dateto' class='to_date form-control' placeholder='Select end date' contenteditable='false'> <input type='submit' name='search' class='btn btn-primary' value='Search'/></div></form>";
    }
    $html .= "<table id='tblItemHistory' class='bordered' cellspacing='0' width='100%'><thead><th filter='false' style='width:15%'>Date Time</th><th filter-type='text'>Process</th><th filter-type='text'>Action</th><th filter-type='text'>User(Storeman)</th><th filter-type='text'>Item Barcode</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Item Description</th><th filter-type='text'>Order Number</th><th filter-type='text'>Source</th><th filter-type='text'>Source Comment</th><th filter-type='text'>Destination</th><th filter-type='text'>Destination Comment</th></thead><tbody id='logTblBody'>";
    if ($items[0][0] === true) {
        foreach ($items as $item) {
            $html .= "<tr><td>$item[1]</td><td>$item[2]</td><td>$item[3]</td><td>$item[4]</td><td><a href='index.php?pid=report&fid=barcode&barcode=$item[5]'>$item[5]</a></td><td>$item[13]</td><td>$item[6]</td><td>$item[7]</td><td>$item[8]</td><td>$item[9]</td><td>$item[10]</td><td>$item[11]</td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='11' style='text-align:center'>" . $items[0][1] . "</td></tr>";
    }

    $html .="</tbody></table><div class='col-md-12 text-center'><ul class='pagination pagination-lg pager' id='logPager'></ul>
      </div>";
    return $html;
}

function viewUntrackedItemHistory($items, $datefrom, $dateto) {
    $html = "<form method='post' action='index.php?pid=report&fid=searchfilterUntracked'><div style='margin:15px;'><h3>View Un-tracked Items History</h3>Please select a date range: ";
    if (isset($datefrom) && isset($dateto)) {
        $html .= "From: <input type='text' style='width:20%' name='datefrom' value='$datefrom' class='from_date form-control' placeholder='Select start date' contenteditable='false'>";
        $html .= " To: <input style='width:20%' type='text' name='dateto' value='$dateto' class='to_date form-control' placeholder='Select end date' contenteditable='false'> <input id='btnSearchLog' type='submit' name='search' class='btn btn-primary' value='Search'/></div></form>";
    } else {
        $html .= "From: <input type='text' style='width:20%' name='datefrom' class='from_date form-control' placeholder='Select start date' contenteditable='false'>";
        $html .= " To: <input style='width:20%' type='text' name='dateto' class='to_date form-control' placeholder='Select end date' contenteditable='false'> <input type='submit' name='search' class='btn btn-primary' value='Search'/></div></form>";
    }
    $html .= "<table id='tblItemHistory' class='bordered' cellspacing='0' width='100%'><thead><th filter='false' style='width:15%'>Date Time</th><th filter-type='text'>Process</th><th filter-type='text'>Action</th><th filter-type='text'>User(Storeman)</th><th filter-type='text'>Supplier Code</th><th filter-type='text'>Item Description</th><th filter-type='text'>Order Number</th><th filter-type='text'>source</th><th filter-type='text'>Source Comment</th><th filter-type='text'>Destination</th><th filter-type='text'>Destination Comment</th></thead><tbody id='logTblBody'>";
    if ($items[0][0] === true) {
        foreach ($items as $item) {
            $html .= "<tr><td>$item[1]</td><td>$item[2]</td><td>$item[3]</td><td>$item[4]</td><td>$item[13]</td><td>$item[5]</td><td>$item[7]</td><td>$item[8]</td><td>$item[9]</td><td>$item[10]</td><td>$item[11]</td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='11' style='text-align:center'>" . $items[0][1] . "</td></tr>";
    }

    $html .="</tbody></table><div class='col-md-12 text-center'><ul class='pagination pagination-lg pager' id='logPager'></ul>
      </div>";
    return $html;
}

function viewRelocateOptions() {
    $html = "<div id='divRelocateOptionHeading'><h2>Relocate items</h2></div><div id='divRelocateOptions' style='display:none'><div class='btn-group btn-group-lg'><button type='button' class='btn btn-primary' id='btnUserToShelf' >User to shelf</button><button type='button'  id='btnUserToUser' class='btn btn-primary'>User to User</button><button type='button' id='btnShelfToUser' class='btn btn-primary'>Shelf to user</button><button type='button'  id='btnShelfToShelf'  class='btn btn-primary'>Shelf to shelf</button></div>";

    $html .="</div>";
    return $html;
}

function viewConsumables($items) {
    $html = "<div>";
    $html .= "<h3>All Consumable</h3>";
    $html .= "<table id='tblConsumable' class='bordered' cellspacing='2' width='50%'><thead>";
    $html .= "<th>Category</th><th>Supplier Code</th><th>Description</th><th>Detail</th><th>Min Quantity</th><th>Max Quantity</th><th>Quantity in Store</th><th>Nett Value</th><th filter='false'>Action</th><th></th>";
    $html .= "</thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($items as $item) {
            $html .= "<tr id='$item[1]'><td><input style='width:100%'  class='form-control' type='text' name='editCategory' value='" . $item[2] . "' /></td><td><input style='width:100%'  class='form-control' type='text' name='editSupplierCode' value='" . $item[8] . "' /></td><td><textarea rows='3' name='editDescription'>$item[3]</textarea></td><td><textarea rows='3' name='editDetails'>$item[4]</textarea></td><td class='smallColumn'><input style='width:100%' class='form-control' type='text' name='editMinQty' value='" . $item[5] . "' /></td><td class='smallColumn'><input style='width:100%'  class='form-control' type='text' name='editMaxQty' value='" . $item[6] . "' /></td><td class='smallColumn'><input style='width:100%'  class='form-control' type='text' name='editQtyInStore' value='" . $item[7] . "' /></td><td class='smallColumn'><input style='width:100%'  class='form-control' type='text' name='editNettValue' value='" . $item[9] . "' /></td><td><a class='btn btn-warning' href='#' onclick='UpdateConsumables(this);'>Update </a></td><td><a class='btn btn-danger' href='#' onclick='DeleteConsumables(this);'>Del</a></td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='10' style='text-align:center'>" . $items[0][1] . "</td></tr>";
    }
    $html .="</tbody></table>";
    $html .= "</div>";
    return $html;
}

function viewAddConsumable() {

    $html = "<h3>Add Consumable Type</h3>";
//        $html .= "<form method='post' action='index.php?pid=consumable&fid=add'>";
    $html .= "<table class='tblForm' >";
    $html .= "<tr><th>Category</th><td><input placeholder='Enter category type' class='form-control' type='text' name='tbName'/></td></tr>";
    $html .= "<tr><th>Description</th><td><input placeholder='Enter item description' class='form-control' type='text' name='tbdescription'/></td></tr>";
    $html .= "<tr><th>Detail</th><td><input placeholder='Enter item details' class='form-control' type='text' name='tbDetail'/></td></tr>";
    $html .= "<tr><th>Supplier Code</th><td><input placeholder='Enter supplier code' class='form-control' type='text' name='tbSupplierCode'/></td></tr>";
    $html .= "<tr><th>Minimum Qty</th><td><input placeholder='Enter minimum quantity' class='form-control' type='text' name='minQty'/></td></tr>";
    $html .= "<tr><th>Maximum Qty</th><td><input placeholder='Enter maximum quantity' class='form-control' type='text' name='maxQty'/></td></tr>";
    $html .= "<tr><th>Quantity in Store</th><td><input  placeholder='Enter quantity in store' class='form-control' type='text' name='qtyInStore'/></td></tr>";
    $html .= "<tr><th>Nett Value</th><td><input placeholder='Enter Nett Value' class='form-control' type='text' name='nettValue'/></td></tr>";
    $html .= "<tr><td></td><td><input type='button' id='btnAddConsumble' class='btn btn-success btn-md' value='Add Consumable'/></td></tr>";
    $html .= "</table>";
    return $html;
}

//DropDown usertypes
function viewDropdownS4Projects($projects) {
    try {
        $userData = $projects;
        if ($projects[0][0] === true) {
            $dropdown = "<select style='min-width:240px;' class='chosen-select' name='s4ProjectsAdd'>";
            $dropdown .="<option value=''>No Project</option>\n";
            foreach ($userData as $row) {
                $dropdown .="<option value=$row[1]>$row[2]</option>\n";
            }
            $dropdown .= "</select>\n";
        } else {
            $dropdown = $projects[0][1];
        }

        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownMMProjects($projects) {
    try {
        $userData = $projects;
        if ($projects[0][0] === true) {
            $dropdown = "<select style='min-width:240px;' class='chosen-select' name='mmProjectsAdd'>";
            $dropdown .="<option value=''>No Project</option>\n";
            foreach ($userData as $row) {
                $dropdown .="<option value=$row[1]>$row[2]</option>\n";
            }
            $dropdown .= "</select>\n";
        } else {
            $dropdown = $projects[0][1];
        }

        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownS4ProjectsWithID($projects, $id) {
    try {
        $userData = $projects;
        if ($projects[0][0] === true) {
            $dropdown = "<select style='min-width:240px;' class='chosen-select' id='$id' name='s4ProjectsAdd'>";
            $dropdown .="<option value=''>No Project</option>\n";
            foreach ($userData as $row) {
                $dropdown .="<option value=$row[1]>$row[2]</option>\n";
            }
            $dropdown .= "</select>\n";
        } else {
            $dropdown = $projects[0][1];
        }

        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownMMProjectsWithID($projects, $id) {
    try {
        $userData = $projects;
        if ($projects[0][0] === true) {
            $dropdown = "<select style='min-width:240px;' class='chosen-select' id='$id' name='mmProjectsAdd'>";
            $dropdown .="<option value=''>No Project</option>\n";
            foreach ($userData as $row) {
                $dropdown .="<option value=$row[1]>$row[2]</option>\n";
            }
            $dropdown .= "</select>\n";
        } else {
            $dropdown = $projects[0][1];
        }

        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewDropdownShelfWithProject($shelfs) {
    try {
        $userData = $shelfs;
        $dropdown = "<select style='min-width:400px;' class='chosen-select' name='ddShelfProject' >";
        $dropdown .="<option value=''></option>\n";
        foreach ($userData as $row) {
            $dropdown .="<option value=$row[1]>$row[2]</option>\n";
        }
        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}

function viewReportByLocation($dropdownLocation) {
    $html = "<div class='divReportSearch'><form  method='post' action='index.php?pid=report&fid=location'>";
    $html .= "<h3>View Items On Shelf</h3>";
    $html .="<p>Please select Location : $dropdownLocation &nbsp;<input type='submit' class='btn btn-primary' value='find'/></p>";
    $html .= "</form></div>";
    return $html;
}

function viewReportByProject($s4Dropdown, $mmDropdown) {
    $html = "<div class='divReportSearch'><form  method='post' action='index.php?pid=report&fid=project'>";
    $html .= "<h3>View Item Locations By Project</h3>";
    $html .="<p><b>Please select a project</b> : S4 Integration - $s4Dropdown &nbsp;&nbsp; <b>OR</b> S4 Automation - $mmDropdown &nbsp;&nbsp;";
    $html .= "<input type='submit' class='btn btn-primary' value='find'/></p>";
    $html .= "</form></div>";
    return $html;
}

function viewReportItemsByLocation($items) {
    $html = "<div style='float:left;'><table class='bordered'><thead><th>Supplier Code</th><th>Item Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th><th>Item Barcode</th><th>Shelf Barcode / Current Location</th><th>Shelf Description / Check In-Out Purpose</th><th>Shelf Storeroom (If Applicable)</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($items as $value) {
            $html .= "<tr><td>$value[1]</td><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td><a href='index.php?pid=report&fid=barcode&barcode=$value[6]'>$value[6]</a></td><td>$value[7]</td><td>$value[8]</td><td>$value[9]</td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='9' style='text-align:center'>No items found</td></tr>";
    }
    $html .= "</table></div><hr />";
    return $html;
}

function viewReportItemHistory($items) {
    $html = "<table class='bordered'><thead><th>Items ID</th><th>Supplier Code</th><th>Item Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th><th>Project</th><th>Order Number</th><th>Requested by</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($items as $value) {
            if ($value[6] == '1') {
                $value[6] = "Tracked Item";
            } elseif ($value[6] == '2') {
                $value[6] = "Tracked/ re-sale Item";
            } elseif ($value[6] == '3') {
                $value[6] = "Un-Tracked / Consumable Item";
            }
            $html .= "<tr><td>$value[1]</td><td>$value[2]</td><td>$value[3]</td><td>$value[4]</td><td>$value[5]</td><td>$value[6]</td><td>$value[8]</td><td>$value[9]</td><td>$value[10]</td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='9' style='text-align:center'>No items found</td></tr>";
    }
    $html .= "</table>";
    return $html;
}

function viewforgotpassword() {
    $html = "<h3>Forgot Password</h3>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<form method='post' action='index.php?pid=manageUsers&fid=forgotPassword'><table class='tblForm'>";
    $html .= "<tr><td>Email</th><td><input class='form-control' type='text' name='email' value=''> <input class='btn btn-primary' type='submit' value='sent'/></td></tr>";
    $html .= "<table></form>";
    return $html;
}

function viewResetPassword($id) {
    $html = "<h3>Change Password</h3>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<table class='tblForm'>";
    $html .= "<form method='post' action='index.php?pid=manageUsers&fid=newPassword'>";
    $html .= "<tr><td>Enter new Password:</th><td><input class='form-control' type='password' name='password' value=''> <input class='btn btn-primary' type='submit' value='save'/></td></tr>";
    $html .= "<input type='hidden' name='hidId' value ='$id'/></form><table>";
    return $html;
}

function viewEmailNotificationSettings($items, $dropdown) {
    $html = "<form action='index.php?pid=manageEmail&fid=add' method='post'>";
    $html .= "<h2>Low Stock Email Notification Settings</h2>";
    if (isset($_SESSION['status'])) {
        $type = $_SESSION['msgType'];
        $html .= "<div class='alert alert-$type'><a href='#' class='close' data-dismiss='alert'>&times;</a> " . $_SESSION['status'] . "</div>";
    }
    $html .= "<h3>Add New Notification User</h3>";
    $html .= "Please select a notification user : &nbsp;$dropdown";
    $html .= " &nbsp;<input type='submit' value='Add' name='btnAdd' class='btn btn-success'/>";
    $html .= "<hr><h3>All Notification Users</h3>";
    $html .= "<table class='bordered' style='width:60%'><thead><th>Name</th><th>Email Address</th><th>Action</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($items as $value) {
            $html .= "<tr><td>" . $value[1] . "</td><td>" . $value[2] . "</td><td><a href='index.php?pid=manageEmail&fid=delete&id=$value[3]' class='btn btn-danger'>Del</a></td></tr>";
        }
    } else {
        $html .= "<tr><td colspan='4' style='text-align:center'>No items found</td></tr>";
    }
    $html .= "</table></form>";
    return $html;
}

function viewDropdownnotificationUsers($userNames) {
    try {
        $dropdown = "<select style='min-width:200px;' class='chosen-select' class=' form-control' name='staffEmail'>";
        if ($userNames[0][0] === true) {
            foreach ($userNames as $row) {
                if (!strpos($dropdown, $row[2]) !== false) {//Improve by using nested select and union on modelGetnotificationUsers
                    $dropdown .="<option value='$row[1]*$row[3]*$row[4]'>$row[2]</option>\n";
                }
            }
        } else {
            $dropdown .="<option value=''>no users found!</option>\n";
        }

        $dropdown .= "</select>\n";
        return $dropdown;
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
}
function viewReportsLocationByProject($orderItems, $items) {
    $html = "";
    $accordion = "";
    $html .= "<table class='bordered'><thead><th>Shelf Barcode / Current Location</th><th>Shelf Description / Check In-Out Purpose</th><th>Shelf Storeroom (If Applicable)</th></thead><tbody>";
    if ($items[0][0] === true) {
        foreach ($orderItems as $keys => $value) {
            $i = 0;
            $html .= "<tr id='$value[12]' class='rowOrderItems'><td>$value[13]</td><td>$value[14]</td><td>$value[15]</td><tr>";
            foreach ($items as $key => $val) {
                if ($value[12] == $val[12]) {
                    reset($items);
                    if ($key === key($items)) {
                        
                    }
                    $i++;
                    if ($i == 1) {
                        $html .= "<tr class='$val[12] tbOrderItemsHeading'><td colspan='3' class='tbColInvisible'><table class='innerTable'><thead><th>Item Barcode</th><th>Supplier code</th><th>Item Description</th><th>Quantity Ordered</th><th>Quantity Received</th><th>Storage Type</th><th>Order Number</th></thead><tbody>";
                    }
                    if (empty($val[3])) {
                        $val[3] = "No barcode";
                    }
                    if (empty($val[4])) {
                        $val[4] = "No barcode";
                    }
                    $html .= "<tr class='$val[12] tbOrderItemsContents'><td><a href='index.php?pid=report&fid=barcode&barcode=$val[16]'>$val[16]</a></td><td>$val[2]</td><td>$val[3]</td><td>$val[4]</td><td>$val[5]</td><td>$val[6]</td><td>$val[9]</td></tr>";
                }
                end($items);
                if ($key === key($items)) {
                    $html .= "</tbody></table></td></tr>";
                }
            }
        }
    } else {
        $html .= "<tr><td colspan='5' style='text-align:center'>No items found on shelves for the selected project!</td></tr>";
    }


    $html .="</tbody></table>";
    return $html;
}
