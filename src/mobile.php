<?php
include 'mvc/mobile_controller.php';
error_reporting(E_ERROR);
session_start();
if ( isset($_GET["pid"]) && isset($_GET["fid"]) )
{
    $pid = $_GET['pid'];
    $fid = $_GET['fid']; 
    if ($pid == "marryItems" && $fid == "update") 
    {
        contrManageOrdersMarryUpdate();
    }
    
    if ($pid =="setLocation" && $fid=='display') 
    {
            echo viewMobileAllTrackedReturnItems(modelGetAllTrackedReturnItems());
    }
    
     if ($pid =="login" && $fid=='details') 
     {
         controlLogin();        
    }
    
    if ($pid =="marryItems" && $fid=='display') 
    {
        DisplayItems(); 
    }
    
    if ($pid == "setLocation" && $fid == "update")
    {
        contrScanItemBarcode();
    }
    
     if ($pid == "setLocation" && $fid == "empty")
    {
        contrSetLocationEmpty();
    }
    
    if ($pid == "setLocation" && $fid == "userUpdate")
    {
        contrUpdateLocationUser();        
    }
    
    if ($pid == "login" && $fid == "user") 
    {
        contrMobileGetUsers();
    }
    if ($pid == "History" && $fid == "Part") {
        contrReportbarcode();
    }
    
    if ($pid == 'untrackedReturn' && $fid == 'update') 
    {
        contrMobileNoOrderItemsUpdate();
    }
    
    if ($pid == 'untrackedReturn' && $fid == 'print') 
    {
        contrMobileNoOrderItemsPrint();
    }
    
    //ajax calls
    if ($pid == "users" && $fid == "dropdownFrom") 
    {
        echo contrUsersDropdown(dropdownFrom);        
    }
    
    if ($pid == "users" && $fid == "dropdownTo") 
    {
        echo contrUsersDropdown(dropdownTo);
        
    }
    
    if ($pid == "users" && $fid == "loginDetails") 
    {
        contrUserAuthenticate();
    }
    
    if ($pid == "relocate" && $fid == "userSpecific") 
     {
        contrManageReturnRelocateUserSpecific();
    }
    
    if ($pid=="marryItems" && $fid=="desktop") 
    {
        contrManageOrdersMarry();
    }
    
    
    
    ///Ajax request from intranet
    if ($pid =="consumable" && $fid == "add") 
    {
        contrAddConsumable();
    }
    
    if ($pid =="consumable" && $fid == "update") 
    {
        contrUpdateConsumable();
    }
    
    if ($pid =="consumable" && $fid == "delete") 
    {
        contrDeleteConsumable();
    }
    
    if ($pid == "AssignLocation" && $fid == "change") 
    {
        contrAssignLocationMobile();
    }
    
    if($pid == "intranetDropdown" && $fid == "project")
    {
        if (isset($_GET['selectVal'])) 
        {
            $selectedVal = $_GET['selectVal'];
        }
        contrGetDropdownIntranetProject($selectedVal);
    }
    if($pid == "autonetDropdown" && $fid == "project")
    {
        if (isset($_GET['selectVal'])) 
        {
            $selectedVal = $_GET['selectVal'];
        }
        contrGetDropdownAutonetProject($selectedVal);
    }
    
    if ($pid == "barcode" && $fid == "reprint") 
    {
        if (isset($_GET['barcode'])) 
        {
            $barcode = $_GET['barcode'];
            $status = modelUdpRequest("B".$barcode, 5);
            echo $status[0][1];
        }   
    }
    if ($pid == "report" && $fid == "order") {
        contrReportByOrder();        
    }
    if ($pid == "report" && $fid == "orderNumberNotReceived") {
        contrReportByOrderNotReceived(); 
    }
    
}
