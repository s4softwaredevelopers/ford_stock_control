package com.example.s4_scanner_v1;

import java.util.Map;

import MyContent.AssignedItemContent;
import MyContent.MyAssignBarcodesAdapter;
import MyContent.MyWebRequest;
import MyContent.UsersContent;
import MyContent.AssignedItemContent.AssignedItem;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AssignActivity extends Activity {
	
	private ListView listview;
	private AutoCompleteTextView actv;
	private EditText etBarcode;
	private ProgressBar spinner;
	private String itemSelectedID;
	private RelativeLayout relativeLayout;
	private AssignedItemContent.AssignedItem currentItem;
	final Context context = this;
	private Button buttonUpdate;
	private Button buttonScan;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assign);
		
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		
		AssignedItemContent.adapter = new MyAssignBarcodesAdapter(this, AssignedItemContent.ITEMS);
		listview = (ListView) findViewById(R.id.listViewAssign);
		listview.setAdapter(AssignedItemContent.adapter);
		
    	actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextViewAssignSort); 
    	AssignedItemContent.adapterOrders = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AssignedItemContent.ORDERS);	
		actv.setAdapter(AssignedItemContent.adapterOrders);	
		
		itemSelectedID = "0";
		
		etBarcode = (EditText) findViewById(R.id.editTextAssignBarcode);
		
	      spinner = (ProgressBar)findViewById(R.id.progressBarAssign);
	      spinner.setVisibility(View.GONE);
	    
	    relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayoutAssignContent);
		
		final TextView tvT_OrderNumber = (TextView) findViewById(R.id.textViewAssignDetails1);
		final TextView tvT_SupplierCode = (TextView) findViewById(R.id.textViewAssignDetails2);
		final TextView tvT_ProjectName = (TextView) findViewById(R.id.textViewAssignDetails3);
		final TextView tvT_Description = (TextView) findViewById(R.id.textViewAssignDetails4);
		
	    tvT_OrderNumber.setText("Order Number :");
		tvT_SupplierCode.setText("Supplier Code :");
		tvT_ProjectName.setText("Project Name :");
		tvT_Description.setText("Description :");
		
		final TextView tvOrderNumber = (TextView) findViewById(R.id.textViewAssignDetails1desc);
		final TextView tvSupplierCode = (TextView) findViewById(R.id.textViewAssignDetails2desc);
		final TextView tvProjectName = (TextView) findViewById(R.id.textViewAssignDetails3desc);
		final TextView tvDescription = (TextView) findViewById(R.id.textViewAssignDetails4desc);
 
		Button buttonOrderSort = (Button) findViewById(R.id.buttonAssignOrderSort);
		buttonUpdate = (Button) findViewById(R.id.buttonAssignUpdate);
		buttonScan = (Button) findViewById(R.id.buttonAssignScan);
		final Button buttonServerUpdate = (Button) findViewById(R.id.buttonAssignServerUpdate);

		if (itemSelectedID.equals("0")) { relativeLayout.setVisibility(View.GONE); }
		else { relativeLayout.setVisibility(View.VISIBLE);}
		
		buttonOrderSort.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
				AssignedItemContent.ReOrder(actv.getText().toString());	
				AssignedItemContent.adapter.notifyDataSetChanged();
				
				itemSelectedID = "0";
				relativeLayout.setVisibility(View.GONE);
			}
			
		});
		
		listview.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {

				  itemSelectedID = ((AssignedItemContent.AssignedItem) parent.getItemAtPosition(position)).id;
				  currentItem = AssignedItemContent.ITEM_MAP.get(itemSelectedID);

				  relativeLayout.setVisibility(View.VISIBLE);
				  
					tvOrderNumber.setText(currentItem.orderNumber);			
					tvSupplierCode.setText(currentItem.supplierCode);			
					tvProjectName.setText(currentItem.projectName);
					tvDescription.setText(currentItem.description);			
					etBarcode.setText(currentItem.barcode);	
					
					if (currentItem.finished)
					{
						etBarcode.setEnabled(false);
						buttonUpdate.setEnabled(false);
						buttonScan.setEnabled(false);
					}
					else
					{
						etBarcode.setEnabled(true);
						buttonUpdate.setEnabled(true);
						buttonScan.setEnabled(true);
					}
			  }
			}); 


		
		buttonUpdate.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
		    	
				if (itemSelectedID != "0") {							
					currentItem.barcode = etBarcode.getText().toString();
					
					currentItem.dublicateBarcode = false;
					
					AssignedItemContent.ITEM_MAP.put(itemSelectedID, currentItem);
					
					AssignedItemContent.adapter.notifyDataSetChanged();
					Toast.makeText(getBaseContext(),"Barcode Assigned",Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		
		buttonScan.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
					
		        BarcodeScan();
			}			
		});
		
		buttonServerUpdate.setOnClickListener(new OnClickListener(){
			
			@Override
			public void onClick(View v) {
				
				new WebRequestAssignUpdate().execute("");
			}			
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.assign, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	class WebRequestAssignUpdate extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			
			String urlParameters = "?pid=marryItems&fid=update";
			String result = "";
			try{
				
				urlParameters += SendParameters();
				
				result = MyWebRequest.HttpRequestS4(urlParameters);
				
				if (result == ""){ return "Connection Lost / Return result empty";}
				return result;
			} catch (Exception e) {
				Log.e("Assign", e.getMessage());
				e.printStackTrace();
				return e.getMessage();
			}	
		}
		
	    @Override
	    protected void onPostExecute(String result) {

	    	spinner.setVisibility(View.GONE);
	    	AssignedItemContent.adapter.notifyDataSetChanged();	  
	    	
			if (currentItem.finished)
			{
				etBarcode.setEnabled(false);
				buttonUpdate.setEnabled(false);
				buttonScan.setEnabled(false);
			}
			else
			{
				etBarcode.setEnabled(true);
				buttonUpdate.setEnabled(true);
				buttonScan.setEnabled(true);
			}
	    	
	    	if (result.contains("Barcode assigned successfully"))  
	    	{
	    		if (result.contains("Failed"))
	    		{	
	    			new AlertDialog.Builder(context)
	    		    .setTitle("Some Items Failed")
	    		    .setMessage(AssignedItemContent.FailedAssignedItems(result))
	    		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	    		        public void onClick(DialogInterface dialog, int which) { 
	    		            // continue with activity
	    		        }
	    		     })
	    		    .setIcon(android.R.drawable.ic_dialog_alert)
	    		     .show();
	    		}
	    		else
	    		{
	    			Toast.makeText(getBaseContext(),"All Barcodes assigned successfully",Toast.LENGTH_LONG).show();
	    			finish();
	    		}
	    	}
	    	else
	    	{
	    		if (result.contains("Failed"))
	    		{	
	    			new AlertDialog.Builder(context)
	    		    .setTitle("All items Failed")
	    		    .setMessage(AssignedItemContent.FailedAssignedItems(result))
	    		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	    		        public void onClick(DialogInterface dialog, int which) { 
	    		            // continue with activity
	    		        }
	    		     })
	    		    .setIcon(android.R.drawable.ic_dialog_alert)
	    		     .show();
	    		}
	    		else
	    		{
	    			new AlertDialog.Builder(context)
	    		    .setTitle("Error")
	    		    .setMessage(result)
	    		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	    		        public void onClick(DialogInterface dialog, int which) { 
	    		            // continue with delete
	    		        }
	    		     })
	    		    .setIcon(android.R.drawable.ic_dialog_alert)
	    		     .show();
	    		}
	    	}

	    }

	    @Override
	    protected void onPreExecute() {
	    	spinner.setVisibility(View.VISIBLE);
	    	Toast.makeText(getBaseContext(),"Please wait, connecting to server.",Toast.LENGTH_SHORT).show();
	    }
		
	    public String SendParameters(){
	    	String param = "";
	    	for (Map.Entry<String, AssignedItem> aItem : AssignedItemContent.ITEM_MAP.entrySet()){
	    		if ((aItem.getValue().barcode != "") && (aItem.getValue().finished == false)){
	    			AssignedItemContent.ITEM_MAP.get(aItem.getValue().id).finished = true;
	    			param += "&ID" + aItem.getValue().id + "=" + aItem.getValue().itemId + "&BC" + aItem.getValue().id + "=" + aItem.getValue().barcode + "&SC" + aItem.getValue().id + "=0";
	    		}
	    	}	  
	    	return param;
	    }
	}
	
    public void BarcodeScan(){
    	Intent intent = new Intent("com.google.zxing.client.android.SCAN");
    	intent.putExtra("SCAN_MODE", "QR_CODE_MODE,PRODUCT_MODE");
    	startActivityForResult(intent, 0);
    }
    
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
	    if (requestCode == 0) {
	    EditText etBarcode = (EditText) findViewById(R.id.editTextAssignBarcode);
	      if (resultCode == RESULT_OK) {
	    	  etBarcode.setText(intent.getStringExtra("SCAN_RESULT"));
	        //tvStatus.setText(intent.getStringExtra("SCAN_RESULT_FORMAT"));
	        //tvResult.setText(intent.getStringExtra("SCAN_RESULT"));
	      } else if (resultCode == RESULT_CANCELED) {
	        //tvStatus.setText("Press a button to start a scan.");
	        //tvResult.setText("Scan cancelled.");
	      }
	    }
	}
}
