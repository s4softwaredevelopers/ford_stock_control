<h2>Storerooms</h2>

<?php
if (isset($_POST['addstoreroom'])) {
    $description = trim($_POST['storeroom_description']);
    
    $q = 'INSERT INTO storeroom (description) VALUES (:description)';
    $params = array(':description' => $description);
    
    $id = null;
    $rows = sql_insert($id, $q, $params);
    
    if ($rows == 1) {
        echo '<p>Storeroom ' . $description . ' added.</p>';
        logger('add', 'Storeroom :: ' . $description);
    }
    else {
        echo '<p class="error">Storeroom not added.</p>';
    }
}
elseif (isset($_POST['updatestoreroom'])) {
    $id = $_POST['updatestoreroom'];
    $description = trim($_POST['storeroom_description']);
    
    $q = 'UPDATE storeroom SET description=:description WHERE id=:id';
    $params = array(':id' => $id, ':description' => $description);
    
    $rows = sql_update($q, $params);
    
    if ($rows == 1) {
        echo '<p>Storeroom ' . $description . ' updated.</p>';
        logger('update', 'Storeroom :: ' . $description);
    }
    else {
        echo '<p class="error">Storeroom not updated.</p>';
    }
}
elseif (isset($_POST['removestoreroom'])) {
    $id = $_POST['removestoreroom'];
    $description = $_POST['storeroom_description'];
    
    $q = 'DELETE FROM storeroom WHERE id=:id';
    $params = array(':id' => $id);
    
    $rows = sql_delete($q, $params);
    
    if ($rows == 1) {
        echo '<p>Storeroom ' . $description . ' removed.</p>';
        logger('remove', 'Storeroom :: ' . $description);
    }
    else {
        echo '<p class="error">Storeroom not removed.</p>';
    }
} ?>
<div class='auxwrapper'>
    <input type='button' value='Add'>
    <div class='aux'>
        <table cellpadding='0' cellspacing='0'>
            <form name='addstoreroomform' action='index.php?pid=storerooms' method='post'>
            <tr>
                <th>Name</th>
                <td>
                    <input id='focus' type='text' name='storeroom_description' value=''>
                </td>
            </tr>
            <tr>
                <th class='empty'></th>
                <td class='button'>
                    <input type='hidden' name='addstoreroom' value='addstoreroom'>
                    <input type='submit' value='Save'>
                    <input class='reset' type='reset' value='Cancel'>
                </td>
            </tr>
            </form>
        </table>
    </div>
</div>
<?php $q = 'SELECT id, description FROM storeroom ORDER BY id';

$result = null;
$rows = sql_select($result, $q);

if ($rows > 0) { ?>
<div class='table'>
    <table cellpadding='0' cellspacing='0'>
        <thead>
            <tr class='thead'>
                <th class='view first last w140'>Name</th>
                <th class='edit' filter='false'></th>
                <th class='empty'></th>
                <th class='empty'></th>
                <th class='empty'></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($result as $row) {
            $id = $row['id'];
            $description = $row['description']; ?>
            <tr>
                <form name='updatestoreroomform<?php echo $id ?>' action='index.php?pid=storerooms' method='post'>
                    <td class='view w140'>
                        <div><?php echo $description; ?></div>
                    </td>
                    <td class='edit w140'>
                        <input type='text' name='storeroom_description' value='<?php echo $description; ?>'>
                    </td>
                    <td class='button'>
                        <input type='hidden' name='updatestoreroom' value='<?php echo $id ?>'>
                        <input class='edit' type='submit' value='Update'>
                    </td>
                </form>
                <form name='removestoreroomform<?php echo $id ?>' action='index.php?pid=storerooms' method='post'>
                    <td class='button'>
                        <input type='hidden' name='removestoreroom' value='<?php echo $id ?>'>
                        <input type='hidden' name='storeroom_description' value='<?php echo $description; ?>'>
                        <input id='<?php echo $id ?>' class='edit actionconfirm' type='button' value='Remove'>
                    </td>
                    <td class='button'>
                        <input id='confirm<?php echo $id ?>' class='hide' type='submit' value='Confirm'>
                    </td>
                </form>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php }
else {
    echo '<p>No storerooms in database.</p>';
} ?>